package com.arko.nextdot.Retrofit.RetrofitModel.DoctorLists;

import com.google.gson.annotations.SerializedName;

public class DoctorLists{

	@SerializedName("result")
	private Result result;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}
}