package com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours;


import com.google.gson.annotations.SerializedName;


public class TimescheduleItem{

	@SerializedName("afternoon")
	private Afternoon afternoon;

	@SerializedName("doctor_id")
	private int doctorId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("time_duration")
	private Object timeDuration;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("doctor_time_schedule_id")
	private int doctorTimeScheduleId;

	@SerializedName("evening")
	private Evening evening;

	@SerializedName("day")
	private String day;

	@SerializedName("morning")
	private Morning morning;

	@SerializedName("status")
	private String status;

	public void setAfternoon(Afternoon afternoon){
		this.afternoon = afternoon;
	}

	public Afternoon getAfternoon(){
		return afternoon;
	}

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setTimeDuration(Object timeDuration){
		this.timeDuration = timeDuration;
	}

	public Object getTimeDuration(){
		return timeDuration;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDoctorTimeScheduleId(int doctorTimeScheduleId){
		this.doctorTimeScheduleId = doctorTimeScheduleId;
	}

	public int getDoctorTimeScheduleId(){
		return doctorTimeScheduleId;
	}

	public void setEvening(Evening evening){
		this.evening = evening;
	}

	public Evening getEvening(){
		return evening;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	public void setMorning(Morning morning){
		this.morning = morning;
	}

	public Morning getMorning(){
		return morning;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}