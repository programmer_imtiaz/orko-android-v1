package com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list;


import com.google.gson.annotations.SerializedName;

public class Profile{

	@SerializedName("allergies")
	private Object allergies;

	@SerializedName("fees")
	private Object fees;

	@SerializedName("gender")
	private String gender;

	@SerializedName("rating")
	private double rating;

	@SerializedName("bio")
	private Object bio;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("promo_code")
	private Object promoCode;

	@SerializedName("speciality")
	private String speciality;

	@SerializedName("doctor_id")
	private Object doctorId;

	@SerializedName("work_experiences")
	private Object workExperiences;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("profession")
	private Object profession;

	@SerializedName("family_history")
	private Object familyHistory;

	@SerializedName("address")
	private Object address;

	@SerializedName("social")
	private Object social;

	@SerializedName("doctor_promo_code")
	private Object doctorPromoCode;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("featured_skill")
	private Object featuredSkill;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("dob")
	private Object dob;

	@SerializedName("organization")
	private Object organization;

	@SerializedName("main_problem")
	private Object mainProblem;

	@SerializedName("district")
	private int district;

	@SerializedName("blood_group")
	private Object bloodGroup;

	@SerializedName("doctor_recommendation")
	private Object doctorRecommendation;

	@SerializedName("interests")
	private Object interests;

	@SerializedName("fees_messaging")
	private Object feesMessaging;

	@SerializedName("age")
	private int age;

	@SerializedName("upazilla")
	private Object upazilla;

	@SerializedName("vaccination")
	private Object vaccination;

	@SerializedName("surgery")
	private Object surgery;

	public void setAllergies(Object allergies){
		this.allergies = allergies;
	}

	public Object getAllergies(){
		return allergies;
	}

	public void setFees(Object fees){
		this.fees = fees;
	}

	public Object getFees(){
		return fees;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setRating(double rating){
		this.rating = rating;
	}

	public double getRating(){
		return rating;
	}

	public void setBio(Object bio){
		this.bio = bio;
	}

	public Object getBio(){
		return bio;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setPromoCode(Object promoCode){
		this.promoCode = promoCode;
	}

	public Object getPromoCode(){
		return promoCode;
	}

	public void setSpeciality(String speciality){
		this.speciality = speciality;
	}

	public String getSpeciality(){
		return speciality;
	}

	public void setDoctorId(Object doctorId){
		this.doctorId = doctorId;
	}

	public Object getDoctorId(){
		return doctorId;
	}

	public void setWorkExperiences(Object workExperiences){
		this.workExperiences = workExperiences;
	}

	public Object getWorkExperiences(){
		return workExperiences;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setProfession(Object profession){
		this.profession = profession;
	}

	public Object getProfession(){
		return profession;
	}

	public void setFamilyHistory(Object familyHistory){
		this.familyHistory = familyHistory;
	}

	public Object getFamilyHistory(){
		return familyHistory;
	}

	public void setAddress(Object address){
		this.address = address;
	}

	public Object getAddress(){
		return address;
	}

	public void setSocial(Object social){
		this.social = social;
	}

	public Object getSocial(){
		return social;
	}

	public void setDoctorPromoCode(Object doctorPromoCode){
		this.doctorPromoCode = doctorPromoCode;
	}

	public Object getDoctorPromoCode(){
		return doctorPromoCode;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setFeaturedSkill(Object featuredSkill){
		this.featuredSkill = featuredSkill;
	}

	public Object getFeaturedSkill(){
		return featuredSkill;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setDob(Object dob){
		this.dob = dob;
	}

	public Object getDob(){
		return dob;
	}

	public void setOrganization(Object organization){
		this.organization = organization;
	}

	public Object getOrganization(){
		return organization;
	}

	public void setMainProblem(Object mainProblem){
		this.mainProblem = mainProblem;
	}

	public Object getMainProblem(){
		return mainProblem;
	}

	public void setDistrict(int district){
		this.district = district;
	}

	public int getDistrict(){
		return district;
	}

	public void setBloodGroup(Object bloodGroup){
		this.bloodGroup = bloodGroup;
	}

	public Object getBloodGroup(){
		return bloodGroup;
	}

	public void setDoctorRecommendation(Object doctorRecommendation){
		this.doctorRecommendation = doctorRecommendation;
	}

	public Object getDoctorRecommendation(){
		return doctorRecommendation;
	}

	public void setInterests(Object interests){
		this.interests = interests;
	}

	public Object getInterests(){
		return interests;
	}

	public void setFeesMessaging(Object feesMessaging){
		this.feesMessaging = feesMessaging;
	}

	public Object getFeesMessaging(){
		return feesMessaging;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	public void setUpazilla(Object upazilla){
		this.upazilla = upazilla;
	}

	public Object getUpazilla(){
		return upazilla;
	}

	public void setVaccination(Object vaccination){
		this.vaccination = vaccination;
	}

	public Object getVaccination(){
		return vaccination;
	}

	public void setSurgery(Object surgery){
		this.surgery = surgery;
	}

	public Object getSurgery(){
		return surgery;
	}

	@Override
 	public String toString(){
		return 
			"Profile{" + 
			"allergies = '" + allergies + '\'' + 
			",fees = '" + fees + '\'' + 
			",gender = '" + gender + '\'' + 
			",rating = '" + rating + '\'' + 
			",bio = '" + bio + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",promo_code = '" + promoCode + '\'' + 
			",speciality = '" + speciality + '\'' + 
			",doctor_id = '" + doctorId + '\'' + 
			",work_experiences = '" + workExperiences + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",id = '" + id + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",profession = '" + profession + '\'' + 
			",family_history = '" + familyHistory + '\'' + 
			",address = '" + address + '\'' + 
			",social = '" + social + '\'' + 
			",doctor_promo_code = '" + doctorPromoCode + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",featured_skill = '" + featuredSkill + '\'' + 
			",user_id = '" + userId + '\'' + 
			",dob = '" + dob + '\'' + 
			",organization = '" + organization + '\'' + 
			",main_problem = '" + mainProblem + '\'' + 
			",district = '" + district + '\'' + 
			",blood_group = '" + bloodGroup + '\'' + 
			",doctor_recommendation = '" + doctorRecommendation + '\'' + 
			",interests = '" + interests + '\'' + 
			",fees_messaging = '" + feesMessaging + '\'' + 
			",age = '" + age + '\'' + 
			",upazilla = '" + upazilla + '\'' + 
			",vaccination = '" + vaccination + '\'' + 
			",surgery = '" + surgery + '\'' + 
			"}";
		}
}