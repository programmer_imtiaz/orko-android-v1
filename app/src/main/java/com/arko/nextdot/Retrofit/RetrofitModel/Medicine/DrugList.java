package com.arko.nextdot.Retrofit.RetrofitModel.Medicine;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ASUS on 01-Aug-17.
 */

public class DrugList{

    @SerializedName("medicineList")
    @Expose
    private List<DrugItem> medicineList = null;

    public List<DrugItem> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(List<DrugItem> medicineList) {
        this.medicineList = medicineList;
    }
}
