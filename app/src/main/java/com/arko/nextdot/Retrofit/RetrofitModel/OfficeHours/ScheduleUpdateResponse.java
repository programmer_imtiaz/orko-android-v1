package com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours;

import com.google.gson.annotations.SerializedName;

public class ScheduleUpdateResponse{

	@SerializedName("status")
	private String status;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}