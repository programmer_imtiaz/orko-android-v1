package com.arko.nextdot.Retrofit.RetrofitModel.Appointments;

/**
 * Created by sakib on 11/14/2017.
 */

public class SchedulePair {
    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    String start;

    public SchedulePair(String start, String end) {
        this.start = start;
        this.end = end;
    }

    String end;

    public String getStart() {
        return start;
    }


    public void setStart(String start) {
        this.start = start;
    }
}
