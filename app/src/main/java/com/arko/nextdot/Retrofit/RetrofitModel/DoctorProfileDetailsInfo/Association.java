
package com.arko.nextdot.Retrofit.RetrofitModel.DoctorProfileDetailsInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Association {

    @SerializedName("association_id")
    @Expose
    private Integer associationId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Integer associationId) {
        this.associationId = associationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
