
package com.arko.nextdot.Retrofit.RetrofitModel.DoctorProfileDetailsInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorProfileDetailsinformation {

    @SerializedName("id")
    @Expose
    private Integer id;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @SerializedName("rating")
    @Expose
    private float rating;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("avatar")
    @Expose
    private String avatar;

    @SerializedName("profile")
    @Expose
    private Profile profile;

    @SerializedName("officehours")
    @Expose
    private List<Officehour> officehours = null;
    @SerializedName("appointments")
    @Expose
    private List<Appointment> appointments = null;
    @SerializedName("associations")
    @Expose
    private List<Association> associations = null;
    @SerializedName("certificates")
    @Expose
    private List<Certificate> certificates = null;
    @SerializedName("educations")
    @Expose
    private List<Education> educations = null;
    @SerializedName("courses")
    @Expose
    private List<Course> courses = null;
    @SerializedName("experiences")
    @Expose
    private List<Experience> experiences = null;
    @SerializedName("honors_awards")
    @Expose
    private List<HonorsAward> honorsAwards = null;
    @SerializedName("publications")
    @Expose
    private List<Publication> publications = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }



    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Officehour> getOfficehours() {
        return officehours;
    }

    public void setOfficehours(List<Officehour> officehours) {
        this.officehours = officehours;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public List<Association> getAssociations() {
        return associations;
    }

    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public List<HonorsAward> getHonorsAwards() {
        return honorsAwards;
    }

    public void setHonorsAwards(List<HonorsAward> honorsAwards) {
        this.honorsAwards = honorsAwards;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

}
