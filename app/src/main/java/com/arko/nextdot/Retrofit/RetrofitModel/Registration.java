package com.arko.nextdot.Retrofit.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by sadikul on 10-Aug-17.
 */

public class Registration {

    @SerializedName("first_name")
    @Expose
    String firstname;
    @SerializedName("last_name")
    @Expose
    String lastname;
    @SerializedName("username")
    @Expose
    String username;
    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("dob")
    @Expose
    String dob;
    @SerializedName("gender")
    @Expose
    String gender;
    @SerializedName("district")
    @Expose
    String district;
    @SerializedName("upazilla")
    @Expose
    String upazilla;

    @SerializedName("speciality")
    @Expose
    String speciality;

    @SerializedName("avatar_file")
    @Expose
    String avatar_file;
    @SerializedName("password")
    @Expose
    String password;
    @SerializedName("password_confirmation")
    @Expose
    String password_confirmation;

    @SerializedName("result")
    @Expose
    String result;

/*
    @SerializedName("msg")
    @Expose
    MessageInfo MessageInfo;



    public MessageInfo getMessageInfo(){
        return MessageInfo;
    }*/

    public String getResult() {
        return result;
    }
}
