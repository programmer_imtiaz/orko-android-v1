package com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Result{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<DoctorListItem> data;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<DoctorListItem> data){
		this.data = data;
	}

	public List<DoctorListItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Result{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}