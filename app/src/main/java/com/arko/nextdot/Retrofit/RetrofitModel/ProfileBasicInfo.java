package com.arko.nextdot.Retrofit.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ASUS on 10-Aug-17.
 */

public class ProfileBasicInfo {
    public String getMessage() {
        return message;
    }

    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;

    @SerializedName("msg")
    @Expose
    String message;

    @SerializedName("token")
    @Expose
    String accessToken;

    @SerializedName("user")
    @Expose
    User user;


    @SerializedName("userProfile")
    @Expose
    UserProfile userProfile;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public class User{

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        @SerializedName("username")
        @Expose
        String username;
        @SerializedName("phone")
        @Expose
        String phone;
        @SerializedName("email")
        @Expose
        String email;
        @SerializedName("avatar")
        @Expose
        String avatar;
    }

    public class UserProfile{
        public int getId() {
            return id;
        }

        public int getUser_id() {
            return user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        @SerializedName("id")
        @Expose
        int id;
        @SerializedName("user_id")
        @Expose
        int user_id;
        @SerializedName("first_name")
        @Expose
        String first_name;
        @SerializedName("last_name")
        @Expose
        String last_name;
    }
}
