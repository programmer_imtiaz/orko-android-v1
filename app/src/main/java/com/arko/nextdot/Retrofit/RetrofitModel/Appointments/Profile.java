
package com.arko.nextdot.Retrofit.RetrofitModel.Appointments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("age")
    @Expose
    private Object age;
    @SerializedName("speciality")
    @Expose
    private Object speciality;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("bio")
    @Expose
    private Object bio;
    @SerializedName("social")
    @Expose
    private Object social;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("organization")
    @Expose
    private Object organization;
    @SerializedName("main_problem")
    @Expose
    private String mainProblem;
    @SerializedName("district")
    @Expose
    private Integer district;
    @SerializedName("upazilla")
    @Expose
    private Integer upazilla;
    @SerializedName("promo_code")
    @Expose
    private Object promoCode;
    @SerializedName("doctor_promo_code")
    @Expose
    private Object doctorPromoCode;
    @SerializedName("doctor_recommendation")
    @Expose
    private Object doctorRecommendation;
    @SerializedName("interests")
    @Expose
    private Object interests;
    @SerializedName("featured_skill")
    @Expose
    private Object featuredSkill;
    @SerializedName("doctor_id")
    @Expose
    private Integer doctorId;
    @SerializedName("family_history")
    @Expose
    private Object familyHistory;
    @SerializedName("allergies")
    @Expose
    private Object allergies;
    @SerializedName("vaccination")
    @Expose
    private Object vaccination;
    @SerializedName("surgery")
    @Expose
    private Object surgery;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public Object getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Object speciality) {
        this.speciality = speciality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getBio() {
        return bio;
    }

    public void setBio(Object bio) {
        this.bio = bio;
    }

    public Object getSocial() {
        return social;
    }

    public void setSocial(Object social) {
        this.social = social;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Object getOrganization() {
        return organization;
    }

    public void setOrganization(Object organization) {
        this.organization = organization;
    }

    public String getMainProblem() {
        return mainProblem;
    }

    public void setMainProblem(String mainProblem) {
        this.mainProblem = mainProblem;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public Integer getUpazilla() {
        return upazilla;
    }

    public void setUpazilla(Integer upazilla) {
        this.upazilla = upazilla;
    }

    public Object getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(Object promoCode) {
        this.promoCode = promoCode;
    }

    public Object getDoctorPromoCode() {
        return doctorPromoCode;
    }

    public void setDoctorPromoCode(Object doctorPromoCode) {
        this.doctorPromoCode = doctorPromoCode;
    }

    public Object getDoctorRecommendation() {
        return doctorRecommendation;
    }

    public void setDoctorRecommendation(Object doctorRecommendation) {
        this.doctorRecommendation = doctorRecommendation;
    }

    public Object getInterests() {
        return interests;
    }

    public void setInterests(Object interests) {
        this.interests = interests;
    }

    public Object getFeaturedSkill() {
        return featuredSkill;
    }

    public void setFeaturedSkill(Object featuredSkill) {
        this.featuredSkill = featuredSkill;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Object getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(Object familyHistory) {
        this.familyHistory = familyHistory;
    }

    public Object getAllergies() {
        return allergies;
    }

    public void setAllergies(Object allergies) {
        this.allergies = allergies;
    }

    public Object getVaccination() {
        return vaccination;
    }

    public void setVaccination(Object vaccination) {
        this.vaccination = vaccination;
    }

    public Object getSurgery() {
        return surgery;
    }

    public void setSurgery(Object surgery) {
        this.surgery = surgery;
    }

}
