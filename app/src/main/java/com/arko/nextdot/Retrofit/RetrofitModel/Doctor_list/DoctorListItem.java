package com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list;


import com.google.gson.annotations.SerializedName;

public class DoctorListItem {

	private Boolean isSet = false;

	@SerializedName("is_active")
	private int isActive;

	@SerializedName("assistant_of")
	private Object assistantOf;

	@SerializedName("profile")
	private Profile profile;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("created_by")
	private int createdBy;

	@SerializedName("password")
	private String password;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("phone")
	private String phone;

	@SerializedName("web")
	private Object web;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("username")
	private String username;

	public void setIsActive(int isActive){
		this.isActive = isActive;
	}

	public int getIsActive(){
		return isActive;
	}

	public void setAssistantOf(Object assistantOf){
		this.assistantOf = assistantOf;
	}

	public Object getAssistantOf(){
		return assistantOf;
	}

	public void setProfile(Profile profile){
		this.profile = profile;
	}

	public Profile getProfile(){
		return profile;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setCreatedBy(int createdBy){
		this.createdBy = createdBy;
	}

	public int getCreatedBy(){
		return createdBy;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setWeb(Object web){
		this.web = web;
	}

	public Object getWeb(){
		return web;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public Boolean getSet() {
		return isSet;
	}

	public void setSet(Boolean set) {
		isSet = set;
	}

	@Override
 	public String toString(){
		return 
			"DoctorListItem{" +
			"is_active = '" + isActive + '\'' + 
			",assistant_of = '" + assistantOf + '\'' + 
			",profile = '" + profile + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",avatar = '" + avatar + '\'' + 
			",created_by = '" + createdBy + '\'' + 
			",password = '" + password + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",phone = '" + phone + '\'' + 
			",web = '" + web + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}