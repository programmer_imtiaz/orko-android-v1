package com.arko.nextdot.Retrofit.RetrofitModel.Appointments;

import com.google.gson.annotations.SerializedName;

public class Timeschedule{

	@SerializedName("afternoon")
	private Afternoon afternoon;

	@SerializedName("evening")
	private Evening evening;

	@SerializedName("morning")
	private Morning morning;

	public void setAfternoon(Afternoon afternoon){
		this.afternoon = afternoon;
	}

	public Afternoon getAfternoon(){
		return afternoon;
	}

	public void setEvening(Evening evening){
		this.evening = evening;
	}

	public Evening getEvening(){
		return evening;
	}

	public void setMorning(Morning morning){
		this.morning = morning;
	}

	public Morning getMorning(){
		return morning;
	}

	@Override
 	public String toString(){
		return 
			"Timeschedule{" + 
			"afternoon = '" + afternoon + '\'' + 
			",evening = '" + evening + '\'' + 
			",morning = '" + morning + '\'' + 
			"}";
		}
}