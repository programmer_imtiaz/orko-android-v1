package com.arko.nextdot.Retrofit.RetrofitModel.Medicine;

/**
 * Created by sadikul on 23-Aug-17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicineById {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("indication")
    @Expose
    private String indication;
    @SerializedName("drug_type")
    @Expose
    private String drugType;
    @SerializedName("medicine_name")
    @Expose
    private String medicineName;
    @SerializedName("generic_name")
    @Expose
    private String genericName;
    @SerializedName("dose")
    @Expose
    private String dose;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("create_by")
    @Expose
    private Integer createBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("medicine_image")
    @Expose
    private String medicineImage;
    @SerializedName("adult_dose")
    @Expose
    private String adultDose;
    @SerializedName("child_dose")
    @Expose
    private String childDose;
    @SerializedName("renal_dose")
    @Expose
    private String renalDose;
    @SerializedName("administration")
    @Expose
    private String administration;
    @SerializedName("contradictions")
    @Expose
    private String contradictions;
    @SerializedName("side_effects")
    @Expose
    private String sideEffects;
    @SerializedName("precautions_and_warnings")
    @Expose
    private String precautionsAndWarnings;
    @SerializedName("pregnancy_category")
    @Expose
    private String pregnancyCategory;
    @SerializedName("therapeutic_class")
    @Expose
    private String therapeuticClass;
    @SerializedName("mode_of_action")
    @Expose
    private String modeOfAction;
    @SerializedName("interaction")
    @Expose
    private String interaction;
    @SerializedName("pack_size_and_prize")
    @Expose
    private String packSizeAndPrize;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMedicineImage() {
        return medicineImage;
    }

    public void setMedicineImage(String medicineImage) {
        this.medicineImage = medicineImage;
    }

    public String getAdultDose() {
        return adultDose;
    }

    public void setAdultDose(String adultDose) {
        this.adultDose = adultDose;
    }

    public String getChildDose() {
        return childDose;
    }

    public void setChildDose(String childDose) {
        this.childDose = childDose;
    }

    public String getRenalDose() {
        return renalDose;
    }

    public void setRenalDose(String renalDose) {
        this.renalDose = renalDose;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getContradictions() {
        return contradictions;
    }

    public void setContradictions(String contradictions) {
        this.contradictions = contradictions;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getPrecautionsAndWarnings() {
        return precautionsAndWarnings;
    }

    public void setPrecautionsAndWarnings(String precautionsAndWarnings) {
        this.precautionsAndWarnings = precautionsAndWarnings;
    }

    public String getPregnancyCategory() {
        return pregnancyCategory;
    }

    public void setPregnancyCategory(String pregnancyCategory) {
        this.pregnancyCategory = pregnancyCategory;
    }

    public String getTherapeuticClass() {
        return therapeuticClass;
    }

    public void setTherapeuticClass(String therapeuticClass) {
        this.therapeuticClass = therapeuticClass;
    }

    public String getModeOfAction() {
        return modeOfAction;
    }

    public void setModeOfAction(String modeOfAction) {
        this.modeOfAction = modeOfAction;
    }

    public String getInteraction() {
        return interaction;
    }

    public void setInteraction(String interaction) {
        this.interaction = interaction;
    }

    public String getPackSizeAndPrize() {
        return packSizeAndPrize;
    }

    public void setPackSizeAndPrize(String packSizeAndPrize) {
        this.packSizeAndPrize = packSizeAndPrize;
    }

}
