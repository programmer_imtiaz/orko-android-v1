
package com.arko.nextdot.Retrofit.RetrofitModel.Appointments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientsLists {

    @SerializedName("status")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private List<PatientList> patientList = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<PatientList> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<PatientList> patientList) {
        this.patientList = patientList;
    }

}
