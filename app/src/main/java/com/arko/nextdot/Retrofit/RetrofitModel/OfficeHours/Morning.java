package com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours;


import com.google.gson.annotations.SerializedName;


public class Morning{

	@SerializedName("start")
	private String start;

	@SerializedName("end")
	private String end;

	@SerializedName("status")
	private String status;

	public void setStart(String start){
		this.start = start;
	}

	public String getStart(){
		return start;
	}

	public void setEnd(String end){
		this.end = end;
	}

	public String getEnd(){
		return end;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}