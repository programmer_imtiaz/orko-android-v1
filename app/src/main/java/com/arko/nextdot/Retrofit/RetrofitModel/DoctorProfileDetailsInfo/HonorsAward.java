
package com.arko.nextdot.Retrofit.RetrofitModel.DoctorProfileDetailsInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HonorsAward {

    @SerializedName("honours")
    @Expose
    private Integer honours;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getHonours() {
        return honours;
    }

    public void setHonours(Integer honours) {
        this.honours = honours;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
