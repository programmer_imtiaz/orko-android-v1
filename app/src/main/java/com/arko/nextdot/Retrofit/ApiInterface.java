package com.arko.nextdot.Retrofit;

import com.arko.nextdot.Retrofit.RetrofitModel.AddFullDetailsValue;

import com.arko.nextdot.Retrofit.RetrofitModel.DoctorProfileDetailsInfo.DoctorProfileDetailsinformation;
import com.arko.nextdot.Retrofit.RetrofitModel.Medicine.DrugList;
import com.arko.nextdot.Retrofit.RetrofitModel.Medicine.MedicineById;
import com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours.ScheduleUpdateResponse;
import com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours.TimeSchedule;
import com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list.DoctorList;
import com.arko.nextdot.Retrofit.RetrofitModel.ProfileBasicInfo;
import com.arko.nextdot.Retrofit.RetrofitModel.Registration;
import com.arko.nextdot.Utils.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by ASUS on 31-Jul-17.
 */

public interface ApiInterface {
    //public static final String url=Constants.medicineListURL;

    @POST(Constants.medicineListURL)
    Call<DrugList> getDruglistJson(@Query("token") String token);

    @POST()
    Call<MedicineById> getDgurById(@Url String url, @Query("token") String token);

    @POST(Constants.logIn)
    Call<ProfileBasicInfo> login(@Query("username") String username, @Query("password") String password);

    @POST(Constants.fb_logIn)
    Call<ProfileBasicInfo> fb_login(@Query("fb_id") String fb_id,
                                    @Query("fb_first_name") String fb_first_name,
                                    @Query("fb_last_name") String fb_last_name,
                                    @Query("fb_profile_pic") String fb_profile_pic,
                                    @Query("fb_gender") String fb_gender,
                                    @Query("role_id") String role_id
    );
    
/*
    @POST("register/doctor")
    Call<Registration> registration(@Query("first_name") String firstname,
                                    @Query("last_name") String lastname,
                                    @Query("username") String username,
                                    @Query("phone") String phone,
                                    @Query("email") String email,
                                    @Query("dob") String dob,
                                    @Query("gender") String gender,
                                    @Query("district") int district,
                                    @Query("upazilla") int upazilla,
                                    @Query("speciality") String speciality,
                                    @Query("organization") String organization,
                                    @Query("password") String password,
                                    @Query("password_confirmation") String password_confirmation);*/
    @POST(Constants.registerDoctor)
    Call<Registration> registration(
            @Query("username") String username,
            @Query("phone") String phone,
            @Query("email") String email,
            @Query("password") String password,
            @Query("password_confirmation") String password_confirmation);



    @POST(Constants.profileDetailsInfo)
    Call<DoctorProfileDetailsinformation> getDoctorProfileInfoJson(@Query("token") String token,
                                                                   @Query("id") String id);

    @POST(Constants.doctorProfileEditBasic)
    Call<DoctorProfileDetailsinformation> updateDoctorBasicProfile(@Query("token") String token,
                                                                   @Query("id") String id);


    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> addCertificate(@Query("token") String token,
                                             @Query("user_id") String id,
                                             @Query("field_name") String field_name,
                                             @Query("description") String description,
                                             @Query("certificate_authority") String certificate_authority,
                                             @Query("license_number") String license_number,
                                             @Query("from_date") String from_date,
                                             @Query("to_date") String to_date,
                                             @Query("certificate_name") String certificate_name);

    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> addExperience(@Query("token") String token,
                                            @Query("user_id") String id,
                                            @Query("field_name") String field_name,
                                            @Query("address") String address,
                                            @Query("company") String company,
                                            @Query("from_date") String from_date,
                                            @Query("to_date") String to_date,
                                            @Query("title") String experience_title);
    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> updateExperience(@Query("token") String token,
                                               @Query("user_id") String id,
                                               @Query("field_name") String field_name,
                                               @Query("field_id") String field_id,
                                               @Query("address") String address,
                                               @Query("company") String company,
                                               @Query("from_date") String from_date,
                                               @Query("to_date") String to_date,
                                               @Query("title") String experience_title);


    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> addPublication(@Query("token") String token,
                                             @Query("user_id") String id,
                                             @Query("field_name") String field_name,
                                             @Query("publisher") String publisher,
                                             @Query("author") String author,
                                             @Query("publication_date") String publication_date,
                                             @Query("description") String description,
                                             @Query("publication_url") String publication_url,
                                             @Query("title") String title);

    @POST(Constants.doctorProfileEditFull)
        Call<AddFullDetailsValue> updatePublication(@Query("token") String token,
                                                    @Query("user_id") String id,
                                                    @Query("field_name") String field_name,
                                                    @Query("field_id") String field_id,
                                                    @Query("publisher") String publisher,
                                                    @Query("author") String author,
                                                    @Query("publication_date") String publication_date,
                                                    @Query("description") String description,
                                                    @Query("publication_url") String publication_url,
                                                    @Query("title") String title);

    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> addEducation(@Query("token") String token,
                                           @Query("user_id") String id,
                                           @Query("field_name") String field_name,
                                           @Query("institute") String institute,
                                           @Query("grade") String grade,
                                           @Query("degree") String degree,
                                           @Query("from_date") String from_date,
                                           @Query("to_date") String to_date,
                                           @Query("field_of_study") String field_of_study);

    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> updateEducation(@Query("token") String token,
                                              @Query("user_id") String id,
                                              @Query("field_name") String field_name,
                                              @Query("field_id") String field_id,
                                              @Query("institute") String institute,
                                              @Query("grade") String grade,
                                              @Query("degree") String degree,
                                              @Query("from_date") String from_date,
                                              @Query("to_date") String to_date,
                                              @Query("field_of_study") String field_of_study);

    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> addCourse(@Query("token") String token,
                                        @Query("user_id") String id,
                                        @Query("field_name") String field_name,
                                        @Query("organized_by") String organized_by,
                                        @Query("venue") String venue,
                                        @Query("from_date") String from_date,
                                        @Query("title") String title,
                                        @Query("to_date") String to_date);
    
    @POST(Constants.doctorProfileEditFull)
    Call<AddFullDetailsValue> updateCourse(@Query("token") String token,
                                           @Query("user_id") String id,
                                           @Query("field_name") String field_name,
                                           @Query("field_id") String field_id,
                                           @Query("organized_by") String organized_by,
                                           @Query("venue") String venue,
                                           @Query("from_date") String from_date,
                                           @Query("title") String title,
                                           @Query("to_date") String to_date);



    
    @POST(Constants.DoctorListsApi)
    Call<DoctorList> patientsList(@Query("token") String token);


    @POST()
    Call<TimeSchedule> doctorTimeSchedule(@Url String url, @Query("token") String token);

    @POST()
    @FormUrlEncoded
    Call<ScheduleUpdateResponse> doctorUpdateShcedule(@Url String url, @Query("token") String token, @Field("schedule") String schedule);
}
