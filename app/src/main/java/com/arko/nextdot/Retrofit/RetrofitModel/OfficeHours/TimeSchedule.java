package com.arko.nextdot.Retrofit.RetrofitModel.OfficeHours;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSchedule{

	@SerializedName("timeschedule")
	private List<TimescheduleItem> timeschedule;

	@SerializedName("status")
	private String status;

	public void setTimeschedule(List<TimescheduleItem> timeschedule){
		this.timeschedule = timeschedule;
	}

	public List<TimescheduleItem> getTimeschedule(){
		return timeschedule;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}