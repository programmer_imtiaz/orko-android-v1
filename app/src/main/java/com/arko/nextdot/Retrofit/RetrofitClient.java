package com.arko.nextdot.Retrofit;

import com.arko.nextdot.Utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ASUS on 31-Jul-17.
 */

public class RetrofitClient {

    public static Retrofit getRetrofitClient(){
        return new Retrofit.Builder().
                baseUrl(Constants.baseUrl).
                addConverterFactory(GsonConverterFactory.create()).build();
        }

    public static ApiInterface getApiInterface(){
        return getRetrofitClient().create(ApiInterface.class);
    }
}
