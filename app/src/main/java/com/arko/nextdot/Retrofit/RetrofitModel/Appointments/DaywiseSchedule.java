package com.arko.nextdot.Retrofit.RetrofitModel.Appointments;

import com.google.gson.annotations.SerializedName;

public class DaywiseSchedule{

	@SerializedName("timeschedule")
	private Timeschedule timeschedule;

	@SerializedName("status")
	private String status;

	public void setTimeschedule(Timeschedule timeschedule){
		this.timeschedule = timeschedule;
	}

	public Timeschedule getTimeschedule(){
		return timeschedule;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DaywiseSchedule{" + 
			"timeschedule = '" + timeschedule + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}