package com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list;


import com.google.gson.annotations.SerializedName;

public class DoctorList {

	@SerializedName("result")
	private Result result;



	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"DoctorList{" +
			"result = '" + result + '\'' + 
			"}";
		}



}