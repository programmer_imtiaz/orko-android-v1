package com.arko.nextdot.Retrofit.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ASUS on 16-Aug-17.
 */

public class MessageInfo{
    @SerializedName("username")
    @Expose
    ArrayList<String> username;

    @SerializedName("phone")
    @Expose
    ArrayList<String>  phone;
    @SerializedName("email")
    @Expose
    ArrayList<String>  email;

    public ArrayList<String>  getUsername() {
        return username;
    }

    public ArrayList<String>  getPhone() {
        return phone;
    }

    public ArrayList<String>  getEmail() {
        return email;
    }
}