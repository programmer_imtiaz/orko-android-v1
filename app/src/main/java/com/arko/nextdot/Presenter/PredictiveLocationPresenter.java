package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.PredictiveLocationFragmentView;
import com.arko.nextdot.Interface.PredictiveLocationRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;
import com.arko.nextdot.Network.InvokePredicitiveLocation;

/**
 * Created by sakib on 11/22/2017.
 */

public class PredictiveLocationPresenter {

    PredictiveLocationFragmentView view ;

    public PredictiveLocationPresenter(PredictiveLocationFragmentView predictiveLocationFragmentView){
        this.view = predictiveLocationFragmentView ;
    }

    public void getPredictiveLocation(String query){

        view.startLoading();
        new InvokePredicitiveLocation(view.getAppContext(), query, new PredictiveLocationRequestComplete() {

            @Override
            public void predictiveLocationRequestComplete(PredictiveLocationRoot predictiveLocationRoot) {

                view.stopLoading();
                view.showSearchResult(predictiveLocationRoot);
            }

            @Override
            public void predictiveLocationRequestError(String msg) {
                view.stopLoading();
            }

        }) ;

    }
}
