package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.OrkoHomeRequestComplete;
import com.arko.nextdot.Interface.OrkoHomeView;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;
import com.arko.nextdot.Network.InvokePredictiveSearch;

/**
 * Created by sakib on 11/15/2017.
 */

public class OrkoHomePresenter {

    OrkoHomeView view ;

    public OrkoHomePresenter(OrkoHomeView orkoHomeView){
        this.view = orkoHomeView ;
    }

    public void getPredictiveSearchData(String query){

        view.startLoading();
        new InvokePredictiveSearch(view.getAppContext(), query, new OrkoHomeRequestComplete() {
            @Override
            public void onRequestComplete(PredictiveSearchRoot predictiveSearchRoot) {
                view.stopLoading();
                view.showSearchResult(predictiveSearchRoot);
            }

            @Override
            public void onRequestError(String msg) {

            }
        });
    }
}
