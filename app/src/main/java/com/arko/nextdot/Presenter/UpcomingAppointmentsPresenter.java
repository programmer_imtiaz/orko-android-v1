package com.arko.nextdot.Presenter;

import android.util.Log;

import com.arko.nextdot.Interface.UpcomingAppointmentView;
import com.arko.nextdot.Interface.UpcomingAppointmentsRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;
import com.arko.nextdot.Network.InvokeUpcomingAppointments;

/**
 * Created by sakib on 11/23/2017.
 */

public class UpcomingAppointmentsPresenter {

    UpcomingAppointmentView view ;

    public UpcomingAppointmentsPresenter(UpcomingAppointmentView upcomingAppointmentView){
        this.view = upcomingAppointmentView ;
    }

    public void getUpcomingAppointments(final String token, final String patient_id){

        view.stopLoading();
        new InvokeUpcomingAppointments(view.getAppContext(), token, patient_id, new UpcomingAppointmentsRequestComplete() {
            @Override
            public void UpcomingAppointmentsRequestComplete(UpcomingAppointmentsRoot upcomingAppointmentsRoot) {
                Log.d("+++presenter+++", "RequestComplete") ;
                view.stopLoading();
                view.showUpcomingAppointmentResult(upcomingAppointmentsRoot);
            }

            @Override
            public void UpcomingAppointmentsRequestError(String msg) {
                view.showMessage(msg);
            }
        });
    }
}
