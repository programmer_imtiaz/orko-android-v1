package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.HistoryAppointmentView;
import com.arko.nextdot.Interface.HistoryAppointmentsRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;
import com.arko.nextdot.Network.InvokeHistoryAppointments;

/**
 * Created by sakib on 11/26/2017.
 */

public class HistoryAppointmentsPresenter {

    HistoryAppointmentView view ;

    public HistoryAppointmentsPresenter(HistoryAppointmentView historyAppointmentView){
        this.view = historyAppointmentView ;
    }

    public void getHistoryAppointments(final String token, final String patient_id){

        view.startLoading();
        new InvokeHistoryAppointments(view.getAppContext(), token, patient_id, new HistoryAppointmentsRequestComplete() {
            @Override
            public void onRequestComplete(HistoryAppointmentsRoot historyAppointmentsRoot) {
                view.stopLoading();
                view.showHistoryAppointmentResult(historyAppointmentsRoot);
            }

            @Override
            public void onRequestError(String msg) {
                view.showMessage(msg);
            }
        });
    }
}
