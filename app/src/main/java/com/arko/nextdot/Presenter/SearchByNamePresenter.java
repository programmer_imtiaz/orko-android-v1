package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.SearchByNameRequestComplete;
import com.arko.nextdot.Interface.SearchByNameView;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameRoot;
import com.arko.nextdot.Network.InvokeSearchByName;

/**
 * Created by Dipto on 10/18/2017.
 */

public class SearchByNamePresenter {

    SearchByNameView searchByNameView ;
    String key_value, end_point ;

    public SearchByNamePresenter(SearchByNameView view){
        this.searchByNameView = view ;

    }

    public void getSearchResult(String end_point, String token, String name){

        this.searchByNameView.startLoading();
        new InvokeSearchByName(searchByNameView.getAppContext(), end_point, token, name, new SearchByNameRequestComplete() {

            @Override
            public void onRequestComplete(SearchByNameRoot searchByNameRoot) {
                searchByNameView.stopLoading();
                searchByNameView.showSearchResult(searchByNameRoot);
            }

            @Override
            public void onRequestError(String msg) {

            }
        });

    }
}