package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.PrescriptionRequestComplete;
import com.arko.nextdot.Interface.PrescriptionView;
import com.arko.nextdot.Model.RetrofitModel.PrescriptionItem;
import com.arko.nextdot.Network.ApiPrescriptions;

import java.util.List;

;

/**
 * Created by sadikul on 01-Nov-17.
 */

public class PrescriptionsPresenter {

    PrescriptionView prescriptionView;

    public PrescriptionsPresenter(PrescriptionView context) {


        prescriptionView= context;
    }

    public void getPrescriptions(String token,String patient_id){

        prescriptionView.startLoading();
        new ApiPrescriptions(prescriptionView.getAppContext(), token, patient_id, new PrescriptionRequestComplete() {
            @Override
            public void onSuccess(List<PrescriptionItem> prescriptionItems) {

                prescriptionView.stopLoading();
                prescriptionView.passData(prescriptionItems);
            }

            @Override
            public void onError(String message) {

                prescriptionView.stopLoading();
                prescriptionView.onError(message);
            }
        });

    }
}
