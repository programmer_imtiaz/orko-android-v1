package com.arko.nextdot.Presenter;

import com.arko.nextdot.Activities.AddNonOrkoDoctorActivity;
import com.arko.nextdot.Interface.AddNonOrkoDoctorRequestComplete;
import com.arko.nextdot.Interface.AddNonOrkoView;
import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;
import com.arko.nextdot.Network.InvokeAddNonOrkoDoctor;

import java.util.List;

/**
 * Created by sakib on 10/30/2017.
 */

public class AddNonOrkoDoctorPresenter {

    AddNonOrkoView view ;

    public AddNonOrkoDoctorPresenter(AddNonOrkoView addNonOrkoView){
        this.view = addNonOrkoView ;
    }

    public void getAddNonOrkoDoctorMsg(List<String> addInfoData){

        new InvokeAddNonOrkoDoctor(view.getAppContext(), addInfoData, new AddNonOrkoDoctorRequestComplete() {
            @Override
            public void onRequestComplete(AddNonOrkoDoctorSuccessMessage addNonOrkoDoctorSuccessMessage) {
                view.showAddNonOrkoSuccessMsg(addNonOrkoDoctorSuccessMessage);
            }

            @Override
            public void onRequestError(String msg) {

            }
        });
    }
}
