package com.arko.nextdot.Presenter;

import android.util.Log;

import com.arko.nextdot.Interface.SearchDoctorResultRequestComplete;
import com.arko.nextdot.Interface.SearchDoctorResultView;
import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;
import com.arko.nextdot.Network.InvokeSearchDoctorResult;

/**
 * Created by sakib on 11/20/2017.
 */

public class SearchDoctorResultPresenter {

    SearchDoctorResultView view ;

    public SearchDoctorResultPresenter(SearchDoctorResultView searchDoctorResultView){

        this.view = searchDoctorResultView ;
    }

    public void getDoctorSearchResult(String exprience, String speciality, String fees, String location){

        view.startLoading();
        new InvokeSearchDoctorResult(view.getAppContext(), exprience, speciality, fees, location, new SearchDoctorResultRequestComplete() {

            @Override
            public void searchDoctorResultRequestComplete(SearchDoctorResultRoot searchDoctorResultRoot) {

                Log.d("ResultRequestComplete", "YESSS");
                view.stopLoading();
                view.showSearchDoctorResult(searchDoctorResultRoot);
            }

            @Override
            public void searchDoctorResultRequestError(String msg) {
                view.stopLoading();
                view.showMessage("Something Went Wrong !");
            }
        });
    }
}
