package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.QuestionaryRequestComplete;
import com.arko.nextdot.Interface.QuestionaryView;
import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;
import com.arko.nextdot.Network.InvokeQuestionary;

import java.util.List;

/**
 * Created by sakib on 10/29/2017.
 */

public class QuestionaryPresenter {

    QuestionaryView view ;

    public QuestionaryPresenter(QuestionaryView questionaryView){
        this.view = questionaryView ;
    }

    public void getQuestionarySubmitMsg(String token, List<String> anslist){

        new InvokeQuestionary(view.getAppContext(), token, anslist, new QuestionaryRequestComplete() {
            @Override
            public void questionaryOnRequestComplete(QuestionaryRoot questionaryRoot) {
                view.showQuestionarySubmitResult(questionaryRoot);
            }

            @Override
            public void questionaryOnRequestError(String msg) {

            }
        });
    }
}
