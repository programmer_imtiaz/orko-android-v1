package com.arko.nextdot.Presenter;

import com.arko.nextdot.Interface.SearchBySpecialityRequestComplete;
import com.arko.nextdot.Interface.SearchBySpecialityView;
import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;
import com.arko.nextdot.Network.InvokeSearchByName;
import com.arko.nextdot.Network.InvokeSearchBySpeciality;

/**
 * Created by sakib on 10/17/2017.
 */

public class SearchBySpecialityPresenter {

    SearchBySpecialityView searchBySpecialityView ;
    String token, name ;

    public SearchBySpecialityPresenter(SearchBySpecialityView view){
        this.searchBySpecialityView = view ;
    }


    public void getSpecialitySearchResult(String end_point, String token, String speciality){
        new InvokeSearchBySpeciality(searchBySpecialityView.getAppContext(), end_point, token, speciality, new SearchBySpecialityRequestComplete() {
            @Override
            public void searchBySpecialityRequestComplete(SearchBySpecialityRoot searchBySpecialityRoot) {
                searchBySpecialityView.showSearchResult(searchBySpecialityRoot);
            }

            @Override
            public void searchBySpecialityRequestError(String msg) {
                searchBySpecialityView.showMessage(msg);
            }
        }) ;
    }
}
