package com.arko.nextdot.Presenter;


import com.arko.nextdot.Interface.ReportsRequestComplete;
import com.arko.nextdot.Interface.ReportsView;
import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.Network.ApiReports;

import java.util.List;

/**
 * Created by sadikul on 01-Nov-17.
 */

public class ReportsPresenter {

    ReportsView reportsView;

    public ReportsPresenter(ReportsView context) {
        reportsView= context;
    }

    public void getReports(String token,String patient_id){

        reportsView.startLoading();
        new ApiReports(reportsView.getAppContext(), token, patient_id, new ReportsRequestComplete() {
            @Override
            public void onSuccess(List<MedicalReportsItem> medicalReportsItems) {

                reportsView.stopLoading();
                reportsView.passData(medicalReportsItems);
            }

            @Override
            public void onError(String message) {

                reportsView.stopLoading();
                reportsView.onError(message);
            }
        });

    }
}
