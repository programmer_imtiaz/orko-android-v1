package com.arko.nextdot.Utils;

/**
 * Created by ASUS on 31-Jul-17.
 */

public class Constants {
    public static final String baseUrl="https://orko.azurewebsites.net/";

    public static final String setAppointmentTimeList="com.arkodoctor.nextdot.setAppintmentTimeList";
    public static final String setAppointmentID="com.arkodoctor.nextdot.setAppointmentID";
    public static final int FragmentAddAppointmentKey=1;
    public static final int FragmentSetAppointmentKey=2;
    public static final int FragmentChangeAppointmentKey=3;
    public static final int FragmentRejectAppointmentKey=4;



//    public static final String baseUrl="http://182.160.109.132/";

    public static final String registerDoctor="api/register/doctor";
    public static final String logIn="api/login";
    public static final String fb_pro_pic_baseurl="api/login";

    public static final String medicineListURL="api/medicine";

    public static final String medicineById=baseUrl+"api/medicine/show/";

    public static final String APPLICATION_ID="com.arkodoctor.nextdot";

    public static final String UpazilaApi=baseUrl+"api/upazilla-list/";

    public static final String districtList="api/district-list";

    public static final String profileDetailsInfo="api/doctor/profile";

    public static final String doctorProfileEditBasic="api/doctor/profile/edit/basic";

    public static final String DoctorListsApi ="api/doctors/list";

    public static final String doctorProfileEditFull="api/doctor/profile/edit/full";

    public static final String addAssistant_newPatient ="api/register/assistant";

    public static final String patientsAppintmentTimeList="com.arkodoctor.nextdot.patientsAppintmentTimeList";
    public static final String fb_logIn="/api/login/fb";
    public static final String fbloginmodelTag="fbloginmodeltag";
    public static final String doctorSchedule="api/doctor-schedule/";
    public static final String doctorScheduleUpdate="api/doctor-schedule/update/";
}
