package com.arko.nextdot.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.arko.nextdot.Model.RetrofitModel.FbLoginUserProfile.PatientProfileRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Retrofit.RetrofitModel.DoctorProfileDetailsInfo.DoctorProfileDetailsinformation;
import com.arko.nextdot.Retrofit.RetrofitModel.ProfileBasicInfo;
import com.arko.nextdot.Model.AppintmentDurations;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by ASUS on 16-Aug-17.
 */

public class PreferenceManager {


    private static PreferenceManager preferenceManager;


    public String appointmentId = "appointmentID";
    public String selectedDate, selectedDay;
    public String userTocken = "userToken";
    public String userProfile = "userProfile";
    public String userProfileDetailsinfo = "com.arkodoctor.nextdot.RetrofitModel.DoctorProfileDetailsinformation";
    public String districtListcount = "com.arkodoctor.nextdot.RetrofitModel.DistrictListCount";
    public String districtListTag = "com.arkodoctor.nextdot.RetrofitModel.DistrictList";
    public String loginChecker = "com.arkodoctor.nextdot.loginChecker";
    public String isFbLoginPhoneSubmitted = "com.arkodoctor.nextdot.isFbLoginPhoneSubmitted";
    public String fbloginChecker = "fb.com.arkodoctor.nextdot.loginChecker";
    public String appointmentdurations = "com.arkodoctor.nextdot.appointmentDuration";
    private SharedPreferences mSharedPreferences;
    private SharedPreferences nSharedPreferences;
    SharedPreferences diptossharedPreferences;

    private PreferenceManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(Constants.APPLICATION_ID, Context.MODE_PRIVATE);

        diptossharedPreferences= context.getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
    }


    public static synchronized PreferenceManager getInstance(Context context) {

        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager(context);
        }

        return preferenceManager;
    }

    public String getAccessToken(){
        return diptossharedPreferences.getString(Constant.token,"null");
    }

    public String getUserID(){
        return diptossharedPreferences.getString(Constant.user_id,"null");
    }

    public boolean setProfileInfo(PatientProfileRoot user){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(userProfile, json);

        return editor.commit();
    }
    public boolean setProfileDetailsInfo(DoctorProfileDetailsinformation DoctorProfileDetailsinformation){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(DoctorProfileDetailsinformation);
        editor.putString(userProfileDetailsinfo, json);

        return editor.commit();
    }

    public PatientProfileRoot getProfileInfo(){
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(userProfile, "");
        return gson.fromJson(json, PatientProfileRoot.class);
    }

    public DoctorProfileDetailsinformation getProfileDetailsInfo(){
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(userProfileDetailsinfo, "");
        return gson.fromJson(json, DoctorProfileDetailsinformation.class);
    }


    public void setFbLogedIn( boolean value){

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(fbloginChecker,value);
        editor.apply();
        editor.commit();
    }

    public void setIsFbLoginPhoneSubmitted (boolean value){

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(isFbLoginPhoneSubmitted,value);
        editor.apply();
        editor.commit();
    }


    public boolean getIsFbLoginPhoneSubmitted(){
        return mSharedPreferences.getBoolean(isFbLoginPhoneSubmitted,false);
    }
    public boolean getFbLogedIn(){
        return mSharedPreferences.getBoolean(fbloginChecker,false);
    }

    public void clearAllData(){

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
        editor.commit();
    }

    public void setLogedIn( boolean value){

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(loginChecker, true);
        editor.apply();
        editor.commit();
    }

    public boolean getLogedIn(){
        return mSharedPreferences.getBoolean(loginChecker,false);
    }

    public void setAppinmentDurations(AppintmentDurations value){

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson=new Gson();
        String json=gson.toJson(value);
        editor.putString(appointmentdurations,json);
        editor.apply();
        editor.commit();
    }

    public AppintmentDurations getAppinmentDurations(){

        Gson gson = new Gson();
        String json = mSharedPreferences.getString(appointmentdurations, "");
        return gson.fromJson(json,AppintmentDurations.class);
    }

    public void setPatientSpinnerArray(ArrayList<String> spinneritemList) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();

        String json = gson.toJson(spinneritemList);

        editor.putString(Constants.patientsAppintmentTimeList, json);
        editor.commit();
    }

    public ArrayList<String> getPatientSpinnerArray() {
        //String[] spinneritemList=mSharedPreferences.getString("").split(",");

        Gson gson = new Gson();
        String json = mSharedPreferences.getString(Constants.patientsAppintmentTimeList, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> spinneritemList = gson.fromJson(json, type);
        return spinneritemList;
    }

    public void setAppointmentTimeRangeArray(ArrayList<String> spinneritemList) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();

        String json = gson.toJson(spinneritemList);

        editor.putString(Constants.setAppointmentTimeList, json);
        editor.commit();
    }


    public ArrayList<String> getAppointmentTimeRangeArray() {
        //String[] spinneritemList=mSharedPreferences.getString("").split(",");

        Gson gson = new Gson();
        String json = mSharedPreferences.getString(Constants.setAppointmentTimeList, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> spinneritemList = gson.fromJson(json, type);
        return spinneritemList;
    }

    public void setAppointmentID(String appointmentID) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(appointmentId, "");
        editor.apply();
        editor.commit();
    }


    public String getAppointmentID() {

        return mSharedPreferences.getString(appointmentId, "");
    }

    public void setSelectedDate(String dateSelected) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(selectedDate, "");
        editor.apply();
        editor.commit();
    }

    public String getSelectedDate() {

        return mSharedPreferences.getString(selectedDate, "");
    }

    public void setSelectedDay(String daySelected) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(selectedDay, "");
        editor.apply();
        editor.commit();
    }

    public String getSelectedDay() {

        return mSharedPreferences.getString(selectedDay, "");
    }

   /* public boolean setMyInfo(MyInfoModel myInfoModel) {

        if (myInfoModel == null)
            return false;

        setUserID(myInfoModel.getMyUserID());
        setUserEmail(myInfoModel.getMyEmail());
        setFirstUserName(myInfoModel.getMyFirstName());
        setLastUserName(myInfoModel.getMyLastName());
        setLogcode(myInfoModel.getMyLogcode());
        setUserOldPasword(myInfoModel.getMyOldPassword());
        //setEmail(myInfoModel.getEmail());

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, myInfoModel.getMyDOBYear());
        c.set(Calendar.MONTH, myInfoModel.getMyDOBMonth());
        c.set(Calendar.DAY_OF_MONTH, myInfoModel.getMyDOBDate());
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        setMyDOB(c.getTimeInMillis() / 1000L);

        setMyPhoneNumber(myInfoModel.getMyPhoneNumber());
        setMyCountry(myInfoModel.getMyCountry());
        setMyGender(myInfoModel.getMyGender());

        setMyPaypal(myInfoModel.getPaypal());
        setMyAddressLine1(myInfoModel.getAddressLine1());
        setMyAddressLine2(myInfoModel.getAddressLine2());
        setMyCity(myInfoModel.getCity());
        setMyState(myInfoModel.getState());
        setMyZip(myInfoModel.getZip());
        setMyPin(myInfoModel.getMyPin());
        setMyReferenceCode(myInfoModel.getMyReference());
        setMyMSAStatus(myInfoModel.getStockishStatus());

        return true;
    }

    public boolean setUrl(String imageURL) {
        if (imageURL == null)
            return false;

        SharedPreferences.Editor editor = nSharedPreferences.edit();

        editor.putString(imageURLKey, imageURL);
        return editor.commit();
    }

    public String getURL() {
        return nSharedPreferences.getString(imageURLKey, "");
    }

    public boolean setUserID(String userID) {
        if (userID == null || userID.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();

        editor.putString(userIDKey, userID);
        return editor.commit();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getUserID() {
        return mSharedPreferences.getString(userIDKey, null);
    }

    public String getUserEmail() {
        return mSharedPreferences.getString(userEmailKey, null);
    }

    public boolean setUserEmail(String userEmail) {
        if (userEmail == null || userEmail.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(userEmailKey, userEmail);
        return editor.commit();
    }

    public String getUserFirstName() {
        return mSharedPreferences.getString(userFirstNameKey, null);
    }

    public boolean setFirstUserName(String userFirstName) {
        if (userFirstName == null || userFirstName.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(userFirstNameKey, userFirstName);
        return editor.commit();
    }

    public String getUserLastName() {
        return mSharedPreferences.getString(userLastNameKey, null);
    }

    public boolean setLastUserName(String userLastName) {
        if (userLastName == null || userLastName.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(userLastNameKey, userLastName);
        return editor.commit();
    }

    public String getUserLogcode() {
        return mSharedPreferences.getString(Constant.LOGCODE_KEY, null);
    }

    public boolean setUserLogcode(String log) {
        if (log == null)
            return false;

        SharedPreferences.Editor editor = nSharedPreferences.edit();

        editor.putString(userLogCode, log);
        return editor.commit();
    }

    public boolean setLogcode(String userLogcode) {
        if (userLogcode == null || userLogcode.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constant.LOGCODE_KEY, userLogcode);
        return editor.commit();
    }

    public String getUserOldPassword() {
        return mSharedPreferences.getString(Constant.OLD_PASSWORD_KEY, null);
    }

    public boolean setUserOldPasword(String password) {
        if (password == null || password.length() < 1)
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constant.OLD_PASSWORD_KEY, password);
        return editor.commit();
    }



    public int getMyGender() {
        return mSharedPreferences.getInt(Constant.GENDER_KEY, 0);
    }

    public boolean setMyGender(int gender) {
        if (!Utility.isValidGender(gender))
            return false;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(Constant.GENDER_KEY, gender);
        return editor.commit();
    }

    public long getMyDOB() {
        return mSharedPreferences.getLong(Constant.DOB_KEY, -1);
    }*/
}