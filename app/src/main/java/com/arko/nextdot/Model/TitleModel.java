package com.arko.nextdot.Model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by sakib on 8/3/2017.
 */

public class TitleModel extends ExpandableGroup<SubTitleModel> {

    String title_paragraph ;
    int img ;

    public TitleModel(String maintitle, List<SubTitleModel> items, String title_paragraph, int img){
        super(maintitle, items);

        this.img = img ;
        this.title_paragraph = title_paragraph ;
    }

    public String getTitle_paragraph() {
        return title_paragraph;
    }

    public int getImg() {
        return img;
    }
}
