package com.arko.nextdot.Model.RetrofitModel.DoctorProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sakib on 9/13/2017.
 */

public class DoctorRatingByPatient {

    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
