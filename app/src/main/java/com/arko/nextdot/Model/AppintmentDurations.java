package com.arko.nextdot.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ASUS on 24-Aug-17.
 */

public class AppintmentDurations {
    @SerializedName("appointmentsDuration")
    @Expose
    private List<AppointmentsDuration> appointmentsDuration = null;


    public List<AppointmentsDuration> getAppointmentsDuration() {
        return appointmentsDuration;
    }

    public void setAppointmentsDuration(List<AppointmentsDuration> appointmentsDuration) {
        this.appointmentsDuration = appointmentsDuration;
    }
}
