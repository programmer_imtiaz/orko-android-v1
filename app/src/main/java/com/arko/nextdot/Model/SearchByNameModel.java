package com.arko.nextdot.Model;

/**
 * Created by sakib on 10/16/2017.
 */

public class SearchByNameModel {
    String name, speciality ;
    float rating ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
