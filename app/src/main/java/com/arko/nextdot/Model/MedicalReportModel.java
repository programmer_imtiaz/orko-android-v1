package com.arko.nextdot.Model;



public class MedicalReportModel {

    String report_name, lab_name, delivary_report ;
    int report_icon ;

    public int getReport_icon() {
        return report_icon;
    }

    public void setReport_icon(int report_icon) {
        this.report_icon = report_icon;
    }


    public String getReport_name() {
        return report_name;
    }

    public void setReport_name(String report_name) {
        this.report_name = report_name;
    }

    public String getLab_name() {
        return lab_name;
    }

    public void setLab_name(String lab_name) {
        this.lab_name = lab_name;
    }

    public String getDelivary_report() {
        return delivary_report;
    }

    public void setDelivary_report(String delivary_report) {
        this.delivary_report = delivary_report;
    }
}
