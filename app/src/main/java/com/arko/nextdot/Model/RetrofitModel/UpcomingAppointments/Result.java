package com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Result{

	@SerializedName("data")
	private List<UpcomingAppointmentsList> data;

	@SerializedName("status")
	private String status;

	public void setData(List<UpcomingAppointmentsList> data){
		this.data = data;
	}

	public List<UpcomingAppointmentsList> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Result{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}