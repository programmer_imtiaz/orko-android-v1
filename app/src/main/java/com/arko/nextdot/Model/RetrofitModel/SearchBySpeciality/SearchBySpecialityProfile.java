package com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality;


import com.google.gson.annotations.SerializedName;


public class SearchBySpecialityProfile {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("name_bn")
	private String name_bn;

	public String getName_bn() {
		return name_bn;
	}

	public void setName_bn(String name_bn) {
		this.name_bn = name_bn;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"UpcomingAppointmentsList{" +
			"name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}