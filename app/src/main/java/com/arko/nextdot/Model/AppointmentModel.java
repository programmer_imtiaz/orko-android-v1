package com.arko.nextdot.Model;

/**
 * Created by Dipto on 5/8/2017.
 */

public class AppointmentModel {
    String appt_title, appt_date ;

    public String getAppt_title() {
        return appt_title;
    }

    public void setAppt_title(String appt_title) {
        this.appt_title = appt_title;
    }

    public String getAppt_date() {
        return appt_date;
    }

    public void setAppt_date(String appt_date) {
        this.appt_date = appt_date;
    }
}
