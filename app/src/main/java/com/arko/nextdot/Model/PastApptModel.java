package com.arko.nextdot.Model;



public class PastApptModel {
    String past_appt_title, past_appt_date ;

    public String getPast_appt_title() {
        return past_appt_title;
    }

    public void setPast_appt_title(String past_appt_title) {
        this.past_appt_title = past_appt_title;
    }

    public String getPast_appt_date() {
        return past_appt_date;
    }

    public void setPast_appt_date(String past_appt_date) {
        this.past_appt_date = past_appt_date;
    }
}
