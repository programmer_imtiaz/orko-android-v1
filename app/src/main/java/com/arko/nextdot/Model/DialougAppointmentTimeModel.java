package com.arko.nextdot.Model;

/**
 * Created by sakib on 7/27/2017.
 */

public class DialougAppointmentTimeModel {

    String time_dataset ;
    int selected_time ;

    public int getSelected_time() {
        return selected_time;
    }

    public void setSelected_time(int selected_time) {
        this.selected_time = selected_time;
    }

    public String getTime_dataset() {
        return time_dataset;
    }

    public void setTime_dataset(String time_dataset) {
        this.time_dataset = time_dataset;
    }
}
