package com.arko.nextdot.Model.RetrofitModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dipto on 8/18/2017.
 */

public class SubProfile {

    @SerializedName("first_name")
    private String first_name ;

    @SerializedName("last_name")
    private String last_name ;

    @SerializedName("dob")
    private String dob ;

    @SerializedName("gender")
    private String gender ;

    @SerializedName("profession")
    private String profession ;

    @SerializedName("main_problem")
    private String main_problem ;

    @SerializedName("upazilla")
    private Upazilla upazilla ;

    @SerializedName("blood_group")
    private String blood_group ;

    @SerializedName("address")
    private String address ;

    @SerializedName("doctor_id")
    private String doctor_id ;

    @SerializedName("district")
    private District district ;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getMain_problem() {
        return main_problem;
    }

    public void setMain_problem(String main_problem) {
        this.main_problem = main_problem;
    }

    public Upazilla getUpazilla() {
        return upazilla;
    }

    public void setUpazilla(Upazilla upazilla) {
        this.upazilla = upazilla;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
