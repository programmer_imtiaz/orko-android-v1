package com.arko.nextdot.Model.RetrofitModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MedicalRepots{

	@SerializedName("result")
	private String result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<MedicalReportsItem> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<MedicalReportsItem> data){
		this.data = data;
	}

	public List<MedicalReportsItem> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"MedicalRepots{" + 
			"result = '" + result + '\'' + 
			",msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}