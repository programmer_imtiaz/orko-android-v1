package com.arko.nextdot.Model;

/**
 * Created by Dipto on 5/5/2017.
 */

public class AmbulanceModel {

    private String ambulance_name ;
    private int ambulance_image ;

    public String getAmbulance_name() {
        return ambulance_name;
    }

    public void setAmbulance_name(String ambulance_name) {
        this.ambulance_name = ambulance_name;
    }

    public int getAmbulance_image() {
        return ambulance_image;
    }

    public void setAmbulance_image(int ambulance_image) {
        this.ambulance_image = ambulance_image;
    }
}
