package com.arko.nextdot.Model.RetrofitModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sakib on 8/20/2017.
 */

public class Upazilla {

    @SerializedName("name")
    private String name ;

    @SerializedName("id")
    private String id ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
