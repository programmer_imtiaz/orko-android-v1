package com.arko.nextdot.Model.RetrofitModel.HistoryApppointments;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result{

	@SerializedName("data")
	private List<HistoryAppointmentList> data;

	@SerializedName("status")
	private String status;

	public void setData(List<HistoryAppointmentList> data){
		this.data = data;
	}

	public List<HistoryAppointmentList> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Result{" + 
			"data = '" + data + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}