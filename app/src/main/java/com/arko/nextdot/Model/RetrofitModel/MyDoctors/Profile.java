package com.arko.nextdot.Model.RetrofitModel.MyDoctors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sakib on 9/17/2017.
 */

public class Profile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private Object dob;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("speciality")
    @Expose
    private String speciality;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("bio")
    @Expose
    private Object bio;
    @SerializedName("social")
    @Expose
    private Object social;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("profession")
    @Expose
    private Object profession;
    @SerializedName("organization")
    @Expose
    private Object organization;
    @SerializedName("main_problem")
    @Expose
    private Object mainProblem;
    @SerializedName("district")
    @Expose
    private Object district;
    @SerializedName("upazilla")
    @Expose
    private Object upazilla;
    @SerializedName("promo_code")
    @Expose
    private Object promoCode;
    @SerializedName("doctor_promo_code")
    @Expose
    private Object doctorPromoCode;
    @SerializedName("doctor_recommendation")
    @Expose
    private Object doctorRecommendation;
    @SerializedName("interests")
    @Expose
    private Object interests;
    @SerializedName("featured_skill")
    @Expose
    private Object featuredSkill;
    @SerializedName("doctor_id")
    @Expose
    private Object doctorId;
    @SerializedName("family_history")
    @Expose
    private Object familyHistory;
    @SerializedName("allergies")
    @Expose
    private Object allergies;
    @SerializedName("vaccination")
    @Expose
    private Object vaccination;
    @SerializedName("surgery")
    @Expose
    private Object surgery;
    @SerializedName("rating")
    @Expose
    private float rating;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getDob() {
        return dob;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getBio() {
        return bio;
    }

    public void setBio(Object bio) {
        this.bio = bio;
    }

    public Object getSocial() {
        return social;
    }

    public void setSocial(Object social) {
        this.social = social;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getProfession() {
        return profession;
    }

    public void setProfession(Object profession) {
        this.profession = profession;
    }

    public Object getOrganization() {
        return organization;
    }

    public void setOrganization(Object organization) {
        this.organization = organization;
    }

    public Object getMainProblem() {
        return mainProblem;
    }

    public void setMainProblem(Object mainProblem) {
        this.mainProblem = mainProblem;
    }

    public Object getDistrict() {
        return district;
    }

    public void setDistrict(Object district) {
        this.district = district;
    }

    public Object getUpazilla() {
        return upazilla;
    }

    public void setUpazilla(Object upazilla) {
        this.upazilla = upazilla;
    }

    public Object getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(Object promoCode) {
        this.promoCode = promoCode;
    }

    public Object getDoctorPromoCode() {
        return doctorPromoCode;
    }

    public void setDoctorPromoCode(Object doctorPromoCode) {
        this.doctorPromoCode = doctorPromoCode;
    }

    public Object getDoctorRecommendation() {
        return doctorRecommendation;
    }

    public void setDoctorRecommendation(Object doctorRecommendation) {
        this.doctorRecommendation = doctorRecommendation;
    }

    public Object getInterests() {
        return interests;
    }

    public void setInterests(Object interests) {
        this.interests = interests;
    }

    public Object getFeaturedSkill() {
        return featuredSkill;
    }

    public void setFeaturedSkill(Object featuredSkill) {
        this.featuredSkill = featuredSkill;
    }

    public Object getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Object doctorId) {
        this.doctorId = doctorId;
    }

    public Object getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(Object familyHistory) {
        this.familyHistory = familyHistory;
    }

    public Object getAllergies() {
        return allergies;
    }

    public void setAllergies(Object allergies) {
        this.allergies = allergies;
    }

    public Object getVaccination() {
        return vaccination;
    }

    public void setVaccination(Object vaccination) {
        this.vaccination = vaccination;
    }

    public Object getSurgery() {
        return surgery;
    }

    public void setSurgery(Object surgery) {
        this.surgery = surgery;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

}
