package com.arko.nextdot.Model;


public class ReminderModel {
    String consult_doc, consult_time ;

    public String getConsult_doc() {
        return consult_doc;
    }

    public void setConsult_doc(String consult_doc) {
        this.consult_doc = consult_doc;
    }

    public String getConsult_time() {
        return consult_time;
    }

    public void setConsult_time(String consult_time) {
        this.consult_time = consult_time;
    }
}
