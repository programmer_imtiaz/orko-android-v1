package com.arko.nextdot.Model;


public class MedicineListModel {

    String medicine_name, medicine_price, medicine_quantity, medicine_discount ;

    public String getMedicine_name() {
        return medicine_name;
    }

    public void setMedicine_name(String medicine_name) {
        this.medicine_name = medicine_name;
    }

    public String getMedicine_price() {
        return medicine_price;
    }

    public void setMedicine_price(String medicine_price) {
        this.medicine_price = medicine_price;
    }

    public String getMedicine_quantity() {
        return medicine_quantity;
    }

    public void setMedicine_quantity(String medicine_quantity) {
        this.medicine_quantity = medicine_quantity;
    }

    public String getMedicine_discount() {
        return medicine_discount;
    }

    public void setMedicine_discount(String medicine_discount) {
        this.medicine_discount = medicine_discount;
    }
}
