package com.arko.nextdot.Model.RetrofitModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PrescriptionItem implements Parcelable {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("patient_id")
	private int patientId;

	@SerializedName("appointment_id")
	private int appointmentId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	protected PrescriptionItem(Parcel in) {
		updatedAt = in.readString();
		imageUrl = in.readString();
		patientId = in.readInt();
		appointmentId = in.readInt();
		createdAt = in.readString();
		id = in.readInt();
		status = in.readInt();
	}

	public static final Creator<PrescriptionItem> CREATOR = new Creator<PrescriptionItem>() {
		@Override
		public PrescriptionItem createFromParcel(Parcel in) {
			return new PrescriptionItem(in);
		}

		@Override
		public PrescriptionItem[] newArray(int size) {
			return new PrescriptionItem[size];
		}
	};

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setPatientId(int patientId){
		this.patientId = patientId;
	}

	public int getPatientId(){
		return patientId;
	}

	public void setAppointmentId(int appointmentId){
		this.appointmentId = appointmentId;
	}

	public int getAppointmentId(){
		return appointmentId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PrescriptionItem{" +
			"updated_at = '" + updatedAt + '\'' + 
			",image_url = '" + imageUrl + '\'' + 
			",patient_id = '" + patientId + '\'' + 
			",appointment_id = '" + appointmentId + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int i) {

		dest.writeString(updatedAt);
		dest.writeString(imageUrl);
		dest.writeInt(patientId);
		dest.writeInt(appointmentId);
		dest.writeString(createdAt);
		dest.writeInt(id);
		dest.writeInt(status);
	}
}