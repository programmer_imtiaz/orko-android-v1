package com.arko.nextdot.Model.RetrofitModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dipto on 8/18/2017.
 */

public class UserProfile {
    @SerializedName("email")
    private String email ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("id")
    private String id ;

    @SerializedName("avatar")
    private String avatar ;

    @SerializedName("profile")
    private SubProfile subProfile ;

    @SerializedName("phone")
    private String phone ;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public SubProfile getSubProfile() {
        return subProfile;
    }

    public void setSubProfile(SubProfile subProfile) {
        this.subProfile = subProfile;
    }
}
