package com.arko.nextdot.Model;


public class PresentApptModel {

    String present_appt_title, present_appt_date ;
    int present_appt_img ;

    public String getPresent_appt_title() {
        return present_appt_title;
    }

    public void setPresent_appt_title(String present_appt_title) {
        this.present_appt_title = present_appt_title;
    }

    public String getPresent_appt_date() {
        return present_appt_date;
    }

    public void setPresent_appt_date(String present_appt_date) {
        this.present_appt_date = present_appt_date;
    }

    public int getPresent_appt_img() {
        return present_appt_img;
    }

    public void setPresent_appt_img(int present_appt_img) {
        this.present_appt_img = present_appt_img;
    }
}
