package com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult;


import com.google.gson.annotations.SerializedName;


public class SearchDoctorResultProfile {

	@SerializedName("speciality")
	private String speciality;

	@SerializedName("fees")
	private Object fees;

	@SerializedName("is_active")
	private int isActive;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("role_id")
	private int roleId;

	@SerializedName("district")
	private int district;

	@SerializedName("rating")
	private float rating;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("upazilla")
	private int upazilla;

	public void setSpeciality(String speciality){
		this.speciality = speciality;
	}

	public String getSpeciality(){
		return speciality;
	}

	public void setFees(Object fees){
		this.fees = fees;
	}

	public Object getFees(){
		return fees;
	}

	public void setIsActive(int isActive){
		this.isActive = isActive;
	}

	public int getIsActive(){
		return isActive;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setRoleId(int roleId){
		this.roleId = roleId;
	}

	public int getRoleId(){
		return roleId;
	}

	public void setDistrict(int district){
		this.district = district;
	}

	public int getDistrict(){
		return district;
	}

	public void setRating(float rating){
		this.rating = rating;
	}

	public float getRating(){
		return rating;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setUpazilla(int upazilla){
		this.upazilla = upazilla;
	}

	public int getUpazilla(){
		return upazilla;
	}

	@Override
 	public String toString(){
		return 
			"UpcomingAppointmentsList{" +
			"speciality = '" + speciality + '\'' + 
			",fees = '" + fees + '\'' + 
			",is_active = '" + isActive + '\'' + 
			",user_id = '" + userId + '\'' + 
			",role_id = '" + roleId + '\'' + 
			",district = '" + district + '\'' + 
			",rating = '" + rating + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",avatar = '" + avatar + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",upazilla = '" + upazilla + '\'' + 
			"}";
		}
}