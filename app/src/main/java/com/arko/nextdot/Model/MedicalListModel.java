package com.arko.nextdot.Model;



public class MedicalListModel {
    String medicine_name, medicine_cause, medicine_taking_time ;

    public String getMedicine_name() {
        return medicine_name;
    }

    public void setMedicine_name(String medicine_name) {
        this.medicine_name = medicine_name;
    }

    public String getMedicine_cause() {
        return medicine_cause;
    }

    public void setMedicine_cause(String medicine_cause) {
        this.medicine_cause = medicine_cause;
    }

    public String getMedicine_taking_time() {
        return medicine_taking_time;
    }

    public void setMedicine_taking_time(String medicine_taking_time) {
        this.medicine_taking_time = medicine_taking_time;
    }
}
