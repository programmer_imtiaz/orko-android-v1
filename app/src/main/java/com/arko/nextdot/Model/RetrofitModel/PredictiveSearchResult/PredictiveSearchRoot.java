package com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PredictiveSearchRoot{

	@SerializedName("result")
	private String result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<SearchList> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<SearchList> data){
		this.data = data;
	}

	public List<SearchList> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"PredictiveSearchRoot{" + 
			"result = '" + result + '\'' + 
			",msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}