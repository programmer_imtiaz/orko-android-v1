package com.arko.nextdot.Model;



public class MessageModel {

    String last_date, message, sender_name ;
    int sender_pro_pic ;

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSender_pro_pic() {
        return sender_pro_pic;
    }

    public void setSender_pro_pic(int sender_pro_pic) {
        this.sender_pro_pic = sender_pro_pic;
    }
}
