package com.arko.nextdot.Model;

/**
 * Created by sakib on 8/7/2017.
 */

public class MedicineTimeDetailsModel {

    String time ;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
