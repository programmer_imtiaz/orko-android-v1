package com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments;

import com.google.gson.annotations.SerializedName;


public class UpcomingAppointmentsRoot{

	@SerializedName("result")
	private Result result;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}

	@Override
 	public String toString(){
		return 
			"UpcomingAppointmentsRoot{" + 
			"result = '" + result + '\'' + 
			"}";
		}
}