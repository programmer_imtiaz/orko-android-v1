package com.arko.nextdot.Model.RetrofitModel.MyDoctors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sakib on 9/17/2017.
 */

//Doctor's root java class

public class MyDoctorsRoot {

    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
