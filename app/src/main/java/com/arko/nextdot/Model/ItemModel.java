package com.arko.nextdot.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ItemModel implements Serializable{
    long id;
    int img;
    String image;
    String name, status, result;
    long price;
    String category;
    String productTitle, productSku, productID, productImage, blogText, commentNumber, sizeData, colorData, regularPrice, salePrice, productSubImage;
    String [] productImages;
    String [] size;
    String [] color;
    ArrayList<String> categoryImages = new ArrayList<String>();
    ArrayList<String> galleryImages = new ArrayList<String>();
    ArrayList<String> productSizes = new ArrayList<String>();
    ArrayList<String> productColors = new ArrayList<String>();
    long likes;
    int total=1;
    public ItemModel(){

    }
    public ItemModel(long id, int img, String image, String name, long price, String category, long likes, String [] productImages, String productTitle, String productImage, String blogText, String commentNumber, String sizeData, String colorData, String regularPrice, String salePrice, String [] size, String [] color, String productSubImage) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.price = price;
        this.category = category;
        this.likes = likes;
        this.image = image;
        this.productImages = productImages;
        this.productTitle = productTitle;
        this.productImage = productImage;
        this.blogText = blogText;
        this.commentNumber = commentNumber;
        this.sizeData = sizeData;
        this.colorData = colorData;
        this.regularPrice = regularPrice;
        this.salePrice = salePrice;
        this.size = size;
        this.color = color;
        this.productSubImage = productSubImage;
    }

    public long getId() {
        return id;
    }

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public String getStrPrice() {
        return "$ "+getPrice();
    }

    public long getPrice() {
        return (price);
    }

    public long getSumPrice() {
        return (price*total);
    }

    public String getCategory() {
        return category;
    }
    public String getLikes() {
        if(likes > 100) {
            return "100+ Likes";
        }else{
            return likes+" Likes";
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String[] getProductImages() {
        return productImages;
    }

    public void setProductImages(String[] productImage) {
        this.productImages = productImage;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getBlogText() {
        return blogText;
    }

    public void setBlogText(String blogText) {
        this.blogText = blogText;
    }

    public String getCommentNumber() {
        return commentNumber;
    }

    public void setCommentNumber(String commentNumber) {
        this.commentNumber = commentNumber;
    }

    public String getSizeData() {
        return sizeData;
    }

    public void setSizeData(String sizeData) {
        this.sizeData = sizeData;
    }

    public String getColorData() {
        return colorData;
    }

    public void setColorData(String colorData) {
        this.colorData = colorData;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(String regularPrice) {
        this.regularPrice = regularPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String[] getSize() {
        return size;
    }

    public void setSize(String[] size) {
        this.size = size;
    }

    public String[] getColor() {
        return color;
    }

    public void setColor(String[] color) {
        this.color = color;
    }

    public String getProductSubImage() {
        return productSubImage;
    }

    public void setProductSubImage(String productSubImage) {
        this.productSubImage = productSubImage;
    }

    public ArrayList<String> getCategoryImages() {
        return categoryImages;
    }

    public void setCategoryImages(ArrayList<String> categoryImages) {
        this.categoryImages = categoryImages;
    }

    public ArrayList<String> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(ArrayList<String> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public ArrayList<String> getProductSizes() {
        return productSizes;
    }

    public void setProductSizes(ArrayList<String> productSizes) {
        this.productSizes = productSizes;
    }

    public ArrayList<String> getProductColors() {
        return productColors;
    }

    public void setProductColors(ArrayList<String> productColors) {
        this.productColors = productColors;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
