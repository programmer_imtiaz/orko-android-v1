package com.arko.nextdot.Model;


public class TherapyModel {

    String therapy_title, therapy_details, keyword ;
    int therapy_img ;

    public String getTherapy_title() {
        return therapy_title;
    }

    public void setTherapy_title(String therapy_title) {
        this.therapy_title = therapy_title;
    }

    public String getTherapy_details() {
        return therapy_details;
    }

    public void setTherapy_details(String therapy_details) {
        this.therapy_details = therapy_details;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getTherapy_img() {
        return therapy_img;
    }

    public void setTherapy_img(int therapy_img) {
        this.therapy_img = therapy_img;
    }
}
