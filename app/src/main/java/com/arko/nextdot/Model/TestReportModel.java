package com.arko.nextdot.Model;


public class TestReportModel {

    String testname, testdate ;
    int test_img ;

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname;
    }

    public String getTestdate() {
        return testdate;
    }

    public void setTestdate(String testdate) {
        this.testdate = testdate;
    }

    public int getTest_img() {
        return test_img;
    }

    public void setTest_img(int test_img) {
        this.test_img = test_img;
    }
}
