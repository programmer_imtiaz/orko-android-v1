package com.arko.nextdot.Model;

/**
 * Created by Arabi on 8/11/2017.
 */

public class AppointmentListModelClass {
    private String userToken;
    private int startTimeInt;
    private int endTimeInt;
    private String appointmentID;
    private String patientID;
    private String creatorID;
    private String patientFirstName;
    private String patientLastName;
    private String reason;
    private String startTime;
    private String endTime;
    private int profileImage;
    private String patientAvatar;
    private Double doctorRating;
    private String doctorSpeciality;
    private String day;
    private String status;
    private String statusColor;
    private String timeDuration;
    private String morningStartTime;
    private String morningEndTime;
    private String afternoonStartTime;
    private String afternoonEndTime;
    private String eveningStartTime;
    private String eveningEndTime;
    private String changedByValue;
    private String morningStatus;
    private String afternoonStatus;
    private String eveningStatus;
    private String doctorID;
    private String doctorAvatar;
    private String doctorFirstName;
    private String doctorLastName;
    private String doctorGender;
    private String doctorEmail;
    private String doctorContact;
    private String appointmentLocation;
    private String appointmentDateForAPI;
    private String appointmentTimeForView;
    private String appointmentTimeTitle;
    private String rejectorID;

    public AppointmentListModelClass(String statusColor, String appointmentTimeForView, String userToken, String doctorID, String doctorAvatar, String doctorFirstName, String doctorLastName, String creatorID, int startTimeInt, int endTimeInt, String appointmentID, String patientID, String patientFirstName, String patientLastName, String reason, String startTime, String endTime, int profileImage, String patientAvatar, Double doctorRating, String doctorSpeciality, String day, String status, String timeDuration, String morningStartTime, String morningEndTime, String afternoonStartTime, String afternoonEndTime, String eveningStartTime, String eveningEndTime, String changedByValue, String morningStatus, String afternoonStatus, String eveningStatus, String doctorGender, String doctorEmail, String doctorContact, String appointmentLocation, String appointmentDateForAPI, String appointmentTimeTitle, String rejectorID) {
        this.statusColor = statusColor;
        this.appointmentTimeForView = appointmentTimeForView;
        this.userToken = userToken;
        this.doctorID = doctorID;
        this.doctorAvatar = doctorAvatar;
        this.doctorFirstName = doctorFirstName;
        this.doctorLastName = doctorLastName;
        this.creatorID = creatorID;
        this.appointmentID = appointmentID;
        this.patientID = patientID;
        this.patientFirstName = patientFirstName;
        this.patientLastName = patientLastName;
        this.reason = reason;
        this.startTime = startTime;
        this.endTime = endTime;
        this.profileImage = profileImage;
        this.patientAvatar = patientAvatar;
        this.doctorRating = doctorRating;
        this.doctorSpeciality = doctorSpeciality;
        this.day = day;
        this.status = status;
        this.timeDuration = timeDuration;
        this.morningStartTime = morningStartTime;
        this.morningEndTime = morningEndTime;
        this.afternoonStartTime = afternoonStartTime;
        this.afternoonEndTime = afternoonEndTime;
        this.eveningStartTime = eveningStartTime;
        this.eveningEndTime = eveningEndTime;
        this.changedByValue = changedByValue;
        this.morningStatus = morningStatus;
        this.afternoonStatus = afternoonStatus;
        this.eveningStatus = eveningStatus;
        this.doctorGender = doctorGender;
        this.doctorEmail = doctorEmail;
        this.doctorContact = doctorContact;
        this.appointmentLocation = appointmentLocation;
        this.appointmentDateForAPI = appointmentDateForAPI;
        this.appointmentTimeTitle = appointmentTimeTitle;
        this.rejectorID = rejectorID;
    }

    public AppointmentListModelClass() {

    }


    public int getStartTimeInt() {
        return startTimeInt;
    }

    public void setStartTimeInt(int startTimeInt) {
        this.startTimeInt = startTimeInt;
    }

    public int getEndTimeInt() {
        return endTimeInt;
    }

    public void setEndTimeInt(int endTimeInt) {
        this.endTimeInt = endTimeInt;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }


    public int getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(int profileImage) {
        this.profileImage = profileImage;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    public String getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getPatientAvatar() {
        return patientAvatar;
    }

    public void setPatientAvatar(String patientAvatar) {
        this.patientAvatar = patientAvatar;
    }

    public String getMorningStartTime() {
        return morningStartTime;
    }

    public void setMorningStartTime(String morningStartTime) {
        this.morningStartTime = morningStartTime;
    }

    public String getMorningEndTime() {
        return morningEndTime;
    }

    public void setMorningEndTime(String morningEndTime) {
        this.morningEndTime = morningEndTime;
    }

    public String getAfternoonStartTime() {
        return afternoonStartTime;
    }

    public void setAfternoonStartTime(String afternoonStartTime) {
        this.afternoonStartTime = afternoonStartTime;
    }

    public String getAfternoonEndTime() {
        return afternoonEndTime;
    }

    public void setAfternoonEndTime(String afternoonEndTime) {
        this.afternoonEndTime = afternoonEndTime;
    }

    public String getEveningStartTime() {
        return eveningStartTime;
    }

    public void setEveningStartTime(String eveningStartTime) {
        this.eveningStartTime = eveningStartTime;
    }

    public String getEveningEndTime() {
        return eveningEndTime;
    }

    public void setEveningEndTime(String eveningEndTime) {
        this.eveningEndTime = eveningEndTime;
    }

    public String getChangedByValue() {
        return changedByValue;
    }

    public void setChangedByValue(String changedByValue) {
        this.changedByValue = changedByValue;
    }

    public String getMorningStatus() {
        return morningStatus;
    }

    public void setMorningStatus(String morningStatus) {
        this.morningStatus = morningStatus;
    }

    public String getAfternoonStatus() {
        return afternoonStatus;
    }

    public void setAfternoonStatus(String afternoonStatus) {
        this.afternoonStatus = afternoonStatus;
    }

    public String getEveningStatus() {
        return eveningStatus;
    }

    public void setEveningStatus(String eveningStatus) {
        this.eveningStatus = eveningStatus;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getDoctorAvatar() {
        return doctorAvatar;
    }

    public void setDoctorAvatar(String doctorAvatar) {
        this.doctorAvatar = doctorAvatar;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(String creatorID) {
        this.creatorID = creatorID;
    }

    public Double getDoctorRating() {
        return doctorRating;
    }

    public void setDoctorRating(Double doctorRating) {
        this.doctorRating = doctorRating;
    }

    public String getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }

    public String getDoctorGender() {
        return doctorGender;
    }

    public void setDoctorGender(String doctorGender) {
        this.doctorGender = doctorGender;
    }

    public String getDoctorEmail() {
        return doctorEmail;
    }

    public void setDoctorEmail(String doctorEmail) {
        this.doctorEmail = doctorEmail;
    }

    public String getDoctorContact() {
        return doctorContact;
    }

    public void setDoctorContact(String doctorContact) {
        this.doctorContact = doctorContact;
    }

    public String getAppointmentLocation() {
        return appointmentLocation;
    }

    public void setAppointmentLocation(String appointmentLocation) {
        this.appointmentLocation = appointmentLocation;
    }

    public String getAppointmentDateForAPI() {
        return appointmentDateForAPI;
    }

    public void setAppointmentDateForAPI(String appointmentDateForAPI) {
        this.appointmentDateForAPI = appointmentDateForAPI;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getAppointmentTimeForView() {
        return appointmentTimeForView;
    }

    public void setAppointmentTimeForView(String appointmentTimeForView) {
        this.appointmentTimeForView = appointmentTimeForView;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public String getAppointmentTimeTitle() {
        return appointmentTimeTitle;
    }

    public void setAppointmentTimeTitle(String appointmentTimeTitle) {
        this.appointmentTimeTitle = appointmentTimeTitle;
    }

    public String getRejectorID() {
        return rejectorID;
    }

    public void setRejectorID(String rejectorID) {
        this.rejectorID = rejectorID;
    }
}
