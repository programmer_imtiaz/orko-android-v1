package com.arko.nextdot.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sakib on 8/3/2017.
 */

public class SubTitleModel implements Parcelable{

    private int img ;
    private String subtitle ;

    public SubTitleModel(int img, String subtitle){
        this.img = img ;
        this.subtitle = subtitle ;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(img);
        dest.writeString(subtitle);
    }


    protected SubTitleModel(Parcel in) {
        img = in.readInt();
        subtitle = in.readString();
    }

    public static final Creator<SubTitleModel> CREATOR = new Creator<SubTitleModel>() {
        @Override
        public SubTitleModel createFromParcel(Parcel in) {
            return new SubTitleModel(in);
        }

        @Override
        public SubTitleModel[] newArray(int size) {
            return new SubTitleModel[size];
        }
    };
}
