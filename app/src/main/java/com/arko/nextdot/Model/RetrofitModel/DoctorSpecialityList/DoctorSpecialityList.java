package com.arko.nextdot.Model.RetrofitModel.DoctorSpecialityList;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DoctorSpecialityList{

	@SerializedName("result")
	private String result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<String> specialityList;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<String> data){
		this.specialityList = data;
	}

	public List<String> getSpecialityList(){
		return specialityList;
	}

	@Override
 	public String toString(){
		return 
			"DoctorSpecialityList{" + 
			"result = '" + result + '\'' + 
			",msg = '" + msg + '\'' + 
			",data = '" + specialityList + '\'' +
			"}";
		}
}