package com.arko.nextdot.Model;

/**
 * Created by sakib on 10/16/2017.
 */

public class SearchBySpeciality {
    String speciality ;

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
}
