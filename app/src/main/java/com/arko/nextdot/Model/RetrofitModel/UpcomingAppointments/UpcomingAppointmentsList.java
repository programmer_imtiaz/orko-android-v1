package com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments;

import com.google.gson.annotations.SerializedName;


public class UpcomingAppointmentsList {

	@SerializedName("doctor_id")
	private int doctorId;

	@SerializedName("appointment_time")
	private Object appointmentTime;

	@SerializedName("appointment_status")
	private String appointmentStatus;

	@SerializedName("doctor_last_name ")
	private String doctorLastName;

	@SerializedName("appointment_id")
	private int appointmentId;

	@SerializedName("appointment_date")
	private String appointmentDate;

	@SerializedName("doctor_pic")
	private String doctorPic;

	@SerializedName("doctor_first_name")
	private String doctorFirstName;

	public void setDoctorId(int doctorId){
		this.doctorId = doctorId;
	}

	public int getDoctorId(){
		return doctorId;
	}

	public void setAppointmentTime(Object appointmentTime){
		this.appointmentTime = appointmentTime;
	}

	public Object getAppointmentTime(){
		return appointmentTime;
	}

	public void setAppointmentStatus(String appointmentStatus){
		this.appointmentStatus = appointmentStatus;
	}

	public String getAppointmentStatus(){
		return appointmentStatus;
	}

	public void setDoctorLastName(String doctorLastName){
		this.doctorLastName = doctorLastName;
	}

	public String getDoctorLastName(){
		return doctorLastName;
	}

	public void setAppointmentId(int appointmentId){
		this.appointmentId = appointmentId;
	}

	public int getAppointmentId(){
		return appointmentId;
	}

	public void setAppointmentDate(String appointmentDate){
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentDate(){
		return appointmentDate;
	}

	public void setDoctorPic(String doctorPic){
		this.doctorPic = doctorPic;
	}

	public String getDoctorPic(){
		return doctorPic;
	}

	public void setDoctorFirstName(String doctorFirstName){
		this.doctorFirstName = doctorFirstName;
	}

	public String getDoctorFirstName(){
		return doctorFirstName;
	}

	@Override
 	public String toString(){
		return 
			"UpcomingAppointmentsList{" +
			"doctor_id = '" + doctorId + '\'' + 
			",appointment_time = '" + appointmentTime + '\'' + 
			",appointment_status = '" + appointmentStatus + '\'' + 
			",doctor_last_name  = '" + doctorLastName + '\'' + 
			",appointment_id = '" + appointmentId + '\'' + 
			",appointment_date = '" + appointmentDate + '\'' + 
			",doctor_pic = '" + doctorPic + '\'' + 
			",doctor_first_name = '" + doctorFirstName + '\'' + 
			"}";
		}
}