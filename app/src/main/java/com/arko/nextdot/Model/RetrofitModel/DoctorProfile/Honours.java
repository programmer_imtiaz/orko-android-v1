package com.arko.nextdot.Model.RetrofitModel.DoctorProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sakib on 9/11/2017.
 */

public class Honours {

    @SerializedName("honours")
    @Expose
    private Integer honours;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getHonours() {
        return honours;
    }

    public void setHonours(Integer honours) {
        this.honours = honours;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
