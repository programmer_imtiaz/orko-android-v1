package com.arko.nextdot.Model.RetrofitModel.Questionary;


import com.google.gson.annotations.SerializedName;


public class QuestionaryRoot{

	@SerializedName("result")
	private String result;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}