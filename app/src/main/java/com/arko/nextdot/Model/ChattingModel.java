package com.arko.nextdot.Model;



public class ChattingModel {

    String message, sender_name ;
    int pro_pic, flag ;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public int getPro_pic() {
        return pro_pic;
    }

    public void setPro_pic(int pro_pic) {
        this.pro_pic = pro_pic;
    }
}
