package com.arko.nextdot.Model.RetrofitModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Prescription{

	@SerializedName("result")
	private String result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<PrescriptionItem> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<PrescriptionItem> data){
		this.data = data;
	}

	public List<PrescriptionItem> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"Prescription{" + 
			"result = '" + result + '\'' + 
			",msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}