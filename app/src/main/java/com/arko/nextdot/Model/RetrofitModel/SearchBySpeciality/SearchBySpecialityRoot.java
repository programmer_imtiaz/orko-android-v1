package com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SearchBySpecialityRoot{

	@SerializedName("result")
	private String result;

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private List<SearchBySpecialityProfile> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(List<SearchBySpecialityProfile> data){
		this.data = data;
	}

	public List<SearchBySpecialityProfile> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"SearchBySpecialityRoot{" + 
			"result = '" + result + '\'' + 
			",msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}