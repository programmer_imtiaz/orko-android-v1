package com.arko.nextdot.Model;


public class TransactionHistoryModel {

    private String trns_time_date, trns_medicine_type, trns_payment, trns_points, trns_medicine_name ;

    public String getTrns_time_date() {
        return trns_time_date;
    }

    public void setTrns_time_date(String trns_time_date) {
        this.trns_time_date = trns_time_date;
    }

    public String getTrns_medicine_type() {
        return trns_medicine_type;
    }

    public void setTrns_medicine_type(String trns_medicine_type) {
        this.trns_medicine_type = trns_medicine_type;
    }

    public String getTrns_payment() {
        return trns_payment;
    }

    public void setTrns_payment(String trns_payment) {
        this.trns_payment = trns_payment;
    }

    public String getTrns_points() {
        return trns_points;
    }

    public void setTrns_points(String trns_points) {
        this.trns_points = trns_points;
    }

    public String getTrns_medicine_name() {
        return trns_medicine_name;
    }

    public void setTrns_medicine_name(String trns_medicine_name) {
        this.trns_medicine_name = trns_medicine_name;
    }
}
