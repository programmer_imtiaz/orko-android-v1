package com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult;

import com.google.gson.annotations.SerializedName;


public class SearchList {


    @SerializedName("name_bn")
	private String nameBn;

	@SerializedName("speciality_id")
	private int specialityId;

	@SerializedName("speciality")
	private Speciality speciality;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("search_type")
	private String searchType;

	@SerializedName("doc_speciality")
	private String docSpeciality;

	@SerializedName("address")
	private String address;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("role_id")
	private int roleId;

	@SerializedName("rating")
	private int rating;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("first_name")
	private String firstName;


	public void setNameBn(String nameBn){
		this.nameBn = nameBn;
	}

	public String getNameBn(){
		return nameBn;
	}

	public void setSpecialityId(int specialityId){
		this.specialityId = specialityId;
	}

	public int getSpecialityId(){
		return specialityId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSearchType(String searchType){
		this.searchType = searchType;
	}

	public String getSearchType(){
		return searchType;
	}

    public String getDocSpeciality() {
        return docSpeciality;
    }

    public void setDocSpeciality(String speciality) {
        this.docSpeciality = speciality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}


	/*@Override
 	public String toString(){
		return
			"SearchList{" +
			"speciality = '" + docSpeciality + '\'' +
			",name_bn = '" + nameBn + '\'' +
			",speciality_id = '" + specialityId + '\'' +
			",name = '" + name + '\'' +
			",id = '" + id + '\'' +
			",search_type = '" + searchType + '\'' +
			"}";
		}*/
}