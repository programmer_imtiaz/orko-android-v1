package com.arko.nextdot.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ASUS on 24-Aug-17.
 */

public class AppointmentsDuration {
    @SerializedName("time")
    @Expose
    private String time;

  /*  public AppointmentsDuration(String time) {
        this.time = time;
    }*/

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
