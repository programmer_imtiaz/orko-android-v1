package com.arko.nextdot.Model.RetrofitModel.DoctorProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sakib on 9/11/2017.
 */

public class DoctorProfileRoot {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("web")
    @Expose
    private Object web;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;

    @SerializedName("created_by")
    @Expose
    private Integer createdBy;

    @SerializedName("profile")
    @Expose
    private Profile profile;

    @SerializedName("rating")
    @Expose
    private String rating;

    @SerializedName("is_reviewed")
    @Expose
    private int isReviewed;

    @SerializedName("appointment_taken")
    @Expose
    private int appointmentTaken;


    /*@SerializedName("recommendations")
        @Expose
        private List<Object> recommendations = null;*/

    @SerializedName("officehours")
    @Expose
    private List<Officehour> officehours = null;
    @SerializedName("associations")
    @Expose
    private List<Association> associations = null;
    @SerializedName("certificates")
    @Expose
    private List<Certificate> certificates = null;
    @SerializedName("educations")
    @Expose
    private List<Education> educations = null;
    @SerializedName("courses")
    @Expose
    private List<Courses> courses = null;
    @SerializedName("experiences")
    @Expose
    private List<Experience> experiences = null;
    @SerializedName("honors_awards")
    @Expose
    private List<Honours> honorsAwards = null;
    @SerializedName("publications")
    @Expose
    private List<Publication> publications = null;
    @SerializedName("review")
    @Expose
    private List<Review> review = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Object getWeb() {
        return web;
    }

    public void setWeb(Object web) {
        this.web = web;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

        /*public List<Object> getRecommendations() {
            return recommendations;
        }

        public void setRecommendations(List<Object> recommendations) {
            this.recommendations = recommendations;
        }*/

    public List<Officehour> getOfficehours() {
        return officehours;
    }

    public void setOfficehours(List<Officehour> officehours) {
        this.officehours = officehours;
    }

    public List<Association> getAssociations() {
        return associations;
    }

    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }

    public List<Courses> getCourses() {
        return courses;
    }

    public void setCourses(List<Courses> courses) {
        this.courses = courses;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public List<Honours> getHonorsAwards() {
        return honorsAwards;
    }

    public void setHonorsAwards(List<Honours> honorsAwards) {
        this.honorsAwards = honorsAwards;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public List<Review> getReview() {
        return review;
    }

    public void setReview(List<Review> review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getIsReviewed() {
        return isReviewed;
    }

    public void setIsReviewed(int isReviewed) {
        this.isReviewed = isReviewed;
    }

    public int getAppointmentTaken() {
        return appointmentTaken;
    }

    public void setAppointmentTaken(int appointmentTaken) {
        this.appointmentTaken = appointmentTaken;
    }
}
