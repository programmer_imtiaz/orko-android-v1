package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.PredictiveLocationRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by sakib on 11/22/2017.
 */

public class InvokePredicitiveLocation {

    PredictiveLocationRequestComplete predictiveLocationRequestComplete ;
    RetrofitInterface retrofitInterface ;
    PublishSubject publishSubject ;
    String url = "" ;

    public InvokePredicitiveLocation(final Context context, final String query, final PredictiveLocationRequestComplete requestComplete){

        this.predictiveLocationRequestComplete = requestComplete ;
        url = Constant.location_link+"input="+query+"&key="+Constant.key+"&components=country:BD" ;
        Log.d("+++MAIN_URL+++", url) ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        if(publishSubject == null){

            Log.d("++checking++", "in Null") ;
            Log.d("+++Searched VALUE+++", query) ;

            publishSubject = PublishSubject.create() ;
            publishSubject
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .switchMap(searchValue -> retrofitInterface.getPredictiveLocation(url).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()))
                    .subscribeWith(new DisposableObserver<PredictiveLocationRoot>() {

                        @Override
                        public void onNext(PredictiveLocationRoot predictiveLocationRoot) {
                            Log.d("++stage++", "on Next") ;
                            predictiveLocationRequestComplete.predictiveLocationRequestComplete(predictiveLocationRoot);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.d("++exception++", String.valueOf(e)) ;
                            predictiveLocationRequestComplete.predictiveLocationRequestError("Something Went Wrong !");
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        publishSubject.onNext(url);
    }
}
