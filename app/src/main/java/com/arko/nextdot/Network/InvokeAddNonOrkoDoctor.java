package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.AddNonOrkoDoctorRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 10/30/2017.
 */

public class InvokeAddNonOrkoDoctor {

    RetrofitInterface retrofitInterface ;
    AddNonOrkoDoctorRequestComplete addNonOrkoDoctorRequestComplete ;
    String end_point = "api/add/non-orko-doctor" ;

    public InvokeAddNonOrkoDoctor(final Context context, final List<String> nonOrkoDoctorInfoList, AddNonOrkoDoctorRequestComplete requestComplete){

        this.addNonOrkoDoctorRequestComplete = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;
        String url = Constant.ROOT_URL+end_point ;
        Call<AddNonOrkoDoctorSuccessMessage> call = retrofitInterface.getAddNonOrkoDoctorMessage(url,
                nonOrkoDoctorInfoList.get(0),nonOrkoDoctorInfoList.get(1),nonOrkoDoctorInfoList.get(2),
                nonOrkoDoctorInfoList.get(3), nonOrkoDoctorInfoList.get(4), nonOrkoDoctorInfoList.get(5)) ;

        call.enqueue(new Callback<AddNonOrkoDoctorSuccessMessage>() {
            @Override
            public void onResponse(Call<AddNonOrkoDoctorSuccessMessage> call, Response<AddNonOrkoDoctorSuccessMessage> response) {
                Log.d("++Connection++","onResponse AddNonOrkoDoctor") ;
                AddNonOrkoDoctorSuccessMessage addNonOrkoDoctorSuccessMessage = response.body() ;
                addNonOrkoDoctorRequestComplete.onRequestComplete(addNonOrkoDoctorSuccessMessage);
            }

            @Override
            public void onFailure(Call<AddNonOrkoDoctorSuccessMessage> call, Throwable t) {
                Log.d("++Connection++","ashe nai !!") ;
                addNonOrkoDoctorRequestComplete.onRequestError("something went wrong !");
            }
        });
    }
}
