package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.OrkoHomeRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by sakib on 11/15/2017.
 */

public class InvokePredictiveSearch {

    OrkoHomeRequestComplete orkoHomeRequestComplete ;
    RetrofitInterface retrofitInterface ;
    PublishSubject publishSubject ;

    public InvokePredictiveSearch(final Context context, final String query, final OrkoHomeRequestComplete requestComplete){

        this.orkoHomeRequestComplete = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        if(publishSubject == null){

            Log.d("++checking++", "in Null") ;
            Log.d("+++Searched VALUE+++", query) ;

            publishSubject = PublishSubject.create() ;
            publishSubject
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .switchMap(searchValue -> retrofitInterface.getPredictiveSearch(query).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()))
                    .subscribeWith(new DisposableObserver<PredictiveSearchRoot>() {

                        @Override
                        public void onNext(PredictiveSearchRoot predictiveSearchRoot) {

                            orkoHomeRequestComplete.onRequestComplete(predictiveSearchRoot);
                            Log.d("++stage++", "on Next") ;
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.d("++exception++", String.valueOf(e)) ;
                            orkoHomeRequestComplete.onRequestError("Something Went Wrong !");
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
        publishSubject.onNext(query);
    }
}
