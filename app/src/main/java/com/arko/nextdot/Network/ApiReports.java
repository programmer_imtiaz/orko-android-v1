package com.arko.nextdot.Network;

import android.content.Context;

import com.arko.nextdot.Interface.ReportsRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.MedicalRepots;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ASUS on 01-Nov-17.
 */

public class ApiReports {
    ReportsRequestComplete reportsRequestComplete;
    public ApiReports(Context context, final String token, final String patient_id, ReportsRequestComplete reportsRequestComplete){

        this.reportsRequestComplete=reportsRequestComplete;
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);

        Call<MedicalRepots> medicalRepotsCall=retrofitInterface.getMedicalReportsjson(token,patient_id);
        medicalRepotsCall.enqueue(new Callback<MedicalRepots>() {
            @Override
            public void onResponse(Call<MedicalRepots> call, Response<MedicalRepots> response) {

                if(response.isSuccessful()){
                    reportsRequestComplete.onSuccess(response.body().getData());

                   // Log.e("presrecy",prescriptionsList.size()+" size");

                }
            }

            @Override
            public void onFailure(Call<MedicalRepots> call, Throwable t) {
                reportsRequestComplete.onError("Something went wrong .. Please try again");
            }
        });
    }



}
