package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.SearchBySpecialityRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by sakib on 10/17/2017.
 */

public class InvokeSearchBySpeciality {

    RetrofitInterface retrofitInterface ;
    SearchBySpecialityRequestComplete searchBySpecialityRequest ;
    PublishSubject publishSubject ;

    public InvokeSearchBySpeciality(final Context context, final String end_point, final String token, final String speciality, final SearchBySpecialityRequestComplete requestComplete){

        this.searchBySpecialityRequest = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        Log.d("++Search Name++",speciality) ;
        if(publishSubject == null){
            publishSubject = PublishSubject.create() ;
            Log.d("+++checking++","null yes") ;

            publishSubject
                    .debounce(300, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .switchMap(searchValue -> retrofitInterface.getSearchBySpeciality(end_point, token, speciality).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()))
                    .subscribeWith(new DisposableObserver<SearchBySpecialityRoot>() {

                        @Override
                        public void onNext(SearchBySpecialityRoot searchBySpecialityRoot) {
                            Log.d("+++Stage+++", "OnNext") ;
                            searchBySpecialityRequest.searchBySpecialityRequestComplete(searchBySpecialityRoot);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            searchBySpecialityRequest.searchBySpecialityRequestError("Something is wrong ! ");
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        }
        publishSubject.onNext(speciality);
    }
}
