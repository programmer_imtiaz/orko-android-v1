package com.arko.nextdot.Network;

import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.DoctorProfileRoot;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.DoctorRatingByPatient;
import com.arko.nextdot.Model.RetrofitModel.DoctorSpecialityList.DoctorSpecialityList;
import com.arko.nextdot.Model.RetrofitModel.FbLoginUserProfile.PatientProfileRoot;
import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;
import com.arko.nextdot.Model.RetrofitModel.LogoutResponse.LogoutResponse;
import com.arko.nextdot.Model.RetrofitModel.MedicalRepots;
import com.arko.nextdot.Model.RetrofitModel.MyDoctors.MyDoctorsRoot;
import com.arko.nextdot.Model.RetrofitModel.Notification.NotificationList;
import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameRoot;
import com.arko.nextdot.Model.RetrofitModel.Prescription;
import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;
import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;
import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;
import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;
import com.arko.nextdot.Model.RetrofitModel.UserProfile;
import com.arko.nextdot.Retrofit.RetrofitModel.Appointments.DaywiseSchedule;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Dipto on 8/18/2017.
 */

public interface RetrofitInterface {

    @FormUrlEncoded
    @POST()
    Call<UserProfile> getUserProfile(@Url String url,
                                     @Field("id") String id,
                                     @Field("token") String token) ;

    @FormUrlEncoded
    @POST()
    Call<DoctorProfileRoot> getDoctorProfile(@Url String url,
                                             @Field("id") String id,
                                             @Field("token") String token,
                                             @Field("user_id") String user_id) ;


    @FormUrlEncoded
    @POST()
    Call<DoctorRatingByPatient> postDoctorRatingReview(@Url String url,
                                                       @Field("rating") int rating,
                                                       @Field("review") String review,
                                                       @Field("patient_id") String user_id,
                                                       @Field("token") String token) ;


    @FormUrlEncoded
    @POST()
    Call<PatientProfileRoot> postFbLogin(@Url String url,
                                         @Field("fb_id") String fb_id,
                                         @Field("fb_first_name") String fb_first_name,
                                         @Field("fb_last_name") String fb_last_name,
                                         @Field("fb_profile_pic") String fb_profile_pic,
                                         @Field("fb_gender") String fb_gender,
                                         @Field("role_id") String role_id) ;


    @FormUrlEncoded
    @POST(Constant.medicalReports)
    Call<MedicalRepots> getMedicalReportsjson(
            @Field("token") String token,
            @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.prescriptions)
    Call<Prescription> getPrescriptionsjson(
            @Field("token") String token,
            @Field("patient_id") String patient_id);


    @FormUrlEncoded
    @POST()
    Call<MyDoctorsRoot> getMyDoctors(@Url String url,
                                     @Field("token") String token) ;

    @FormUrlEncoded
    @POST("api/submit/doctor/search")
    Call<SearchDoctorResultRoot> getDoctorSearchResult(@Field("location") String localtion,
                                                       @Field("work_experiences") String work_experiences,
                                                       @Field("fees") String fees,
                                                       @Field("name_symptom_speciality") String symptom_speciality) ;

    @FormUrlEncoded
    @POST()
    Observable<SearchByNameRoot> getSearchData(@Url String url,
                                               @Field("token") String token,
                                               @Field("name_symptom_speciality") String name);


    @FormUrlEncoded
    @POST("api/doctor/search")
    Observable<PredictiveSearchRoot> getPredictiveSearch(@Field("name_symptom_speciality") String query);


    @GET
    Observable<PredictiveLocationRoot> getPredictiveLocation(@Url String url);


    @FormUrlEncoded
    @POST()
    Observable<SearchBySpecialityRoot> getSearchBySpeciality(@Url String url,
                                                             @Field("token") String token,
                                                             @Field("symptom_speciality") String speciality) ;

    @FormUrlEncoded
    @POST()
    Call<DoctorSpecialityList> getdoctorSpecialityList(@Url String url,
                                                       @Field("token") String token) ;


    @FormUrlEncoded
    @POST()
    Call<QuestionaryRoot> getQuestionaryResult(@Url String url,
                                               @Field("token") String token,
                                               @Field("appointment_id") String appointment_id,
                                               @Field("Shoulder1") String Shoulder1,
                                               @Field("Shoulder2") String Shoulder2,
                                               @Field("Shoulder3") String Shoulder3,
                                               @Field("Shoulder4") String Shoulder4,
                                               @Field("Shoulder5") String Shoulder5,
                                               @Field("Shoulder6") String Shoulder6,
                                               @Field("Shoulder7") String Shoulder7,
                                               @Field("Shoulder8") String Shoulder8,
                                               @Field("Shoulder9") String Shoulder9,
                                               @Field("Shoulder10") String Shoulder10,
                                               @Field("Elbow1") String Elbow1,
                                               @Field("Elbow2") String Elbow2,
                                               @Field("Elbow3") String Elbow3,
                                               @Field("Elbow4") String Elbow4,
                                               @Field("Elbow5") String Elbow5,
                                               @Field("Elbow6") String Elbow6,
                                               @Field("Elbow7") String Elbow7,
                                               @Field("Elbow8") String Elbow8,
                                               @Field("Elbow9") String Elbow9,
                                               @Field("Elbow10") String Elbow10,
                                               @Field("Elbow11") String Elbow11,
                                               @Field("Elbow12") String Elbow12,
                                               @Field("Elbow13") String Elbow13,
                                               @Field("knee1") String knee1,
                                               @Field("knee2") String knee2,
                                               @Field("knee3") String knee3,
                                               @Field("knee4") String knee4,
                                               @Field("knee5") String knee5,
                                               @Field("knee6") String knee6,
                                               @Field("knee7") String knee7,
                                               @Field("knee8") String knee8,
                                               @Field("knee9") String knee9,
                                               @Field("knee10") String knee10,
                                               @Field("knee11") String knee11,
                                               @Field("knee12") String knee12,
                                               @Field("knee13") String knee13,
                                               @Field("knee14") String knee14) ;

    @FormUrlEncoded
    @POST()
    Call<AddNonOrkoDoctorSuccessMessage> getAddNonOrkoDoctorMessage(@Url String url,
                                                                    @Field("token") String token,
                                                                    @Field("first_name") String first_name,
                                                                    @Field("last_name") String last_name,
                                                                    @Field("phone") String phone,
                                                                    @Field("speciality") String speciality,
                                                                    @Field("created_by") String created_by) ;

    @POST()
    Call<NotificationList> getNotification(@Url String url);

    @FormUrlEncoded
    @POST("api/doctor-schedule-by-date")
    Call<DaywiseSchedule> getScheduleByDate(@Field("token") String token, @Field("doctor_id") String doctor_id,
                                            @Field("date") String date);


    @FormUrlEncoded
    @POST(Constant.logout)
    Call<LogoutResponse> logout(@Field("user_id") String userID, @Field("device_id") String deviceID) ;


    @GET
    Call<UpcomingAppointmentsRoot> getUpcomingAppointments(@Url String url) ;

    @GET
    Call<HistoryAppointmentsRoot> getHistoryAppointment(@Url String url) ;
}
