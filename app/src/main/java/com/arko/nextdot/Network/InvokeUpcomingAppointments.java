package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.UpcomingAppointmentsRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 11/23/2017.
 */

public class InvokeUpcomingAppointments {

    RetrofitInterface retrofitInterface ;
    UpcomingAppointmentsRequestComplete upcomingAppointmentsRequestComplete ;


    public InvokeUpcomingAppointments(final Context context, final String token, final String patient_id, final UpcomingAppointmentsRequestComplete requestComplete){

        this.upcomingAppointmentsRequestComplete = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        String url = Constant.ROOT_URL+"api/appointments/upcoming?token="+token+"&patient_id="+patient_id ;
        Log.d("URL", url) ;

        Call<UpcomingAppointmentsRoot> call = retrofitInterface.getUpcomingAppointments(url) ;

        call.enqueue(new Callback<UpcomingAppointmentsRoot>() {
            @Override
            public void onResponse(Call<UpcomingAppointmentsRoot> call, Response<UpcomingAppointmentsRoot> response) {

                Log.d("+++CONNECTION+++", "asche");
                if(response.isSuccessful()){
                    Log.d("+++CONNECTION+++", "response successful");
                    UpcomingAppointmentsRoot upcomingAppointmentsRoot = response.body() ;
                    upcomingAppointmentsRequestComplete.UpcomingAppointmentsRequestComplete(upcomingAppointmentsRoot);
                }
                else{
                    upcomingAppointmentsRequestComplete.UpcomingAppointmentsRequestError("Sorry Server Problem !");
                }
            }

            @Override
            public void onFailure(Call<UpcomingAppointmentsRoot> call, Throwable t) {
                Log.d("+++CONNECTION+++", String.valueOf(t));
                upcomingAppointmentsRequestComplete.UpcomingAppointmentsRequestError("Something Went Wrong !");
            }
        });
    }
}
