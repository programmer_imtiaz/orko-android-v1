package com.arko.nextdot.Network;

import android.content.Context;

import com.arko.nextdot.Interface.PrescriptionRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.Prescription;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ASUS on 01-Nov-17.
 */

public class ApiPrescriptions {
    PrescriptionRequestComplete prescriptionRequestComplete;
    public ApiPrescriptions(Context context,final String token,final String patient_id,PrescriptionRequestComplete prescriptionRequestComplete){

        this.prescriptionRequestComplete=prescriptionRequestComplete;
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);

        Call<Prescription> medicalRepotsCall=retrofitInterface.getPrescriptionsjson(token,patient_id);
        medicalRepotsCall.enqueue(new Callback<Prescription>() {
            @Override
            public void onResponse(Call<Prescription> call, Response<Prescription> response) {

                if(response.isSuccessful()){
                    prescriptionRequestComplete.onSuccess(response.body().getData());

                   // Log.e("presrecy",prescriptionsList.size()+" size");

                }
            }

            @Override
            public void onFailure(Call<Prescription> call, Throwable t) {
                prescriptionRequestComplete.onError("Something went wrong .. Please try again");
            }
        });
    }



}
