package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.SearchDoctorResultRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 11/20/2017.
 */

public class InvokeSearchDoctorResult {

    RetrofitInterface retrofitInterface ;
    SearchDoctorResultRequestComplete searchDoctorResultRequestComplete ;

    public InvokeSearchDoctorResult(final Context context, final String exprience, final String speciality, final String fees, final String location, final SearchDoctorResultRequestComplete resultRequestComplete){

        this.searchDoctorResultRequestComplete = resultRequestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);
        Log.d("SearchDoctorResult","YES") ;
        Log.d("Search Value", speciality) ;

        Call<SearchDoctorResultRoot> call = retrofitInterface.getDoctorSearchResult(location, exprience, fees, speciality) ;

        call.enqueue(new Callback<SearchDoctorResultRoot>() {
            @Override
            public void onResponse(Call<SearchDoctorResultRoot> call, Response<SearchDoctorResultRoot> response) {
                Log.d("++CONNECTION++", "asche") ;
                if(response.isSuccessful()){

                    if(response.body().getResult().equals("Success")){

                        Log.d("++Result Success++", "Success") ;
                        SearchDoctorResultRoot searchDoctorResultRoot = response.body() ;
                        searchDoctorResultRequestComplete.searchDoctorResultRequestComplete(searchDoctorResultRoot);
                    }
                }
                else{
                    Log.d("++Result Success++", "NONE") ;
                    searchDoctorResultRequestComplete.searchDoctorResultRequestError("Something Went Wrong !");
                }
            }

            @Override
            public void onFailure(Call<SearchDoctorResultRoot> call, Throwable t) {
                searchDoctorResultRequestComplete.searchDoctorResultRequestError("Something Went Wrong !");
                Log.d("++CONNECTION++", "ashe nai") ;
                Log.d("++CONNECTION++", String.valueOf(t)) ;
            }
        });

    }
}
