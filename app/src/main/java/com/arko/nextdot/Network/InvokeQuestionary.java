package com.arko.nextdot.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.arko.nextdot.Interface.QuestionaryRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 10/29/2017.
 */

public class InvokeQuestionary {

    RetrofitInterface retrofitInterface ;
    QuestionaryRequestComplete questionaryRequestComplete ;
    SharedPreferences sharedPreferences ;

    public InvokeQuestionary(final Context context, final String token, final List<String>anslist, final QuestionaryRequestComplete requestComplete){


        this.questionaryRequestComplete = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;
        String url = Constant.ROOT_URL+"api/patient/survey_questionnaire" ;

        Call<QuestionaryRoot> call = retrofitInterface.getQuestionaryResult(url,token,anslist.get(0),
                anslist.get(1),anslist.get(2),anslist.get(3), anslist.get(4), anslist.get(5), anslist.get(6),
                anslist.get(7), anslist.get(8), anslist.get(9), anslist.get(10), anslist.get(11), anslist.get(12),
                anslist.get(13), anslist.get(14), anslist.get(15), anslist.get(16), anslist.get(17),
                anslist.get(18), anslist.get(19), anslist.get(20), anslist.get(21), anslist.get(22), anslist.get(23),
                anslist.get(24), anslist.get(25), anslist.get(26), anslist.get(27), anslist.get(28), anslist.get(29),
                anslist.get(30), anslist.get(31), anslist.get(32), anslist.get(33), anslist.get(34), anslist.get(35),
                anslist.get(36), anslist.get(37));

        call.enqueue(new Callback<QuestionaryRoot>() {
            @Override
            public void onResponse(Call<QuestionaryRoot> call, Response<QuestionaryRoot> response) {
                Log.d("++Connection++", "onResponse") ;
                QuestionaryRoot questionaryRoot = response.body() ;
                questionaryRequestComplete.questionaryOnRequestComplete(questionaryRoot);
            }

            @Override
            public void onFailure(Call<QuestionaryRoot> call, Throwable t) {
                Log.d("++Connection++", "onFailure") ;
                Log.d("++Connection++", String.valueOf(t)) ;
                questionaryRequestComplete.questionaryOnRequestError("Something Went Wrong !!");
            }
        });
    }
}
