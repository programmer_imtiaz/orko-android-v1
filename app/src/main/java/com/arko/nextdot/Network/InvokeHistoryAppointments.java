package com.arko.nextdot.Network;

import android.content.Context;
import android.util.Log;

import com.arko.nextdot.Interface.HistoryAppointmentsRequestComplete;
import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 11/26/2017.
 */

public class InvokeHistoryAppointments {

    RetrofitInterface retrofitInterface ;
    HistoryAppointmentsRequestComplete historyAppointmentsRequestComplete ;

    public InvokeHistoryAppointments(final Context context, final String token, final String patient_id, final HistoryAppointmentsRequestComplete requestComplete){

        this.historyAppointmentsRequestComplete = requestComplete ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        String url = Constant.ROOT_URL+"api/appointments/history?token="+token+"&patient_id="+patient_id ;
        Log.d("URL", url) ;

        Call<HistoryAppointmentsRoot> call = retrofitInterface.getHistoryAppointment(url);

        call.enqueue(new Callback<HistoryAppointmentsRoot>() {
            @Override
            public void onResponse(Call<HistoryAppointmentsRoot> call, Response<HistoryAppointmentsRoot> response) {
                Log.d("+++CONNECTION+++", "asche");
                if(response.isSuccessful()){
                    Log.d("+++CONNECTION+++", "response successful");
                    HistoryAppointmentsRoot historyAppointmentsRoot = response.body() ;
                    historyAppointmentsRequestComplete.onRequestComplete(historyAppointmentsRoot);
                }
                else{
                    historyAppointmentsRequestComplete.onRequestError("Sorry Server Problem !");
                }
            }

            @Override
            public void onFailure(Call<HistoryAppointmentsRoot> call, Throwable t) {

            }
        });
    }
}
