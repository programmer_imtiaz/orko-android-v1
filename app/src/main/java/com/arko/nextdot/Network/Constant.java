package com.arko.nextdot.Network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.List;

/**
 * Created by sakib on 8/10/2017.
 */

public class Constant {

    private Context context ;

    public Constant(Context context){
        this.context = context ;
    }


    public static final String ROOT_URL = "http://orko.azurewebsites.net/" ;
    //public static final String ROOT_URL = "http://182.160.109.132/" ;

    public static final String user_type = "patient" ;


    //public static final String logIn="login";
    public static final String logout = "api/logout";
//    public static final String APPLICATION_ID="com.nextdot.firebasesample";
    public static final String notification_list = "api/get_notification_by_user_id/";

    public static final String medicalReports = "api/patient/medical_reports";
    public static final String prescriptions = "api/patient/prescriptions";

    public static final String key = "AIzaSyCAZ5roTF2faz_wdo4tcWaOlSUHG8vS6Is" ;
    public static final String location_link = "https://maps.googleapis.com/maps/api/place/autocomplete/json?" ;


    public static final String MyPREFERENCES = "userlogin" ;
    public static final String token = "token" ;
    public static final String userlogin_flag = "userlogin_flag" ;
    public static final String user_name = "user_name";
    public static final String user_id = "user_id";
    public static final String first_name = "firstname";
    public static final String last_name = "lastname";
    public static final String user_pro_pic = "propic";
    public static final String email = "emailKey";
    public static final String facebook_login = "facebook_login";
    public static final String fb_req_login = "fb_req_login" ;
    public static final String header_name = "header_name" ;
    public static final String age_string = "age_string" ;
    public static final String district = "district" ;
    public static final String district_id = "district_id" ;
    public static final String upazilla = "upazilla" ;
    public static final String upazilla_id = "upazilla_id" ;
    public static final String profession = "profession" ;
    public static final String main_problem = "main_problem" ;
    public static final String phone = "phone" ;
    public static final String gender = "gender" ;
    public static final String dob = "dob" ;
    public static final String birthday = "birthday" ;
    public static final String searchbyspeciality = "searchbyspeciality" ;
    public static final String FCM_TOKEN = "FCMToken";

    public static final String[] district_array = { "Choose Your District",
            "Dhaka",
            "Faridpur",
            "Gazipur",
            "Gopalganj",
            "Jamalpur",
            "Kishoreganj",
            "Madaripur",
            "Manikganj",
            "Munshiganj",
            "Mymensingh",
            "Narayanganj",
            "Narsingdi",
            "Netrokona",
            "Rajbari",
            "Shariatpur",
            "Sherpur",
            "Tangail",
            "Bogra",
            "Joypurhat",
            "Naogaon",
            "Natore",
            "Nawabganj",
            "Pabna",
            "Rajshahi",
            "Sirajgonj",
            "Dinajpur",
            "Gaibandha",
            "Kurigram",
            "Lalmonirhat",
            "Nilphamari",
            "Panchagarh",
            "Rangpur",
            "Thakurgaon",
            "Barguna",
            "Barisal",
            "Bhola",
            "Jhalokati",
            "Patuakhali",
            "Pirojpur",
            "Bandarban",
            "Brahmanbaria",
            "Chandpur",
            "Chittagong",
            "Comilla",
            "Cox's Bazar",
            "Feni",
            "Khagrachari",
            "Lakshmipur",
            "Noakhali",
            "Rangamati",
            "Habiganj",
            "Maulvibazar",
            "Sunamganj",
            "Sylhet",
            "Bagerhat",
            "Chuadanga",
            "Jessore",
            "Jhenaidah",
            "Khulna",
            "Kushtia",
            "Magura",
            "Meherpur",
            "Narail",
            "Satkhira"
    } ;

    public static final String[] blood_group_array = { "Choose Your Blood Group",
            "A+",
            "A-",
            "B+",
            "B-",
            "AB+",
            "AB-",
            "O+",
            "O-"
    } ;

    public static List<String> list ;


    public int isNetworkActive(){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo() ;
        if(networkInfo != null){
            return 0 ;
        }
        else{
            return 1 ;
        }

    }

}
