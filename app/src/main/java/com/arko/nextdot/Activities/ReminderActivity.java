package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.Adapters.ReminderAdapter;
import com.arko.nextdot.Model.ReminderModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ReminderActivity extends ToolbarBaseActivity {

    LinearLayoutManager layoutManager;
    TextView reminder_date ;
    ImageView btn_next, btn_back ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        toolbarwithoutdrawer("Reminder");
        initilization();

        RecyclerView reminder_recyclerView = (RecyclerView) findViewById(R.id.reminder_recyler);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ReminderAdapter myAdapter = new ReminderAdapter(this, getData()) ;
        reminder_recyclerView.setAdapter(myAdapter);
        reminder_recyclerView.setLayoutManager(layoutManager);

//        Calendar c = Calendar.getInstance() ;
//        Log.d("date :", String.valueOf(c.getTime())) ;
//        c.get(Calendar.MO)
    }

    private void initilization(){
        reminder_date = (TextView) findViewById(R.id.reminder_date);
        btn_next = (ImageView) findViewById(R.id.rght_icon);
        btn_back = (ImageView) findViewById(R.id.left_icon);
    }

    public static List<ReminderModel> getData(){

        List<ReminderModel> list = new ArrayList<>() ;
        String[] doc_name = {"Consult With Doctor Arabi", "Consult With Doctor Imtiaz", "Consult With Doctor samsul", "Consult With Doctor dipto",
        "Consult With Doctor Mahbub", "Consult With Doctor Mir", "Consult With Doctor Rocky", "Consult With Doctor Maruf", "Consult With Doctor Anjan"} ;

        for(int i = 0 ; i < doc_name.length ; i++){
            ReminderModel reminderModel = new ReminderModel() ;
            reminderModel.setConsult_doc(doc_name[i]);
            list.add(reminderModel);
        }
        return list ;
    }
}
