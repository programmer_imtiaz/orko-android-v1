package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.arko.nextdot.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class NearbyDiagnosticsActivity extends ToolbarBaseActivity implements OnMapReadyCallback {


    Button nearby_btn ;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_diagnostics);
        toolbarwithoutdrawer("Nearby Diagnostics");
        initilization();
        nearby_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Development Under Process !", Toast.LENGTH_SHORT).show();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nearby_map);
        mapFragment.getMapAsync(this);
    }

    private void initilization(){
        nearby_btn = (Button) findViewById(R.id.btn_nearby_diagnostics);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        LatLng apollo = new LatLng(23.810103, 90.431047);
        mMap.addMarker(new MarkerOptions().position(apollo).title("Apollo Hospital"));

        LatLng united = new LatLng(23.804855, 90.415557);
        mMap.addMarker(new MarkerOptions().position(united).title("United Hospital"));

        LatLng labaid = new LatLng(23.741704, 90.383213);
        mMap.addMarker(new MarkerOptions().position(labaid).title("Labaid Hospital"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(apollo, 12f));

    }
}
