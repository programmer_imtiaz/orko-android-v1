package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.DoctorSearchResultAdapter;
import com.arko.nextdot.Fragment.SearchDoctorFillterFragment;
import com.arko.nextdot.Interface.SearchDoctorResultView;
import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultProfile;
import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.Presenter.SearchDoctorResultPresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 10/19/2017.
 */

public class SearchDoctorResultActivity extends ToolbarBaseActivity implements SearchDoctorResultView, SearchDoctorFillterFragment.DataPassToActivity, DoctorSearchResultAdapter.ClickListener {

    @BindView(R.id.doctor_search_result_recyler)
    RecyclerView doctorSearchResultRecyler;
    SharedPreferences sharedPreferences;
    String speciality_string = "", exp_string = "", fees_string = "", district_string = "", thana_string = "", location_string = "", token;
    LinearLayoutManager layoutManager;
    DoctorSearchResultAdapter doctorSearchResultAdapter;
    List<SearchDoctorResultProfile> list;
    ArrayList<String> intent_list;
    RetrofitInterface retrofitInterface;
    String end_point = "";
    SearchDoctorResultPresenter presenter;
    SearchDoctorFillterFragment searchDoctorFillterFragment;
    @BindView(R.id.progressbar_doctor_search_layout)
    ProgressBar progressbarDoctorSearchLayout;
    @BindView(R.id.progress_layout)
    LinearLayout progressLayout;
    @BindView(R.id.search_result_no_result_found)
    TextView searchResultNoResultFound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_search_result);
        ButterKnife.bind(this);
        Intent intent = this.getIntent();
        toolbarwithoutdrawer("Search Result");
        intent_list = intent.getStringArrayListExtra("key");
        initilization();
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        presenter = new SearchDoctorResultPresenter(this);

        /*intent_list.add(exp_string);
        intent_list.add(speciality_string);
        intent_list.add(fees_string) ;
        intent_list.add(location_string) ;*/

        exp_string = intent_list.get(0);
        speciality_string = intent_list.get(1);
        fees_string = intent_list.get(2);
        location_string = intent_list.get(3);

        Log.d("exp_string", exp_string + "a");
        Log.d("speciality_string", speciality_string);
        Log.d("fees_string", fees_string + "a");
        Log.d("location_string", location_string + "a");

        presenter.getDoctorSearchResult(exp_string, speciality_string, fees_string, location_string);

    }

    private void initilization() {
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        doctorSearchResultRecyler.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_doctor_fillter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_fillter_search_doctor: {
                FragmentManager fragmentManager = getSupportFragmentManager();
                searchDoctorFillterFragment = SearchDoctorFillterFragment.newInstance(speciality_string);
                searchDoctorFillterFragment.onAttach(SearchDoctorResultActivity.this);
                searchDoctorFillterFragment.show(fragmentManager, "Fillter Dialouge Fragment");
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSearchDoctorResult(SearchDoctorResultRoot searchDoctorResultRoot) {

        Log.d("AdapterSet", "YES");
        list.clear();
        list = searchDoctorResultRoot.getData();
        if(list.isEmpty()){
            searchResultNoResultFound.setVisibility(View.VISIBLE);
        }
        else{
            searchResultNoResultFound.setVisibility(View.GONE);
            doctorSearchResultAdapter = new DoctorSearchResultAdapter(getApplicationContext(), list);
            doctorSearchResultAdapter.setClicklistner(this);
            doctorSearchResultRecyler.setAdapter(doctorSearchResultAdapter);
        }
    }

    @Override
    public void startLoading() {
        progressLayout.setVisibility(View.VISIBLE);
        progressbarDoctorSearchLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressLayout.setVisibility(View.GONE);
        progressbarDoctorSearchLayout.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void dataPassing(String exp_string, String speciality_string, String fees_string, String location) {

        Log.d("++speciality_string++", speciality_string);
        Log.d("++location++", location);
        Log.d("+++exp_string+++", exp_string);

        /*progressLayout.setVisibility(View.GONE);
        progressbarDoctorSearchLayout.setVisibility(View.GONE);*/

        presenter.getDoctorSearchResult(exp_string, speciality_string, fees_string, location);
    }

    @Override
    public void ItemClicked(View view, int position) {

        Intent intent = new Intent(SearchDoctorResultActivity.this, DoctorProfileActivity.class);
        String doctor_id = String.valueOf(list.get(position).getUserId());
        intent.putExtra("id", doctor_id);
        startActivity(intent);
    }
}
