package com.arko.nextdot.Activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.arko.nextdot.Model.ItemModel;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.Constants;
import com.arko.nextdot.Utils.PreferenceManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class UploadPhotoActivity extends AppCompatActivity {

    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
    private static final int PICK_IMAGE = 1;

    TextView txt, title;
    ImageButton photo;
    public Bitmap savedImage;
    public Bitmap resizedImage;
    public String encoded2, captionText, user_id, product_id, product_name;
    public static String encoded="";
    public Button postButton;
    public Spinner productSpinner;
    public TextView productNameView;
    public EditText productCaption;
    private ScrollView scrollViewLayout;
    public List<ItemModel> items;
    public String[] productNames;
    public String[] productID;
    public ArrayAdapter<String> adapter;
    public Activity activity = UploadPhotoActivity.this;
    public Context context = this;
    public LinearLayout spinnerLayout, photoContainer;
    private ImageView imageView;
    public List<String> instituteList;
    public int arrayPosition;
    public String url = "";
    public String reportTypeURL = Constants.baseUrl + "api/medical_report_types";
    private String appointmentID, patientID, doctorID, userToken;
    private String docType, documentName;
    private String reportType;
    private String ImageDecode;
    private LinearLayout reportSelectionLayout, circularLoaderLayout;

    public static void startUploadPhotoFromLocation(int[] startingLocation, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, UploadPhotoActivity.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        startingActivity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_photo);
        txt = (TextView) findViewById(R.id.txt);
        txt.setFocusable(true);
        txt.setFocusableInTouchMode(true);
        txt.requestFocus();
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        Intent intent = UploadPhotoActivity.this.getIntent();
        appointmentID = intent.getStringExtra("appointment_id");
        docType = intent.getStringExtra("doc_type");
        patientID = intent.getStringExtra("patient_id");
        userToken = sharedPreferences.getString(Constant.token, "");
        doctorID = intent.getStringExtra("doctor_id");

        scrollViewLayout = (ScrollView) findViewById(R.id.scrollViewLayout);

        photoContainer = (LinearLayout) findViewById(R.id.imageLayout);

        reportSelectionLayout = (LinearLayout) findViewById(R.id.reportSelectionLayout);

        circularLoaderLayout = (LinearLayout) findViewById(R.id.circularLoader);


        productSpinner = (Spinner) findViewById(R.id.product_spinner);
        spinnerLayout = (LinearLayout) findViewById(R.id.spinnerLayout);

        if (docType.equals("1")) {
            documentName = "prescription";
            url = Constants.baseUrl + "api/patient/add/prescription";
            reportSelectionLayout.setVisibility(View.GONE);

        } else if (docType.equals("2")) {
            documentName = "report";
            url = Constants.baseUrl + "api/patient/add/report";

            //call product list api
            new productListAPI(reportTypeURL, activity, context, userToken).execute();

            reportSelectionLayout.setVisibility(View.VISIBLE);
        }

        imageView = (ImageView) findViewById(R.id.imageView1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });

        Log.d("PRES_URL", "onCreate: "+url);






        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                product_id = productID[position];

            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

                product_id = productID[0];

            }
        });


        postButton = (Button) findViewById(R.id.btn_post);
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // call PostAPI
                Log.d("credentials", "captionText: " + captionText);
                if(!encoded.equals("")) {
                    new PostAPI(patientID, product_id, encoded, appointmentID, userToken).execute();
                } else {
                    Toast.makeText(UploadPhotoActivity.this, "Please select a photo.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void selectImageFromGallery() {
        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), PICK_IMAGE);
        Log.d("static", "***************************_________-select hoise-___________*********************************");

    }


    public static String getRealFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if
                (ContentResolver.SCHEME_CONTENT.equals(scheme)) {

            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {

                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Log.d("static1", "on activity result ************************************************************");
//
//        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK
//                && null != data) {
//            Uri selectedImage = data.getData();
//
//
//            String picturePath = getRealFilePath(UploadPhotoActivity.this, selectedImage);
//            Log.d("if_body", "************************************" + picturePath);
//
//            try {
//
//                savedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
//
//
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                savedImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//
//                encoded2 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//
//                byte[] imageAsBytes;
//                imageAsBytes = Base64.decode(encoded2.getBytes(), 0);
//
//                Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
//                resizedImage = Bitmap.createScaledBitmap(b, 800, 600, false);
//
//                BitmapDrawable background = new BitmapDrawable(resizedImage);
//                photoContainer.setBackgroundDrawable(background);
//
//                ByteArrayOutputStream byteArrayOutputStream1 = new ByteArrayOutputStream();
//                resizedImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream1);
//                byte[] byteArray1 = byteArrayOutputStream1.toByteArray();
//
//                encoded = Base64.encodeToString(byteArray1, Base64.DEFAULT);
//
//
////                photo.setImageBitmap(savedImage);
//
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } else
//            Log.d("static2", "else e dhukse ************************************");
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == PICK_IMAGE && resultCode == RESULT_OK
                    && null != data) {

                Uri URI = data.getData();

                InputStream imageStream = null;
                try {
                    imageStream = this.getContentResolver().openInputStream(URI);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);

                imageView.setBackgroundColor(Color.parseColor("#E2E2E2"));
                imageView.setImageBitmap(yourSelectedImage);

                encodeTobase64(yourSelectedImage);


            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }

    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

        encoded = imageEncoded;

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }


    public class productListAPI extends AsyncTask<String, String, String> {

        String url;
        Activity activity;
        Context context;
        String user_token;

        public productListAPI(String url, Activity activity, Context context, String user_token) {
            //set context variables if required
            this.url = url;
            this.activity = activity;
            this.context = context;
            this.user_token = user_token;
            Log.d("Track", "Ami asi ekhane");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            scrollViewLayout.setVisibility(View.GONE);

            Log.d("Track2", "Ami ekhane asi");

        }


        @Override
        protected String doInBackground(String... params) {

//            String urlString = params[0]; // URL to call

            String resultToDisplay = "";

            Log.d("Track", "Ekhane Ashchi");
//*****************************************************************
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(reportTypeURL);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("token", user_token));


// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }


        @Override
        protected void onPostExecute(String result) {
            //Update the UI
            super.onPostExecute(result);

            try {
                Log.d("jsonData", "+++++++++" + result);
                JSONObject jsonObj = new JSONObject(result);
                JSONArray jsonArray = jsonObj.getJSONArray("data");

                productNames = new String[jsonArray.length()];
                productID = new String[jsonArray.length()];
                items = new ArrayList<ItemModel>();


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject productListJson = jsonArray.getJSONObject(i);
                    ItemModel item = new ItemModel();
                    item.setId(Long.parseLong(productListJson.getString("id")));
                    item.setProductTitle(productListJson.getString("type"));
                    items.add(item);

                    productNames[i] = item.getProductTitle();
                    productID[i] = String.valueOf(item.getId());
                }


                adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, productNames);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                adapter.notifyDataSetChanged();

                productSpinner.setAdapter(adapter);

                scrollViewLayout.setVisibility(View.VISIBLE);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class PostAPI extends AsyncTask<String, String, String> {

        String user_id, product_id, encoded, appointment_id, user_token;


        public PostAPI(String user_id, String product_id, String encoded, String appointment_id, String user_token) {
            //set context variables if required
            this.user_id = user_id;
            this.product_id = product_id;
            this.encoded = encoded;
            this.appointment_id = appointment_id;
            this.user_token = user_token;
            Log.d("Track", "Ami asi ekhane");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            scrollViewLayout.setVisibility(View.GONE);
            circularLoaderLayout.setVisibility(View.VISIBLE);

            Log.d("Track2", "Ami ekhane asi");

        }


        @Override
        protected String doInBackground(String... params) {

//            String urlString = params[0]; // URL to call

            String resultToDisplay = "";

            Log.d("Track", "Ekhane Ashchi"+"URL: "+url);
//*****************************************************************
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("patient_id", user_id));
            nameValuePairs.add(new BasicNameValuePair("appointment_id", appointment_id));
            nameValuePairs.add(new BasicNameValuePair("image", encoded));
            nameValuePairs.add(new BasicNameValuePair("token", user_token));
            Log.d("UPLOAD_TOKEN", "doInBackground: token: "+user_token);
            Log.d("UPLOAD_TAG", "doInBackground: "+"patientID: "+patientID+" appointmentID: "+appointment_id+" token: "+user_token);
//            nameValuePairs.add(new BasicNameValuePair("report_type", product_id));


// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }


        @Override
        protected void onPostExecute(String result) {
            //Update the UI
            super.onPostExecute(result);

            try {
                Log.d("jsonDataUpload", "+++++++++" + result);

                circularLoaderLayout.setVisibility(View.GONE);

                Toast.makeText(UploadPhotoActivity.this, "New " + documentName + " uploaded.", Toast.LENGTH_LONG).show();

                finish();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom, menu);
        super.onCreateOptionsMenu(menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
