package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arko.nextdot.Interface.AddNonOrkoView;
import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;
import com.arko.nextdot.Model.RetrofitModel.DoctorSpecialityList.DoctorSpecialityList;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.Presenter.AddNonOrkoDoctorPresenter;
import com.arko.nextdot.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 10/30/2017.
 */

public class AddNonOrkoDoctorActivity extends ToolbarBaseActivity implements AddNonOrkoView {

    @BindView(R.id.non_doctor_first_name)
    EditText nonDoctorFirstName;
    @BindView(R.id.add_non_doctor_first_name)
    TextInputLayout addNonDoctorFirstName;
    @BindView(R.id.non_doctor_last_name)
    EditText nonDoctorLastName;
    @BindView(R.id.add_non_doctor_last_name)
    TextInputLayout addNonDoctorLastName;
    @BindView(R.id.non_doctor_phone_number)
    EditText nonDoctorPhoneNumber;
    @BindView(R.id.add_non_doctor_phone_number)
    TextInputLayout addNonDoctorPhoneNumber;
    @BindView(R.id.add_non_orko_doctor_submit_btn)
    Button addNonOrkoDoctorSubmitBtn;

    String firstName="", lastName="", speciality="", phoneNumber="", patient_id="", field_flag = "";
    @BindView(R.id.spinner_add_non_orko_speciality)
    MaterialSpinner spinnerAddNonOrkoSpeciality;

    String end_point = "api/doctor/specialities", token, url ;
    RetrofitInterface retrofitInterface ;
    SharedPreferences sharedPreferences ;
    List<String> specialist_list, speciality_list_new, sendInfoList ;
    ProgressDialog progressDialog ;
    AddNonOrkoDoctorPresenter presenter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_non_orko_doctor);
        ButterKnife.bind(this);
        toolbarwithoutdrawer("Add Non Orko Doctor");

        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        specialist_list = new ArrayList<String>() ;
        speciality_list_new = new ArrayList<String>() ;
        sendInfoList = new ArrayList<String>() ;
        specialist_list.clear();
        speciality_list_new.clear();
        token = sharedPreferences.getString(Constant.token, "") ;
        patient_id = sharedPreferences.getString(Constant.user_id, "") ;
        presenter = new AddNonOrkoDoctorPresenter(this) ;

        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;
        url = Constant.ROOT_URL+end_point ;
        Log.d("+++URL+++", url) ;
        getDoctorSpecialityList();
        specialitySpinnerOnClick();

    }

    @OnClick(R.id.add_non_orko_doctor_submit_btn)
    public void onAddNonOrkoSubmitClicked() {

        sendInfoList.clear();
        field_flag = "0" ;
        firstName = nonDoctorFirstName.getText().toString().trim();
        lastName = nonDoctorLastName.getText().toString().trim();
        phoneNumber = nonDoctorPhoneNumber.getText().toString().trim();

        if(firstName.equals("")){
            nonDoctorFirstName.setError("This field can not be blank");
            field_flag = "0" ;
        }
        else{
            field_flag = "1" ;
        }
        if(lastName.equals("")){
            nonDoctorLastName.setError("This field can not be blank");
            field_flag = "0" ;
        }
        else{
            field_flag = "1" ;
        }
        if(phoneNumber.equals("")){
            nonDoctorPhoneNumber.setError("This field can not be blank");
            field_flag = "0" ;
        }
        else{
            field_flag = "1" ;
        }
        if(speciality.equals("")){
            field_flag = "0" ;
        }
        else{
            field_flag = "1" ;
        }

        if(field_flag.equals("1")){

            sendInfoList.add(token);
            sendInfoList.add(firstName);
            sendInfoList.add(lastName);
            sendInfoList.add(phoneNumber);
            sendInfoList.add(speciality);
            sendInfoList.add(patient_id) ;
            presenter.getAddNonOrkoDoctorMsg(sendInfoList);
        }
    }


    private void getDoctorSpecialityList(){

        Call<DoctorSpecialityList> call = retrofitInterface.getdoctorSpecialityList(url, token) ;
        setProgressDialog();
        call.enqueue(new Callback<DoctorSpecialityList>() {

            @Override
            public void onResponse(Call<DoctorSpecialityList> call, Response<DoctorSpecialityList> response) {

                progressDialog.dismiss();
                Log.d("++CONNECTION++", "Asche !!") ;
                if(response.body().getResult().equals("Success")){
                    if(response.body().getSpecialityList() != null){
                        speciality_list_new = response.body().getSpecialityList() ;
                        Log.d("+++speciality+++", speciality_list_new.get(0)) ;
                        addSpecialitytoSpinner();
                    }
                }
            }

            @Override
            public void onFailure(Call<DoctorSpecialityList> call, Throwable t) {
                Log.d("++CONNECTION++", "Ashe nai !") ;
            }
        });

    }


    private void addSpecialitytoSpinner(){
        specialist_list = new ArrayList<String>() ;
        specialist_list.add("Search by SpecialityClass");
        for(int i = 0 ; i < speciality_list_new.size() ; i++){

            specialist_list.add(speciality_list_new.get(i));

            Log.d("+++speciality loop++", specialist_list.get(i)) ;
        }
        spinnerAddNonOrkoSpeciality.setItems(specialist_list);
    }


    private void specialitySpinnerOnClick(){

        spinnerAddNonOrkoSpeciality.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position == 0){
                    speciality = "" ;
                    Log.d("+++Specilaity+++", speciality+"aa") ;
                }
                else{
                    speciality = specialist_list.get(position) ;
                    Log.d("+++Specilaity+++", speciality+"aa") ;
                }
            }
        });
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(AddNonOrkoDoctorActivity.this) ;
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void showAddNonOrkoSuccessMsg(AddNonOrkoDoctorSuccessMessage addNonOrkoDoctorSuccessMessage) {

        if(addNonOrkoDoctorSuccessMessage.getResult().equals("Success")){
            Toast.makeText(this, addNonOrkoDoctorSuccessMessage.getMsg(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
