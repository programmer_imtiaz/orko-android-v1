package com.arko.nextdot.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;


public class SplashActivity extends Activity {

    String check_login_is_active, check_fb_login_is_active, check_fb_require;
    private int flag = 0, fb_login = 0,  fb_require_login = 0 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE) ;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        final SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

//        final ImageView imgView=(ImageView) findViewById(R.id.gif_image);
//        Glide.with(this)
//                .load("android.resource://" + getPackageName() + "/" + R.drawable.loading)
//                .asGif()
//                .animate(R.drawable.loading)
//                .crossFade()
//                .fitCenter()
//                .into(imgView);

        Thread mythread = new Thread(){
            @Override
            public void run() {
                try {

                    check_login_is_active = sharedPreferences.getString(Constant.userlogin_flag, "") ;
                    check_fb_login_is_active = sharedPreferences.getString(Constant.facebook_login, "") ;
                    check_fb_require = sharedPreferences.getString(Constant.fb_req_login, "") ;

                    if(check_login_is_active.equals("1")){
                        Log.d("check_login_active", "YESSS") ;
                        flag = 1 ;
                    }

                    if(check_fb_login_is_active.equals("1")){
                        Log.d("check_fb_login_active", "YESSS") ;
                        fb_login = 1 ;

                        if(check_fb_require.equals("1")){
                            Log.d("check_fb_require", "YESSS") ;
                            fb_require_login = 1 ;
                        }
                    }

                    sleep(1500);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {

                    if(flag == 1){
                        Log.d("++LOGIN++", "1") ;
                        Intent intent = new Intent(getApplicationContext(), OrkoHomeActivity.class) ;
                        startActivity(intent);
                        finish();
                    }
                    else if(fb_login == 1  && fb_require_login == 0){
                        Log.d("++LOGIN++", "2") ;
                        Intent intent = new Intent(getApplicationContext(), FbLoginEditProfile.class) ;
                        startActivity(intent);
                        finish();
                    }
                    else if(fb_login == 1  && fb_require_login == 1){
                        Log.d("++LOGIN++", "3") ;
                        Intent intent = new Intent(getApplicationContext(), Appointments.class) ;
                        startActivity(intent);
                        finish();
                    }

                    /*else if(fb_login == 1){
                        Log.d("++LOGIN++", "5") ;
                        Intent intent = new Intent(getApplicationContext(), FbLoginEditProfile.class) ;
                        startActivity(intent);
                        finish();
                    }*/
                    else if(flag == 0 && fb_login == 0){

                        Log.d("++LOGIN++", "4") ;
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class) ;
                        startActivity(intent);
                        finish();
                    }

                    /*Intent intent = new Intent(getApplicationContext(), HomeActivity.class) ;
                    startActivity(intent);*/
                }
            }
        };

        mythread.start();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
