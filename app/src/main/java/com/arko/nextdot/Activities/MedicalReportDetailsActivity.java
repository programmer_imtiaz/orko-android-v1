package com.arko.nextdot.Activities;

import android.os.Bundle;

import com.arko.nextdot.R;


/**
 * Created by sakib on 6/11/2017.
 */

public class MedicalReportDetailsActivity extends ToolbarBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_report_detail);
        toolbarwithoutdrawer("Medical Report Details");
    }
}
