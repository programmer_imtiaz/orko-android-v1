package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.arko.nextdot.Adapters.TestReportAdapter;
import com.arko.nextdot.Model.TestReportModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sakib on 8/2/2017.
 */

public class PatientsReportsActivity extends ToolbarBaseActivity {

    RecyclerView test_recyler;
    TestReportAdapter testReportAdapter;
    Button test_report_view_btn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_report);

        toolbarwithoutdrawer("Medical Reports");

        test_recyler = (RecyclerView) findViewById(R.id.patient_report_recyler) ;
        RecyclerView.LayoutManager gridlayoutmanager = new GridLayoutManager(getApplicationContext(), 2);
        test_recyler.setLayoutManager(gridlayoutmanager);
        TestReportAdapter testReportAdapter = new TestReportAdapter(getApplicationContext(), getData()) ;
        test_recyler.setAdapter(testReportAdapter);
    }

    public static List<TestReportModel> getData(){

        List<TestReportModel> list = new ArrayList<>() ;
        String[] test_name = {"Blood Test", "Urine Test", "Blood Test", "Urine Test",
                "Blood Test", "Blood Test", "Urine Test", "Blood Test"} ;

        for(int i = 0 ; i < test_name.length ; i++){
            TestReportModel testReportModel = new TestReportModel() ;
            testReportModel.setTestname(test_name[i]);
            list.add(testReportModel);
        }
        return list ;//
    }
}
