package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arko.nextdot.Adapters.ReportsShowAdapter;
import com.arko.nextdot.Fragment.FragmentShowReportNprescriptionDialog;
import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.R;
import com.arko.nextdot.ShadowTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportsShowActivity extends ToolbarBaseActivity implements ViewPager.OnPageChangeListener, ReportsShowAdapter.ShowReport {

    @BindView(R.id.reportShowDotsLayout)
    LinearLayout reportDotLayout;
    /*@BindView(R.id.prescriptionShowSkip_btn)
    Button prescriptionShowSkipBtn;
    @BindView(R.id.prescriptionShowNext_btn)
    Button prescriptionShowNextBtn;*/


    @BindView(R.id.reportBackimageview)
    ImageView reportBackimageview;
    @BindView(R.id.reportShowviewpager)
    ViewPager reportShowviewpager;
    @BindView(R.id.reportlinearLayout3)
    LinearLayout linearLayout3;
    private ReportsShowAdapter reportsShowAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private int[] img;
    private ImageView[] dots;
    private int dotsCount;

    List<MedicalReportsItem> reportsItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_show);
        ButterKnife.bind(this);
        reportsShowAdapter = new ReportsShowAdapter(getApplicationContext(), this);
        reportShowviewpager.setOnPageChangeListener(this);



        reportsItems=new ArrayList<>();
        reportsItems = getIntent().getParcelableArrayListExtra("datalist");
        for (int i = 0; i < reportsItems.size(); i++) {
            reportsShowAdapter.addCardItem(reportsItems.get(i));
        }



        // img = new int[]{R.drawable.blue_wlakthrough_background, R.drawable.green_walkthrough_background, R.drawable.purple_walkthrough_background};


        mCardShadowTransformer = new ShadowTransformer(reportShowviewpager, reportsShowAdapter);

        reportShowviewpager.setAdapter(reportsShowAdapter);
        reportShowviewpager.setPageTransformer(false, mCardShadowTransformer);
        reportShowviewpager.setOffscreenPageLimit(3);
        int tmp = reportShowviewpager.getCurrentItem();

        Log.d("TAGGGG : ", String.valueOf(tmp));
        setUiPageViewController();
        reportBackimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
/*
        prescriptionShowNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prescriptionShowviewpager.getCurrentItem() == img.length - 1){
                    Intent intent = new Intent(getApplicationContext(), RegistrationFromDoctor.class) ;
                    startActivity(intent);
                }
                else{
                    prescriptionShowviewpager.setCurrentItem(prescriptionShowviewpager.getCurrentItem()+1, true);

                    if(prescriptionShowviewpager.getCurrentItem() == img.length - 1){
                        prescriptionShowNextBtn.setText("Finish");
                        Log.d("tag", "!!!!!!") ;
                    }
                }
            }
        });
*/

    }

    private void setUiPageViewController() {

        dotsCount = reportsShowAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.filled_ash_circle));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            reportDotLayout.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.filled_blue_circle));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.filled_ash_circle));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.filled_blue_circle));

   /*     if (position == (img.length - 1)) {
            prescriptionShowNextBtn.setText("Finish");
        } else {
            prescriptionShowNextBtn.setText("Next");
        }*/
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



    @Override
    public void showReport(String imageurl) {

        FragmentShowReportNprescriptionDialog fragmentShowReportNprescriptionDialog = new FragmentShowReportNprescriptionDialog();
        Bundle args = new Bundle();
        args.putString("imageurl", imageurl);
        fragmentShowReportNprescriptionDialog.setArguments(args);
        FragmentManager fragmentmanager = getSupportFragmentManager();
        fragmentShowReportNprescriptionDialog.show(fragmentmanager, "show dialog");
    }
}
