package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.arko.nextdot.Adapters.SearchByNameAdapter;
import com.arko.nextdot.Interface.SearchByNameView;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameProfile;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.SearchByNamePresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchByNameActivity extends AppCompatActivity implements SearchByNameView, SearchByNameAdapter.ClickListener {

    @BindView(R.id.searchview)
    SearchView searchview;
    @BindView(R.id.search_by_name_recyler)
    RecyclerView searchByNameRecyler;
    List<SearchByNameProfile> list;
    SearchByNamePresenter presenter;
    String end_point = "api/doctor/search_by_name", token;
    SearchByNameAdapter searchByNameAdapter;
    LinearLayoutManager layoutManager;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_name);
        ButterKnife.bind(this);
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "");
        Log.d("++TOKEN++", token);
        presenter = new SearchByNamePresenter(this);
        initilization();

        searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.length() < 1) {
                    searchByNameAdapter.clearList();
                }
                if (newText.length() >= 1) {
                    presenter.getSearchResult(end_point, token, newText);
                }
                return false;
            }
        });
    }

    @Override
    public void showSearchResult(SearchByNameRoot searchByNameRoot) {
        list = searchByNameRoot.getData();
        if (list != null) {
            searchByNameAdapter.setFilter(list);
            for (SearchByNameProfile searchByNameProfile : list) {
                Log.d("++name++", searchByNameProfile.getFirstName());
            }
        } else {
            searchByNameAdapter.clearList();
        }
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }


    public void initilization() {
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getAppContext(), LinearLayoutManager.VERTICAL, false);
        searchByNameAdapter = new SearchByNameAdapter(getAppContext(), list);
        searchByNameRecyler.setLayoutManager(layoutManager);
        searchByNameAdapter.setClickListener(this);
        searchByNameRecyler.setAdapter(searchByNameAdapter);
        searchByNameAdapter.notifyDataSetChanged();
    }

    @Override
    public void ItemClicked(View view, int position) {
        Intent intent = new Intent(getAppContext(), DoctorProfileActivity.class);
        Log.d("++DOCTOR_ID++", list.get(position).getUserId() + " doctor id");
        intent.putExtra("id", String.valueOf(list.get(position).getUserId()));
        startActivity(intent);
    }
}