package com.arko.nextdot.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.arko.nextdot.Fragment.PrescriptionFragment;
import com.arko.nextdot.Fragment.TestReportFragment;
import com.arko.nextdot.Fragment.TherapyFragment;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class MedicalHistoryPastActivity extends ToolbarBaseActivity {

    ViewPager viewPager ;
    TabLayout tabLayout ;
    Button present_btn, past_btn ;
    LinearLayout btn_layout, single_line ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_past);
        initilization();
        Intent intent = this.getIntent();
        String className = intent.getStringExtra("caller");

        //String className = getIntent().getStringExtra("caller");
        /*Class callerClass = null;
        try {
            callerClass = Class.forName(className);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(callerClass == AfterLoginHomeActivity.class){
            btn_layout.setVisibility(View.VISIBLE);
            single_line.setVisibility(View.VISIBLE);
        }
        else if(callerClass == MedicalHistoryPresentActivity.class){
            btn_layout.setVisibility(View.VISIBLE);
            single_line.setVisibility(View.VISIBLE);
        }
        else if(callerClass == DoctorActivity.class){
            Log.d("TAG : ", "ok ") ;
        }*/

        if(className.equals("AfterLoginHomeActivity")){
            btn_layout.setVisibility(View.VISIBLE);
            single_line.setVisibility(View.VISIBLE);
        }
        else if(className.equals("MedicalHistoryPresentActivity")){
            btn_layout.setVisibility(View.VISIBLE);
            single_line.setVisibility(View.VISIBLE);
        }
        else if(className.equals("DoctorActivity")){
            btn_layout.setVisibility(View.GONE);
            single_line.setVisibility(View.GONE);
        }

        toolbarwithoutdrawer("Medial Report / History");
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setDividerBetweenTabs();

        present_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                present_btn.setTextColor(getResources().getColor(R.color.your_selected_text_color));
                past_btn.setTextColor(getResources().getColor(R.color.your_unselected_text_color));
                Intent intent = new Intent(getApplicationContext(), MedicalHistoryPresentActivity.class);
                startActivity(intent);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter_Med_past viewPagerAdapter = new ViewPagerAdapter_Med_past(getSupportFragmentManager()) ;
        viewPagerAdapter.addFragment(new TestReportFragment(), "Test Report");
        viewPagerAdapter.addFragment(new TherapyFragment(), "Therapy");
        viewPagerAdapter.addFragment(new PrescriptionFragment(), "Prescription");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter_Med_past extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>() ;
        private final List<String> fragmentTitle = new ArrayList<>() ;

        public ViewPagerAdapter_Med_past(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position) ;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }


        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmentTitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position) ;
        }
    }

    private void initilization(){
        btn_layout = (LinearLayout) findViewById(R.id.med_btn_layout) ;
        single_line = (LinearLayout) findViewById(R.id.before_btn_single_line) ;
        tabLayout = (TabLayout) findViewById(R.id.med_past_tablayout);
        viewPager = (ViewPager) findViewById(R.id.med_past_view_pager) ;
        present_btn = (Button) findViewById(R.id.med_present_btn);
        past_btn = (Button) findViewById(R.id.med_past_btn) ;
    }

    private void setDividerBetweenTabs(){

        //divider between tabs
        LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);
        //divider between tabs
    }
}
