package com.arko.nextdot.Activities;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arko.nextdot.Adapters.ChooseApptTimeAdapter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class ChooseApptTimeActivity extends ToolbarBaseActivity {


    GridView gridView  ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_appt_time);
        toolbarwithoutdrawer("Choose Time");

        gridView = (GridView) findViewById(R.id.appt_time_schedule);
        ChooseApptTimeAdapter chooseApptTimeAdapter = new ChooseApptTimeAdapter(this, R.layout.item_choose_time, getdata()) ;
        gridView.setAdapter(chooseApptTimeAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "ok! got it",  Toast.LENGTH_SHORT).show(); ;
                dialogCartAction();
            }
        });
    }

    private List<String> getdata(){

        List<String> time_list = new ArrayList() ;

        String[] time_arr = {"07:00", "07:10", "07:20", "07:30", "07:40",
                "07:50", "08:00", "08:10", "08:20",
                "08:30", "08:40", "08:50", "09:00", "09:10", "09:20", "09:30", "09:40",
                "09:50", "10:00", "10:10", "10:20", "10:30", "10:40", "10:50"} ;

        for(int i = 0 ; i < time_arr.length ; i++){
            time_list.add(time_arr[i]);
        }
        return time_list ;
    }

    private void dialogCartAction() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialoug_appointment_window);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT ;


        ImageView close_btn = (ImageView) dialog.findViewById(R.id.close_btn) ;
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        LinearLayout close_layout, booknow_layout ;
        close_layout = (LinearLayout) dialog.findViewById(R.id.close_layout) ;
        booknow_layout = (LinearLayout) dialog.findViewById(R.id.booknow_layout) ;

        close_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        booknow_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Appointment Booked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
