package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.arko.nextdot.Adapters.TherapyDetailsImageSwipeAdapter;
import com.arko.nextdot.R;

public class TherapyDetailsActivity extends ToolbarBaseActivity {


    ViewPager viewPager ;
    TherapyDetailsImageSwipeAdapter therapyDetailsImageSwipeAdapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapy_details);
        toolbarwithoutdrawer("Therapy Details");

        viewPager = (ViewPager) findViewById(R.id.therapy_details_viewpager) ;
        therapyDetailsImageSwipeAdapter = new TherapyDetailsImageSwipeAdapter(this) ;
        viewPager.setAdapter(therapyDetailsImageSwipeAdapter);
    }
}
