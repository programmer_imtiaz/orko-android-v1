package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arko.nextdot.Adapters.MedicineTimeDetailsAdapter;
import com.arko.nextdot.Model.MedicineTimeDetailsModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sakib on 7/25/2017.
 */

public class MedicineTimeDetailsActivity extends ToolbarBaseActivity {

    String[] time_list = {"10:00 AM", "01:30 PM", "09:00 PM"} ;
    RecyclerView recyclerView ;
    MedicineTimeDetailsAdapter medicineTimeDetailsAdapter ;
    LinearLayoutManager layoutManager ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_details);
        toolbarwithoutdrawer("Medicine Details");

//        ToolbarBaseActivity obj = new ToolbarBaseActivity() ;
//        obj.toolbarwithoutdrawer("Medicine Details");

        recyclerView = (RecyclerView) findViewById(R.id.medicine_time_recyler);
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        medicineTimeDetailsAdapter = new MedicineTimeDetailsAdapter(getApplicationContext(), getData()) ;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(medicineTimeDetailsAdapter);

    }

    private List<MedicineTimeDetailsModel> getData(){

        List<MedicineTimeDetailsModel> list = new ArrayList<>() ;

        for(int i = 0 ; i < time_list.length ; i++){

            MedicineTimeDetailsModel medicineTimeDetailsModel = new MedicineTimeDetailsModel() ;
            medicineTimeDetailsModel.setTime(time_list[i]);
            list.add(medicineTimeDetailsModel);
        }
        return list ;
    }
}
