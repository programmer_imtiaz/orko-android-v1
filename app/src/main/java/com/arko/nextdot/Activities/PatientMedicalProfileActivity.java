package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.PatientMedicalProfileAdapter;
import com.arko.nextdot.Model.SubTitleModel;
import com.arko.nextdot.Model.TitleModel;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sakib on 7/31/2017.
 */

public class PatientMedicalProfileActivity extends ToolbarBaseActivity {



    TextView see_more, medical_profile_age, medical_profile_medical_condition, patient_medical_profile_name ;
    Context context;
    String pro_pic, exact_age, patient_age, main_problem, avatar_path, dob ;
    CircleImageView patient_medical_profile_pic;
    SharedPreferences sharedPreferences;
    //CardView family_history, physical_examination, investigation, medication, advice, reports, appointments, allergies, surgeries, vacinations ;
    String[] title = {"Family History", "Physical Examination", "Investigation", "Medication", "Advice", "Reports", "Appointments", "Allergies", "Surgeries", "Vaccination"} ;
    int[] title_img = {R.drawable.family, R.drawable.physical_examination, R.drawable.investigation, R.drawable.medication,
            R.drawable.advice, R.drawable.advice, R.drawable.advice, R.drawable.advice, R.drawable.advice, R.drawable.advice} ;

    String[] title_paragraph = {"my text", "my text", "my text", "my text", "my text", "my text", "my text", "my text",
            "my text", "my text"} ;

    String[] subtitle = {"Sub Title"} ;
    int[] subtitle_icon = {R.drawable.family} ;
    public static final int CONNECTION_TIMEOUT = 6000;
    public static final int READ_TIMEOUT = 6500 ;

    int dd, mm, yy, age = 0 ;
    String[] split_birthday;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_profile);

        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        toolbarwithoutdrawer("Profile");
        initilization();

        String id = sharedPreferences.getString(Constant.user_id, "");
        String token = sharedPreferences.getString(Constant.token, "");
        ApiTaskMedicalProfile apiTaskMedicalProfile = new ApiTaskMedicalProfile(PatientMedicalProfileActivity.this) ;
        apiTaskMedicalProfile.execute(token, id) ;

        see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class) ;
                startActivityForResult(intent,200);
                //finish();
            }
        });

        /*reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PatientsReportsActivity.class) ;
                startActivity(intent);
            }
        });*/

        //Glide.with(PatientMedicalProfileActivity.this).load(Constant.ROOT_URL+pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(profile_pic);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.patient_profile_recyler);
        PatientMedicalProfileAdapter patientMedicalProfileAdapter = new PatientMedicalProfileAdapter(getdata(), getApplicationContext()) ;
        LinearLayoutManager layoutManager =  new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false) ;
        recyclerView.setLayoutManager(layoutManager);

        //recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setAdapter(patientMedicalProfileAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(patientMedicalProfileAdapter);
    }

    private void initilization(){
        see_more = (TextView) findViewById(R.id.see_more);
        patient_medical_profile_pic = (CircleImageView) findViewById(R.id.medical_profile_circle_pic) ;
        medical_profile_age = (TextView) findViewById(R.id.medical_profile_age) ;
        medical_profile_medical_condition = (TextView) findViewById(R.id.medical_profile_medical_condition) ;
        patient_medical_profile_name = (TextView) findViewById(R.id.patient_medical_profile_name);
        /*family_history = (CardView) findViewById(R.id.family_history);
        physical_examination = (CardView) findViewById(R.id.physical_examination) ;
        investigation = (CardView) findViewById(R.id.investigation) ;
        medication = (CardView) findViewById(R.id.medication) ;
        advice = (CardView) findViewById(R.id.advice) ;
        reports = (CardView) findViewById(R.id.reports) ;
        appointments = (CardView) findViewById(R.id.appointment) ;
        physical_examination = (CardView) findViewById(R.id.physical_examination) ;*/
    }

    private List<TitleModel> getdata(){
        List<TitleModel> list = new ArrayList<>() ;

        for(int i = 0 ; i < title.length ; i++){

            List<SubTitleModel> subtitlelist = new ArrayList<>() ;

            for(int j = 0 ; j < subtitle.length ; j++){

                SubTitleModel subTitleModel = new SubTitleModel(subtitle_icon[j], subtitle[j]) ;
                subtitlelist.add(subTitleModel);
                Log.d("subtitle :",  subtitle[j]) ;
            }

            TitleModel titleModel = new TitleModel(title[i], subtitlelist, title_paragraph[i], title_img[i]) ;
            list.add(titleModel);

        }

        return list ;
    }

    public class ApiTaskMedicalProfile extends AsyncTask<String, Void, String> {

        String login_url = Constant.ROOT_URL + "api/patient/profile" ;
        Context context ;
        ProgressDialog progressDialog ;

        public ApiTaskMedicalProfile(Context context){
            this.context = context ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(login_url) ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setReadTimeout(READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                String token = params[0] ;
                String id = params[1] ;

                String data = URLEncoder.encode("token", "UTF-8")+"="+URLEncoder.encode(token, "UTF-8")+"&"+
                        URLEncoder.encode("id", "UTF-8")+"="+ URLEncoder.encode(id, "UTF-8") ;

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();


                InputStream inputStream = httpURLConnection.getInputStream() ;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                StringBuilder stringBuilder = new StringBuilder() ;
                String line = "" ;

                while((line = bufferedReader.readLine()) != null){

                    stringBuilder.append(line + "\n") ;
                }
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim() ;
            }
            catch (MalformedURLException e) {
                Log.d("MalformedURLException :", String.valueOf(e)) ;
            }
            catch (IOException e) {
                Log.d("IOException :", String.valueOf(e)) ;
            }
            finally {
                progressDialog.dismiss();
                //Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                JSONObject jsonObject = new JSONObject(json) ;


                JSONObject profile_obj = jsonObject.getJSONObject("profile") ;
                String first_name = profile_obj.getString("first_name") ;
                String last_name = profile_obj.getString("last_name");

                if(profile_obj.getString("dob") != null){
                    dob = profile_obj.getString("dob") ;
                }
                else{
                    dob = "" ;
                }

                if(profile_obj.getString("main_problem") != null){
                    main_problem = profile_obj.getString("main_problem") ;
                }
                else{
                    main_problem = "" ;
                }

                if(jsonObject.getString("avatar") != null){
                    avatar_path = jsonObject.getString("avatar") ;
                }
                else{
                    avatar_path = "" ;
                }

                setValue(first_name, last_name, dob, main_problem, avatar_path) ;

                /*SharedPreferences.Editor editor = sharedPreferences.edit() ;

                editor.putString(Constant.email, email);
                editor.putString(Constant.user_pro_pic, avatar_path) ;
                editor.putString(Constant.first_name, firstname) ;
                editor.putString(Constant.last_name, lastname) ;
                editor.putString(Constant.user_name, username) ;
                editor.putString(Constant.user_id, userid) ;
                editor.commit() ;*/

            }
            catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException n){
                progressDialog.dismiss();
                Toast.makeText(context, "Error in network connection!", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(PatientMedicalProfileActivity.this, PatientMedicalProfileActivity.class) ;
//                startActivity(intent);
//                finish();
                n.printStackTrace();
            }
        }
    }

    private void setValue(String first_name, String last_name, String dob, String main_problem, String avatar_path){

        if(dob.equals("")){
            patient_age = "N/A" ;
        }
        else{
            patient_age = setAge(dob) ;
        }
        if(Patterns.WEB_URL.matcher(avatar_path).matches()){

            Glide.with(PatientMedicalProfileActivity.this).load(avatar_path).placeholder(R.drawable.avtaar).dontAnimate().into(patient_medical_profile_pic);
        }
        else{

            Glide.with(PatientMedicalProfileActivity.this).load(Constant.ROOT_URL+avatar_path).placeholder(R.drawable.avtaar).dontAnimate().into(patient_medical_profile_pic);
        }

        //Glide.with(PatientMedicalProfileActivity.this).load(Constant.ROOT_URL+avatar_path).placeholder(R.drawable.avtaar).dontAnimate().into(patient_medical_profile_pic);
        patient_medical_profile_name.setText(first_name+" "+last_name);
        medical_profile_age.setText(patient_age);
        medical_profile_medical_condition.setText(main_problem);
    }

    private String setAge(String dob){

        split_birthday = dob.split("-") ;
        yy = Integer.parseInt(split_birthday[0]) ;
        mm = Integer.parseInt(split_birthday[1]) ;
        dd = Integer.parseInt(split_birthday[2]) ;
        Log.d("+++YY+++", String.valueOf(yy)) ;
        Log.d("+++mm+++", String.valueOf(mm)) ;
        Log.d("+++dd+++", String.valueOf(dd)) ;
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int yyy = calendar.get(Calendar.YEAR) ;
        int mmm = calendar.get(Calendar.MONTH) ;
        mmm += 1 ;
        int ddd = calendar.get(Calendar.DATE) ;
        if(mmm < mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            age = age - 1 ;
            if(age == 0){
                mmm += 12 ;
                mmm = mmm - mm ;
                exact_age = Integer.toString(mmm)+" Month" ;
            }
            else{
                exact_age = Integer.toString(age) ;
            }
            Log.d("+++AGE++", exact_age) ;
        }
        else if(mmm >= mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            if(age == 0){
                mmm = mmm - mm ;
                exact_age = Integer.toString(mmm)+" Month" ;
            }
            else{
                exact_age = Integer.toString(age) ;
            }
            Log.d("+++AGE++", exact_age) ;
        }
        return exact_age ;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(PatientMedicalProfileActivity.this, NewHomeActivity.class) ;
//        startActivity(intent);
        finish();
    }
}
