package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arko.nextdot.Adapters.TransactionHistoryAdapter;
import com.arko.nextdot.Model.TransactionHistoryModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class TransactionHistoryActivity extends ToolbarBaseActivity {


    RecyclerView transaction_recyler ;
    TransactionHistoryAdapter transactionHistoryAdapter ;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        toolbarwithoutdrawer("Transaction history");

        transaction_recyler = (RecyclerView) findViewById(R.id.transaction_recyler);
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        transactionHistoryAdapter = new TransactionHistoryAdapter(getApplicationContext(), getData()) ;
        transaction_recyler.setAdapter(transactionHistoryAdapter);
        transaction_recyler.setLayoutManager(layoutManager);


    }

    public static List<TransactionHistoryModel> getData(){

        List<TransactionHistoryModel> list = new ArrayList<>() ;
        String[] transactionHistory_date_time = {"June 12, 11.42 Am", "July 13, 11.42 Am", "August 11, 12.42 Am",
                "September 11, 11.42 Pm", "October 20, 11.42 Am", "December 09, 01.42 Am",} ;
        String[] transactionHistory_payment = {"$20.00", "$30.00", "$40.00", "$50.00", "$70.00", "$80.00"} ;
        String[] transactionHistory_points = {"15.20Pts", "15.20Pts", "15.20Pts", "15.20Pts", "15.20Pts", "15.20Pts"} ;

        for(int i = 0 ; i < transactionHistory_date_time.length ; i++){
            TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel() ;
            transactionHistoryModel.setTrns_time_date(transactionHistory_date_time[i]);
            transactionHistoryModel.setTrns_payment(transactionHistory_payment[i]);
            transactionHistoryModel.setTrns_points(transactionHistory_points[i]);
            list.add(transactionHistoryModel);
        }
        return list ;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
