package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Model.EditProfileModel;
import com.arko.nextdot.Model.RetrofitModel.UserProfile;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileActivity extends ToolbarBaseActivity{

    TextView header_name_textview, age_textview, name_textview, main_problem_textview, email_textview,
            gender_textview, birthday_textview, profession_textview, district_textview, upzila_textview,
            phone_textview, blood_group_textview, address_textview ;
    String header_name, pro_pic, age_string, main_problem, email, gender, birthday, profession, district, district_id, upazilla, upazilla_id, first_name, last_name, user_id, phone ;
    ProgressDialog progressDialog ;
    RetrofitInterface retrofitInterface ;
    String Data, id, token, dob, birthday_string ;
    int dd, mm, yy, age = 0 ;
    String[] split_birthday;
    String month ;
    CircleImageView profile_pic ;
    ArrayList<String> list ;
    SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbarwithoutdrawer("Profile");
        initilization();
        setProgressDialog();
        list = new ArrayList<>() ;

        sharedPreferences  = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        id = sharedPreferences.getString(Constant.user_id, "") ;
        token = sharedPreferences.getString(Constant.token, "") ;

        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;


        String url = Constant.ROOT_URL+"api/patient/profile" ;
        Log.d("+++URL+++", url) ;
        Call<UserProfile> call = retrofitInterface.getUserProfile(url, id, token) ;

        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                Log.d("+++CONNECTION++", "asche") ;
                /*Data = response.body().getSubProfile().getDistrict().getName() ;
                Log.d("+++DATA+++", Data) ;*/

                setProgressDialogDismiss();

                if(response.body().getEmail() != null){
                    email_textview.setText(response.body().getEmail());
                    email = response.body().getEmail() ;
                }
                else{
                    email_textview.setText("");
                    email = "" ;
                }
                if(response.body().getPhone() != null){
                    phone = response.body().getPhone() ;
                    phone_textview.setText(phone);
                }
                else{
                    phone = "" ;
                    phone_textview.setText("");
                }
                user_id = response.body().getId() ;
                /*editProfileModel.setToken(id);
                editProfileModel.setAvatar_file(Constant.ROOT_URL+response.body().getAvatar());
                editProfileModel.setEmail(response.body().getEmail());
                editProfileModel.setPhone(response.body().getPhone());
                editProfileModel.setUser_id(response.body().getId());*/

                if(response.body().getSubProfile() != null){
                    header_name_textview.setText(response.body().getSubProfile().getFirst_name()+" "+response.body().getSubProfile().getLast_name());
                    name_textview.setText(response.body().getSubProfile().getFirst_name()+" "+response.body().getSubProfile().getLast_name());

                    if(response.body().getSubProfile().getMain_problem() != null){
                        main_problem_textview.setText(response.body().getSubProfile().getMain_problem());
                    }
                    else{
                        main_problem_textview.setText("") ;
                    }
                    if(response.body().getSubProfile().getGender() != null){
                        gender_textview.setText(response.body().getSubProfile().getGender());
                    }
                    else{
                        gender_textview.setText("") ;
                    }
                    if(response.body().getSubProfile().getProfession() != null){
                        Log.d("+++profession+++", response.body().getSubProfile().getProfession()) ;
                        profession_textview.setText(response.body().getSubProfile().getProfession());
                    }
                    else{
                        Log.d("+++profession+++", response.body().getSubProfile().getProfession()+"") ;
                        profession_textview.setText("") ;
                    }
                    if(response.body().getSubProfile().getAddress() != null){
                        address_textview.setText(response.body().getSubProfile().getAddress());
                    }
                    else{
                        address_textview.setText("");
                    }
                    if(response.body().getSubProfile().getBlood_group() != null){
                        blood_group_textview.setText(response.body().getSubProfile().getBlood_group());
                    }
                    else{
                        blood_group_textview.setText("");
                    }
                    dob = response.body().getSubProfile().getDob() ;

                    first_name = response.body().getSubProfile().getFirst_name() ;
                    last_name = response.body().getSubProfile().getLast_name() ;
                    header_name = response.body().getSubProfile().getFirst_name()+" "+response.body().getSubProfile().getLast_name() ;

                    if(response.body().getSubProfile().getDob() != null){
                        dob = response.body().getSubProfile().getDob() ;
                        split_birthday = dob.split("-") ;
                        yy = Integer.parseInt(split_birthday[0]) ;
                        mm = Integer.parseInt(split_birthday[1]) ;
                        dd = Integer.parseInt(split_birthday[2]) ;
                        Log.d("+++YY+++", String.valueOf(yy)) ;
                        Log.d("+++mm+++", String.valueOf(mm)) ;
                        Log.d("+++dd+++", String.valueOf(dd)) ;
                        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                        int yyy = calendar.get(Calendar.YEAR) ;
                        int mmm = calendar.get(Calendar.MONTH) ;
                        mmm += 1 ;
                        int ddd = calendar.get(Calendar.DATE) ;

                        if(mmm < mm){

                            Log.d("+++YEAR++", String.valueOf(yyy)) ;
                            age = yyy - yy ;
                            age = age - 1 ;
                            Log.d("+++AGE++", String.valueOf(age)) ;
                        }
                        else if(mmm >= mm){

                            Log.d("+++YEAR++", String.valueOf(yyy)) ;
                            age = yyy - yy ;
                            Log.d("+++AGE++", String.valueOf(age)) ;
                        }

                        age_textview.setText("Age "+age);

                        age_string = Integer.toString(age) ;
                        month = new DateFormatSymbols().getMonths()[mm-1];
                        Log.d("+++MONTH++", month) ;
                        //MONTH.of(monthNumber).name()
                        birthday_string = Integer.toString(dd)+ " " + month + ", " + Integer.toString(yy) ;
                        Log.d("+++MONTH++", birthday_string) ;
                        birthday_textview.setText(birthday_string);
                        birthday = birthday_string ;
                    }
                    else{
                        birthday_string = "" ;
                        birthday_textview.setText(birthday_string);
                        birthday = birthday_string ;
                    }


                    if(response.body().getSubProfile().getMain_problem() != null){
                        main_problem = response.body().getSubProfile().getMain_problem() ;
                    }
                    else{
                        main_problem = "" ;
                    }
                    if(response.body().getSubProfile().getGender() != null){
                        gender = response.body().getSubProfile().getGender() ;
                    }
                    else{
                        gender = "" ;
                    }

                    if(response.body().getSubProfile().getProfession() != null){
                        profession = response.body().getSubProfile().getProfession() ;
                    }
                    else{
                        profession = "" ;
                    }

                    /*editProfileModel.setFirst_name(response.body().getSubProfile().getFirst_name());
                    editProfileModel.setLast_name(response.body().getSubProfile().getLast_name());
                    editProfileModel.setFull_name(response.body().getSubProfile().getFirst_name()+" "+response.body().getSubProfile().getLast_name());
                    editProfileModel.setMain_problem(response.body().getSubProfile().getMain_problem());
                    editProfileModel.setGender(response.body().getSubProfile().getGender());
                    editProfileModel.setProfession(response.body().getSubProfile().getProfession());*/


                    if(response.body().getSubProfile().getUpazilla() != null){
                        upzila_textview.setText(response.body().getSubProfile().getUpazilla().getName());
                        upazilla_id = response.body().getSubProfile().getUpazilla().getId() ;
                        upazilla = response.body().getSubProfile().getUpazilla().getName() ;
                        /*editProfileModel.setUpazilla(response.body().getSubProfile().getUpazilla().getName());
                        editProfileModel.setUpazilla_id(response.body().getSubProfile().getUpazilla().getId());*/
                    }
                    else {
                        upazilla = "" ;
                        upazilla_id = "" ;
                    }

                    if(response.body().getSubProfile().getDistrict() != null){

                        district_textview.setText(response.body().getSubProfile().getDistrict().getName());
                        district = response.body().getSubProfile().getDistrict().getName() ;
                        district_id = response.body().getSubProfile().getDistrict().getId() ;
                        /*editProfileModel.setDistrict(response.body().getSubProfile().getDistrict().getName());
                        editProfileModel.setDistrict_id(response.body().getSubProfile().getDistrict().getId());*/

                    }
                    if(response.body().getSubProfile().getDistrict() == null){

                        //district_textview.setText(response.body().getSubProfile().getDistrict().getName());
                        /*editProfileModel.setDistrict("N/A");
                        editProfileModel.setDistrict_id("N/A");*/
                        district = "" ;
                        district_id = "" ;

                    }
                }
                //placeholder(R.drawable.avtaar)
                pro_pic = response.body().getAvatar() ;
                Log.d("+++PRO_PIC+++",Constant.ROOT_URL+pro_pic) ;

                if(Patterns.WEB_URL.matcher(pro_pic).matches()){

                    Glide.with(ProfileActivity.this).load(pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(profile_pic);
                }
                else{

                    Glide.with(ProfileActivity.this).load(Constant.ROOT_URL+pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(profile_pic);
                }
                //pro_pic = response.body().getAvatar() ;


                list.add(token);//0
                list.add(user_id);//1
                list.add(first_name);//2
                list.add(last_name);//3
                list.add(header_name);//4
                list.add(email);//5
                list.add(birthday);//6
                list.add(age_string);//7
                list.add(district);//8
                list.add(district_id);//9
                list.add(upazilla);//10
                list.add(upazilla_id);//11
                list.add(profession);//12
                list.add(phone);//13
                list.add(main_problem) ;//14
                list.add(pro_pic) ;//15
                list.add(gender);//16
                list.add(dob);//17
                //list.add(first_name);

                Log.d("+++USER_NEW_PRO_PIC++++", pro_pic) ;

                SharedPreferences.Editor editor = sharedPreferences.edit() ;
                editor.putString(Constant.token, token) ;
                editor.putString(Constant.email, email);
                editor.putString(Constant.user_pro_pic, pro_pic) ;
                editor.putString(Constant.first_name, first_name) ;
                editor.putString(Constant.last_name, last_name) ;
                editor.putString(Constant.header_name, header_name) ;
                editor.putString(Constant.user_id, user_id) ;
                editor.putString(Constant.main_problem, main_problem) ;
                editor.putString(Constant.dob, dob) ;
                editor.putString(Constant.profession, profession) ;
                editor.putString(Constant.upazilla, upazilla) ;
                editor.putString(Constant.upazilla_id, upazilla_id) ;
                editor.putString(Constant.district_id, district_id) ;
                editor.putString(Constant.district, district) ;
                editor.putString(Constant.age_string, age_string) ;
                editor.putString(Constant.birthday, birthday) ;
                editor.putString(Constant.phone, phone) ;
                editor.putString(Constant.gender, gender) ;
                editor.commit() ;

            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {

                Log.d("+++CONNECTION++", "ashe nai") ;
                Log.d("+++IOException++", String.valueOf(t)) ;
                Toast.makeText(ProfileActivity.this, "Network Problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId() ;

        if(id == R.id.action_edit){
            Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class) ;
            //intent.putExtra("arraylist", (Serializable) list);
            intent.putStringArrayListExtra("key", list);
            startActivity(intent);
            finish();
        }

        /*else if(id == android.R.id.home){
            finish();
        }*/
        return super.onOptionsItemSelected(item) ;
    }

    private void initilization(){

        header_name_textview = (TextView) findViewById(R.id.profile_name);
        age_textview = (TextView) findViewById(R.id.profile_age) ;
        name_textview = (TextView) findViewById(R.id.profile_full_name) ;
        main_problem_textview = (TextView) findViewById(R.id.profile_recent_medical_condition) ;
        email_textview = (TextView) findViewById(R.id.profile_email) ;
        gender_textview = (TextView) findViewById(R.id.profile_gender) ;
        birthday_textview = (TextView) findViewById(R.id.profile_birthday) ;
        district_textview = (TextView) findViewById(R.id.profile_district) ;
        upzila_textview = (TextView) findViewById(R.id.profile_upzila) ;
        profile_pic = (CircleImageView) findViewById(R.id.profile_circle_pic) ;
        profession_textview = (TextView) findViewById(R.id.profile_profession) ;
        phone_textview = (TextView) findViewById(R.id.profile_phone) ;
        address_textview = (TextView) findViewById(R.id.address_details) ;
        blood_group_textview = (TextView) findViewById(R.id.blood_group_details) ;
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(ProfileActivity.this) ;
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        progressDialog.show();
    }

    private void setProgressDialogDismiss(){
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ProfileActivity.this, PatientMedicalProfileActivity.class) ;
        startActivity(intent);
        finish();
    }
}
