package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Model.RetrofitModel.FbLoginUserProfile.PatientProfileRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.PreferenceManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    EditText login_username, login_password ;
    Button login_btn ;
    TextView register_btn ;
    AlertDialog.Builder internet_connection_failed ;
    int network_flag, field_missing_flag = 1 ;
    SharedPreferences sharedPreferences ;
    LoginButton loginButton ;
    CallbackManager callbackManager ;
    String fb_first_name, fb_last_name, fb_email, fb_id, fb_pro_pic, user_phone_number, fb_gender ;
    RetrofitInterface retrofitInterface ;
    ProgressDialog progressDialog ;
    private ArrayList<String> list ;
    String token, header_name, pro_pic, age_string, main_problem, email, gender, birthday, profession, district, district_id, upazilla, upazilla_id, first_name, last_name, user_id, phone, dob ;
    String reg_token = "" ;

    int dd, mm, yy, age = 0 ;
    String[] split_birthday ;
    String month, exact_age, birthday_string ;

    ConnectivityManager mConnectivity = null;
    TelephonyManager mTelephony = null;
    String device_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE) ;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);
        initilization();



        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = login_username.getText().toString().trim() ;
                String password = login_password.getText().toString().trim() ;

                // CHECKING WIFI OR MOBILE DATA IS ACTIVE OR NOT
                Constant constant = new Constant(LoginActivity.this) ;
                network_flag = constant.isNetworkActive() ;
                // CHECKING WIFI OR MOBILE DATA IS ACTIVE OR NOT

                if(username.equals("")){
                    login_username.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else if(password.equals("")){
                    login_password.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(network_flag == 0 && field_missing_flag == 0){

                    String device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                            Settings.Secure.ANDROID_ID);

                    if (sharedPreferences.getString(Constant.FCM_TOKEN, "").equals("empty")) {

                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

                        try {

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Constant.FCM_TOKEN, refreshedToken) ;
                            editor.commit() ;
                            //preferenceManager.setFCMToken(refreshedToken);


//Displaying token on logcat

                            Log.d("LoginToken", "REFRESHED TOKEN:" + refreshedToken);
                            Log.d("LoginToken", "SHARED PREFERANCE REFRESHED TOKEN:" + sharedPreferences.getString(Constant.FCM_TOKEN, ""));


                        } catch (Exception e) {
                            e.printStackTrace();


                            // If an exception happens while fetching the new token or updating our registration data
                            // on a third-party server, this ensures that we'll attempt the update at a later time.

                        }
                    }


                    String reg_token = sharedPreferences.getString(Constant.FCM_TOKEN, "");

                    Log.d("LoginToken", "SHARED PREFERANCE REFRESHED TOKEN:" + sharedPreferences.getString(Constant.FCM_TOKEN, ""));


                    //checking for play services on device asper firebase guidelines

                    if (checkPlayServices()) {

                        //Sending necessary data to the loginAPI

                        //loginAPI(usernameText, passwordText, reg_token, device_id);
                        login_btn.setBackgroundResource(R.drawable.login_button_background_active);
                        login_btn.setTextColor(getResources().getColor(R.color.white));
                        ApiTaskLogin apiTaskLogin = new ApiTaskLogin(LoginActivity.this) ;
                        apiTaskLogin.execute(username, password, Constant.user_type, reg_token, device_id) ;


                    } else {


                        showPlayServicesAlert();

                    }


                }
                if(network_flag == 1){
                    internetConnectFailedMessage();
                    AlertDialog alertDialog = internet_connection_failed.create() ;
                    alertDialog.show();
                }
                if(field_missing_flag == 1){
                    Log.d("++++Field Missing++++", String.valueOf(field_missing_flag)) ;
                }

            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class) ;
                startActivity(intent);
            }
        });


        // FACEBOOK LOGIN IMPLEMENTATION

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("DEVICE_ID", device_id) ;

        if (sharedPreferences.getString(Constant.FCM_TOKEN, "").equals("empty")) {

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            try {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constant.FCM_TOKEN, refreshedToken) ;
                editor.commit() ;
                //preferenceManager.setFCMToken(refreshedToken);


//Displaying token on logcat

                Log.d("LoginToken", "REFRESHED TOKEN:" + refreshedToken);
                Log.d("LoginToken", "SHARED PREFERANCE REFRESHED TOKEN:" + sharedPreferences.getString(Constant.FCM_TOKEN, ""));


            } catch (Exception e) {
                e.printStackTrace();


                // If an exception happens while fetching the new token or updating our registration data
                // on a third-party server, this ensures that we'll attempt the update at a later time.

            }
        }


        reg_token = sharedPreferences.getString(Constant.FCM_TOKEN, "");

        Log.d("LoginToken", "SHARED PREFERANCE REFRESHED TOKEN:" + sharedPreferences.getString(Constant.FCM_TOKEN, ""));


        //checking for play services on device asper firebase guidelines

        if (checkPlayServices()) {

            //Sending necessary data to the loginAPI

            //loginAPI(usernameText, passwordText, reg_token, device_id);

            loginButton.setReadPermissions("email", "public_profile");
            callbackManager = CallbackManager.Factory.create() ;
            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                @Override
                public void onSuccess(LoginResult loginResult) {
                    String userid = loginResult.getAccessToken().getUserId() ;

                    GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            displayUserInfo(object) ;
                        }
                    });
                    Bundle parameters = new Bundle() ;
                    parameters.putString("fields", "first_name, last_name, email, id, birthday, gender");
                    graphRequest.setParameters(parameters);
                    graphRequest.executeAsync() ;
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            });


        } else {

            showPlayServicesAlert();

        }

        // FACEBOOK LOGIN IMPLEMENTATION
    }

    private void initilization(){
        login_username = (EditText) findViewById(R.id.log_user_name) ;
        login_password = (EditText) findViewById(R.id.log_user_pass) ;
        login_btn = (Button) findViewById(R.id.btn_login) ;
        register_btn = (TextView) findViewById(R.id.dont_acc_sign_up);
        loginButton = (LoginButton) findViewById(R.id.facebook_login_button) ;


        login_username.setTypeface(Typeface.DEFAULT);
        login_password.setTypeface(Typeface.DEFAULT);
        login_password.setTransformationMethod(new PasswordTransformationMethod());
        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE)
                        .show();
            } else {
                Log.i("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void showPlayServicesAlert() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Please Google Play Services First");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void internetConnectFailedMessage(){
        internet_connection_failed = new AlertDialog.Builder(LoginActivity.this);
        internet_connection_failed.setTitle("Warning!") ;
        internet_connection_failed.setMessage("Please Check Your Internet Connection") ;
        internet_connection_failed.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void displayUserInfo(JSONObject jsonObject){

        try {

            URL profile_pic = null;
            list = new ArrayList<String>() ;

            //first_name = jsonObject.getString("first_name");
            Log.d("Jsonobject", jsonObject.toString());

            if(jsonObject.has("first_name")){
                fb_first_name = jsonObject.getString("first_name");
            }
            else{
                fb_first_name = "" ;
            }

            if(jsonObject.has("last_name")){
                fb_last_name = jsonObject.getString("last_name") ;
            }
            else{
                fb_last_name = "" ;
            }

            if(jsonObject.has("email")){
                fb_email = jsonObject.getString("email") ;
            }
            else{
                fb_email = "" ;
            }

            if(jsonObject.has("id")){
                fb_id = jsonObject.getString("id") ;
            }
            else{
                fb_id = "" ;
            }

            if(jsonObject.has("gender")){
                fb_gender = jsonObject.getString("gender") ;
                Log.d("Gender", fb_gender) ;
            }
            else{
                fb_gender = "" ;
            }

            try {
                profile_pic = new URL("https://graph.facebook.com/" + fb_id + "/picture?width=200&height=150");
                fb_pro_pic = profile_pic.toString() ;
                Log.d("PROFILE PIC URL", profile_pic.toString()) ;
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("PROFILE PIC URL", profile_pic.toString()) ;
            }

            Log.d("First Name", fb_first_name) ;
            Log.d("Last Name", fb_last_name) ;
            //Log.d("email", email) ;
            Log.d("id", fb_id) ;

            retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;
            String url = Constant.ROOT_URL+"api/login/fb" ;

            Call<PatientProfileRoot> call = retrofitInterface.postFbLogin(url, fb_id, fb_first_name, fb_last_name, fb_pro_pic, fb_gender, "2");

            setProgressDialog();
            call.enqueue(new Callback<PatientProfileRoot>() {
                @Override
                public void onResponse(Call<PatientProfileRoot> call, Response<PatientProfileRoot> response) {

                    progressDialog.dismiss();
                    PreferenceManager.getInstance(LoginActivity.this).setProfileInfo(response.body());
                    Log.d("+++CONNECTION++", "ashche") ;
                    //Log.d("+++CONNECTION++", response.body().getMsg()) ;
                    if(response.body().getMsg().equals("success")){

                        if(response.body().getToken() != null){
                            token = response.body().getToken();
                        }
                        if(response.body().getUser() != null){
                            if(response.body().getUser().getId() != null){
                                user_id = response.body().getUser().getId().toString() ;
                            }
                            else{
                                user_id = "" ;
                            }
                            if(response.body().getUser().getEmail() != null){
                                email = response.body().getUser().getEmail() ;
                            }
                            else{
                                email = "" ;
                            }
                            if(response.body().getUser().getPhone() != null){
                                phone = response.body().getUser().getPhone() ;
                            }
                            else{
                                phone = "" ;
                            }
                            if(response.body().getUser().getAvatar() != null){
                                pro_pic = response.body().getUser().getAvatar() ;
                                Log.d("++FACEBOOK_PIC++", pro_pic) ;
                            }
                            else{
                                pro_pic = "" ;
                            }
                        }

                        if(response.body().getUserProfile() != null){
                            if(response.body().getUserProfile().getFirstName() != null){
                                first_name = response.body().getUserProfile().getFirstName() ;
                            }
                            else{
                                first_name = "" ;
                            }
                            if(response.body().getUserProfile().getLastName() != null){
                                last_name = response.body().getUserProfile().getLastName() ;
                            }
                            else{
                                last_name = "" ;
                            }
                            header_name = first_name + " " + last_name ;

                            if(response.body().getUserProfile().getGender() != null){
                                gender = response.body().getUserProfile().getGender() ;
                                Log.d("++GENDER++", gender) ;
                            }
                            else{
                                gender = "" ;
                            }
                            if(response.body().getUserProfile().getDistrict() != null){
                                district = response.body().getUserProfile().getDistrict() ;
                                district_id = "" ;
                            }
                            else{
                                district = "" ;
                                district_id = "" ;
                            }
                            if(response.body().getUserProfile().getUpazilla() != null){
                                upazilla = response.body().getUserProfile().getUpazilla() ;
                                upazilla_id = "" ;
                            }
                            else{
                                upazilla = "" ;
                                upazilla_id = "" ;
                            }
                            if(response.body().getUserProfile().getMainProblem() != null){
                                main_problem = response.body().getUserProfile().getMainProblem() ;
                            }
                            else{
                                main_problem = "" ;
                            }
                            if(response.body().getUserProfile().getProfession() != null) {
                                profession = response.body().getUserProfile().getProfession() ;
                            }
                            else{
                                profession = "" ;
                            }
                            if(response.body().getUserProfile().getDob() != null){
                                dob = response.body().getUserProfile().getDob() ;
                                setAge(dob);
                            }
                            else{
                                dob = "" ;
                                age_string = "0" ;
                                birthday = "" ;
                            }
                        }
                         /*list.add(token);//0
                        list.add(user_id);//1
                        list.add(first_name);//2
                        list.add(last_name);//3
                        list.add(header_name);//4
                        list.add(email);//5
                        list.add(birthday);//6
                        list.add(age_string);//7
                        list.add(district);//8
                        list.add(district_id);//9
                        list.add(upazilla);//10
                        list.add(upazilla_id);//11
                        list.add(profession);//12
                        list.add(phone);//13
                        list.add(main_problem) ;//14
                        list.add(pro_pic) ;//15
                        list.add(gender);//16
                        list.add(dob);//17*/

                        if(response.body().getUser().getPhone() == null || response.body().getUserProfile().getGender() == null || response.body().getUserProfile().getDob() == null){

                            SharedPreferences.Editor editor = sharedPreferences.edit() ;
                            editor.putString(Constant.userlogin_flag, "0");
                            editor.putString(Constant.facebook_login, "1") ;
                            editor.putString(Constant.fb_req_login, "0") ;
                            editor.putString(Constant.token, token) ;
                            editor.putString(Constant.first_name, first_name) ;
                            editor.putString(Constant.last_name, last_name) ;
                            editor.putString(Constant.user_id, user_id) ;
                            editor.putString(Constant.user_name, "") ;
                            editor.putString(Constant.email, email);
                            editor.putString(Constant.user_pro_pic, pro_pic) ;
                            editor.putString(Constant.header_name, first_name+" "+last_name);
                            editor.putString(Constant.birthday, birthday) ;
                            editor.putString(Constant.age_string, age_string) ;
                            editor.putString(Constant.district, district) ;
                            editor.putString(Constant.district_id, district_id);
                            editor.putString(Constant.upazilla, upazilla) ;
                            editor.putString(Constant.upazilla_id, upazilla_id);
                            editor.putString(Constant.profession, profession);
                            editor.putString(Constant.phone, phone);
                            editor.putString(Constant.main_problem, main_problem);
                            editor.putString(Constant.gender, gender) ;
                            editor.putString(Constant.dob, dob);
                            editor.commit() ;

                            Log.d("++FB_LOGIN_FLAG++", sharedPreferences.getString(Constant.facebook_login, "")) ;
                            Log.d("++USER_LOGIN_FLAG++", sharedPreferences.getString(Constant.userlogin_flag, "")) ;
                            Log.d("++FB_REQUIRE_FLAG++", sharedPreferences.getString(Constant.fb_req_login, "")) ;

                            Intent intent = new Intent(LoginActivity.this, FbLoginEditProfile.class) ;
                            startActivity(intent);
                            finish();
                        }
                        else{
                            SharedPreferences.Editor editor = sharedPreferences.edit() ;
                            editor.putString(Constant.userlogin_flag, "0");
                            editor.putString(Constant.facebook_login, "1") ;
                            editor.putString(Constant.fb_req_login, "1") ;
                            editor.putString(Constant.token, token) ;
                            editor.putString(Constant.first_name, first_name) ;
                            editor.putString(Constant.last_name, last_name) ;
                            editor.putString(Constant.user_id, user_id) ;
                            editor.putString(Constant.user_name, "") ;
                            editor.putString(Constant.email, email);
                            editor.putString(Constant.user_pro_pic, pro_pic) ;
                            editor.putString(Constant.header_name, first_name+" "+last_name);
                            editor.putString(Constant.birthday, birthday) ;
                            editor.putString(Constant.age_string, age_string) ;
                            editor.putString(Constant.district, district) ;
                            editor.putString(Constant.district_id, district_id);
                            editor.putString(Constant.upazilla, upazilla) ;
                            editor.putString(Constant.upazilla_id, upazilla_id);
                            editor.putString(Constant.profession, profession);
                            editor.putString(Constant.phone, phone);
                            editor.putString(Constant.main_problem, main_problem);
                            editor.putString(Constant.gender, gender) ;
                            editor.putString(Constant.dob, dob);
                            editor.commit() ;

                            Log.d("++FB_LOGIN_FLAG++", sharedPreferences.getString(Constant.facebook_login, "")) ;
                            Log.d("++USER_LOGIN_FLAG++", sharedPreferences.getString(Constant.userlogin_flag, "")) ;
                            Log.d("++FB_REQUIRE_FLAG++", sharedPreferences.getString(Constant.fb_req_login, "")) ;

                            Intent intent = new Intent(LoginActivity.this, Appointments.class) ;
                            startActivity(intent);
                            finish();
                        }
                    }

                }

                @Override
                public void onFailure(Call<PatientProfileRoot> call, Throwable t) {
                    Log.d("+++CONNECTION++", "ashe nai") ;
                }
            });

            /*SharedPreferences.Editor editor = sharedPreferences.edit() ;
            editor.putString(Constant.userlogin_flag, "0");
            editor.putString(Constant.user_pro_pic, fb_pro_pic) ;
            editor.putString(Constant.first_name, first_name) ;
            editor.putString(Constant.last_name, last_name) ;
            editor.putString(Constant.user_id, fb_id) ;
            editor.putString(Constant.facebook_login, "1") ;
            editor.putString(Constant.user_name, "") ;
            editor.putString(Constant.email, "");
            editor.putString(Constant.token, "") ;
            editor.commit() ;

            list.add(fb_id);
            list.add(first_name);
            list.add(last_name);
            list.add(fb_pro_pic);*/

            /*Intent intent = new Intent(LoginActivity.this, NewHomeActivity.class) ;
            intent.putStringArrayListExtra("facebook_login", (ArrayList<String>) list) ;
            startActivity(intent);
            finish();*/


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /*private int checkInternetActive(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        int      flag = 1 ;

        // Only update if WiFi or 3G is connected and not roaming

        if(info != null){
            Log.d("+++INTERNET+++", "asche") ;
            Log.d("+++INTERNET_FLAG+++", String.valueOf(flag)) ;
            int netType = info.getType();
            int netSubtype = info.getSubtype();
            if (netType == ConnectivityManager.TYPE_WIFI) {
                Log.d("+++WIFI+++", "asche") ;
                Log.d("+++INTERNET_FLAG+++", String.valueOf(flag)) ;
                flag = 0 ;
                return flag ;
            }
            else if (netType == ConnectivityManager.TYPE_MOBILE
                    && netSubtype == TelephonyManager.NETWORK_TYPE_UMTS
                    && !mTelephony.isNetworkRoaming()) {

                Log.d("+++MOBILE+++", "asche") ;
                Log.d("+++INTERNET_FLAG+++", String.valueOf(flag)) ;
                flag = 0 ;
                return flag;
            }
            else {
                Log.d("+++INTERNET+++", "asche") ;
                Log.d("+++INTERNET_ELSE+++", String.valueOf(flag)) ;

                flag = 1 ;
                return flag;
            }
        }
        return flag;
    }*/

    public class ApiTaskLogin extends AsyncTask<String, Void, String> {

        String login_url = Constant.ROOT_URL + "api/login";
        Context context ;
        ProgressDialog progressDialog ;
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);



        public ApiTaskLogin(Context context){
            this.context = context ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(login_url) ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                String login_username = params[0] ;
                String login_password = params[1] ;
                String user_type = params[2] ;
                String registration_token = params[3] ;
                String device_id = params[4] ;

                Log.d("registration_token", registration_token) ;
                Log.d("device_id", device_id) ;

                String data = URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(login_username, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(login_password, "UTF-8")+"&"+
                        URLEncoder.encode("type", "UTF-8")+"="+URLEncoder.encode(user_type, "UTF-8")+"&"+
                        URLEncoder.encode("registration_token", "UTF-8")+"="+URLEncoder.encode(registration_token, "UTF-8")+"&"+
                        URLEncoder.encode("device_id", "UTF-8")+"="+URLEncoder.encode(device_id, "UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpURLConnection.getInputStream() ;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                StringBuilder stringBuilder = new StringBuilder() ;
                String line = "" ;

                while((line = bufferedReader.readLine()) != null){
                    Log.d("++++TRUE+++", "YES");
                    stringBuilder.append(line + "\n") ;
                }
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim() ;

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                if(json == null){
                    Log.d("++++GetJSON+++++", "NULL") ;
                }
                else{
                    Log.d("++++GetJSON+++++", String.valueOf(jsonObject)) ;
                    String msg = jsonObject.getString("msg") ;
                    Log.d("++++MSG+++++", msg) ;

                    if(msg.equals("invalid username or password")){
                        login_btn.setBackgroundResource(R.drawable.login_button_background_inactive);
                        login_btn.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }

                    else if(msg.equals("success")){
                        String username = "", user_pro_pic = "", email = "", phone = "", firstname = "", lastname = "", userid = "", main_problem = "",
                                dob = "", profession = "", upazilla_name = "", upazilla_id = "", district_name = "", district_id = "", gender = "" ;
                        Toast.makeText(context, "Login Successfull", Toast.LENGTH_SHORT).show();

                        String token = jsonObject.getString("token") ;

                        JSONObject user_obj = jsonObject.getJSONObject("user") ;
                        if(user_obj.getString("username") != null){
                            username = user_obj.getString("username") ;
                        }
                        else{
                            username = "" ;
                        }
                        if(user_obj.getString("avatar") != null){
                            user_pro_pic = user_obj.getString("avatar") ;
                        }
                        else{
                            user_pro_pic = "" ;
                        }
                        if(user_obj.getString("email") != null){
                            email = user_obj.getString("email") ;
                        }
                        else{
                            email = "" ;
                        }
                        if(user_obj.getString("phone") != null){
                            phone = user_obj.getString("phone") ;
                        }
                        else{
                            phone = "" ;
                        }

                        Log.d("++++username+++++", username) ;
                        Log.d("++++Profile Pic+++++", user_pro_pic) ;
                        Log.d("++++Email+++++", email) ;


                        //jsonObject.getJSONObject("userProfile") != null
                        if(jsonObject.isNull("userProfile")){

                        }
                        else{

                            JSONObject user_profile = jsonObject.getJSONObject("userProfile") ;
                            if(user_profile.getString("first_name") != null){
                                firstname = user_profile.getString("first_name") ;
                            }
                            else{
                                firstname = "" ;
                            }
                            if(user_profile.getString("last_name") != null){
                                lastname = user_profile.getString("last_name") ;
                            }
                            else{
                                lastname = "" ;
                            }
                            if(user_profile.getString("user_id") != null){
                                userid = user_profile.getString("user_id") ;
                            }
                            else{
                                userid = "" ;
                            }
                            if(user_profile.getString("main_problem") != null){
                                main_problem = user_profile.getString("main_problem") ;
                            }
                            else{
                                main_problem = "" ;
                            }
                            if(user_profile.getString("dob") != null){
                                dob = user_profile.getString("dob") ;
                                setAge(dob);
                            }
                            else{
                                dob = "" ;
                                age_string = "" ;
                                birthday = "" ;
                            }
                            if(user_profile.getString("profession") != null){
                                profession = user_profile.getString("profession") ;
                            }
                            else{
                                profession = "" ;
                            }
                            //user_profile.getJSONObject("upazilla") != null
                            if(jsonObject.isNull("upazilla")){
                                upazilla_name = "" ;
                                upazilla_id = "" ;
                            }

                            else {

                                JSONObject user_upazilla = user_profile.getJSONObject("upazilla");
                                if(user_upazilla.getString("name") != null){
                                    upazilla_name = user_upazilla.getString("name") ;
                                    upazilla_id = String.valueOf(user_upazilla.getInt("id"));
                                }
                                else{
                                    upazilla_name = "" ;
                                    upazilla_id = "" ;
                                }
                            }
                            //user_profile.getJSONObject("district") != null
                            if(jsonObject.isNull("district")){
                                district_name = "" ;
                                district_id = "" ;
                            }
                            else{

                                JSONObject user_district = user_profile.getJSONObject("district");
                                if(user_district.getString("name") != null){
                                    district_name = user_district.getString("name") ;
                                    district_id = String.valueOf(user_district.getInt("id"));
                                }
                                else{
                                    district_name = "" ;
                                    district_id = "" ;
                                }
                            }

                            if(user_profile.getString("gender") != null){
                                gender = user_profile.getString("gender") ;
                            }
                            else{
                                gender = "" ;
                            }
                        }

                        Log.d("++++FirstName+++++", firstname) ;
                        Log.d("++++LastName+++++", lastname) ;
                        Log.d("++++UserID+++++", userid) ;

                        SharedPreferences.Editor editor = sharedPreferences.edit() ;
                        editor.putString(Constant.userlogin_flag, "1");
                        editor.putString(Constant.facebook_login, "0") ;
                        editor.putString(Constant.fb_req_login, "0") ;
                        editor.putString(Constant.token, token) ;
                        editor.putString(Constant.email, email);
                        editor.putString(Constant.user_pro_pic, user_pro_pic) ;
                        editor.putString(Constant.first_name, firstname) ;
                        editor.putString(Constant.last_name, lastname) ;
                        editor.putString(Constant.user_name, username) ;
                        editor.putString(Constant.user_id, userid) ;
                        editor.putString(Constant.main_problem, main_problem) ;
                        editor.putString(Constant.dob, dob) ;
                        editor.putString(Constant.profession, profession) ;
                        editor.putString(Constant.upazilla, upazilla_name) ;
                        editor.putString(Constant.upazilla_id, upazilla_id) ;
                        editor.putString(Constant.district_id, district_id) ;
                        editor.putString(Constant.district, district_name) ;
                        editor.putString(Constant.age_string, age_string) ;
                        editor.putString(Constant.birthday, birthday) ;
                        editor.putString(Constant.phone, phone) ;
                        editor.putString(Constant.gender, gender) ;
                        editor.commit() ;

                        Log.d("++FB_LOGIN_FLAG++", sharedPreferences.getString(Constant.facebook_login, "")) ;
                        Log.d("++USER_LOGIN_FLAG++", sharedPreferences.getString(Constant.userlogin_flag, "")) ;
                        Log.d("++FB_REQUIRE_FLAG++", sharedPreferences.getString(Constant.fb_req_login, "")) ;

                        Intent intent = new Intent(getApplicationContext(), Appointments.class) ;
                        startActivity(intent);
                        finish();
                    }
                }

                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
               e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setAge(String dob){

        split_birthday = dob.split("-") ;
        yy = Integer.parseInt(split_birthday[0]) ;
        mm = Integer.parseInt(split_birthday[1]) ;
        dd = Integer.parseInt(split_birthday[2]) ;
        Log.d("+++YY+++", String.valueOf(yy)) ;
        Log.d("+++mm+++", String.valueOf(mm)) ;
        Log.d("+++dd+++", String.valueOf(dd)) ;
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int yyy = calendar.get(Calendar.YEAR) ;
        int mmm = calendar.get(Calendar.MONTH) ;
        mmm += 1 ;
        int ddd = calendar.get(Calendar.DATE) ;
        if(mmm < mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            age = age - 1 ;
            if(age == 0){
                mmm += 12 ;
                mmm = mmm - mm ;
                age_string = Integer.toString(mmm)+" Month" ;
            }
            else{
                age_string = Integer.toString(age) ;
            }
            Log.d("+++AGE++", exact_age+"") ;
        }
        else if(mmm >= mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            if(age == 0){
                mmm = mmm - mm ;
                age_string = Integer.toString(mmm)+" Month" ;
            }
            else{
                age_string = Integer.toString(age) ;
            }
            Log.d("+++AGE++", age_string) ;
        }

        month = new DateFormatSymbols().getMonths()[mm-1];
        Log.d("+++MONTH++", month) ;
        //MONTH.of(monthNumber).name()
        birthday_string = Integer.toString(dd)+ " " + month + ", " + Integer.toString(yy) ;
        Log.d("+++MONTH++", birthday_string) ;
        birthday = birthday_string ;
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(LoginActivity.this) ;
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}
