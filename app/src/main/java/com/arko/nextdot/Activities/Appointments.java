package com.arko.nextdot.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Fragment.CreateAppointmentFragment;
import com.arko.nextdot.Fragment.HistoryFragment;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.Constants;
import com.arko.nextdot.Utils.PreferenceManager;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.arko.nextdot.Model.AppointmentListModelClass;
import com.arko.nextdot.Adapters.AppointmentListAdapter;

import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Appointments extends BaseActivity {
    @BindView(R.id.rv_pending_works)
    RecyclerView rvPendingWorks;
    Unbinder unbinder;

    private ProgressDialog progressDialog;
    private List<AppointmentListModelClass> pendingJobItem = new ArrayList<>();
    private AppointmentListAdapter adapter;
    CalendarDay currentDay, eventDay;
    MaterialCalendarView calendar;
    Calendar startingCal, cal;
    int scrollState;
    DayViewDecorator currentDayDecorator;
    Boolean isSelected = true, isCurrentDayDecoratorAdded = false;
    String currentDayInString, selectedDayInString, dateInString, currentDateInString, selectedDateInString;

    TextView toolbar_title;

    private static final String TAG = "Appointments";
    private String userId = "69";
    private String patientId;
    private String userToken;
    private String user_token;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatForDay = new SimpleDateFormat("E", Locale.getDefault());
    private SimpleDateFormat dateFormatForAppointmentDay = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private Boolean shouldHide = true, shouldShowAnimated = false;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView calendarTitle;
    private Date currentDate;
    private Date selectedDate;
    private TextView textView, textView1;
    private String dayInString, monthInString, yearInString;
    private AppointmentListModelClass schedule, appointments, appointmentDay;
    private List<AppointmentListModelClass> allItems = new ArrayList<AppointmentListModelClass>();
    private ImageButton showPreviousMonthBut;
    private ImageButton showNextMonthBut;
    private Boolean hasAppointmentDays = false;
    private Boolean fromDisabledDays = false;
    private int titleToggle = 1;
    public SharedPreferences sp;
    public SharedPreferences.Editor editor;
    private long startTimeInMillis = 0;
    private long endTimeInMillis = 0;
    private String startTime = "10:00:00";
    private String endTime = "10:30:00";
    private RelativeLayout rl;
    private Boolean isPast;
    SharedPreferences sharedPreferences;
    private LinearLayout container;

    private int onPreHide = 0;
    private MenuItem item1, item2;
    private RelativeLayout mainLayout, innerMainLayout;
    private LinearLayout innerLL;
    private PreferenceManager preferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_appointments);
        setupNavDrawer1();
        initilization();
        unbinder = ButterKnife.bind(this);


//        toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
//        toolbar.setTitle("My Appointments");
//        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
//        setSupportActionBar(toolbar);


        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        patientId = sharedPreferences.getString(Constant.user_id, "");
        userToken = sharedPreferences.getString(Constant.token, "");
        user_token = sharedPreferences.getString(Constant.token, "");

        Log.d("credentials", "user id: " + patientId + " user token: " + userToken);

        preferenceManager = PreferenceManager.getInstance(this);

        calendar = (MaterialCalendarView) findViewById(R.id.calendarView);
        container = (LinearLayout) findViewById(R.id.container);

        textView = (TextView) findViewById(R.id.textView);
        textView1 = (TextView) findViewById(R.id.textView1);

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        innerMainLayout = (RelativeLayout) findViewById(R.id.innerMainLayout);
        innerLL = (LinearLayout) findViewById(R.id.innerLL);

        currentDay = CalendarDay.today();
        calendar.setSelectedDate(currentDay);


        currentDayInString = convertInString(currentDay);
        selectedDateInString = convertInString1(currentDay);

        toolbar_title.setText(currentDayInString);
//        Toast.makeText(this, "Current date is: " + currentDayInString, Toast.LENGTH_SHORT).show();


        currentDayDecorator = new DayViewDecorator() {   //Decorate the current day only
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                Calendar cal1 = day.getCalendar();
                Calendar cal2 = Calendar.getInstance();

                return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                        && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                        && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
            }

            @Override
            public void decorate(DayViewFacade view) {
                view.setBackgroundDrawable(ContextCompat.getDrawable(Appointments.this, R.drawable.selector));
            }
        };


//        new scheduleAPI(userId, userToken).execute();

        InitPendingJobRecyclerView();


        new appointmentsAPI(patientId, userToken).execute();

        currentDate = new Date();
        new daywiseAppointmentsAPI(patientId, userToken, dateFormatForAppointmentDay.format(currentDate)).execute();
        Log.d("formattedDate", dateFormatForDay.format(currentDate));


        calendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                new appointmentsAPI(patientId, userToken).execute();

                Log.d("patientDetails", "patientId: " + patientId + ", userToken: " + userToken);

                currentDayInString = convertInString(calendar.getSelectedDate());

                Log.d("calenderCurrentDate",currentDayInString+"");
                isPast = calendar.getSelectedDate().isBefore(currentDay);

                editor = sharedPreferences.edit();
                editor.putBoolean("isPast", isPast);
                editor.apply();
                editor.commit();

                if (isPast) {
                    item2.setVisible(false);
                } else {
                    item2.setVisible(true);
                }

                isSelected = calendar.getSelectedDate().equals(currentDay);

                if (!isSelected && !isCurrentDayDecoratorAdded) {
                    calendar.addDecorator(currentDayDecorator);
                    isCurrentDayDecoratorAdded = true;
                } else if (isSelected && isCurrentDayDecoratorAdded) {
                    calendar.removeDecorator(currentDayDecorator);
                    isCurrentDayDecoratorAdded = false;
                }


                selectedDateInString = convertInString1(date);
                Log.d("formattedDate", selectedDateInString);
                new daywiseAppointmentsAPI(patientId, userToken, selectedDateInString).execute();


//                calendar.state().edit()
//                        .setFirstDayOfWeek(Calendar.SUNDAY)
//                        .setMinimumDate(CalendarDay.from(calendar.getSelectedDate().getYear(), calendar.getSelectedDate().getMonth(), (calendar.getSelectedDate().getDay()-6)))
//                        .setMaximumDate(CalendarDay.from(calendar.getSelectedDate().getYear(), calendar.getSelectedDate().getMonth(), (calendar.getSelectedDate().getDay()+6)))
//                        .setCalendarDisplayMode(CalendarMode.WEEKS)
//                        .commit();
            }
        });

    }

    private void initilization() {
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fragment_main, menu);

        item1 = menu.findItem(R.id.hideShowButton);
        item2 = menu.findItem(R.id.action_add);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.hideShowButton) {

            if (titleToggle == 0) {


                calendar.animate()
                        .translationX(calendar.getWidth())
                        .setDuration(100)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                item1.setIcon(R.drawable.ic_calendar_show_white);
//                                item2.setIcon(R.drawable.ic_add_event);

                                toolbar_title.setText(currentDayInString);
//                                toolbar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.whiteColor, null));

                                calendar.setVisibility(View.GONE);
                            }
                        });

                titleToggle = 1;
                onPreHide = 0;
            } else {


                calendar.animate()
                        .translationX(0)
                        .alpha(1.0f)
                        .setDuration(20)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                calendar.setVisibility(View.VISIBLE);

                                item1.setIcon(R.drawable.ic_calendar_hide_white);
//                                item2.setIcon(R.drawable.ic_add_event_white);

                                toolbar_title.setText("My Appointments");
//                                toolbar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));
                            }
                        });

                titleToggle = 0;
                onPreHide = 0;
            }

            return true;
        } else if (id == R.id.action_add) {

//            new createAppointmentAPI(patientId, userToken, userId, selectedDateInString, startTime, endTime).execute();

            preferenceManager.setSelectedDate(selectedDateInString);
            preferenceManager.setSelectedDay(dateFormatForDay.format(currentDate));

            CreateAppointmentFragment createAppointmentFragment=new CreateAppointmentFragment();
            Bundle bundle=new Bundle();
            bundle.putString("date",selectedDateInString);
            createAppointmentFragment.setArguments(bundle);
            createAppointmentFragment.show(getSupportFragmentManager(),"createAppoinmentFragment");
            //fragmentTransaction.replace(R.id.container, new CreateAppointmentFragment());
//            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();

//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.container, new CreateAppointmentFragment()).commit();

        }

        return super.onOptionsItemSelected(item);
    }


    public class EventDecorator implements DayViewDecorator {

        int color;
        CalendarDay dates;


        public EventDecorator(int color, CalendarDay dates) {
            this.color = color;
            this.dates = dates;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates != null && day.equals(dates);
        }

        @Override
        public void decorate(DayViewFacade view) {

            view.addSpan(new DotSpan(8, color));
            view.setDaysDisabled(false);

        }
    }

    public class DisabledDayDecorator implements DayViewDecorator {

        CalendarDay dates;


        public DisabledDayDecorator(CalendarDay dates) {
            this.dates = dates;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates != null && day.equals(dates);
        }

        @Override
        public void decorate(DayViewFacade view) {

            view.setDaysDisabled(true);

        }
    }


    String convertInString(CalendarDay selectedDay) {

        if (selectedDay.getDay() < 10) {
            dayInString = "0" + selectedDay.getDay();
        } else {
            dayInString = String.valueOf(selectedDay.getDay());
        }

        switch (selectedDay.getMonth() + 1) {
            case 1:
                monthInString = "January";
                break;
            case 2:
                monthInString = "February";
                break;
            case 3:
                monthInString = "March";
                break;
            case 4:
                monthInString = "April";
                break;
            case 5:
                monthInString = "May";
                break;
            case 6:
                monthInString = "June";
                break;
            case 7:
                monthInString = "July";
                break;
            case 8:
                monthInString = "August";
                break;
            case 9:
                monthInString = "September";
                break;
            case 10:
                monthInString = "October";
                break;
            case 11:
                monthInString = "November";
                break;
            case 12:
                monthInString = "December";
                break;
        }

        yearInString = String.valueOf(selectedDay.getYear());

        dateInString = dayInString + " " + monthInString + " " + yearInString;

        return dateInString;
    }


    String convertInString1(CalendarDay selectedDay) {

        if (selectedDay.getDay() < 10) {
            dayInString = "0" + selectedDay.getDay();
        } else {
            dayInString = String.valueOf(selectedDay.getDay());
        }

        if ((selectedDay.getMonth() + 1) < 10) {
            monthInString = "0" + (selectedDay.getMonth() + 1);
        } else {
            monthInString = String.valueOf(selectedDay.getMonth() + 1);
        }

        yearInString = String.valueOf(selectedDay.getYear());

        dateInString = yearInString + "-" + monthInString + "-" + dayInString;

        return dateInString;
    }


    private void InitPendingJobRecyclerView() {

        rvPendingWorks.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(Appointments.this, LinearLayoutManager.VERTICAL, false);
        rvPendingWorks.setLayoutManager(mLayoutManager);


    }


    private class appointmentsAPI extends AsyncTask<String, String, String> {
        String userId, userToken;


        private appointmentsAPI(String userId, String userToken) {
            this.userId = userId;
            this.userToken = userToken;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

//

            mainLayout.setClickable(false);

            Log.d("error", "statusReport: onPreExecute e dhukse!!!");
            Log.d("patientDetails", "patientId: " + patientId + ", userToken: " + userToken);

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentsPatient");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("patient_id", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            ArrayList listDecor = new ArrayList();

            try {
                Log.d("jsonData", "+++++++++TEST" + result);
                JSONObject jsonObj = new JSONObject(result);
                JSONObject json = new JSONObject(jsonObj.getString("result"));


                if (json.getString("status").equals("success")) {
                    JSONArray scheduleArray = json.getJSONArray("appointments");

                    Log.d("array", "length of appointmentsArray: " + scheduleArray.length());


                    if (scheduleArray.length() != 0) {

                        int cou = 0;

                        for (int i = 0; i < scheduleArray.length(); i++) {

                            JSONObject scheduleJson = scheduleArray.getJSONObject(i);
                            Log.d("jsonData", "jsonData#" + i + "+++++++++" + scheduleJson);


                            AppointmentListModelClass appointments = new AppointmentListModelClass();
                            appointments.setDay(scheduleJson.getString("appointment_date"));
                            appointments.setStatus(scheduleJson.getString("status"));


                            if (!appointments.getStatus().equals("rejected")) {
                                cou++;
                                Log.d("cou", "value of cou: " + cou);

                                cal = new GregorianCalendar();
                                String dateStr = appointments.getDay();
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = format.parse(dateStr);
                                cal.setTime(date);
                                eventDay = CalendarDay.from(cal);
                                listDecor.add((new EventDecorator(Color.argb(255, 0, 255, 0), eventDay)));

                            }
                        }


                        calendar.addDecorators(listDecor);


                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }



            mainLayout.setClickable(true);
        }
    }


    private class daywiseAppointmentsAPI extends AsyncTask<String, String, String> {
        String userId, userToken, dateClickedByUser;


        private daywiseAppointmentsAPI(String userId, String userToken, String dateClickedByUser) {
            this.userId = userId;
            this.userToken = userToken;
            this.dateClickedByUser = dateClickedByUser;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("DATEFORMAT", "onPreExecute: "+dateClickedByUser);

            innerMainLayout.setClickable(false);

            Log.d("heyo", "statusReport: onPreExecute e dhukse!!!");
            Log.d("patientDetails", "patientId: " + userId + ", userToken: " + userToken);
            rvPendingWorks.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            textView1.setVisibility(View.VISIBLE);

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("heyo", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentListPatientDateWise");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("patient_id", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("appointment_date", dateClickedByUser));


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Latest util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("heyo", "statusReport: onPostExecute e dhukse!!!");

            try {
                Log.d("jsonData", "+++++++++TEST" + result);
                JSONObject jsonObj = new JSONObject(result);
                JSONObject json = new JSONObject(jsonObj.getString("result"));


                if (json.getString("status").equals("success")) {
                    textView.setVisibility(View.GONE);
                    textView1.setVisibility(View.GONE);

                    pendingJobItem.clear();

                    JSONArray scheduleArray = json.getJSONArray("appointments");

                    Log.d("array", "length of dateWiseArray: " + scheduleArray.length());


                    if (scheduleArray.length() != 0) {

                        Log.d("checker", "" + scheduleArray.length());

                        for (int i = 0; i < scheduleArray.length(); i++) {

                            JSONObject scheduleJson = scheduleArray.getJSONObject(i);
                            Log.d("appointmentDetails", "jsonData#" + i + "+++++++++" + scheduleJson);
                            JSONObject patientData = scheduleJson.getJSONObject("patient");
                            JSONObject patientProfileData = patientData.getJSONObject("profile");

                            JSONObject doctorData = scheduleJson.getJSONObject("doctor");
                            JSONObject doctorProfileData = doctorData.getJSONObject("profile");


                            appointmentDay = new AppointmentListModelClass();
                            appointmentDay.setAppointmentID(scheduleJson.getString("id"));
                            appointmentDay.setDoctorID(scheduleJson.getString("doctor_id"));
                            appointmentDay.setPatientID(scheduleJson.getString("patient_id"));
                            appointmentDay.setUserToken(user_token);
                            appointmentDay.setCreatorID(scheduleJson.getString("created_by"));
                            appointmentDay.setDay(scheduleJson.getString("appointment_date"));
                            appointmentDay.setStatus(scheduleJson.getString("status"));

                            if(appointmentDay.getStatus().equals("rejected")){
                                appointmentDay.setRejectorID(scheduleJson.getString("rejected_by"));
                            }

                            appointmentDay.setDoctorAvatar(doctorData.getString("avatar"));
                            appointmentDay.setDoctorEmail(doctorData.getString("email"));
                            appointmentDay.setDoctorContact(doctorData.getString("phone"));
                            appointmentDay.setDoctorFirstName(doctorProfileData.getString("first_name"));
                            appointmentDay.setDoctorLastName(doctorProfileData.getString("last_name"));
                            appointmentDay.setDoctorSpeciality(doctorProfileData.getString("speciality"));
                            appointmentDay.setDoctorRating(doctorProfileData.getDouble("rating"));
                            appointmentDay.setDoctorGender(doctorProfileData.getString("gender"));
                            appointmentDay.setReason(patientProfileData.getString("main_problem"));

                          /*  JSONObject upazillaData = doctorProfileData.getString("upazilla");
                            JSONObject districtData = doctorProfileData.getJSONObject("district");
                            appointmentDay.setAppointmentLocation(doctorProfileData.getString("address") + ", " + upazillaData.getString("name") + ", " + districtData.getString("name"));
*/

                            if (scheduleJson.getString("appointment_start_time").length() > 5) {

                                appointmentDay.setStartTime(scheduleJson.getString("appointment_start_time").substring(0, 5));

                                Log.d("komne", "hetha " + appointmentDay.getStartTime());


                            }


                            if (scheduleJson.getString("appointment_end_time").length() > 5) {

                                appointmentDay.setEndTime(scheduleJson.getString("appointment_end_time").substring(0, 5));

                                Log.d("komne", "hetha " + appointmentDay.getEndTime());

                            }


                            pendingJobItem.add(appointmentDay);
                        }


                        adapter = new AppointmentListAdapter(Appointments.this, pendingJobItem);
                        rvPendingWorks.setItemAnimator(new DefaultItemAnimator());
                        rvPendingWorks.setAdapter(adapter);

                        rvPendingWorks.setVisibility(View.VISIBLE);


                    } else {

                        textView.setVisibility(View.VISIBLE);
                    }


                }

                innerMainLayout.setClickable(true);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }
}

