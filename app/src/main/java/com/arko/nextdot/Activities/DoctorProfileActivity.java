package com.arko.nextdot.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.DoctorCertificationAdapter;
import com.arko.nextdot.Adapters.DoctorCoursesAdapter;
import com.arko.nextdot.Adapters.DoctorEducationAdapter;
import com.arko.nextdot.Adapters.DoctorExperienceAdapter;
import com.arko.nextdot.Adapters.DoctorOfficeHourAdapter;
import com.arko.nextdot.Adapters.DoctorPublicationAdapter;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Certificate;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Courses;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.DoctorProfileRoot;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.DoctorRatingByPatient;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Education;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Experience;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Officehour;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Publication;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorProfileActivity extends ToolbarBaseActivity {

    RetrofitInterface retrofitInterface ;
    SharedPreferences sharedPreferences ;
    String user_id, token ;
    String doc_review_by_user = "" ;
    ProgressDialog progressDialog ;
    CircleImageView doctorpic ;
    TextView tv_doc_profile_doc_name, tv_current_designation, tv_current_organization, rate_this_doc ;
    String doc_first_name, doc_last_name, doc_cur_designation, doc_cur_origanization, doc_pro_pic ;
    RatingBar ratingBar ;
    float doc_cur_rating ;
    int doc_rating_by_user = 1 ;
    String doctor_id ;

    List<Publication> publicationlist ;
    List<Courses> courseslist ;
    List<Education> educationlist ;
    List<Officehour> officelist ;
    List<Certificate> certificatelist ;
    List<Experience> experiencelist ;
    List<String> list ;


    RecyclerView doctorOfficeHourRecyler, doctorExperienceRecyler, doctorEducationRecyler, doctorCoursesRecyler,
            doctorPublicationRecyler, doctorCertificationRecyler ;
    LinearLayoutManager OfficeHourlayoutManager, ExperiencelayoutManager, EducationlayoutManager, CourseslayoutManager,
            PublicationlayoutManager, CertificationlayoutManager;

    DoctorPublicationAdapter doctorPublicationAdapter ;
    DoctorCoursesAdapter doctorCoursesAdapter ;
    DoctorEducationAdapter doctorEducationAdapter ;
    DoctorOfficeHourAdapter doctorOfficeHourAdapter ;
    DoctorCertificationAdapter doctorCertificationAdapter ;
    DoctorExperienceAdapter doctorExperienceAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);
        Intent intent = this.getIntent();
        doctor_id = intent.getStringExtra("id");
        Log.d("+++doctor_id++", doctor_id+"") ;

        Initilization();
        toolbarwithoutdrawer("Doctor Profile");

        sharedPreferences  = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString(Constant.user_id, "") ;
        token = sharedPreferences.getString(Constant.token, "") ;

        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        String url = Constant.ROOT_URL+"api/doctor/profile" ;
        Log.d("+++URL+++", url) ;

        Call<DoctorProfileRoot> call = retrofitInterface.getDoctorProfile(url, doctor_id, token, user_id) ;

        //list.add("profile/1500281638samim.jpg");
        setProgressDialog();
        call.enqueue(new Callback<DoctorProfileRoot>() {
            @Override
            public void onResponse(Call<DoctorProfileRoot> call, Response<DoctorProfileRoot> response) {

                progressDialog.dismiss();
//                String username = response.body().getUsername();
                Log.d("+++CONNECTION++", "ashche") ;
//                Log.d("+++USERNAME++", username+"") ;
                doc_cur_rating = Float.parseFloat(response.body().getRating());

                /*if(response.body().getIsReviewed() == 0 && response.body().getAppointmentTaken() == 1){
                    rate_this_doc.setVisibility(View.VISIBLE);
                    rate_this_doc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialougeRatingAction();
                        }
                    });
                }*/

                if(response.body().getAppointmentTaken() == 1){
                    rate_this_doc.setVisibility(View.VISIBLE);
                    rate_this_doc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialougeRatingAction();
                        }
                    });
                }

                /*rate_this_doc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialougeRatingAction();
                    }
                });*/

                if(response.body().getAvatar() != null){

                    doc_pro_pic = response.body().getAvatar() ;
                    //list.add(doc_pro_pic);
                    Log.d("pro pic", doc_pro_pic) ;
                }
                else{
                    doc_pro_pic = "" ;
                    //list.add(doc_pro_pic);
                    Log.d("pro pic", doc_pro_pic) ;
                }



                if(response.body().getProfile() != null){

                    if(response.body().getProfile().getFirstName() != null){
                        doc_first_name = response.body().getProfile().getFirstName() ;
                        //list.add(doc_first_name);
                    }
                    else{
                        doc_first_name = "" ;
                        //list.add(doc_first_name);
                    }

                    if(response.body().getProfile().getLastName() != null){
                        doc_last_name = response.body().getProfile().getLastName() ;
                        //list.add(doc_last_name);
                    }
                    else{
                        doc_last_name = "" ;
                        //list.add(doc_last_name);
                    }

                    if(response.body().getProfile().getProfession() != null){

                        doc_cur_designation = response.body().getProfile().getProfession() ;
                        //list.add(doc_cur_designation);
                        Log.d("doc_cur_designation", doc_cur_designation) ;
                    }
                    else{
                        doc_cur_designation = "" ;
                        Log.d("doc_cur_designation", "dgsfhh") ;
                        //list.add(doc_cur_designation);
                    }

                    if(response.body().getProfile().getOrganization() != null){

                        doc_cur_origanization = response.body().getProfile().getOrganization() ;
                        //list.add(doc_cur_origanization);
                        Log.d("doc_cur_organization", doc_cur_origanization) ;

                    }
                    else{
                        doc_cur_origanization = "" ;
                        //list.add(doc_cur_origanization);
                        Log.d("doc_cur_organization", "dsgdfhjdghfs") ;

                    }
                }
                if(response.body().getOfficehours() != null){
                    officelist = response.body().getOfficehours() ;
                    doctorOfficeHourAdapter = new DoctorOfficeHourAdapter(getApplicationContext(), officelist) ;
                    doctorOfficeHourRecyler.setAdapter(doctorOfficeHourAdapter);
                }
                if(response.body().getCertificates() != null){

                    certificatelist = response.body().getCertificates() ;
                    doctorCertificationAdapter = new DoctorCertificationAdapter(getApplicationContext(), certificatelist) ;
                    doctorCertificationRecyler.setAdapter(doctorCertificationAdapter);
                }
                if(response.body().getEducations() != null){

                    educationlist = response.body().getEducations() ;
                    doctorEducationAdapter = new DoctorEducationAdapter(getApplicationContext(), educationlist) ;
                    doctorEducationRecyler.setAdapter(doctorEducationAdapter);
                }
                if(response.body().getCourses() != null){

                    courseslist = response.body().getCourses() ;
                    doctorCoursesAdapter = new DoctorCoursesAdapter(getApplicationContext(), courseslist) ;
                    doctorCoursesRecyler.setAdapter(doctorCoursesAdapter);
                }
                if(response.body().getExperiences() != null){

                    experiencelist = response.body().getExperiences() ;
                    doctorExperienceAdapter = new DoctorExperienceAdapter(getApplicationContext(), experiencelist) ;
                    doctorExperienceRecyler.setAdapter(doctorExperienceAdapter);
                }
                if(response.body().getPublications() != null){

                    publicationlist = response.body().getPublications() ;
                    doctorPublicationAdapter = new DoctorPublicationAdapter(getApplicationContext(), publicationlist) ;
                    doctorPublicationRecyler.setAdapter(doctorPublicationAdapter);
                }
                setData(doc_pro_pic, doc_first_name, doc_last_name, doc_cur_designation, doc_cur_origanization, doc_cur_rating);
            }

            @Override
            public void onFailure(Call<DoctorProfileRoot> call, Throwable t) {
                Log.d("+++CONNECTION++", "ashe nai") ;
                Log.d("+++IOException++", String.valueOf(t)) ;
                Toast.makeText(DoctorProfileActivity.this, "Network Problem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(DoctorProfileActivity.this) ;
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void setProgressDialogDismiss(){
        progressDialog.dismiss();
    }


    private void Initilization(){
        doctorpic = (CircleImageView) findViewById(R.id.doc_profile_doc_img) ;
        tv_doc_profile_doc_name = (TextView) findViewById(R.id.tv_doc_profile_doc_name);
        tv_current_designation = (TextView) findViewById(R.id.tv_current_designation) ;
        tv_current_organization = (TextView) findViewById(R.id.tv_current_organization) ;

        doctorOfficeHourRecyler = (RecyclerView) findViewById(R.id.recyclerViewOfficehour);
        doctorExperienceRecyler = (RecyclerView) findViewById(R.id.recyclerViewExperience) ;
        doctorEducationRecyler = (RecyclerView) findViewById(R.id.recyclerViewEducation) ;
        doctorCoursesRecyler = (RecyclerView) findViewById(R.id.recyclerViewCourses);
        doctorPublicationRecyler = (RecyclerView) findViewById(R.id.recyclerViewPublication) ;
        doctorCertificationRecyler = (RecyclerView) findViewById(R.id.recyclerViewCertification) ;

        OfficeHourlayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        ExperiencelayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        EducationlayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        CourseslayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        PublicationlayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        CertificationlayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false); ;


        doctorOfficeHourRecyler.setLayoutManager(OfficeHourlayoutManager);
        doctorExperienceRecyler.setLayoutManager(ExperiencelayoutManager);
        doctorEducationRecyler.setLayoutManager(EducationlayoutManager);
        doctorCoursesRecyler.setLayoutManager(CourseslayoutManager);
        doctorPublicationRecyler.setLayoutManager(PublicationlayoutManager);
        doctorCertificationRecyler.setLayoutManager(CertificationlayoutManager);

        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        rate_this_doc = (TextView) findViewById(R.id.rateDoctor) ;
    }

    private void setData(String doc_pro_pic, String doc_first_name, String doc_last_name,
                         String doc_cur_designation, String doc_cur_origanization, float doc_cur_rating){

        if(Patterns.WEB_URL.matcher(doc_pro_pic).matches()){

            Glide.with(DoctorProfileActivity.this).load(doc_pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(doctorpic);
        }
        else{
            Glide.with(DoctorProfileActivity.this).load(Constant.ROOT_URL+doc_pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(doctorpic);
        }

        //Glide.with(DoctorProfileActivity.this).load(Constant.ROOT_URL+doc_pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(doctorpic);
        tv_doc_profile_doc_name.setText(doc_first_name + " " + doc_last_name);
        tv_current_designation.setText(doc_cur_designation);
        tv_current_organization.setText(doc_cur_origanization);
        ratingBar.setRating(doc_cur_rating);
    }

    private void dialougeRatingAction() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialouge_doctor_rating);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT ;

        dialog.show();
        dialog.getWindow().setAttributes(lp);

        final ImageView star_1, star_2, star_3, star_4, star_5 ;
        final EditText review ;
        TextView cancle, apply ;

        star_1 = (ImageView) dialog.findViewById(R.id.star_1);
        star_2 = (ImageView) dialog.findViewById(R.id.star_2);
        star_3 = (ImageView) dialog.findViewById(R.id.star_3);
        star_4 = (ImageView) dialog.findViewById(R.id.star_4);
        star_5 = (ImageView) dialog.findViewById(R.id.star_5);
        review = (EditText) dialog.findViewById(R.id.give_a_review) ;
        cancle = (TextView) dialog.findViewById(R.id.cancle_review) ;
        apply = (TextView) dialog.findViewById(R.id.apply_review) ;



        //Log.d("doctor_Review", doc_review_by_user) ;

        star_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star_1.setImageResource(R.drawable.ic_star_golden);
                star_2.setImageResource(R.drawable.ic_star);
                star_3.setImageResource(R.drawable.ic_star);
                star_4.setImageResource(R.drawable.ic_star);
                star_5.setImageResource(R.drawable.ic_star);
                doc_rating_by_user = 1 ;
            }
        });

        star_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star_1.setImageResource(R.drawable.ic_star_golden);
                star_2.setImageResource(R.drawable.ic_star_golden);
                star_3.setImageResource(R.drawable.ic_star);
                star_4.setImageResource(R.drawable.ic_star);
                star_5.setImageResource(R.drawable.ic_star);
                doc_rating_by_user = 2 ;
            }
        });

        star_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star_1.setImageResource(R.drawable.ic_star_golden);
                star_2.setImageResource(R.drawable.ic_star_golden);
                star_3.setImageResource(R.drawable.ic_star_golden);
                star_4.setImageResource(R.drawable.ic_star);
                star_5.setImageResource(R.drawable.ic_star);
                doc_rating_by_user = 3 ;
            }
        });

        star_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star_1.setImageResource(R.drawable.ic_star_golden);
                star_2.setImageResource(R.drawable.ic_star_golden);
                star_3.setImageResource(R.drawable.ic_star_golden);
                star_4.setImageResource(R.drawable.ic_star_golden);
                star_5.setImageResource(R.drawable.ic_star);
                doc_rating_by_user = 4 ;
            }
        });

        star_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                star_1.setImageResource(R.drawable.ic_star_golden);
                star_2.setImageResource(R.drawable.ic_star_golden);
                star_3.setImageResource(R.drawable.ic_star_golden);
                star_4.setImageResource(R.drawable.ic_star_golden);
                star_5.setImageResource(R.drawable.ic_star_golden);
                doc_rating_by_user = 5 ;
            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        final String url = Constant.ROOT_URL+"api/review/create/"+doctor_id ;

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doc_review_by_user = review.getText().toString().trim() ;
                Log.d("doctor_Review", doc_review_by_user) ;

                Call<DoctorRatingByPatient> call = retrofitInterface.postDoctorRatingReview(url, doc_rating_by_user, doc_review_by_user, user_id, token) ;
                call.enqueue(new Callback<DoctorRatingByPatient>() {
                    @Override
                    public void onResponse(Call<DoctorRatingByPatient> call, Response<DoctorRatingByPatient> response) {
                        Log.d("+++CONNECTION++", "ashche") ;
                        String result = response.body().getStatus();
                        if(result.equals("success")){
                            Log.d("+++success++", "YESSSS") ;
                            Intent intent = new Intent(DoctorProfileActivity.this, DoctorProfileActivity.class) ;
                            intent.putExtra("id", doctor_id) ;
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<DoctorRatingByPatient> call, Throwable t) {
                        Log.d("+++CONNECTION++", "ashe nai") ;
                        Log.d("+++IOException++", String.valueOf(t)) ;
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
