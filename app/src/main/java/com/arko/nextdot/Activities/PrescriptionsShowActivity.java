package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arko.nextdot.Adapters.PrescriptionsShowAdapter;
import com.arko.nextdot.Fragment.FragmentShowReportNprescriptionDialog;
import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.Model.RetrofitModel.PrescriptionItem;
import com.arko.nextdot.R;
import com.arko.nextdot.ShadowTransformer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrescriptionsShowActivity extends ToolbarBaseActivity implements ViewPager.OnPageChangeListener ,PrescriptionsShowAdapter.ShowReportAndPrescriptoin{

    @BindView(R.id.prescriptionshowDotsLayout)
    LinearLayout prescriptionshowDotsLayout;
    /*@BindView(R.id.prescriptionShowSkip_btn)
    Button prescriptionShowSkipBtn;
    @BindView(R.id.prescriptionShowNext_btn)
    Button prescriptionShowNextBtn;*/
    @BindView(R.id.prescriptionShowviewpager)
    ViewPager prescriptionShowviewpager;
    @BindView(R.id.backimageview)
    ImageView backimageview;
    private PrescriptionsShowAdapter prescriptionsNreportsShowAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private int[] img;
    private ImageView[] dots;
    private int dotsCount;

    List<PrescriptionItem> prescriptionItems;
    List<MedicalReportsItem> reportsItems;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptions_nreports_show);
        ButterKnife.bind(this);
        prescriptionsNreportsShowAdapter = new PrescriptionsShowAdapter(getApplicationContext(),this);

        prescriptionShowviewpager.setOnPageChangeListener(this);


        prescriptionItems = getIntent().getParcelableArrayListExtra("datalist");
        for (int i = 0; i < prescriptionItems.size(); i++) {
            prescriptionsNreportsShowAdapter.addCardItem(prescriptionItems.get(i));
        }



        Log.e("datalist", prescriptionItems.size() + " " + prescriptionItems.get(0).getImageUrl());


        // img = new int[]{R.drawable.blue_wlakthrough_background, R.drawable.green_walkthrough_background, R.drawable.purple_walkthrough_background};



        mCardShadowTransformer = new ShadowTransformer(prescriptionShowviewpager, prescriptionsNreportsShowAdapter);

        prescriptionShowviewpager.setAdapter(prescriptionsNreportsShowAdapter);
        prescriptionShowviewpager.setPageTransformer(false, mCardShadowTransformer);
        prescriptionShowviewpager.setOffscreenPageLimit(3);
        int tmp = prescriptionShowviewpager.getCurrentItem();

        Log.d("TAGGGG : ", String.valueOf(tmp));
        setUiPageViewController();
        backimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
/*
        prescriptionShowNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prescriptionShowviewpager.getCurrentItem() == img.length - 1){
                    Intent intent = new Intent(getApplicationContext(), RegistrationFromDoctor.class) ;
                    startActivity(intent);
                }
                else{
                    prescriptionShowviewpager.setCurrentItem(prescriptionShowviewpager.getCurrentItem()+1, true);

                    if(prescriptionShowviewpager.getCurrentItem() == img.length - 1){
                        prescriptionShowNextBtn.setText("Finish");
                        Log.d("tag", "!!!!!!") ;
                    }
                }
            }
        });
*/

    }

    private void setUiPageViewController() {

        dotsCount = prescriptionsNreportsShowAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.filled_ash_circle));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            prescriptionshowDotsLayout.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.filled_blue_circle));
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.filled_ash_circle));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.filled_blue_circle));

   /*     if (position == (img.length - 1)) {
            prescriptionShowNextBtn.setText("Finish");
        } else {
            prescriptionShowNextBtn.setText("Next");
        }*/
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void show(String imageurl) {
        FragmentShowReportNprescriptionDialog fragmentShowReportNprescriptionDialog=new FragmentShowReportNprescriptionDialog();
        Bundle args = new Bundle();
        args.putString("imageurl", imageurl);
        fragmentShowReportNprescriptionDialog.setArguments(args);
        FragmentManager fragmentmanager=getSupportFragmentManager();
        fragmentShowReportNprescriptionDialog.show(fragmentmanager,"show dialog");
    }


}
