package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arko.nextdot.Adapters.MedicineListAdapter;
import com.arko.nextdot.Model.MedicineListModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class MedicineListActivity extends ToolbarBaseActivity {

    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_list);
        toolbarwithoutdrawer("Medicine List");

        RecyclerView medicine_recyclerView = (RecyclerView) findViewById(R.id.medicine_list_recyler);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        MedicineListAdapter myAdapter = new MedicineListAdapter(this, getData()) ;
        medicine_recyclerView.setAdapter(myAdapter);
        medicine_recyclerView.setLayoutManager(layoutManager);
    }

    public static List<MedicineListModel> getData(){

        List<MedicineListModel> list = new ArrayList<>() ;
        String[] medicine_name = {"RINOSTAT PLUS", "CIROCIN", "RINOSTAT PLUS", "CIROCIN"} ;
        String[] medicine_price = {"RS. 13.70 per 6 tablet", "RS. 130.70 per 6 tablet", "RS. 243.70 per 20 tablet", "RS. 13.70 per 6 tablet"} ;

        for(int i = 0 ; i < medicine_name.length ; i++){
            MedicineListModel medicineListModel = new MedicineListModel() ;
            medicineListModel.setMedicine_name(medicine_name[i]);
            medicineListModel.setMedicine_price(medicine_price[i]);
            list.add(medicineListModel);
        }
        return list ;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
