package com.arko.nextdot.Activities;

import android.os.Bundle;

import com.arko.nextdot.R;


public class TestReportActivity extends ToolbarBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_report);
        toolbarwithoutdrawer("Test Report");
    }
}
