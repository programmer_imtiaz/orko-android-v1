package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arko.nextdot.R;



public class VerifyMobileActivity extends AppCompatActivity {

    Toolbar toolbar ;
    TextView toolbar_title ;
    Button btn_verify_submit ;
    EditText enter_otp ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile);
        initilization();
        setSupportActionBar(toolbar);
        showCustomToolbar();
        toolbar_title.setText("Verify Mobile");

        btn_verify_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AfterLoginHomeActivity.class) ;
                startActivity(intent);
            }
        });
    }

    private void initilization(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        btn_verify_submit = (Button) findViewById(R.id.verify_submit);
        enter_otp = (EditText) findViewById(R.id.enter_otp);
    }

    private void showCustomToolbar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
