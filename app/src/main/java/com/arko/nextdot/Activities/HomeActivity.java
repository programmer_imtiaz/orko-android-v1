package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.arko.nextdot.R;


public class HomeActivity extends AppCompatActivity {

    Button login_btn, register_btn ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE) ;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);
        initilization();

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class) ;
                startActivity(intent);
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class) ;
                startActivity(intent);
            }
        });
    }

    private void initilization(){
        login_btn = (Button) findViewById(R.id.go_for_login);
        register_btn = (Button) findViewById(R.id.go_for_signup) ;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
