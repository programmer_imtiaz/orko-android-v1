package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Interface.QuestionaryView;
import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.QuestionaryPresenter;
import com.arko.nextdot.R;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakib on 10/24/2017.
 */

public class QuestionaryActivity extends ToolbarBaseActivity implements QuestionaryView {


    @BindView(R.id.shoulder_qus_1_down)
    ImageView shoulderQus1Down;
    @BindView(R.id.shoulder_qus_1_layout)
    LinearLayout shoulderQus1Layout;
    @BindView(R.id.shoulder_qus_1_radio_yes)
    RadioButton shoulderQus1RadioYes;
    @BindView(R.id.shoulder_qus_1_radio_no)
    RadioButton shoulderQus1RadioNo;
    @BindView(R.id.shoulder_qus_1_radio_group)
    RadioGroup shoulderQus1RadioGroup;
    @BindView(R.id.shoulder_qus_1_expand_layout)
    ExpandableLinearLayout shoulderQus1ExpandLayout;
    @BindView(R.id.shoulder_qus_2_down)
    ImageView shoulderQus2Down;
    @BindView(R.id.shoulder_qus_2_layout)
    LinearLayout shoulderQus2Layout;
    @BindView(R.id.shoulder_qus_2_radio_yes)
    RadioButton shoulderQus2RadioYes;
    @BindView(R.id.shoulder_qus_2_radio_no)
    RadioButton shoulderQus2RadioNo;
    @BindView(R.id.shoulder_qus_2_radio_group)
    RadioGroup shoulderQus2RadioGroup;
    @BindView(R.id.shoulder_qus_2_expand_layout)
    ExpandableLinearLayout shoulderQus2ExpandLayout;
    @BindView(R.id.shoulder_qus_3_down)
    ImageView shoulderQus3Down;
    @BindView(R.id.shoulder_qus_3_layout)
    LinearLayout shoulderQus3Layout;
    @BindView(R.id.shoulder_qus_3_edittext)
    EditText shoulderQus3Edittext;
    @BindView(R.id.shoulder_qus_3_expand_layout)
    ExpandableLinearLayout shoulderQus3ExpandLayout;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.shoulder_qus_4_down)
    ImageView shoulderQus4Down;
    @BindView(R.id.shoulder_qus_4_layout)
    LinearLayout shoulderQus4Layout;
    @BindView(R.id.shoulder_qus_4_radio_yes)
    RadioButton shoulderQus4RadioYes;
    @BindView(R.id.shoulder_qus_4_radio_no)
    RadioButton shoulderQus4RadioNo;
    @BindView(R.id.shoulder_qus_4_radio_group)
    RadioGroup shoulderQus4RadioGroup;
    @BindView(R.id.shoulder_qus_4_expand_layout)
    ExpandableLinearLayout shoulderQus4ExpandLayout;
    @BindView(R.id.shoulder_qus_5_down)
    ImageView shoulderQus5Down;
    @BindView(R.id.shoulder_qus_5_layout)
    LinearLayout shoulderQus5Layout;
    @BindView(R.id.shoulder_qus_5_radio_yes)
    RadioButton shoulderQus5RadioYes;
    @BindView(R.id.shoulder_qus_5_radio_no)
    RadioButton shoulderQus5RadioNo;
    @BindView(R.id.shoulder_qus_5_radio_group)
    RadioGroup shoulderQus5RadioGroup;
    @BindView(R.id.shoulder_qus_5_expand_layout)
    ExpandableLinearLayout shoulderQus5ExpandLayout;
    @BindView(R.id.shoulder_qus_6_down)
    ImageView shoulderQus6Down;
    @BindView(R.id.shoulder_qus_6_layout)
    LinearLayout shoulderQus6Layout;
    @BindView(R.id.shoulder_qus_6_edittext)
    EditText shoulderQus6Edittext;
    @BindView(R.id.shoulder_qus_6_expand_layout)
    ExpandableLinearLayout shoulderQus6ExpandLayout;
    @BindView(R.id.shoulder_qus_7_down)
    ImageView shoulderQus7Down;
    @BindView(R.id.shoulder_qus_7_layout)
    LinearLayout shoulderQus7Layout;
    @BindView(R.id.shoulder_qus_7_seekBar)
    SeekBar shoulderQus7SeekBar;
    @BindView(R.id.shoulder_qus_7_seekBar_progress_text)
    TextView shoulderQus7SeekBarProgressText;
    @BindView(R.id.shoulder_qus_7_expand_layout)
    ExpandableLinearLayout shoulderQus7ExpandLayout;
    @BindView(R.id.shoulder_qus_8_down)
    ImageView shoulderQus8Down;
    @BindView(R.id.shoulder_qus_8_layout)
    LinearLayout shoulderQus8Layout;
    @BindView(R.id.shoulder_qus_8_radio_yes)
    RadioButton shoulderQus8RadioYes;
    @BindView(R.id.shoulder_qus_8_radio_no)
    RadioButton shoulderQus8RadioNo;
    @BindView(R.id.shoulder_qus_8_radio_group)
    RadioGroup shoulderQus8RadioGroup;
    @BindView(R.id.shoulder_qus_8_expand_layout)
    ExpandableLinearLayout shoulderQus8ExpandLayout;
    @BindView(R.id.shoulder_qus_9_down)
    ImageView shoulderQus9Down;
    @BindView(R.id.shoulder_qus_9_layout)
    LinearLayout shoulderQus9Layout;
    @BindView(R.id.shoulder_qus_9_seekBar)
    SeekBar shoulderQus9SeekBar;
    @BindView(R.id.shoulder_qus_9_seekBar_progress_text)
    TextView shoulderQus9SeekBarProgressText;
    @BindView(R.id.shoulder_qus_9_expand_layout)
    ExpandableLinearLayout shoulderQus9ExpandLayout;
    @BindView(R.id.shoulder_qus_10_down)
    ImageView shoulderQus10Down;
    @BindView(R.id.shoulder_qus_10_layout)
    LinearLayout shoulderQus10Layout;
    @BindView(R.id.shoulder_qus_10_radio_yes)
    RadioButton shoulderQus10RadioYes;
    @BindView(R.id.shoulder_qus_10_radio_no)
    RadioButton shoulderQus10RadioNo;
    @BindView(R.id.shoulder_qus_10_radio_group)
    RadioGroup shoulderQus10RadioGroup;
    @BindView(R.id.shoulder_qus_10_expand_layout)
    ExpandableLinearLayout shoulderQus10ExpandLayout;
    @BindView(R.id.elbow_qus_1_down)
    ImageView elbowQus1Down;
    @BindView(R.id.elbow_qus_1_layout)
    LinearLayout elbowQus1Layout;
    @BindView(R.id.elbow_qus_1_radio_yes)
    RadioButton elbowQus1RadioYes;
    @BindView(R.id.elbow_qus_1_radio_no)
    RadioButton elbowQus1RadioNo;
    @BindView(R.id.elbow_qus_1_radio_group)
    RadioGroup elbowQus1RadioGroup;
    @BindView(R.id.elbow_qus_1_expand_layout)
    ExpandableLinearLayout elbowQus1ExpandLayout;
    @BindView(R.id.elbow_qus_2_down)
    ImageView elbowQus2Down;
    @BindView(R.id.elbow_qus_2_layout)
    LinearLayout elbowQus2Layout;
    @BindView(R.id.elbow_qus_2_radio_yes)
    RadioButton elbowQus2RadioYes;
    @BindView(R.id.elbow_qus_2_radio_no)
    RadioButton elbowQus2RadioNo;
    @BindView(R.id.elbow_qus_2_radio_group)
    RadioGroup elbowQus2RadioGroup;
    @BindView(R.id.elbow_qus_2_expand_layout)
    ExpandableLinearLayout elbowQus2ExpandLayout;
    @BindView(R.id.elbow_qus_3_down)
    ImageView elbowQus3Down;
    @BindView(R.id.elbow_qus_3_layout)
    LinearLayout elbowQus3Layout;
    @BindView(R.id.elbow_qus_3_edittext)
    EditText elbowQus3Edittext;
    @BindView(R.id.elbow_qus_3_expand_layout)
    ExpandableLinearLayout elbowQus3ExpandLayout;
    @BindView(R.id.elbow_qus_4_down)
    ImageView elbowQus4Down;
    @BindView(R.id.elbow_qus_4_layout)
    LinearLayout elbowQus4Layout;
    @BindView(R.id.elbow_qus_4_radio_yes)
    RadioButton elbowQus4RadioYes;
    @BindView(R.id.elbow_qus_4_radio_no)
    RadioButton elbowQus4RadioNo;
    @BindView(R.id.elbow_qus_4_radio_group)
    RadioGroup elbowQus4RadioGroup;
    @BindView(R.id.elbow_qus_4_expand_layout)
    ExpandableLinearLayout elbowQus4ExpandLayout;
    @BindView(R.id.elbow_qus_5_down)
    ImageView elbowQus5Down;
    @BindView(R.id.elbow_qus_5_layout)
    LinearLayout elbowQus5Layout;
    @BindView(R.id.elbow_qus_5_radio_yes)
    RadioButton elbowQus5RadioYes;
    @BindView(R.id.elbow_qus_5_radio_no)
    RadioButton elbowQus5RadioNo;
    @BindView(R.id.elbow_qus_5_radio_group)
    RadioGroup elbowQus5RadioGroup;
    @BindView(R.id.elbow_qus_5_expand_layout)
    ExpandableLinearLayout elbowQus5ExpandLayout;
    @BindView(R.id.elbow_qus_6_down)
    ImageView elbowQus6Down;
    @BindView(R.id.elbow_qus_6_layout)
    LinearLayout elbowQus6Layout;
    @BindView(R.id.elbow_qus_6_edittext)
    EditText elbowQus6Edittext;
    @BindView(R.id.elbow_qus_6_expand_layout)
    ExpandableLinearLayout elbowQus6ExpandLayout;
    @BindView(R.id.elbow_qus_7_down)
    ImageView elbowQus7Down;
    @BindView(R.id.elbow_qus_7_layout)
    LinearLayout elbowQus7Layout;
    @BindView(R.id.elbow_qus_7_seekBar)
    SeekBar elbowQus7SeekBar;
    @BindView(R.id.elbow_qus_7_seekBar_progress_text)
    TextView elbowQus7SeekBarProgressText;
    @BindView(R.id.elbow_qus_7_expand_layout)
    ExpandableLinearLayout elbowQus7ExpandLayout;
    @BindView(R.id.elbow_qus_8_down)
    ImageView elbowQus8Down;
    @BindView(R.id.elbow_qus_8_layout)
    LinearLayout elbowQus8Layout;
    @BindView(R.id.elbow_qus_8_radio_yes)
    RadioButton elbowQus8RadioYes;
    @BindView(R.id.elbow_qus_8_radio_no)
    RadioButton elbowQus8RadioNo;
    @BindView(R.id.elbow_qus_8_radio_group)
    RadioGroup elbowQus8RadioGroup;
    @BindView(R.id.elbow_qus_8_expand_layout)
    ExpandableLinearLayout elbowQus8ExpandLayout;
    @BindView(R.id.elbow_qus_9_down)
    ImageView elbowQus9Down;
    @BindView(R.id.elbow_qus_9_layout)
    LinearLayout elbowQus9Layout;
    @BindView(R.id.elbow_qus_9_seekBar)
    SeekBar elbowQus9SeekBar;
    @BindView(R.id.elbow_qus_9_seekBar_progress_text)
    TextView elbowQus9SeekBarProgressText;
    @BindView(R.id.elbow_qus_9_expand_layout)
    ExpandableLinearLayout elbowQus9ExpandLayout;
    @BindView(R.id.elbow_qus_10_down)
    ImageView elbowQus10Down;
    @BindView(R.id.elbow_qus_10_layout)
    LinearLayout elbowQus10Layout;
    @BindView(R.id.elbow_qus_10_seekBar)
    SeekBar elbowQus10SeekBar;
    @BindView(R.id.elbow_qus_10_seekBar_progress_text)
    TextView elbowQus10SeekBarProgressText;
    @BindView(R.id.elbow_qus_10_expand_layout)
    ExpandableLinearLayout elbowQus10ExpandLayout;
    @BindView(R.id.elbow_qus_11_down)
    ImageView elbowQus11Down;
    @BindView(R.id.elbow_qus_11_layout)
    LinearLayout elbowQus11Layout;
    @BindView(R.id.elbow_qus_11_seekBar)
    SeekBar elbowQus11SeekBar;
    @BindView(R.id.elbow_qus_11_seekBar_progress_text)
    TextView elbowQus11SeekBarProgressText;
    @BindView(R.id.elbow_qus_11_expand_layout)
    ExpandableLinearLayout elbowQus11ExpandLayout;
    @BindView(R.id.elbow_qus_12_down)
    ImageView elbowQus12Down;
    @BindView(R.id.elbow_qus_12_layout)
    LinearLayout elbowQus12Layout;
    @BindView(R.id.elbow_qus_12_seekBar)
    SeekBar elbowQus12SeekBar;
    @BindView(R.id.elbow_qus_12_seekBar_progress_text)
    TextView elbowQus12SeekBarProgressText;
    @BindView(R.id.elbow_qus_12_expand_layout)
    ExpandableLinearLayout elbowQus12ExpandLayout;
    @BindView(R.id.elbow_qus_13_down)
    ImageView elbowQus13Down;
    @BindView(R.id.elbow_qus_13_layout)
    LinearLayout elbowQus13Layout;
    @BindView(R.id.elbow_qus_13_seekBar)
    SeekBar elbowQus13SeekBar;
    @BindView(R.id.elbow_qus_13_seekBar_progress_text)
    TextView elbowQus13SeekBarProgressText;
    @BindView(R.id.elbow_qus_13_expand_layout)
    ExpandableLinearLayout elbowQus13ExpandLayout;
    @BindView(R.id.keen_qus_1_down)
    ImageView keenQus1Down;
    @BindView(R.id.keen_qus_1_layout)
    LinearLayout keenQus1Layout;
    @BindView(R.id.keen_qus_1_radio_never)
    RadioButton keenQus1RadioNever;
    @BindView(R.id.keen_qus_1_radio_monthly)
    RadioButton keenQus1RadioMonthly;
    @BindView(R.id.keen_qus_1_radio_weekly)
    RadioButton keenQus1RadioWeekly;
    @BindView(R.id.keen_qus_1_radio_daily)
    RadioButton keenQus1RadioDaily;
    @BindView(R.id.keen_qus_1_radio_always)
    RadioButton keenQus1RadioAlways;
    @BindView(R.id.keen_qus_1_radio_group)
    RadioGroup keenQus1RadioGroup;
    @BindView(R.id.keen_qus_1_expand_layout)
    ExpandableLinearLayout keenQus1ExpandLayout;
    @BindView(R.id.keen_qus_3_down)
    ImageView keenQus3Down;
    @BindView(R.id.keen_qus_3_layout)
    LinearLayout keenQus3Layout;
    @BindView(R.id.keen_qus_3_radio_none)
    RadioButton keenQus3RadioNone;
    @BindView(R.id.keen_qus_3_radio_mild)
    RadioButton keenQus3RadioMild;
    @BindView(R.id.keen_qus_3_radio_moderate)
    RadioButton keenQus3RadioModerate;
    @BindView(R.id.keen_qus_3_radio_severe)
    RadioButton keenQus3RadioSevere;
    @BindView(R.id.keen_qus_3_radio_extreme)
    RadioButton keenQus3RadioExtreme;
    @BindView(R.id.keen_qus_3_radio_group)
    RadioGroup keenQus3RadioGroup;
    @BindView(R.id.keen_qus_3_expand_layout)
    ExpandableLinearLayout keenQus3ExpandLayout;
    @BindView(R.id.keen_qus_4_down)
    ImageView keenQus4Down;
    @BindView(R.id.keen_qus_4_layout)
    LinearLayout keenQus4Layout;
    @BindView(R.id.keen_qus_4_radio_none)
    RadioButton keenQus4RadioNone;
    @BindView(R.id.keen_qus_4_radio_mild)
    RadioButton keenQus4RadioMild;
    @BindView(R.id.keen_qus_4_radio_moderate)
    RadioButton keenQus4RadioModerate;
    @BindView(R.id.keen_qus_4_radio_severe)
    RadioButton keenQus4RadioSevere;
    @BindView(R.id.keen_qus_4_radio_extreme)
    RadioButton keenQus4RadioExtreme;
    @BindView(R.id.keen_qus_4_radio_group)
    RadioGroup keenQus4RadioGroup;
    @BindView(R.id.keen_qus_4_expand_layout)
    ExpandableLinearLayout keenQus4ExpandLayout;
    @BindView(R.id.keen_qus_5_down)
    ImageView keenQus5Down;
    @BindView(R.id.keen_qus_5_layout)
    LinearLayout keenQus5Layout;
    @BindView(R.id.keen_qus_5_radio_none)
    RadioButton keenQus5RadioNone;
    @BindView(R.id.keen_qus_5_radio_mild)
    RadioButton keenQus5RadioMild;
    @BindView(R.id.keen_qus_5_radio_moderate)
    RadioButton keenQus5RadioModerate;
    @BindView(R.id.keen_qus_5_radio_severe)
    RadioButton keenQus5RadioSevere;
    @BindView(R.id.keen_qus_5_radio_extreme)
    RadioButton keenQus5RadioExtreme;
    @BindView(R.id.keen_qus_5_radio_group)
    RadioGroup keenQus5RadioGroup;
    @BindView(R.id.keen_qus_5_expand_layout)
    ExpandableLinearLayout keenQus5ExpandLayout;
    @BindView(R.id.keen_qus_6_down)
    ImageView keenQus6Down;
    @BindView(R.id.keen_qus_6_layout)
    LinearLayout keenQus6Layout;
    @BindView(R.id.keen_qus_6_radio_none)
    RadioButton keenQus6RadioNone;
    @BindView(R.id.keen_qus_6_radio_mild)
    RadioButton keenQus6RadioMild;
    @BindView(R.id.keen_qus_6_radio_moderate)
    RadioButton keenQus6RadioModerate;
    @BindView(R.id.keen_qus_6_radio_severe)
    RadioButton keenQus6RadioSevere;
    @BindView(R.id.keen_qus_6_radio_extreme)
    RadioButton keenQus6RadioExtreme;
    @BindView(R.id.keen_qus_6_radio_group)
    RadioGroup keenQus6RadioGroup;
    @BindView(R.id.keen_qus_6_expand_layout)
    ExpandableLinearLayout keenQus6ExpandLayout;
    @BindView(R.id.keen_qus_7_down)
    ImageView keenQus7Down;
    @BindView(R.id.keen_qus_7_layout)
    LinearLayout keenQus7Layout;
    @BindView(R.id.keen_qus_7_radio_none)
    RadioButton keenQus7RadioNone;
    @BindView(R.id.keen_qus_7_radio_mild)
    RadioButton keenQus7RadioMild;
    @BindView(R.id.keen_qus_7_radio_moderate)
    RadioButton keenQus7RadioModerate;
    @BindView(R.id.keen_qus_7_radio_severe)
    RadioButton keenQus7RadioSevere;
    @BindView(R.id.keen_qus_7_radio_extreme)
    RadioButton keenQus7RadioExtreme;
    @BindView(R.id.keen_qus_7_radio_group)
    RadioGroup keenQus7RadioGroup;
    @BindView(R.id.keen_qus_7_expand_layout)
    ExpandableLinearLayout keenQus7ExpandLayout;
    @BindView(R.id.keen_qus_8_down)
    ImageView keenQus8Down;
    @BindView(R.id.keen_qus_8_layout)
    LinearLayout keenQus8Layout;
    @BindView(R.id.keen_qus_8_radio_none)
    RadioButton keenQus8RadioNone;
    @BindView(R.id.keen_qus_8_radio_mild)
    RadioButton keenQus8RadioMild;
    @BindView(R.id.keen_qus_8_radio_moderate)
    RadioButton keenQus8RadioModerate;
    @BindView(R.id.keen_qus_8_radio_severe)
    RadioButton keenQus8RadioSevere;
    @BindView(R.id.keen_qus_8_radio_extreme)
    RadioButton keenQus8RadioExtreme;
    @BindView(R.id.keen_qus_8_radio_group)
    RadioGroup keenQus8RadioGroup;
    @BindView(R.id.keen_qus_8_expand_layout)
    ExpandableLinearLayout keenQus8ExpandLayout;
    @BindView(R.id.keen_qus_9_down)
    ImageView keenQus9Down;
    @BindView(R.id.keen_qus_9_layout)
    LinearLayout keenQus9Layout;
    @BindView(R.id.keen_qus_9_radio_none)
    RadioButton keenQus9RadioNone;
    @BindView(R.id.keen_qus_9_radio_mild)
    RadioButton keenQus9RadioMild;
    @BindView(R.id.keen_qus_9_radio_moderate)
    RadioButton keenQus9RadioModerate;
    @BindView(R.id.keen_qus_9_radio_severe)
    RadioButton keenQus9RadioSevere;
    @BindView(R.id.keen_qus_9_radio_extreme)
    RadioButton keenQus9RadioExtreme;
    @BindView(R.id.keen_qus_9_radio_group)
    RadioGroup keenQus9RadioGroup;
    @BindView(R.id.keen_qus_9_expand_layout)
    ExpandableLinearLayout keenQus9ExpandLayout;
    @BindView(R.id.keen_qus_10_down)
    ImageView keenQus10Down;
    @BindView(R.id.keen_qus_10_layout)
    LinearLayout keenQus10Layout;
    @BindView(R.id.keen_qus_10_radio_never)
    RadioButton keenQus10RadioNever;
    @BindView(R.id.keen_qus_10_radio_rarely)
    RadioButton keenQus10RadioRarely;
    @BindView(R.id.keen_qus_10_radio_sometimes)
    RadioButton keenQus10RadioSometimes;
    @BindView(R.id.keen_qus_10_radio_often)
    RadioButton keenQus10RadioOften;
    @BindView(R.id.keen_qus_10_radio_always)
    RadioButton keenQus10RadioAlways;
    @BindView(R.id.keen_qus_10_radio_group)
    RadioGroup keenQus10RadioGroup;
    @BindView(R.id.keen_qus_10_expand_layout)
    ExpandableLinearLayout keenQus10ExpandLayout;
    @BindView(R.id.keen_qus_11_down)
    ImageView keenQus11Down;
    @BindView(R.id.keen_qus_11_layout)
    LinearLayout keenQus11Layout;
    @BindView(R.id.keen_qus_11_radio_never)
    RadioButton keenQus11RadioNever;
    @BindView(R.id.keen_qus_11_radio_rarely)
    RadioButton keenQus11RadioRarely;
    @BindView(R.id.keen_qus_11_radio_sometimes)
    RadioButton keenQus11RadioSometimes;
    @BindView(R.id.keen_qus_11_radio_often)
    RadioButton keenQus11RadioOften;
    @BindView(R.id.keen_qus_11_radio_always)
    RadioButton keenQus11RadioAlways;
    @BindView(R.id.keen_qus_11_radio_group)
    RadioGroup keenQus11RadioGroup;
    @BindView(R.id.keen_qus_11_expand_layout)
    ExpandableLinearLayout keenQus11ExpandLayout;
    @BindView(R.id.keen_qus_12_down)
    ImageView keenQus12Down;
    @BindView(R.id.keen_qus_12_layout)
    LinearLayout keenQus12Layout;
    @BindView(R.id.keen_qus_12_radio_never)
    RadioButton keenQus12RadioNever;
    @BindView(R.id.keen_qus_12_radio_rarely)
    RadioButton keenQus12RadioRarely;
    @BindView(R.id.keen_qus_12_radio_sometimes)
    RadioButton keenQus12RadioSometimes;
    @BindView(R.id.keen_qus_12_radio_often)
    RadioButton keenQus12RadioOften;
    @BindView(R.id.keen_qus_12_radio_always)
    RadioButton keenQus12RadioAlways;
    @BindView(R.id.keen_qus_12_radio_group)
    RadioGroup keenQus12RadioGroup;
    @BindView(R.id.keen_qus_12_expand_layout)
    ExpandableLinearLayout keenQus12ExpandLayout;
    @BindView(R.id.keen_qus_13_down)
    ImageView keenQus13Down;
    @BindView(R.id.keen_qus_13_layout)
    LinearLayout keenQus13Layout;
    @BindView(R.id.keen_qus_13_radio_never)
    RadioButton keenQus13RadioNever;
    @BindView(R.id.keen_qus_13_radio_rarely)
    RadioButton keenQus13RadioRarely;
    @BindView(R.id.keen_qus_13_radio_sometimes)
    RadioButton keenQus13RadioSometimes;
    @BindView(R.id.keen_qus_13_radio_often)
    RadioButton keenQus13RadioOften;
    @BindView(R.id.keen_qus_13_radio_always)
    RadioButton keenQus13RadioAlways;
    @BindView(R.id.keen_qus_13_radio_group)
    RadioGroup keenQus13RadioGroup;
    @BindView(R.id.keen_qus_13_expand_layout)
    ExpandableLinearLayout keenQus13ExpandLayout;
    @BindView(R.id.keen_qus_14_down)
    ImageView keenQus14Down;
    @BindView(R.id.keen_qus_14_layout)
    LinearLayout keenQus14Layout;
    @BindView(R.id.keen_qus_14_radio_none)
    RadioButton keenQus14RadioNone;
    @BindView(R.id.keen_qus_14_radio_mild)
    RadioButton keenQus14RadioMild;
    @BindView(R.id.keen_qus_14_radio_moderate)
    RadioButton keenQus14RadioModerate;
    @BindView(R.id.keen_qus_14_radio_severe)
    RadioButton keenQus14RadioSevere;
    @BindView(R.id.keen_qus_14_radio_extreme)
    RadioButton keenQus14RadioExtreme;
    @BindView(R.id.keen_qus_14_radio_group)
    RadioGroup keenQus14RadioGroup;
    @BindView(R.id.keen_qus_14_expand_layout)
    ExpandableLinearLayout keenQus14ExpandLayout;
    @BindView(R.id.keen_qus_15_down)
    ImageView keenQus15Down;
    @BindView(R.id.keen_qus_15_layout)
    LinearLayout keenQus15Layout;
    @BindView(R.id.keen_qus_15_radio_none)
    RadioButton keenQus15RadioNone;
    @BindView(R.id.keen_qus_15_radio_mild)
    RadioButton keenQus15RadioMild;
    @BindView(R.id.keen_qus_15_radio_moderate)
    RadioButton keenQus15RadioModerate;
    @BindView(R.id.keen_qus_15_radio_severe)
    RadioButton keenQus15RadioSevere;
    @BindView(R.id.keen_qus_15_radio_extreme)
    RadioButton keenQus15RadioExtreme;
    @BindView(R.id.keen_qus_15_radio_group)
    RadioGroup keenQus15RadioGroup;
    @BindView(R.id.keen_qus_15_expand_layout)
    ExpandableLinearLayout keenQus15ExpandLayout;
    @BindView(R.id.submit_questionary)
    Button submitQuestionary;

    String shoulder_qus_1_ans = "", shoulder_qus_2_ans = "", shoulder_qus_3_ans = "", shoulder_qus_4_ans = "", shoulder_qus_5_ans = "",
            shoulder_qus_6_ans = "", shoulder_qus_7_ans = "", shoulder_qus_8_ans = "", shoulder_qus_9_ans = "", shoulder_qus_10_ans = "";


    String elbow_qus_1_ans = "", elbow_qus_2_ans = "", elbow_qus_3_ans = "", elbow_qus_4_ans = "", elbow_qus_5_ans = "",
            elbow_qus_6_ans = "", elbow_qus_7_ans = "", elbow_qus_8_ans = "", elbow_qus_9_ans = "", elbow_qus_10_ans = "",
            elbow_qus_11_ans = "", elbow_qus_12_ans = "", elbow_qus_13_ans = "";

    String keen_qus_1_ans = "", keen_qus_2_ans = "", keen_qus_3_ans = "", keen_qus_4_ans = "", keen_qus_5_ans = "",
            keen_qus_6_ans = "", keen_qus_7_ans = "", keen_qus_8_ans = "", keen_qus_9_ans = "", keen_qus_10_ans = "",
            keen_qus_11_ans = "", keen_qus_12_ans = "", keen_qus_13_ans = "", keen_qus_14_ans = "", keen_qus_15_ans = "";


    SharedPreferences sharedPreferences ;
    List<String> anslist ;
    QuestionaryPresenter presenter ;
    String token ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionary);
        ButterKnife.bind(this);
        toolbarwithoutdrawer("Questions");

        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "") ;
        anslist = new ArrayList<>() ;
        presenter = new QuestionaryPresenter(this) ;

        shoulderQus7SeekBarClicked();
        shoulderQus9SeekBarClicked();
        elbowQus7SeekBarClicked();
        elbowQus9SeekBarClicked();
        elbowQus10SeekBarClicked();
        elbowQus11SeekBarClicked();
        elbowQus12SeekBarClicked();
        elbowQus13SeekBarClicked();
    }

    @OnClick(R.id.shoulder_qus_1_layout)
    public void onShoulderQus1LayoutClicked() {
        shoulderQus1ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_2_layout)
    public void onShoulderQus2LayoutClicked() {
        shoulderQus2ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_3_layout)
    public void onShoulderQus3LayoutClicked() {
        shoulderQus3ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_4_layout)
    public void onShoulderQus4LayoutClicked() {
        shoulderQus4ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_5_layout)
    public void onShoulderQus5LayoutClicked() {
        shoulderQus5ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_6_layout)
    public void onShoulderQus6LayoutClicked() {
        shoulderQus6ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_7_layout)
    public void onShoulderQus7LayoutClicked() {
        shoulderQus7ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_8_layout)
    public void onShoulderQus8LayoutClicked() {
        shoulderQus8ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_9_layout)
    public void onShoulderQus9LayoutClicked() {
        shoulderQus9ExpandLayout.toggle();
    }

    @OnClick(R.id.shoulder_qus_10_layout)
    public void onShoulderQus10LayoutClicked() {
        shoulderQus10ExpandLayout.toggle();
    }

    @OnClick({R.id.elbow_qus_1_layout, R.id.elbow_qus_2_layout, R.id.elbow_qus_3_layout, R.id.elbow_qus_4_layout, R.id.elbow_qus_5_layout, R.id.elbow_qus_6_layout, R.id.elbow_qus_7_layout, R.id.elbow_qus_8_layout, R.id.elbow_qus_9_layout, R.id.elbow_qus_10_layout, R.id.elbow_qus_11_layout, R.id.elbow_qus_12_layout, R.id.elbow_qus_13_layout, R.id.keen_qus_1_layout, R.id.keen_qus_3_layout, R.id.keen_qus_4_layout, R.id.keen_qus_5_layout, R.id.keen_qus_6_layout, R.id.keen_qus_7_layout, R.id.keen_qus_8_layout, R.id.keen_qus_9_layout, R.id.keen_qus_10_layout, R.id.keen_qus_11_layout, R.id.keen_qus_12_layout, R.id.keen_qus_13_layout, R.id.keen_qus_14_layout, R.id.keen_qus_15_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.elbow_qus_1_layout:
                elbowQus1ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_2_layout:
                elbowQus2ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_3_layout:
                elbowQus3ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_4_layout:
                elbowQus4ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_5_layout:
                elbowQus5ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_6_layout:
                elbowQus6ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_7_layout:
                elbowQus7ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_8_layout:
                elbowQus8ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_9_layout:
                elbowQus9ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_10_layout:
                elbowQus10ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_11_layout:
                elbowQus11ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_12_layout:
                elbowQus12ExpandLayout.toggle();
                break;
            case R.id.elbow_qus_13_layout:
                elbowQus13ExpandLayout.toggle();
                break;
            case R.id.keen_qus_1_layout:
                keenQus1ExpandLayout.toggle();
                break;
            case R.id.keen_qus_3_layout:
                keenQus3ExpandLayout.toggle();
                break;
            case R.id.keen_qus_4_layout:
                keenQus4ExpandLayout.toggle();
                break;
            case R.id.keen_qus_5_layout:
                keenQus5ExpandLayout.toggle();
                break;
            case R.id.keen_qus_6_layout:
                keenQus6ExpandLayout.toggle();
                break;
            case R.id.keen_qus_7_layout:
                keenQus7ExpandLayout.toggle();
                break;
            case R.id.keen_qus_8_layout:
                keenQus8ExpandLayout.toggle();
                break;
            case R.id.keen_qus_9_layout:
                keenQus9ExpandLayout.toggle();
                break;
            case R.id.keen_qus_10_layout:
                keenQus10ExpandLayout.toggle();
                break;
            case R.id.keen_qus_11_layout:
                keenQus11ExpandLayout.toggle();
                break;
            case R.id.keen_qus_12_layout:
                keenQus12ExpandLayout.toggle();
                break;
            case R.id.keen_qus_13_layout:
                keenQus13ExpandLayout.toggle();
                break;
            case R.id.keen_qus_14_layout:
                keenQus14ExpandLayout.toggle();
                break;
            case R.id.keen_qus_15_layout:
                keenQus15ExpandLayout.toggle();
                break;
        }
    }


    public void shoulderSelectQusOne(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_1_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_1_ans = "yes";
                    Toast.makeText(this, shoulder_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_1_radio_no:
                if (shoulderQus) {
                    shoulder_qus_1_ans = "no";
                    Toast.makeText(this, shoulder_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void shoulderSelectQusTwo(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_2_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_2_ans = "yes";
                    Toast.makeText(this, shoulder_qus_2_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_2_radio_no:
                if (shoulderQus) {
                    shoulder_qus_2_ans = "no";
                    Toast.makeText(this, shoulder_qus_2_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void shoulderSelectQusFour(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_4_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_4_ans = "yes";
                    Toast.makeText(this, shoulder_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_4_radio_no:
                if (shoulderQus) {
                    shoulder_qus_4_ans = "no";
                    Toast.makeText(this, shoulder_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void shoulderSelectQusFive(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_5_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_5_ans = "yes";
                    Toast.makeText(this, shoulder_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_5_radio_no:
                if (shoulderQus) {
                    shoulder_qus_5_ans = "no";
                    Toast.makeText(this, shoulder_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void shoulderSelectQusEight(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_8_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_8_ans = "yes";
                    Toast.makeText(this, shoulder_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_8_radio_no:
                if (shoulderQus) {
                    shoulder_qus_8_ans = "no";
                    Toast.makeText(this, shoulder_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void shoulderSelectQusTen(View view) {

        boolean shoulderQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.shoulder_qus_10_radio_yes:
                if (shoulderQus) {
                    shoulder_qus_10_ans = "yes";
                    Toast.makeText(this, shoulder_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.shoulder_qus_10_radio_no:
                if (shoulderQus) {
                    shoulder_qus_10_ans = "no";
                    Toast.makeText(this, shoulder_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void elbowSelectQusOne(View view) {

        boolean elbowQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.elbow_qus_1_radio_yes:
                if (elbowQus) {
                    elbow_qus_1_ans = "yes";
                    Toast.makeText(this, elbow_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.elbow_qus_1_radio_no:
                if (elbowQus) {
                    elbow_qus_1_ans = "no";
                    Toast.makeText(this, elbow_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void elbowSelectQusTwo(View view) {

        boolean elbowQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.elbow_qus_2_radio_yes:
                if (elbowQus) {
                    elbow_qus_2_ans = "yes";
                    Toast.makeText(this, elbow_qus_2_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.elbow_qus_2_radio_no:
                if (elbowQus) {
                    elbow_qus_2_ans = "no";
                    Toast.makeText(this, elbow_qus_2_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void elbowSelectQusFour(View view) {

        boolean elbowQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.elbow_qus_4_radio_yes:
                if (elbowQus) {
                    elbow_qus_4_ans = "yes";
                    Toast.makeText(this, elbow_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.elbow_qus_4_radio_no:
                if (elbowQus) {
                    elbow_qus_4_ans = "no";
                    Toast.makeText(this, elbow_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void elbowSelectQusFive(View view) {

        boolean elbowQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.elbow_qus_5_radio_yes:
                if (elbowQus) {
                    elbow_qus_5_ans = "yes";
                    Toast.makeText(this, elbow_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.elbow_qus_5_radio_no:
                if (elbowQus) {
                    elbow_qus_5_ans = "no";
                    Toast.makeText(this, elbow_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void elbowSelectQusEight(View view) {

        boolean elbowQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.elbow_qus_8_radio_yes:
                if (elbowQus) {
                    elbow_qus_8_ans = "yes";
                    Toast.makeText(this, elbow_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.elbow_qus_8_radio_no:
                if (elbowQus) {
                    elbow_qus_8_ans = "no";
                    Toast.makeText(this, elbow_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusOne(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_1_radio_never:
                if (keenQus) {
                    keen_qus_1_ans = "never";
                    Toast.makeText(this, keen_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_1_radio_monthly:
                if (keenQus) {
                    keen_qus_1_ans = "monthly";
                    Toast.makeText(this, keen_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_1_radio_weekly:
                if (keenQus) {
                    keen_qus_1_ans = "weekly";
                    Toast.makeText(this, keen_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_1_radio_daily:
                if (keenQus) {
                    keen_qus_1_ans = "daily";
                    Toast.makeText(this, keen_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_1_radio_always:
                if (keenQus) {
                    keen_qus_1_ans = "always";
                    Toast.makeText(this, keen_qus_1_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusThree(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_3_radio_none:
                if (keenQus) {
                    keen_qus_3_ans = "none";
                    Toast.makeText(this, keen_qus_3_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_3_radio_mild:
                if (keenQus) {
                    keen_qus_3_ans = "mild";
                    Toast.makeText(this, keen_qus_3_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_3_radio_moderate:
                if (keenQus) {
                    keen_qus_3_ans = "moderate";
                    Toast.makeText(this, keen_qus_3_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_3_radio_severe:
                if (keenQus) {
                    keen_qus_3_ans = "severe";
                    Toast.makeText(this, keen_qus_3_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_3_radio_extreme:
                if (keenQus) {
                    keen_qus_3_ans = "extreme";
                    Toast.makeText(this, keen_qus_3_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusFour(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_4_radio_none:
                if (keenQus) {
                    keen_qus_4_ans = "none";
                    Toast.makeText(this, keen_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_4_radio_mild:
                if (keenQus) {
                    keen_qus_4_ans = "mild";
                    Toast.makeText(this, keen_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_4_radio_moderate:
                if (keenQus) {
                    keen_qus_4_ans = "moderate";
                    Toast.makeText(this, keen_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_4_radio_severe:
                if (keenQus) {
                    keen_qus_4_ans = "severe";
                    Toast.makeText(this, keen_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_4_radio_extreme:
                if (keenQus) {
                    keen_qus_3_ans = "extreme";
                    Toast.makeText(this, keen_qus_4_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusFive(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_5_radio_none:
                if (keenQus) {
                    keen_qus_5_ans = "none";
                    Toast.makeText(this, keen_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_5_radio_mild:
                if (keenQus) {
                    keen_qus_5_ans = "mild";
                    Toast.makeText(this, keen_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_5_radio_moderate:
                if (keenQus) {
                    keen_qus_5_ans = "moderate";
                    Toast.makeText(this, keen_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_5_radio_severe:
                if (keenQus) {
                    keen_qus_5_ans = "severe";
                    Toast.makeText(this, keen_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_5_radio_extreme:
                if (keenQus) {
                    keen_qus_5_ans = "extreme";
                    Toast.makeText(this, keen_qus_5_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusSix(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_6_radio_none:
                if (keenQus) {
                    keen_qus_6_ans = "none";
                    Toast.makeText(this, keen_qus_6_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_6_radio_mild:
                if (keenQus) {
                    keen_qus_6_ans = "mild";
                    Toast.makeText(this, keen_qus_6_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_6_radio_moderate:
                if (keenQus) {
                    keen_qus_6_ans = "moderate";
                    Toast.makeText(this, keen_qus_6_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_6_radio_severe:
                if (keenQus) {
                    keen_qus_6_ans = "severe";
                    Toast.makeText(this, keen_qus_6_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_6_radio_extreme:
                if (keenQus) {
                    keen_qus_6_ans = "extreme";
                    Toast.makeText(this, keen_qus_6_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusSeven(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_7_radio_none:
                if (keenQus) {
                    keen_qus_7_ans = "none";
                    Toast.makeText(this, keen_qus_7_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_7_radio_mild:
                if (keenQus) {
                    keen_qus_7_ans = "mild";
                    Toast.makeText(this, keen_qus_7_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_7_radio_moderate:
                if (keenQus) {
                    keen_qus_7_ans = "moderate";
                    Toast.makeText(this, keen_qus_7_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_7_radio_severe:
                if (keenQus) {
                    keen_qus_7_ans = "severe";
                    Toast.makeText(this, keen_qus_7_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_7_radio_extreme:
                if (keenQus) {
                    keen_qus_7_ans = "extreme";
                    Toast.makeText(this, keen_qus_7_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusEight(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_8_radio_none:
                if (keenQus) {
                    keen_qus_8_ans = "none";
                    Toast.makeText(this, keen_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_8_radio_mild:
                if (keenQus) {
                    keen_qus_8_ans = "mild";
                    Toast.makeText(this, keen_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_8_radio_moderate:
                if (keenQus) {
                    keen_qus_8_ans = "moderate";
                    Toast.makeText(this, keen_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_8_radio_severe:
                if (keenQus) {
                    keen_qus_8_ans = "severe";
                    Toast.makeText(this, keen_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_8_radio_extreme:
                if (keenQus) {
                    keen_qus_8_ans = "extreme";
                    Toast.makeText(this, keen_qus_8_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusNine(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_9_radio_none:
                if (keenQus) {
                    keen_qus_9_ans = "none";
                    Toast.makeText(this, keen_qus_9_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_9_radio_mild:
                if (keenQus) {
                    keen_qus_9_ans = "mild";
                    Toast.makeText(this, keen_qus_9_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_9_radio_moderate:
                if (keenQus) {
                    keen_qus_9_ans = "moderate";
                    Toast.makeText(this, keen_qus_9_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_9_radio_severe:
                if (keenQus) {
                    keen_qus_9_ans = "severe";
                    Toast.makeText(this, keen_qus_9_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_9_radio_extreme:
                if (keenQus) {
                    keen_qus_9_ans = "extreme";
                    Toast.makeText(this, keen_qus_9_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusFourteen(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_14_radio_none:
                if (keenQus) {
                    keen_qus_14_ans = "none";
                    Toast.makeText(this, keen_qus_14_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_14_radio_mild:
                if (keenQus) {
                    keen_qus_14_ans = "mild";
                    Toast.makeText(this, keen_qus_14_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_14_radio_moderate:
                if (keenQus) {
                    keen_qus_14_ans = "moderate";
                    Toast.makeText(this, keen_qus_14_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_14_radio_severe:
                if (keenQus) {
                    keen_qus_14_ans = "severe";
                    Toast.makeText(this, keen_qus_14_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_14_radio_extreme:
                if (keenQus) {
                    keen_qus_14_ans = "extreme";
                    Toast.makeText(this, keen_qus_14_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusFifteen(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_15_radio_none:
                if (keenQus) {
                    keen_qus_15_ans = "none";
                    Toast.makeText(this, keen_qus_15_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_15_radio_mild:
                if (keenQus) {
                    keen_qus_15_ans = "mild";
                    Toast.makeText(this, keen_qus_15_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_15_radio_moderate:
                if (keenQus) {
                    keen_qus_15_ans = "moderate";
                    Toast.makeText(this, keen_qus_15_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_15_radio_severe:
                if (keenQus) {
                    keen_qus_15_ans = "severe";
                    Toast.makeText(this, keen_qus_15_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_15_radio_extreme:
                if (keenQus) {
                    keen_qus_15_ans = "extreme";
                    Toast.makeText(this, keen_qus_15_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusTen(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_10_radio_never:
                if (keenQus) {
                    keen_qus_10_ans = "never";
                    Toast.makeText(this, keen_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_10_radio_rarely:
                if (keenQus) {
                    keen_qus_10_ans = "rarely";
                    Toast.makeText(this, keen_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_10_radio_sometimes:
                if (keenQus) {
                    keen_qus_10_ans = "sometimes";
                    Toast.makeText(this, keen_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_10_radio_often:
                if (keenQus) {
                    keen_qus_10_ans = "often";
                    Toast.makeText(this, keen_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_10_radio_always:
                if (keenQus) {
                    keen_qus_10_ans = "always";
                    Toast.makeText(this, keen_qus_10_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusEleven(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_11_radio_never:
                if (keenQus) {
                    keen_qus_11_ans = "never";
                    Toast.makeText(this, keen_qus_11_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_11_radio_rarely:
                if (keenQus) {
                    keen_qus_11_ans = "rarely";
                    Toast.makeText(this, keen_qus_11_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_11_radio_sometimes:
                if (keenQus) {
                    keen_qus_11_ans = "sometimes";
                    Toast.makeText(this, keen_qus_11_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_11_radio_often:
                if (keenQus) {
                    keen_qus_11_ans = "often";
                    Toast.makeText(this, keen_qus_11_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_11_radio_always:
                if (keenQus) {
                    keen_qus_11_ans = "always";
                    Toast.makeText(this, keen_qus_11_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusTwelve(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_12_radio_never:
                if (keenQus) {
                    keen_qus_12_ans = "never";
                    Toast.makeText(this, keen_qus_12_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_12_radio_rarely:
                if (keenQus) {
                    keen_qus_12_ans = "rarely";
                    Toast.makeText(this, keen_qus_12_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_12_radio_sometimes:
                if (keenQus) {
                    keen_qus_12_ans = "sometimes";
                    Toast.makeText(this, keen_qus_12_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_12_radio_often:
                if (keenQus) {
                    keen_qus_12_ans = "often";
                    Toast.makeText(this, keen_qus_12_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_12_radio_always:
                if (keenQus) {
                    keen_qus_12_ans = "always";
                    Toast.makeText(this, keen_qus_12_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void keenSelectQusThirteen(View view) {

        boolean keenQus = ((RadioButton) view).isChecked();
        switch (view.getId()) {

            case R.id.keen_qus_13_radio_never:
                if (keenQus) {
                    keen_qus_13_ans = "never";
                    Toast.makeText(this, keen_qus_13_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_13_radio_rarely:
                if (keenQus) {
                    keen_qus_13_ans = "rarely";
                    Toast.makeText(this, keen_qus_13_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_13_radio_sometimes:
                if (keenQus) {
                    keen_qus_13_ans = "sometimes";
                    Toast.makeText(this, keen_qus_13_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_13_radio_often:
                if (keenQus) {
                    keen_qus_13_ans = "often";
                    Toast.makeText(this, keen_qus_13_ans, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.keen_qus_13_radio_always:
                if (keenQus) {
                    keen_qus_13_ans = "always";
                    Toast.makeText(this, keen_qus_13_ans, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void shoulderQus7SeekBarClicked() {

        shoulderQus7SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shoulder_qus_7_ans = String.valueOf(progress);
                shoulderQus7SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++Shoulder 7++", shoulder_qus_7_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void shoulderQus9SeekBarClicked() {

        shoulderQus9SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shoulder_qus_9_ans = String.valueOf(progress);
                shoulderQus9SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++Shoulder 9++", shoulder_qus_9_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus7SeekBarClicked() {

        elbowQus7SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_7_ans = String.valueOf(progress);
                elbowQus7SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++Shoulder 9++", elbow_qus_7_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus9SeekBarClicked() {

        elbowQus9SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_9_ans = String.valueOf(progress);
                elbowQus9SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++elbow 9++", elbow_qus_9_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus10SeekBarClicked() {

        elbowQus10SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_10_ans = String.valueOf(progress);
                elbowQus10SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++elbow 10++", elbow_qus_10_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus11SeekBarClicked() {

        elbowQus11SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_11_ans = String.valueOf(progress);
                elbowQus11SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++elbow 11++", elbow_qus_11_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus12SeekBarClicked() {

        elbowQus12SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_12_ans = String.valueOf(progress);
                elbowQus12SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++elbow 12++", elbow_qus_12_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void elbowQus13SeekBarClicked() {

        elbowQus13SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elbow_qus_13_ans = String.valueOf(progress);
                elbowQus13SeekBarProgressText.setText("Pain Scale : " + progress + "");
                Log.d("++elbow 13++", elbow_qus_13_ans);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @OnClick(R.id.submit_questionary)
    public void onSubmitQuestionaryClicked() {
        shoulder_qus_3_ans = shoulderQus3Edittext.getText().toString().trim() ;
        shoulder_qus_6_ans = shoulderQus6Edittext.getText().toString().trim() ;
        elbow_qus_3_ans = elbowQus3Edittext.getText().toString().trim() ;
        elbow_qus_6_ans = elbowQus6Edittext.getText().toString().trim() ;

        anslist.clear();
        anslist.add("286");
        anslist.add(shoulder_qus_1_ans);
        anslist.add(shoulder_qus_2_ans);
        anslist.add(shoulder_qus_3_ans);
        anslist.add(shoulder_qus_4_ans);
        anslist.add(shoulder_qus_5_ans);
        anslist.add(shoulder_qus_6_ans);
        anslist.add(shoulder_qus_7_ans);
        anslist.add(shoulder_qus_8_ans);
        anslist.add(shoulder_qus_9_ans);
        anslist.add(shoulder_qus_10_ans);
        anslist.add(elbow_qus_1_ans);
        anslist.add(elbow_qus_2_ans);
        anslist.add(elbow_qus_3_ans);
        anslist.add(elbow_qus_4_ans);
        anslist.add(elbow_qus_5_ans);
        anslist.add(elbow_qus_6_ans);
        anslist.add(elbow_qus_7_ans);
        anslist.add(elbow_qus_8_ans);
        anslist.add(elbow_qus_9_ans);
        anslist.add(elbow_qus_10_ans);
        anslist.add(elbow_qus_11_ans);
        anslist.add(elbow_qus_12_ans);
        anslist.add(elbow_qus_13_ans);
        anslist.add(keen_qus_1_ans);
        anslist.add(keen_qus_3_ans);
        anslist.add(keen_qus_4_ans);
        anslist.add(keen_qus_5_ans);
        anslist.add(keen_qus_6_ans);
        anslist.add(keen_qus_7_ans);
        anslist.add(keen_qus_8_ans);
        anslist.add(keen_qus_9_ans);
        anslist.add(keen_qus_10_ans);
        anslist.add(keen_qus_11_ans);
        anslist.add(keen_qus_12_ans);
        anslist.add(keen_qus_13_ans);
        anslist.add(keen_qus_14_ans);
        anslist.add(keen_qus_15_ans);

        presenter.getQuestionarySubmitMsg(token, anslist);
    }

    @Override
    public void showQuestionarySubmitResult(QuestionaryRoot questionaryRoot) {

        String msg = questionaryRoot.getResult() ;
        Log.d("++SUBMIT MSG++", msg) ;
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
