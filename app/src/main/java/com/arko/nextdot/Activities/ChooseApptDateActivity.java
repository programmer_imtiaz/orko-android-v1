package com.arko.nextdot.Activities;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arko.nextdot.Adapters.DialougAppointmentTimeAdapter;
import com.arko.nextdot.Model.DialougAppointmentTimeModel;
import com.arko.nextdot.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by ImtiazDipto on 5/6/2017.
 */

public class ChooseApptDateActivity extends ToolbarBaseActivity {


    RecyclerView recyclerView ;
    LinearLayoutManager layoutManager ;
    DialougAppointmentTimeAdapter dialougAppointmentTimeAdapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_appt_date);
        toolbarwithoutdrawer("Choose Appointment");

        MaterialCalendarView materialCalendarView = (MaterialCalendarView) findViewById(R.id.choose_appt_calendarView);

        Calendar calendar = Calendar.getInstance();

        materialCalendarView.state().edit()
                .setFirstDayOfWeek(Calendar.SUNDAY)
                .setMinimumDate(CalendarDay.from(2017, 4, 1))
                .setMaximumDate(CalendarDay.from(2018, 12, 30))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();




        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                Toast.makeText(getApplicationContext(), ""+ date, Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(getApplicationContext(), ChooseApptTimeActivity.class) ;
                startActivity(intent);*/
                View appt_time_view = getLayoutInflater().inflate(R.layout.dialoug_appointment_time, null) ;
                recyclerView = (RecyclerView) appt_time_view.findViewById(R.id.dialoug_appt_recyler);
                dialougAppointmentTimeAdapter = new DialougAppointmentTimeAdapter(ChooseApptDateActivity.this, getdata());
                dialogCartAction(appt_time_view);
                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setAdapter(dialougAppointmentTimeAdapter);
                recyclerView.setLayoutManager(layoutManager);

            }
        });
    }

    private List<DialougAppointmentTimeModel> getdata(){
        List<DialougAppointmentTimeModel> list = new ArrayList<DialougAppointmentTimeModel>();
        String[] appt_time = {"07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30"} ;
        for(int i = 0 ; i < appt_time.length ; i++){
            DialougAppointmentTimeModel dialougAppointmentTimeModel = new DialougAppointmentTimeModel() ;
            dialougAppointmentTimeModel.setTime_dataset(appt_time[i]);
            list.add(dialougAppointmentTimeModel);
        }
        return list ;
    }

    private void dialogCartAction(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(view);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT ;

        LinearLayout close_layout, book_now_layout ;

        close_layout = (LinearLayout) dialog.findViewById(R.id.close_layout);
        book_now_layout = (LinearLayout) dialog.findViewById(R.id.book_now_layout) ;

        close_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        book_now_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }



}
