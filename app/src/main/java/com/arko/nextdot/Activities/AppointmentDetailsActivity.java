package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AppointmentDetailsActivity extends ToolbarBaseActivity {

    private String user_token = "";
    private String patient_id="";
    private String doctor_id = "";
    private String doctor_name = "";
    private String doctor_speciality = "";
    private Double doctor_rating = 0.0;
    private String doctor_gender = "";
    private String doctor_email = "";
    private String doctor_contact = "";
    private String appointment_reason = "";
    private String appointment_id = "";
    private String appointment_date = "";
    private String appointment_time = "";
    private String appointment_time_for_API = "";
    private String appointment_status = "";
    private String appointment_status_color = "";
    private String appointment_time_title = "";
    private String appointment_creator = "";
    private String doctorAvatar;
    private String appointmentLocation;
    private Boolean isPast;
    private CircleImageView doctorpic;
    private TextView tv_doc_profile_doc_name, tv_doc_speciality, tv_doc_gender, tv_doc_email, tv_doc_contact;
    private TextView tv_details, tv_accept, tv_reject;
    private LinearLayout decisionLayout, reportsLayout, prescriptionLayout, reportLayout;
    private TextView tv_case, tv_appointment_id, tv_date, tv_appointment_time_title, tv_appointment_time, tv_status, tv_location;
    private RatingBar ratingBar;
    private RelativeLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);
        toolbarwithoutdrawer("Appointment Details");
        Initialization();
        getData();
        setData(doctorAvatar, doctor_name, doctor_speciality, doctor_gender, doctor_email, doctor_contact, doctor_rating, appointment_reason, appointment_id, appointment_date, appointment_time_title, appointment_time, appointment_status, appointmentLocation, appointment_status_color);

        setOnClickListeners();

        if (appointment_status.equals("APPROVED")) {
            decisionLayout.setVisibility(View.GONE);
            reportsLayout.setVisibility(View.VISIBLE);
            appointment_status_color = "#006400";
        } else if (appointment_status.equals("PENDING")) {
            if(!isPast) {
                if (appointment_creator.equals(doctor_id)) {
                    decisionLayout.setVisibility(View.VISIBLE);
                    reportsLayout.setVisibility(View.GONE);
                    appointment_status_color = "#2BB8E8";
                } else {
                    decisionLayout.setVisibility(View.GONE);
                    reportsLayout.setVisibility(View.GONE);
                    appointment_status_color = "#2BB8E8";
                }
            }
        } else if (appointment_status.equals("CHANGED")) {
            if(!isPast) {
                decisionLayout.setVisibility(View.VISIBLE);
                reportsLayout.setVisibility(View.GONE);
                appointment_status_color = "#2BB8E8";
            }
        } else if (appointment_status.equals("REJECTED")) {
            reportsLayout.setVisibility(View.GONE);
            decisionLayout.setVisibility(View.GONE);
            appointment_status_color = "#FC464A";

        } else if (appointment_status.equals("COMPLETED")) {
            decisionLayout.setVisibility(View.GONE);
            reportsLayout.setVisibility(View.GONE);
            appointment_status_color = "#006400";
        }

    }


    @Override
    public void onBackPressed() {
        Intent testIntent = new Intent(AppointmentDetailsActivity.this, Appointments.class);
        startActivity(testIntent);
        finish();
    }

    private void Initialization() {
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        doctorpic = (CircleImageView) findViewById(R.id.doc_profile_doc_img);
        tv_doc_profile_doc_name = (TextView) findViewById(R.id.tv_doc_profile_doc_name);
        tv_doc_speciality = (TextView) findViewById(R.id.tv_speciality);
        tv_doc_gender = (TextView) findViewById(R.id.tv_gender);
        tv_doc_email = (TextView) findViewById(R.id.tv_email);
        tv_doc_contact = (TextView) findViewById(R.id.tv_contact);
        ratingBar = (RatingBar) findViewById(R.id.ratingbar);
        tv_case = (TextView) findViewById(R.id.tv_case);
        tv_appointment_id = (TextView) findViewById(R.id.tv_appointment_id);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_appointment_time_title = (TextView) findViewById(R.id.tv_appointment_time_title);
        tv_appointment_time = (TextView) findViewById(R.id.tv_appointment_time);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_location = (TextView) findViewById(R.id.tv_location);

        tv_details = (TextView) findViewById(R.id.details);
        decisionLayout = (LinearLayout) findViewById(R.id.decision_button_layout);
        tv_accept = (TextView) findViewById(R.id.accept);
        tv_reject = (TextView) findViewById(R.id.reject);
        reportsLayout = (LinearLayout) findViewById(R.id.reports);
        prescriptionLayout = (LinearLayout) findViewById(R.id.prescription);
        reportLayout = (LinearLayout) findViewById(R.id.report);

    }

    private void setOnClickListeners() {

        tv_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(AppointmentDetailsActivity.this, DoctorProfileActivity.class);
                intent1.putExtra("id", doctor_id);
                startActivity(intent1);
            }
        });

        tv_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new setAppointmentAPI(patient_id, user_token, appointment_time_for_API, appointment_id).execute();
            }
        });

        tv_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new rejectAppointmentAPI(patient_id, user_token, appointment_time_for_API, appointment_id).execute();
            }
        });

        prescriptionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testIntent1 = new Intent(AppointmentDetailsActivity.this, UploadPhotoActivity.class);
                testIntent1.putExtra("doc_type", "1") ;
                testIntent1.putExtra("appointment_id", appointment_id) ;
                testIntent1.putExtra("patient_id", patient_id) ;
                testIntent1.putExtra("user_token", user_token) ;
                testIntent1.putExtra("doctor_id", doctor_id) ;
                startActivity(testIntent1);
            }
        });

        reportLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testIntent1 = new Intent(AppointmentDetailsActivity.this, UploadPhotoActivity.class);
                testIntent1.putExtra("doc_type", "2") ;
                testIntent1.putExtra("appointment_id", appointment_id) ;
                testIntent1.putExtra("patient_id", patient_id) ;
                testIntent1.putExtra("user_token", user_token) ;
                testIntent1.putExtra("doctor_id", doctor_id) ;
                startActivity(testIntent1);
            }
        });

    }

    private void setData(String doctorAvatar, String doctor_name, String doctor_speciality, String doctor_gender, String doctor_email, String doctor_contact, Double doctor_rating, String appointment_reason, String appointment_id, String appointment_date, String appointment_time_title, String appointment_time, String appointment_status, String appointmentLocation, String appointment_status_color) {

        if (Patterns.WEB_URL.matcher(doctorAvatar).matches()) {
            Glide.with(AppointmentDetailsActivity.this).load(doctorAvatar).placeholder(R.drawable.avtaar).dontAnimate().into(doctorpic);
        } else {
            Glide.with(AppointmentDetailsActivity.this).load(Constant.ROOT_URL + doctorAvatar).placeholder(R.drawable.avtaar).dontAnimate().into(doctorpic);
        }

        tv_doc_profile_doc_name.setText(doctor_name);
        tv_doc_speciality.setText(doctor_speciality);
        tv_doc_gender.setText(doctor_gender);
        tv_doc_email.setText(doctor_email);
        tv_doc_contact.setText(doctor_contact);
        ratingBar.setRating(Float.parseFloat(String.valueOf(doctor_rating)));
        tv_case.setText(appointment_reason);
        tv_appointment_id.setText(appointment_id);
        tv_date.setText(appointment_date);
        tv_appointment_time_title.setText(appointment_time_title);
        tv_appointment_time.setText(appointment_time);
        tv_status.setText(appointment_status);
        tv_location.setText(appointmentLocation);
        tv_status.setTextColor(Color.parseColor(appointment_status_color));

    }

    private void getData() {
        Intent intent = AppointmentDetailsActivity.this.getIntent();
        user_token = intent.getStringExtra("user_token");
        appointment_id = intent.getStringExtra("appointment_id");
        appointment_reason = intent.getStringExtra("appointment_reason");
        appointment_date = intent.getStringExtra("appointment_date");
        appointment_time_for_API = intent.getStringExtra("appointment_time_for_API");
        patient_id = intent.getStringExtra("patient_id");
        doctor_id = intent.getStringExtra("doctor_id");
        doctor_name = intent.getStringExtra("doctor_name");
        doctor_speciality = intent.getStringExtra("doctor_speciality");
        doctorAvatar = intent.getStringExtra("doctor_avatar");
        doctor_rating = intent.getDoubleExtra("doctor_rating", 0.0);
        doctor_gender = intent.getStringExtra("doctor_gender");
        doctor_email = intent.getStringExtra("doctor_email");
        doctor_contact = intent.getStringExtra("doctor_contact");
        appointment_status = intent.getStringExtra("appointment_status");
        appointment_status_color = intent.getStringExtra("appointment_status_color");
        appointment_time = intent.getStringExtra("appointment_time");
        appointment_time_title = intent.getStringExtra("appointment_time_title");
        appointment_creator = intent.getStringExtra("appointment_creator");
        appointmentLocation = intent.getStringExtra("appointment_location");
        isPast = intent.getBooleanExtra("is_past", false);
    }

    private class setAppointmentAPI extends AsyncTask<String, String, String> {
        String userId, userToken, startTime, appointmentID;


        private setAppointmentAPI(String userId, String userToken, String startTime, String appointmentID) {
            this.userId = userId;
            this.userToken = userToken;
            this.startTime = startTime;
            this.appointmentID = appointmentID;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("error", "statusReport: onPreExecute e dhukse!!!");

            mainLayout.setClickable(false);

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************//


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentApproved");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            nameValuePairs.add(new BasicNameValuePair("approved_by", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("status", "approved"));
            nameValuePairs.add(new BasicNameValuePair("appointment_id", appointmentID));
            nameValuePairs.add(new BasicNameValuePair("appointment_start_time", startTime));


            Log.d("getAppointmentID", appointmentID + " " + startTime + " " + userId + " " + userToken);


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            Toast.makeText(AppointmentDetailsActivity.this, "Appointment set", Toast.LENGTH_LONG).show();

            tv_status.setText("APPROVED");
            tv_status.setTextColor(Color.parseColor("#006400"));
            decisionLayout.setVisibility(View.GONE);
            reportsLayout.setVisibility(View.VISIBLE);
            mainLayout.setClickable(true);
        }
    }


    private class rejectAppointmentAPI extends AsyncTask<String, String, String> {
        String userId, userToken, startTime, appointmentID;


        private rejectAppointmentAPI(String userId, String userToken, String startTime, String appointmentID) {
            this.userId = userId;
            this.userToken = userToken;
            this.startTime = startTime;
            this.appointmentID = appointmentID;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("error", "statusReport: onPreExecute e dhukse!!!");

            mainLayout.setClickable(false);

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************//


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentReject");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            nameValuePairs.add(new BasicNameValuePair("rejected_by", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("status", "rejected"));
            nameValuePairs.add(new BasicNameValuePair("appointment_id", appointmentID));


            Log.d("getAppointmentID", appointmentID + " " + startTime + " " + userId + " " + userToken);


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(AppointmentDetailsActivity.this, "Appointment request rejected", Toast.LENGTH_LONG).show();

            tv_status.setText("REJECTED");
            tv_status.setTextColor(Color.parseColor("#FC464A"));
            decisionLayout.setVisibility(View.GONE);
            reportsLayout.setVisibility(View.GONE);
            mainLayout.setClickable(true);
        }
    }
}
