package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arko.nextdot.Adapters.PredictiveSearchAdapter;
import com.arko.nextdot.Interface.OrkoHomeView;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;
import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchList;
import com.arko.nextdot.Presenter.OrkoHomePresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakib on 11/12/2017.
 */

public class OrkoHomeActivity extends BaseActivity implements OrkoHomeView, PredictiveSearchAdapter.ClickListener {

    @BindView(R.id.my_image_view)
    ImageView myImageView;
    @BindView(R.id.logo_layout)
    LinearLayout logoLayout;
    @BindView(R.id.search_doc_by_name_test)
    EditText searchDocByNameTest;
    @BindView(R.id.searchbar_layout)
    LinearLayout searchbarLayout;
    @BindView(R.id.space_layout)
    LinearLayout spaceLayout;
    @BindView(R.id.orko_home_search_recyler)
    RecyclerView orkoHomeSearchRecyler;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_item_list)
    ListView navItemList;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.predictive_no_result_found)
    TextView predictiveNoResultFound;

    Animation slide_up;
    int animate_flag = 0;

    List<SearchList> list;
    ArrayList<String> intent_list;
    OrkoHomePresenter presenter;
    PredictiveSearchAdapter predictiveSearchAdapter;
    LinearLayoutManager layoutManager;
    String exp_string = "", fees_string = "", speciality_string = "", location_string = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orko_home);
        ButterKnife.bind(this);
        setupNavDrawer();
        setSlideUp();
        intilization();
        intent_list = new ArrayList<>();
        presenter = new OrkoHomePresenter(this);
    }

    @OnClick(R.id.search_doc_by_name_test)
    public void onSearchEditTextClicked() {

        searchDocByNameTest.requestFocus();
        searchDocByNameTest.setFocusableInTouchMode(true);

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(searchDocByNameTest, InputMethodManager.SHOW_FORCED);

        if (animate_flag == 0) {
            logoLayout.setVisibility(View.GONE);
            searchbarLayout.startAnimation(slide_up);
            animate_flag = 1;
        }

        searchDocByNameTest.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String query = s.toString();
                Log.d("++++QUERY++++", query);
                if (query.length() >= 1) {
                    orkoHomeSearchRecyler.setVisibility(View.VISIBLE);
                    predictiveNoResultFound.setVisibility(View.GONE);
                    presenter.getPredictiveSearchData(query);
                }
                if (query.length() < 1) {
                    predictiveNoResultFound.setVisibility(View.GONE);
                    Log.d("++++QUERY++++", "NONE");
                    orkoHomeSearchRecyler.setVisibility(View.GONE);
                    stopLoading();
                    predictiveSearchAdapter.clearList();
                }
                //presenter.getPredictiveSearchData(query);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setSlideUp() {
        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up_animation);
    }

    @Override
    public void showSearchResult(PredictiveSearchRoot predictiveSearchRoot) {

        //Log.d("++METHOD CHECKING++", "asche");

        if(predictiveSearchRoot.getMsg().equals("Success")){
            list.clear();
            list = predictiveSearchRoot.getData();
            if (list != null) {

                //Log.d("++RESULT CHECKING++", "Yes result");
                predictiveNoResultFound.setVisibility(View.GONE);
                predictiveSearchAdapter.setFilter(list);
                for (SearchList searchList : list) {
                    if (searchList.getName() != null) {
                        Log.d("+++SEARCH_VALUE+++", searchList.getName());
                    }
                }
            }

        }
        else if(predictiveSearchRoot.getMsg().equals("No data found")){

            //Log.d("++NO RESULT CHECKING++", "No result");
            predictiveSearchAdapter.clearList();
            predictiveNoResultFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    public void intilization() {
        toolbarTitle.setText("Orko Search");
        list = new ArrayList<>();
        list.clear();
        layoutManager = new LinearLayoutManager(getAppContext(), LinearLayoutManager.VERTICAL, false);
        predictiveSearchAdapter = new PredictiveSearchAdapter(getAppContext(), list);
        orkoHomeSearchRecyler.setLayoutManager(layoutManager);
        predictiveSearchAdapter.setClicklistner(this);
        orkoHomeSearchRecyler.setAdapter(predictiveSearchAdapter);
        predictiveSearchAdapter.notifyDataSetChanged();
    }

    @Override
    public void itemClicked(View view, int position) {

        if (list.get(position).getSearchType().equals("name")) {
            Intent intent = new Intent(OrkoHomeActivity.this, DoctorProfileActivity.class);
            String doctor_id = String.valueOf(list.get(position).getUserId());
            intent.putExtra("id", doctor_id);
            startActivity(intent);
        }
        else if (list.get(position).getSearchType().equals("symptom_speciality")) {

            if (list.get(position).getSpeciality() != null) {

                Log.d("++SPECIALITY CHECK++", "Yup Speciality is there");
                if (list.get(position).getSpeciality().getName() != null) {

                    speciality_string = list.get(position).getSpeciality().getName();
                    Log.d("++speciality_string++", speciality_string);
                }
            }

            if (list.get(position).getSpeciality() == null) {
                if (list.get(position).getName() != null) {
                    speciality_string = list.get(position).getName();
                    Log.d("++speciality_string++", speciality_string);
                }
            }

            intent_list.clear();
            intent_list.add(exp_string);
            intent_list.add(speciality_string);
            intent_list.add(fees_string);
            intent_list.add(location_string);

            Intent intent = new Intent(OrkoHomeActivity.this, SearchDoctorResultActivity.class);
            intent.putStringArrayListExtra("key", intent_list);
            startActivity(intent);
        }
    }
}
