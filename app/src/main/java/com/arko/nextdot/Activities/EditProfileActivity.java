package com.arko.nextdot.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Model.EditProfileModel;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;


public class EditProfileActivity extends ToolbarBaseActivity {

    NumberPicker date, month, year ;
    Spinner edit_gender, edit_district, edit_thana ;
    TextView edit_birthday,  edit_name, edit_age ;
    EditText edit_first_name, edit_last_name, edit_email, edit_profession, edit_phone, edit_main_problem ;
    LinearLayout edit_birthday_layout ;
    ArrayList<String> list ;
    List<String> gender_list, edit_district_list, edit_thana_list ;
    Button submit, edit_submit ;
    int gender_flag = 0, dob_flag = 0, bitmap_flag = 0, district_flag = 0, thana_flag = 0 ;
    RadioButton male_btn, female_btn ;
    String header_name, token, pro_pic, age_string, main_problem, email, gender = "", birthday="", profession, district, district_id, upazilla, upazilla_id, first_name, last_name, user_id, phone ;
    String YY, MM, DD, dob = "", new_dob ;
    Bitmap bitmap ;
    private static final int PICK_IMAGE = 100 ;
    Uri imageURI ;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    int dd, mm, yy, age = 0 ;
    String[] split_birthday ;
    String month_str, exact_age, birthday_string ;

    ArrayList<String> upzila_id = new ArrayList<>() ;
    ArrayList<String> upzila_name = new ArrayList<>() ;

    CircleImageView edit_profile_pic ;
    String[] month_array = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"} ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Intent intent = getIntent();
        list = intent.getStringArrayListExtra("key");
        Log.d("++USER_ID+++", list.get(1));



        //EditProfileModel editProfileModel = list.get(0) ;
        //Log.d("++TAG+++", editProfileModel.getAvatar_file()) ;



        toolbarwithoutdrawer("Edit Profile");
        initilization();

        setValue();
        gender_list = new ArrayList<String>() ;
        edit_district_list = new ArrayList<String>() ;
        edit_thana_list = new ArrayList<String>() ;

        for(int i = 0 ; i < Constant.district_array.length ; i++){

            Log.d(Constant.district_array[i], String.valueOf(i)) ;
            edit_district_list.add(Constant.district_array[i]);
        }

        edit_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("+++clicked", "clicked"+bitmap_flag);
                takeImageFromGallery() ;
            }
        });


        edit_thana_list.add("Something");
        edit_thana_list.add("Something");


        /*ArrayAdapter<String> gender_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_edit_profile_gender_view, gender_list) ;
        gender_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_edit_profile_gender);
        edit_gender.setAdapter(gender_arrayAdapter);

        edit_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        ArrayAdapter<String> district_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_district_view, edit_district_list) ;
        district_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_district);
        edit_district.setAdapter(district_arrayAdapter);

        edit_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    district_flag = 0 ;
                    edit_thana_list.clear();
                    //district_id = "" ;
                    edit_thana_list.add("Choose Your Upzila");
                    ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, edit_thana_list) ;
                    thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                    edit_thana.setAdapter(thana_arrayAdapter);
                }

                if(position > 0){
                    district_flag = 1 ;
                    edit_thana_list.clear();
                    edit_thana_list.add("Choose Your Upzila");
                    district_id = Integer.toString(position) ;
                    Log.d("+++District+++", district_id) ;
                    String URL = Constant.ROOT_URL + "api/upazilla-list/"+district_id ;
                    Log.d("+++MAIN URL+++", URL) ;
                    ApiTaskUpzilaList apiTaskUpzilaList = new ApiTaskUpzilaList(EditProfileActivity.this, URL) ;
                    apiTaskUpzilaList.execute() ;

                    /*for(int i = 0 ; i < upzila_name.size() ; i++){
                        thana_list.add(upzila_name.get(i));
                    }
                    ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
                    thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                    thana_spinner.setAdapter(thana_arrayAdapter);*/
                    //thana_spinner.setAdapter(thana_arrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, edit_thana_list) ;
        thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
        edit_thana.setAdapter(thana_arrayAdapter);
        edit_thana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){
                    thana_flag = 0 ;
                }

                if(position > 0){

                    /*if(thana_list.get(position - 1) == upzila_name.get(position - 1)){
                        Log.d("+++++THANA++++", upzila_name.get(position-1));
                    }*/
                    thana_flag = 1 ;
                    Log.d("++++THANA++++",upzila_id.get(position - 1)) ;
                    upazilla_id = upzila_id.get(position - 1) ;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edit_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCartAction();
            }
        });


        edit_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                token = list.get(0);
                user_id = list.get(1) ;

                if(edit_first_name.getText().toString().trim().equals("")){
                    first_name = list.get(2);
                }
                else{
                    first_name = edit_first_name.getText().toString().trim() ;
                }
                Log.d("+++first_name+++",first_name) ;

                if(edit_last_name.getText().toString().trim().equals("")){
                    last_name = list.get(3);
                }
                else{
                    last_name = edit_last_name.getText().toString().trim() ;
                }
                Log.d("+++last_name+++",last_name) ;

                if(edit_email.getText().toString().trim().equals("")){
                    email = list.get(5);
                }
                else{
                    email = edit_first_name.getText().toString().trim() ;
                }
                Log.d("+++email+++", email) ;

                if(edit_profession.getText().toString().trim().equals("")){
                    profession = list.get(12);
                }
                else{
                    profession = edit_profession.getText().toString().trim() ;
                }
                Log.d("+++profession+++",profession) ;

                if(edit_phone.getText().toString().trim().equals("")){
                    phone = list.get(13);
                }
                else{
                    phone = edit_phone.getText().toString().trim() ;
                }
                Log.d("+++phone+++",phone) ;

                if(edit_main_problem.getText().toString().trim().equals("")){
                    main_problem = list.get(14);
                }
                else{
                    main_problem = edit_main_problem.getText().toString().trim() ;
                }
                Log.d("+++Profession+++",profession) ;

                if(gender_flag == 0){
                    gender = list.get(16) ;
                }
                Log.d("+++Gender+++",gender) ;
                if(dob_flag == 1){
                    Log.d("+++BirthdayON+++",birthday+"") ;
                    birthday = dob ;
                }
                if(dob_flag == 0){
                    Log.d("+++BirthdayOFF+++",birthday+"") ;
                    birthday = list.get(17);
                }

                if(bitmap_flag == 1){
                    Log.d("+++bitmap_flag++++", "yes") ;
                    pro_pic = getStringImage(bitmap) ;
                }
                else {
                    Log.d("+++bitmap_flag++++", "NO") ;
                    pro_pic = "" ;
                }
                Log.d("+++Pro_pic+++",pro_pic) ;

                if(district_flag == 0){
                    district_id = list.get(9) ;
                }
                Log.d("+++district_id+++",district_id) ;
                if(thana_flag == 0){
                    upazilla_id = list.get(11) ;
                }
                Log.d("+++upazilla_id+++",upazilla_id) ;

                ApiTaskRegister apiTaskRegister = new ApiTaskRegister(EditProfileActivity.this) ;
                apiTaskRegister.execute(first_name, last_name, email, gender,
                        district_id, upazilla_id, main_problem, birthday, pro_pic, token, user_id,
                        phone, profession) ;
            }
        });

    }

    private void initilization(){

        edit_district = (Spinner) findViewById(R.id.spinner_edit_profile_district) ;
        edit_thana = (Spinner) findViewById(R.id.spinner_edit_profile_thana) ;

        edit_birthday = (TextView) findViewById(R.id.edit_profile_birthday) ;
        edit_name = (TextView) findViewById(R.id.edit_profile_name) ;
        edit_age = (TextView) findViewById(R.id.edit_age) ;

        edit_email = (EditText) findViewById(R.id.edit_profile_email);
        edit_first_name = (EditText) findViewById(R.id.edit_profile_first_name);
        edit_last_name = (EditText) findViewById(R.id.edit_profile_last_name);
        edit_profession = (EditText) findViewById(R.id.edit_profile_profession);
        edit_phone = (EditText) findViewById(R.id.edit_profile_phone);
        edit_main_problem = (EditText) findViewById(R.id.edit_profile_main_problem);

        edit_profile_pic = (CircleImageView) findViewById(R.id.profile_edit_circle_pic);

        male_btn = (RadioButton) findViewById(R.id.male_btn) ;
        female_btn = (RadioButton) findViewById(R.id.female_btn);

        edit_submit = (Button) findViewById(R.id.edit_submit_button);
    }

    private void setValue(){

        edit_name.setText(list.get(4));
        edit_age.setText("Age "+list.get(7));
        edit_birthday.setText(list.get(6));

        edit_email.setHint(list.get(5));
        edit_first_name.setHint(list.get(2));
        edit_last_name.setHint(list.get(3));
        edit_phone.setHint(list.get(13));
        edit_profession.setHint(list.get(12));
        edit_main_problem.setHint(list.get(5));

        if(list.get(16).equals("Male") || list.get(16).equals("male")){
            male_btn.setChecked(true);
        }
        else if(list.get(16).equals("Female") || list.get(16).equals("female")){
            female_btn.setChecked(true);
        }

        Log.d("+++IMAGE NULL+++", list.get(15)) ;
        //Log.d("+++IMAGE NULL+++", list.get(15)) ;
        String pro_pic = list.get(15) ;

        if(bitmap_flag == 0){
            if(Patterns.WEB_URL.matcher(pro_pic).matches()){

                Glide.with(EditProfileActivity.this).load(pro_pic).placeholder(R.drawable.avtaar).dontAnimate().into(edit_profile_pic);
            }
            else{

                Glide.with(EditProfileActivity.this).load(Constant.ROOT_URL+pro_pic).placeholder(R.drawable.avtaar).into(edit_profile_pic);
            }
        }
        /*if(bitmap_flag == 1){

        }*/


    }

    private void dialogCartAction() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dailoug_edit_birthday);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT ;

        date = (NumberPicker) dialog.findViewById(R.id.dialog_date);
        month = (NumberPicker) dialog.findViewById(R.id.dialog_month);
        year = (NumberPicker) dialog.findViewById(R.id.dialog_year);

        date.setMaxValue(31);
        date.setMinValue(1);

        Calendar calendar = Calendar.getInstance();
        int cur_year = calendar.get(Calendar.YEAR);
        year.setMinValue(1950);
        year.setMaxValue(cur_year);
        year.setValue(cur_year);

        month.setDisplayedValues(month_array);
        month.setMinValue(0);
        month.setMaxValue(month_array.length - 1);

        setDividerColor(date, Color.parseColor("#bdbdbd"));
        setDividerColor(month, Color.parseColor("#bdbdbd"));
        setDividerColor(year, Color.parseColor("#bdbdbd"));
        dob = list.get(17) ;
        ImageView close_btn = (ImageView) dialog.findViewById(R.id.edit_dailog_close) ;

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submit = (Button) dialog.findViewById(R.id.edit_birthday_submit_btn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dob_flag = 1 ;
                YY = Integer.toString(year.getValue()) ;
                int mm = month.getValue() ;
                mm += 1 ;
                if(mm <= 9){
                    MM = "0"+Integer.toString(mm) ;
                }
                else{
                    MM = Integer.toString(mm) ;
                }

                int dd = date.getValue() ;
                if(dd <= 9){
                    DD = "0"+Integer.toString(date.getValue()) ;
                }
                else{
                    DD = Integer.toString(date.getValue()) ;
                }

                dob = YY+"-"+MM+"-"+DD ;
                setAge(dob);
                dialog.dismiss();

            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void selectGender(View view){

        boolean check_gender = ((RadioButton) view).isChecked() ;
        switch (view.getId()){

            case R.id.male_btn:
                if(check_gender){
                    gender_flag = 1 ;
                    gender = "Male" ;
                }
                break ;

            case R.id.female_btn:
                if(check_gender){
                    gender_flag = 1 ;
                    gender = "Female" ;
                }
                break ;
        }

    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void takeImageFromGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI) ;
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            bitmap_flag = 1 ;

            imageURI = data.getData() ;
            //edit_profile_pic.setImageURI(imageURI);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageURI) ;
                Log.d("+++SET PROFILE++", "YESSSS") ;
                edit_profile_pic.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.d("+++SET PROFILE++", "NO") ;
                e.printStackTrace();
            }
        }
    }

    private String getStringImage(Bitmap bitmap){
        /*final int maxSize = 960;
        int outWidth;
        int outHeight;
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        if(inWidth > inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }*/

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 800, 600, false);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream() ;
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream) ;
        byte[] imagebyte = byteArrayOutputStream.toByteArray() ;
        String encodeString = Base64.encodeToString(imagebyte, Base64.DEFAULT) ;
        Log.d("++++image++++", encodeString) ;
        return encodeString ;
    }

    public class ApiTaskUpzilaList extends AsyncTask<String, Void, String> {

        Context context ;
        String main_URL ;
        ProgressDialog progressDialog ;


        public ApiTaskUpzilaList(Context context, String URL){
            this.context = context ;
            this.main_URL = URL ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(main_URL) ;
                Log.d("+++URL+++", main_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setReadTimeout(READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                /*String login_username = params[0] ;
                String login_password = params[1] ;

                String data = URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(login_username, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(login_password, "UTF-8") ;
*/
                //bufferedWriter.write();
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                int response_code = httpURLConnection.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = httpURLConnection.getInputStream() ;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                    StringBuilder stringBuilder = new StringBuilder() ;
                    String line = "" ;

                    while((line = bufferedReader.readLine()) != null){
                        Log.d("++++TRUE+++", "YES");
                        stringBuilder.append(line + "\n") ;
                        Log.d("++++value+++", line) ;
                    }

                    httpURLConnection.disconnect();
                    return stringBuilder.toString().trim() ;
                }
                else{
                    Log.d("++++NOT WORKING+++", "network problem") ;
                    progressDialog.dismiss();
                    Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
                }

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            return null ;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                JSONArray mainjson_array = jsonObject.getJSONArray("result") ;
                Log.d("++++JSONObject+++++", String.valueOf(mainjson_array)) ;

                upzila_id.clear();
                upzila_name.clear();
                //thana_list.clear();

                for(int i = 0 ; i < mainjson_array.length() ; i++){
                    JSONObject array_jsonobj = mainjson_array.getJSONObject(i) ;
                    String id = array_jsonobj.getString("id");
                    String name = array_jsonobj.getString("name");
                    Log.d("+++id+++", id) ;
                    Log.d("+++name+++", name) ;
                    upzila_id.add(id);
                    upzila_name.add(name);
                }

                for(int i = 0 ; i < upzila_name.size() ; i++){
                    edit_thana_list.add(upzila_name.get(i));
                }

                ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, edit_thana_list) ;
                thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                edit_thana.setAdapter(thana_arrayAdapter);
                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
                e.printStackTrace();
                Log.d("++++JSONException+++++", String.valueOf(e)) ;
            }

        }
    }

    public class ApiTaskRegister extends AsyncTask<String, Void, String> {

        String register_url = Constant.ROOT_URL + "api/patients/profile/update/basic" ;
        Context context ;
        ProgressDialog progressDialog ;
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        public ApiTaskRegister(Context context){
            this.context = context ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(register_url) ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                /*apiTaskRegister.execute(first_name, last_name, email, gender,
                        district_id, upazilla_id, main_problem, dob, token, user_id,
                        pro_pic, phone, profession) ;*/

                String first_name = params[0] ;
                String last_name = params[1] ;
                //String username = params[2] ;
                String email = params[2] ;
                String gender = params[3] ;
                String district_position = params[4] ;
                String upzila_position = params[5] ;
                String main_problem = params[6] ;
                String dob = params[7] ;
                //String doctor_promo_code = params[9] ;
                String pro_pic = params[8] ;
                String token = params[9] ;
                String user_id = params[10] ;
                String phone = params[11] ;
                String profession = params[12] ;
                Log.d("+++MY PROFESSION+++", profession) ;
                Log.d("+++MY EMAIL+++", email) ;
                String data1 = URLEncoder.encode("first_name", "UTF-8")+"="+ URLEncoder.encode(first_name, "UTF-8")+"&"+
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(last_name, "UTF-8")+"&"+
                        URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("gender", "UTF-8")+"="+URLEncoder.encode(gender, "UTF-8")+"&"+
                        URLEncoder.encode("district", "UTF-8")+"="+URLEncoder.encode(district_position, "UTF-8")+"&"+
                        URLEncoder.encode("upazilla", "UTF-8")+"="+URLEncoder.encode(upzila_position, "UTF-8")+"&"+
                        URLEncoder.encode("main_problem", "UTF-8")+"="+URLEncoder.encode(main_problem, "UTF-8")+"&"+
                        URLEncoder.encode("dob", "UTF-8")+"="+URLEncoder.encode(dob, "UTF-8")+"&"+
                        URLEncoder.encode("token", "UTF-8")+"="+URLEncoder.encode(token, "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8")+"&"+
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8")+"&"+
                        URLEncoder.encode("profession", "UTF-8")+"="+URLEncoder.encode(profession, "UTF-8");

                String data = URLEncoder.encode("first_name", "UTF-8")+"="+ URLEncoder.encode(first_name, "UTF-8")+"&"+
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(last_name, "UTF-8")+"&"+
                        URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("gender", "UTF-8")+"="+URLEncoder.encode(gender, "UTF-8")+"&"+
                        URLEncoder.encode("district", "UTF-8")+"="+URLEncoder.encode(district_position, "UTF-8")+"&"+
                        URLEncoder.encode("upazilla", "UTF-8")+"="+URLEncoder.encode(upzila_position, "UTF-8")+"&"+
                        URLEncoder.encode("main_problem", "UTF-8")+"="+URLEncoder.encode(main_problem, "UTF-8")+"&"+
                        URLEncoder.encode("dob", "UTF-8")+"="+URLEncoder.encode(dob, "UTF-8")+"&"+
                        URLEncoder.encode("avatar_file", "UTF-8")+"="+URLEncoder.encode(pro_pic, "UTF-8")+"&"+
                        URLEncoder.encode("token", "UTF-8")+"="+URLEncoder.encode(token, "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(user_id, "UTF-8")+"&"+
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8")+"&"+
                        URLEncoder.encode("profession", "UTF-8")+"="+URLEncoder.encode(profession, "UTF-8");

                if(pro_pic.equals("")){
                    Log.d("+++Connection+++","DATA_1") ;
                    bufferedWriter.write(data1);
                }
                else{
                    Log.d("+++Connection+++","DATA") ;
                    bufferedWriter.write(data);
                }

                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpURLConnection.getInputStream() ;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                StringBuilder stringBuilder = new StringBuilder() ;
                String line = "" ;

                while((line = bufferedReader.readLine()) != null){
                    Log.d("++++TRUE+++", "YES");
                    stringBuilder.append(line + "\n") ;
                }
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim() ;

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                String result = jsonObject.getString("result") ;
                Log.d("+++JSONOBJ+++", String.valueOf(jsonObject)) ;

                JSONObject result_OBJ = jsonObject.getJSONObject("result") ;

                if(result_OBJ.getString("status").equals("success")){

                    /*SharedPreferences.Editor editor = sharedPreferences.edit() ;
                    editor.putString(Constant.user_pro_pic, pro_pic) ;
                    editor.putString(Constant.first_name, first_name) ;
                    editor.putString(Constant.last_name, last_name) ;
                    editor.commit() ;*/

                    Toast.makeText(context, "Profile Updated Successfull", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class) ;
                    startActivity(intent);
                    finish();

                }
                /*if(result.equals("error")){

                    Toast.makeText(context, "Invalid Email OR Phone OR Doctor Promo Code", Toast.LENGTH_SHORT).show();

                    *//*JSONObject msg_obj = jsonObject.getJSONObject("msg") ;
                    JSONArray err_email, err_doc_promo, err_phone, err_username ;
                    if(msg_obj.has("email")){
                        err_email = msg_obj.getJSONArray("email");
                        Log.d("+++Error_EMAIL++", err_email.getString(0)) ;
                        Email.setError(err_email.getString(0));
                    }

                    else if(msg_obj.has("doctor_promo_code")){
                        err_doc_promo = msg_obj.getJSONArray("doctor_promo_code");
                        Log.d("+++Error_DOCTOR_PROMO++", err_doc_promo.getString(0)) ;
                        doctor_Promo_Code.setError(err_doc_promo.getString(0));
                    }

                    else if(msg_obj.has("phone")){
                        err_phone = msg_obj.getJSONArray("phone");
                        Log.d("+++Error_PHONE++", err_phone.getString(0)) ;
                        Phone.setError(err_phone.getString(0));
                    }

                    else if(msg_obj.has("username")){
                        err_username = msg_obj.getJSONArray("username") ;
                        Log.d("+++Error_USERNAME++", err_username.getString(0)) ;
                        user_Name.setError(err_username.getString(0));
                    }*//*

                }*/
                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
                Log.d("+++JSONExcepREG+++", String.valueOf(e)) ;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void setAge(String dob){

        split_birthday = dob.split("-") ;
        yy = Integer.parseInt(split_birthday[0]) ;
        mm = Integer.parseInt(split_birthday[1]) ;
        dd = Integer.parseInt(split_birthday[2]) ;
        Log.d("+++YY+++", String.valueOf(yy)) ;
        Log.d("+++mm+++", String.valueOf(mm)) ;
        Log.d("+++dd+++", String.valueOf(dd)) ;
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int yyy = calendar.get(Calendar.YEAR) ;
        int mmm = calendar.get(Calendar.MONTH) ;
        mmm += 1 ;
        int ddd = calendar.get(Calendar.DATE) ;
        if(mmm < mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            age = age - 1 ;
            if(age == 0){
                mmm += 12 ;
                mmm = mmm - mm ;
                age_string = Integer.toString(mmm)+" Month" ;
            }
            else{
                age_string = Integer.toString(age) ;
            }
            Log.d("+++AGE++", exact_age) ;
        }
        else if(mmm >= mm){

            Log.d("+++YEAR++", String.valueOf(yyy)) ;
            age = yyy - yy ;
            if(age == 0){
                mmm = mmm - mm ;
                age_string = Integer.toString(mmm)+" Month" ;
            }
            else{
                age_string = Integer.toString(age) ;
            }
            Log.d("+++AGE++", age_string) ;
        }

        month_str = new DateFormatSymbols().getMonths()[mm-1];
        Log.d("+++MONTH++", month_str) ;
        //MONTH.of(monthNumber).name()
        birthday_string = Integer.toString(dd)+ " " + month_str + ", " + Integer.toString(yy) ;
        Log.d("+++MONTH++", birthday_string) ;
        edit_birthday.setText(birthday_string);
        birthday = birthday_string ;
    }
}
