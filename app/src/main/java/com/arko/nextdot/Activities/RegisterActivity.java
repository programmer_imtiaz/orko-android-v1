package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class RegisterActivity extends AppCompatActivity {

    Toolbar toolbar ;
    TextView toolbar_title ;
    ImageView profile_pic ;
    NumberPicker date, month, year ;
    Uri imageURI ;
    Bitmap bitmap ;
    Spinner district_spinner, thana_spinner, blood_group_spinner ;
    List<String> district_list, thana_list, blood_group_list ;
    AlertDialog.Builder internet_connection_failed ;
    int network_flag = 1, field_missing_flag = 0, bitmap_flag = 1 ;
    EditText first_Name, last_Name, user_Name, Email, Profession, main_Problem, Password, password_Confirmation, Phone, address_edittext ;
    ArrayList<String> upzila_id = new ArrayList<>() ;
    ArrayList<String> upzila_name = new ArrayList<>() ;
    private static final int PICK_IMAGE = 100 ;
    public static final int CONNECTION_TIMEOUT = 6000;
    public static final int READ_TIMEOUT = 6500;

    String first_name, last_name, username, email, gender = "Male", main_problem,
            doctor_promo_code="", avatar_file = "", password, password_confirmation, phone, profession, dob,
            address_string="", blood_group_string = "";

    String YY, MM, DD, district_position, upzila_position ;

    String[] month_array = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"} ;

    Button btn_signup ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initilization();
        setSupportActionBar(toolbar);
        showCustomToolbar();
        toolbar_title.setText("Sign Up");
        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap_flag = 0 ;
                takeImageFromGallery() ;
            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(getApplicationContext(), VerifyMobileActivity.class) ;
                startActivity(intent);*/

                Constant constant = new Constant(RegisterActivity.this) ;
                network_flag = constant.isNetworkActive() ;

                first_name = first_Name.getText().toString().trim() ;
                last_name = last_Name.getText().toString().trim() ;
                username = user_Name.getText().toString().trim() ;
                email = Email.getText().toString().trim() ;
                phone = Phone.getText().toString().trim() ;
                password = Password.getText().toString().trim() ;
                password_confirmation = password_Confirmation.getText().toString().trim() ;
                profession = Profession.getText().toString().trim() ;
                doctor_promo_code = "" ;
                main_problem = main_Problem.getText().toString().trim() ;
                address_string = address_edittext.getText().toString().trim() ;

                YY = Integer.toString(year.getValue()) ;
                int mm = month.getValue() ;
                mm += 1 ;
                if(mm <= 9){
                    MM = "0"+Integer.toString(mm) ;
                }
                else{
                    MM = Integer.toString(mm) ;
                }

                int dd = date.getValue() ;
                if(dd <= 9){
                    DD = "0"+Integer.toString(date.getValue()) ;
                }
                else{
                    DD = Integer.toString(date.getValue()) ;
                }

                if(bitmap_flag == 0){
                    avatar_file = getStringImage(bitmap) ;
                }
                else if(bitmap_flag == 1){
                    avatar_file = "" ;
                }
                dob = YY+"-"+MM+"-"+DD ;

                Log.d("+++first_name+++", first_name);
                Log.d("+++last_name+++", last_name);
                Log.d("+++username+++", username);
                Log.d("+++email+++", email);
                Log.d("+++phone+++", phone);
                Log.d("+++password+++", password);
                Log.d("+++password+++", password_confirmation);
                Log.d("+++profession+++", profession);
                Log.d("+++doctor_promo_code+++", doctor_promo_code);
                Log.d("+++main_problem+++", main_problem);
//                Log.d("+++YY+++", YY);
//                Log.d("+++MM+++", MM);
//                Log.d("+++DD+++", DD);
                Log.d("+++Gender+++", gender);
                Log.d("+++DOB+++", dob);
                Log.d("+++AVATAR+++", avatar_file);
                Log.d("+++District+++", district_position);
                Log.d("+++Upzila+++", upzila_position);

                if(username.equals("")){
                    user_Name.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                    Log.d("++Flase++", "1") ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(first_name.equals("")){
                    first_Name.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(first_name.equals("")){
                    last_Name.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(password.equals("")){
                    Password.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(password_confirmation.equals("")){
                    password_Confirmation.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }

                if(phone.equals("")){
                    Phone.setError("This field can not be blank");
                    field_missing_flag = 1 ;
                }
                else{
                    field_missing_flag = 0 ;
                }


                if(network_flag == 0 && field_missing_flag == 0){

                    ApiTaskRegister apiTaskRegister = new ApiTaskRegister(RegisterActivity.this) ;
                    apiTaskRegister.execute(first_name, last_name, username, email, gender,
                            district_position, upzila_position, main_problem, dob,doctor_promo_code,
                            avatar_file, password, password_confirmation, phone, profession, blood_group_string, address_string);
                }
                else if(network_flag == 1){

                    internetConnectFailedMessage();
                    AlertDialog alertDialog = internet_connection_failed.create() ;
                    alertDialog.show();
                }

            }
        });

        date.setMaxValue(31);
        date.setMinValue(1);

        Calendar calendar = Calendar.getInstance();
        int cur_year = calendar.get(Calendar.YEAR);

        year.setMinValue(1950);
        year.setMaxValue(cur_year);
        year.setValue(cur_year);

        month.setDisplayedValues(month_array);
        month.setMinValue(0);
        month.setMaxValue(month_array.length - 1);

        setDividerColor(date, Color.parseColor("#bdbdbd"));
        setDividerColor(month, Color.parseColor("#bdbdbd"));
        setDividerColor(year, Color.parseColor("#bdbdbd"));

        district_list = new ArrayList<String>() ;
        thana_list = new ArrayList<String>() ;
        blood_group_list = new ArrayList<String>() ;

        for(int i = 0 ; i < Constant.district_array.length ; i++){

            Log.d(Constant.district_array[i], String.valueOf(i)) ;
            district_list.add(Constant.district_array[i]);
        }
        for(int i = 0 ; i < Constant.blood_group_array.length ; i++){

            Log.d(Constant.blood_group_array[i], String.valueOf(i)) ;
            blood_group_list.add(Constant.blood_group_array[i]);
        }

        thana_list.add("Choose District first");


        ArrayAdapter<String> district_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_district_view, district_list) ;
        district_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_district);
        district_spinner.setAdapter(district_arrayAdapter);

        ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
        thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
        thana_spinner.setAdapter(thana_arrayAdapter);

        ArrayAdapter<String> blood_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_blood_group_view, blood_group_list) ;
        blood_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_blood_group);
        blood_group_spinner.setAdapter(blood_arrayAdapter);


        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){
                    thana_list.clear();
                    district_position = "" ;
                    thana_list.add("Choose Your Upzila");
                    ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
                    thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                    thana_spinner.setAdapter(thana_arrayAdapter);
                }

                if(position > 0){
                    thana_list.clear();
                    thana_list.add("Choose Your Upzila");
                    district_position = Integer.toString(position) ;
                    Log.d("+++District+++", district_position) ;
                    String URL = Constant.ROOT_URL + "api/upazilla-list/" + district_position ;
                    Log.d("+++MAIN URL+++", URL) ;
                    ApiTaskUpzilaList apiTaskUpzilaList = new ApiTaskUpzilaList(RegisterActivity.this, URL) ;
                    apiTaskUpzilaList.execute() ;

                    /*for(int i = 0 ; i < upzila_name.size() ; i++){
                        thana_list.add(upzila_name.get(i));
                    }
                    ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
                    thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                    thana_spinner.setAdapter(thana_arrayAdapter);*/
                    //thana_spinner.setAdapter(thana_arrayAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        thana_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){
                    upzila_position = "" ;
                }

                if(position > 0){

                    /*if(thana_list.get(position - 1) == upzila_name.get(position - 1)){
                        Log.d("+++++THANA++++", upzila_name.get(position-1));
                    }*/
                    Log.d("++++THANA++++",upzila_id.get(position - 1)) ;
                    upzila_position = upzila_id.get(position - 1) ;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        blood_group_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    blood_group_string = "" ;
                }
                if(position > 0){
                    blood_group_string = blood_group_list.get(position) ;
                    Log.d("+++Blood_group+++", blood_group_string) ;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void initilization(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        profile_pic = (ImageView) findViewById(R.id.circle_profile_pic) ;
        btn_signup = (Button) findViewById(R.id.reg_signup);
        date = (NumberPicker) findViewById(R.id.date);
        month = (NumberPicker) findViewById(R.id.month) ;
        year = (NumberPicker) findViewById(R.id.year);

//        male = (LinearLayout) findViewById(R.id.male);
//        female = (LinearLayout) findViewById(R.id.female);

       // medical_condition = (Spinner) findViewById(R.id.spinner_medical_condition);

        district_spinner = (Spinner) findViewById(R.id.spinner_district) ;
        thana_spinner  = (Spinner) findViewById(R.id.spinner_thana) ;
        blood_group_spinner  = (Spinner) findViewById(R.id.spinner_blood_group) ;

        first_Name = (EditText) findViewById(R.id.reg_your_first_name);
        last_Name  = (EditText) findViewById(R.id.reg_your_last_name) ;
        main_Problem = (EditText) findViewById(R.id.medical_condition) ;
        Password = (EditText) findViewById(R.id.reg_password) ;
        password_Confirmation = (EditText) findViewById(R.id.reg_rep_passwrod) ;
        Phone = (EditText) findViewById(R.id.reg_mobile) ;
        user_Name = (EditText) findViewById(R.id.reg_user_name) ;
        Email = (EditText) findViewById(R.id.reg_email_address) ;
        Profession = (EditText) findViewById(R.id.reg_profession) ;
        address_edittext = (EditText) findViewById(R.id.patient_address) ;

    }


    private void internetConnectFailedMessage(){
        internet_connection_failed = new AlertDialog.Builder(RegisterActivity.this);
        internet_connection_failed.setTitle("Warning!") ;
        internet_connection_failed.setMessage("Please Check Your Internet Connection") ;
        internet_connection_failed.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void showCustomToolbar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void takeImageFromGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI) ;
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            bitmap_flag = 0 ;
            imageURI = data.getData() ;
            //profile_pic.setImageURI(imageURI);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageURI) ;
                profile_pic.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getStringImage(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream() ;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream) ;
        byte[] imagebyte = byteArrayOutputStream.toByteArray() ;
        String encodeString = Base64.encodeToString(imagebyte, Base64.DEFAULT) ;
        return encodeString ;
    }


    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void selectGender(View view){

        boolean check_gender = ((RadioButton) view).isChecked() ;
        switch (view.getId()){

            case R.id.male_btn:
                if(check_gender){
                    gender = "Male" ;
                }
                break ;

            case R.id.female_btn:
                if(check_gender){
                    gender = "Female" ;
                }
                break ;
        }

    }

    public class ApiTaskUpzilaList extends AsyncTask<String, Void, String> {

        Context context ;
        String main_URL ;
        ProgressDialog progressDialog ;


        public ApiTaskUpzilaList(Context context, String URL){
            this.context = context ;
            this.main_URL = URL ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(main_URL) ;
                Log.d("+++URL+++", main_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setReadTimeout(READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                /*String login_username = params[0] ;
                String login_password = params[1] ;

                String data = URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(login_username, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(login_password, "UTF-8") ;
*/
                //bufferedWriter.write();
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                int response_code = httpURLConnection.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = httpURLConnection.getInputStream() ;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                    StringBuilder stringBuilder = new StringBuilder() ;
                    String line = "" ;

                    while((line = bufferedReader.readLine()) != null){
                        Log.d("++++TRUE+++", "YES");
                        stringBuilder.append(line + "\n") ;
                        Log.d("++++value+++", line) ;
                    }

                    httpURLConnection.disconnect();
                    return stringBuilder.toString().trim() ;
                }
                else{
                    Log.d("++++NOT WORKING+++", "network problem") ;
                    progressDialog.dismiss();
                    Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
                }

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            /*finally {
                progressDialog.dismiss();
                Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
            }*/
            return null ;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                JSONArray mainjson_array = jsonObject.getJSONArray("result") ;
                Log.d("++++JSONObject+++++", String.valueOf(mainjson_array)) ;

                upzila_id.clear();
                upzila_name.clear();
                //thana_list.clear();

                for(int i = 0 ; i < mainjson_array.length() ; i++){
                    JSONObject array_jsonobj = mainjson_array.getJSONObject(i) ;
                    String id = array_jsonobj.getString("id");
                    String name = array_jsonobj.getString("name");
                    Log.d("+++id+++", id) ;
                    Log.d("+++name+++", name) ;
                    upzila_id.add(id);
                    upzila_name.add(name);
                }

                for(int i = 0 ; i < upzila_name.size() ; i++){
                    thana_list.add(upzila_name.get(i));
                }
                ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
                thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                thana_spinner.setAdapter(thana_arrayAdapter);
                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
                e.printStackTrace();
                Log.d("++++JSONException+++++", String.valueOf(e)) ;
            }

        }
    }

    public class ApiTaskRegister extends AsyncTask<String, Void, String> {

        String register_url = Constant.ROOT_URL + "api/register/patient" ;
        Context context ;
        ProgressDialog progressDialog ;

        public ApiTaskRegister(Context context){
            this.context = context ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(register_url) ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                String first_name = params[0] ;
                String last_name = params[1] ;
                String username = params[2] ;
                String email = params[3] ;
                String gender = params[4] ;
                String district_position = params[5] ;
                String upzila_position = params[6] ;
                String main_problem = params[7] ;
                String dob = params[8] ;
                String doctor_promo_code = params[9] ;
                String avatar_file = params[10] ;
                String password = params[11] ;
                String password_confirmation= params[12] ;
                String phone = params[13] ;
                String profession = params[14] ;
                String blood_group = params[15] ;
                String address = params[16] ;

                String data = URLEncoder.encode("first_name", "UTF-8")+"="+ URLEncoder.encode(first_name, "UTF-8")+"&"+
                        URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(last_name, "UTF-8")+"&"+
                        URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(username, "UTF-8")+"&"+
                        URLEncoder.encode("email", "UTF-8")+"="+URLEncoder.encode(email, "UTF-8")+"&"+
                        URLEncoder.encode("gender", "UTF-8")+"="+URLEncoder.encode(gender, "UTF-8")+"&"+
                        URLEncoder.encode("district", "UTF-8")+"="+URLEncoder.encode(district_position, "UTF-8")+"&"+
                        URLEncoder.encode("upazilla", "UTF-8")+"="+URLEncoder.encode(upzila_position, "UTF-8")+"&"+
                        URLEncoder.encode("main_problem", "UTF-8")+"="+URLEncoder.encode(main_problem, "UTF-8")+"&"+
                        URLEncoder.encode("dob", "UTF-8")+"="+URLEncoder.encode(dob, "UTF-8")+"&"+
                        URLEncoder.encode("doctor_promo_code", "UTF-8")+"="+URLEncoder.encode(doctor_promo_code, "UTF-8")+"&"+
                        URLEncoder.encode("avatar_file", "UTF-8")+"="+URLEncoder.encode(avatar_file, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8")+"&"+
                        URLEncoder.encode("password_confirmation", "UTF-8")+"="+URLEncoder.encode(password_confirmation, "UTF-8")+"&"+
                        URLEncoder.encode("phone", "UTF-8")+"="+URLEncoder.encode(phone, "UTF-8")+"&"+
                        URLEncoder.encode("profession", "UTF-8")+"="+URLEncoder.encode(profession, "UTF-8")+"&"+
                        URLEncoder.encode("blood_group", "UTF-8")+"="+URLEncoder.encode(blood_group, "UTF-8")+"&"+
                        URLEncoder.encode("address", "UTF-8")+"="+URLEncoder.encode(address, "UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                InputStream inputStream = httpURLConnection.getInputStream() ;
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                StringBuilder stringBuilder = new StringBuilder() ;
                String line = "" ;

                while((line = bufferedReader.readLine()) != null){
                    Log.d("++++TRUE+++", "YES");
                    stringBuilder.append(line + "\n") ;
                }
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim() ;

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                String result = jsonObject.getString("result") ;
                Log.d("+++JSONOBJ+++", String.valueOf(jsonObject)) ;
                if(result.equals("success")){
                    Toast.makeText(context, "Registration Successfull", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class) ;
                    startActivity(intent);
                    finish();
                }
                if(result.equals("error")){

                    Toast.makeText(context, "Invalid Email OR Phone OR Doctor Promo Code", Toast.LENGTH_SHORT).show();

                    JSONObject msg_obj = jsonObject.getJSONObject("msg") ;
                    JSONArray err_email, err_doc_promo, err_phone, err_username ;
                    if(msg_obj.has("email")){
                        err_email = msg_obj.getJSONArray("email");
                        Log.d("+++Error_EMAIL++", err_email.getString(0)) ;
                        Email.setError(err_email.getString(0));
                    }

                    if(msg_obj.has("phone")){
                        err_phone = msg_obj.getJSONArray("phone");
                        Log.d("+++Error_PHONE++", err_phone.getString(0)) ;
                        Phone.setError(err_phone.getString(0));
                    }

                    if(msg_obj.has("username")){
                        err_username = msg_obj.getJSONArray("username") ;
                        Log.d("+++Error_USERNAME++", err_username.getString(0)) ;
                        user_Name.setError(err_username.getString(0));
                    }

                }
                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
                Log.d("+++JSONExcepREG+++", String.valueOf(e)) ;
            }
        }
    }
}
