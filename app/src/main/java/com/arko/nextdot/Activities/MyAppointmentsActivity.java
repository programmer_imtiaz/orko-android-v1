package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.arko.nextdot.Fragment.HistoryAppointmentFragment;
import com.arko.nextdot.Fragment.UpcomingAppointmentFragment;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 11/23/2017.
 */

public class MyAppointmentsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appointments_history_tablayout)
    TabLayout appointmentsHistoryTablayout;
    @BindView(R.id.appointments_history_viewpager)
    ViewPager appointmentsHistoryViewpager;
    @BindView(R.id.appointments_container)
    CoordinatorLayout appointmentsContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments_history);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        showCustomToolbar();
        toolbarTitle.setText("My Appointments");
        setupViewPager(appointmentsHistoryViewpager);
        appointmentsHistoryTablayout.setupWithViewPager(appointmentsHistoryViewpager);
    }

    private void showCustomToolbar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager()) ;
        viewPagerAdapter.addFragment(new HistoryAppointmentFragment(), "HISTORY");
        viewPagerAdapter.addFragment(new UpcomingAppointmentFragment(), "UPCOMING");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>() ;
        private final List<String> fragmentTitle = new ArrayList<>() ;

        public ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position) ;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }


        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmentTitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position) ;
        }
    }
}
