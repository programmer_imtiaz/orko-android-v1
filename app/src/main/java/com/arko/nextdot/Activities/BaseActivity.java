package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.NavDrawerAdapter;
import com.arko.nextdot.Model.RetrofitModel.LogoutResponse.LogoutResponse;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BaseActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    List<String> nav_item_titles = new ArrayList();
    List<Integer> nav_item_img = new ArrayList();
    TextView patient_name, patient_email;
    CircleImageView patient_pic;
    String pro_pic_image_URL, pro_firstname, pro_secondname, pro_email, userId;
    String[] titles;
    private static int lastClicked = 0;
    //SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

    /*int[] img = {R.drawable.home,
            R.drawable.my_profile,
            R.drawable.transaction,
            R.drawable.helpline,
            R.drawable.capsule,
            R.drawable.appointments,
            R.drawable.settings,
            R.drawable.logout,
            R.drawable.logout} ;*/
    int[] img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //hideStatusBar();
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedPreferences.getString(Constant.facebook_login, "").equals("0")) {
            titles = new String[]{"My Appointments", "Profile", "Medical Records", "Search Doctor", "Notifications", "Add Non Orko Doctor", "My Doctors", "Settings", "Logout"};
            img = new int[]{R.drawable.appointments,
                    R.drawable.my_profile,
                    R.drawable.records,
                    R.drawable.doctor_search,
                    R.drawable.transaction,
                    R.drawable.capsule,
                    R.drawable.my_doctor,
                    R.drawable.settings,
                    R.drawable.logout
            };
        } else if (sharedPreferences.getString(Constant.facebook_login, "").equals("1")) {
            titles = new String[]{"My Appointment", "Profile", "Medical Records", "Search Doctor", "Notifications", "Add Non Orko Doctor", "My Doctors", "Logout"};
            img = new int[]{R.drawable.appointments,
                    R.drawable.my_profile,
                    R.drawable.records,
                    R.drawable.doctor_search,
                    R.drawable.transaction,
                    R.drawable.capsule,
                    R.drawable.my_doctor,
                    R.drawable.logout
            };
        }
    }

    private void hideStatusBar() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initilization() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View navigationViewHeaderView = navigationView.getHeaderView(0);
        patient_name = (TextView) navigationViewHeaderView.findViewById(R.id.nav_pro_name);
        patient_pic = (CircleImageView) navigationViewHeaderView.findViewById(R.id.nav_profile_img);
        patient_email = (TextView) navigationViewHeaderView.findViewById(R.id.nav_pro_email);
    }

    private void addtolist() {
        for (int i = 0; i < img.length; i++) {
            nav_item_img.add(img[i]);
            nav_item_titles.add(titles[i]);
        }
    }

    public void toolbarwithoutdrawer(String toolbartitle) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        showCustomToolbar(toolbar);
        toolbar_title.setText(toolbartitle);
    }

    private void showCustomToolbar(Toolbar toolbar) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initilization();
        setupNavDrawerItem();
    }

    private void setupNavDrawerItem() {
        //ArrayList<Category> categories = DemoData.fetchCategories();

        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        if (sharedPreferences.getString(Constant.facebook_login, "").equals("1")) {

            pro_pic_image_URL = sharedPreferences.getString(Constant.user_pro_pic, "");
            pro_firstname = sharedPreferences.getString(Constant.first_name, "");
            pro_secondname = sharedPreferences.getString(Constant.last_name, "");
            patient_name.setText(pro_firstname + " " + pro_secondname);
            Log.d("++++NAV_IMAGE+++", pro_pic_image_URL);
            if (Patterns.WEB_URL.matcher(pro_pic_image_URL).matches()) {

                Glide.with(this).load(pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(patient_pic);
            } else {

                Glide.with(this).load(Constant.ROOT_URL + pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(patient_pic);
            }
        } else {

            pro_pic_image_URL = sharedPreferences.getString(Constant.user_pro_pic, "");
            pro_firstname = sharedPreferences.getString(Constant.first_name, "");
            pro_secondname = sharedPreferences.getString(Constant.last_name, "");
            pro_email = sharedPreferences.getString(Constant.email, "");

            patient_name.setText(pro_firstname + " " + pro_secondname);
            Log.d("++++NAV_IMAGE+++", Constant.ROOT_URL + pro_pic_image_URL);

            if (Patterns.WEB_URL.matcher(pro_pic_image_URL).matches()) {

                Glide.with(this).load(pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(patient_pic);
            } else {
                Glide.with(this).load(Constant.ROOT_URL + pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(patient_pic);
            }
            patient_email.setText(pro_email);
        }

        addtolist();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ListView categoryList = (ListView) navigationView.findViewById(R.id.nav_item_list);
        categoryList.setAdapter(new NavDrawerAdapter(this, nav_item_titles, nav_item_img));
        categoryList.setOnItemClickListener(this);


    }

    void setupNavDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }


    void setupNavDrawer1() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        setSupportActionBar(toolbar);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    void setupActionUpNavDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_activity_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_cart) {
            return true;
        } else if (id == R.id.action_help) {
            return true;
        }*/

//        if(id == R.id.action_help)
//        {
//            return true ;
//        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        Handler mHandler = new Handler();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

                if (sharedPreferences.getString(Constant.facebook_login, "").equals("0")) {
                    /*if (lastClicked != position) {


                    }
                    lastClicked = position;*/

                    if (position == 0) {

                        Intent intent = new Intent(getApplicationContext(), MyAppointmentsActivity.class);
                        startActivity(intent);
//                            finish();

                    }
                    else if (position == 1) {
                        Intent intent = new Intent(getApplicationContext(), PatientMedicalProfileActivity.class);
                        startActivity(intent);
//                            finish();
                    }
                    else if (position == 2) {
                        Intent intent = new Intent(getApplicationContext(), PrescriptionsNReports.class);
                        startActivity(intent);
//                            finish();

                    }
                    else if (position == 3) {

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Constant.searchbyspeciality, "Search By SpecialityClass, Issue");
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(), OrkoHomeActivity.class);
                        startActivity(intent);
//                            finish();
                    }
                    else if (position == 4) {
                        Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
                        startActivity(intent);
//                            finish();
                    }
                    else if (position == 5) {

                        Intent intent = new Intent(getApplicationContext(), AddNonOrkoDoctorActivity.class);
                        startActivity(intent);
//                            finish();
                    }
                    else if (position == 6) {

                        Intent intent = new Intent(getApplicationContext(), MyDoctorsActivity.class);
                        startActivity(intent);
//                            finish();
                    }
                    else if (position == 7) {
                        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else if (position == 8) {

                        Toast.makeText(getApplicationContext(), "Signing out...", Toast.LENGTH_SHORT).show();


                        userId = sharedPreferences.getString(Constant.user_id, "");
                        String deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID);

                            /*SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Constant.facebook_login, "0");
                            editor.putString(Constant.fb_req_login, "0");
                            editor.putString(Constant.userlogin_flag, "0");
                            editor.putString(Constant.user_name, "");
                            editor.putString(Constant.email, "");
                            editor.putString(Constant.token, "");
                            editor.putString(Constant.user_pro_pic, "");
                            editor.putString(Constant.first_name, "");
                            editor.putString(Constant.last_name, "");
                            editor.putString(Constant.user_id, "");
                            editor.putString(Constant.FCM_TOKEN, "empty") ;
                            editor.commit();

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();*/
                        logoutAPI(deviceID);
                    }
                } else if (sharedPreferences.getString(Constant.facebook_login, "").equals("1")) {

                    titles = new String[]{"My Appointments", "Profile", "Medical Records", "Search Doctors", "Transaction History", "Order Medicine", "My Doctors", "Logout"};
                    /*if (lastClicked != position) {

                    }
                    lastClicked = position;*/

                    if (position == 0) {
                        Intent intent = new Intent(getApplicationContext(), Appointments.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 1) {
                        Intent intent = new Intent(getApplicationContext(), PatientMedicalProfileActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 2) {
                        Intent intent = new Intent(getApplicationContext(), PrescriptionsNReports.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 3) {

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Constant.searchbyspeciality, "Search By SpecialityClass, Issue");
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(), OrkoHomeActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 4) {
                        Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 5) {

                        Intent intent = new Intent(getApplicationContext(), AddNonOrkoDoctorActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 6) {
                        Intent intent = new Intent(getApplicationContext(), MyDoctorsActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                    else if (position == 7) {

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Constant.facebook_login, "0");
                        editor.putString(Constant.fb_req_login, "0");
                        editor.putString(Constant.userlogin_flag, "0");
                        editor.putString(Constant.user_name, "");
                        editor.putString(Constant.email, "");
                        editor.putString(Constant.token, "");
                        editor.putString(Constant.user_pro_pic, "");
                        editor.putString(Constant.first_name, "");
                        editor.putString(Constant.last_name, "");
                        editor.putString(Constant.user_id, "");
                        editor.commit();

                        LoginManager.getInstance().logOut();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, 200);


    }

    private void logoutAPI(String deviceID) {
        Toast.makeText(this, "Wow, Im here", Toast.LENGTH_SHORT).show();
        SharedPreferences sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);

        Log.d("USER_ID", userId);

        Log.d("DEVICE_ID", deviceID);

        RetrofitInterface apiInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);
        Call<LogoutResponse> logoutResponseCall = apiInterface.logout(userId,deviceID);
        logoutResponseCall.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        String msg = response.body().getMsg();

                        if (msg.equals("success")) {


                            Log.e("logoutAPICALL", " MESSAGE" + " id=" + msg);

                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Constant.facebook_login, "0");
                            editor.putString(Constant.fb_req_login, "0");
                            editor.putString(Constant.userlogin_flag, "0");
                            editor.putString(Constant.user_name, "");
                            editor.putString(Constant.email, "");
                            editor.putString(Constant.token, "");
                            editor.putString(Constant.user_pro_pic, "");
                            editor.putString(Constant.first_name, "");
                            editor.putString(Constant.last_name, "");
                            editor.putString(Constant.user_id, "");
                            editor.putString(Constant.FCM_TOKEN, "empty") ;
                            editor.commit();

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();

//                            String token = user.getToken();
//                            String id = user.getUserProfile().getUserId() + "";
//
//                            loadProfileInfoWithRetrofit(token, id);


                        } else {

                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {


                // Do something to make a continuous call until success!

                Toast.makeText(getApplicationContext(), "Please check your internet connection...", Toast.LENGTH_SHORT).show();
                Log.e("log_in", t + "");

            }
        });
    }
}
