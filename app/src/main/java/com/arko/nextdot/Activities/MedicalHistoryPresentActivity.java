package com.arko.nextdot.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.arko.nextdot.Fragment.PrescriptionFragment;
import com.arko.nextdot.Fragment.TestReportFragment;
import com.arko.nextdot.Fragment.TherapyFragment;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dipto on 5/10/2017.
 */

public class MedicalHistoryPresentActivity extends ToolbarBaseActivity {


    ViewPager viewPager ;
    TabLayout tabLayout ;

    Button present_btn, past_btn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_present);
        toolbarwithoutdrawer("Medial Report / History");
        initilization();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setDividerBetweenTabs();

        past_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                past_btn.setTextColor(getResources().getColor(R.color.your_selected_text_color));
                present_btn.setTextColor(getResources().getColor(R.color.your_unselected_text_color));
                Intent intent = new Intent(getApplicationContext(), MedicalHistoryPastActivity.class);
                intent.putExtra("caller", "MedicalHistoryPresentActivity");
                startActivity(intent);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter_Med_present viewPagerAdapter = new ViewPagerAdapter_Med_present(getSupportFragmentManager()) ;
        viewPagerAdapter.addFragment(new TestReportFragment(), "Test Report");
        viewPagerAdapter.addFragment(new TherapyFragment(), "Therapy");
        viewPagerAdapter.addFragment(new PrescriptionFragment(), "Prescription");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter_Med_present extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>() ;
        private final List<String> fragmentTitle = new ArrayList<>() ;

        public ViewPagerAdapter_Med_present(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position) ;
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }


        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmentTitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position) ;
        }
    }

    private void initilization(){
        tabLayout = (TabLayout) findViewById(R.id.med_present_tablayout);
        viewPager = (ViewPager) findViewById(R.id.med_present_view_pager) ;
        present_btn = (Button) findViewById(R.id.med_PRESENT_present_btn);
        past_btn = (Button) findViewById(R.id.med_PRESENT_past_btn) ;
    }

    private void setDividerBetweenTabs(){

        //divider between tabs
        LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);
        //divider between tabs
    }
}
