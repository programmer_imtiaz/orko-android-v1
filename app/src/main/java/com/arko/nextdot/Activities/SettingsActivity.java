package com.arko.nextdot.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SettingsActivity extends ToolbarBaseActivity {

    int i;

    private Resources res = null;
    private AlertDialog alert;
    private boolean networkState;
    private ConnectivityManager cm;
    private NetworkInfo activeNetwork;

    private TextView change_pass_button;
    private TextView change_office_hours;
    private EditText inputOldPass, inputNewPass, inputConfirmPass;
    private String oldPassword, newPassword, confirmPassword;

    private LinearLayout buttonLayout;
    private RelativeLayout changePasswordLayout;
    private LinearLayout innerLL;

    private Button submitButton, cancelButton;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    //PreferenceManager preferenceManager = null;

    String user_id, user_token;
    String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        getUserData();
        initializeValues();
        networkChecker();
        onTouchListeners();

        toolbarwithoutdrawer("Settings");


    }


    /**
     * Validating form
     */
    private void submitForgotForm() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }


        if (networkState) {

            if (checkPasswordFields()) {
                Log.d("error", "statusReport:");
                new ChangePasswordAPI(inputOldPass.getText().toString().trim(), inputNewPass.getText().toString().trim(), inputConfirmPass.getText().toString().trim()).execute();
            }

        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
            alert.setTitle("WARNING!!!"); //Set Alert dialogqqqq title here
            alert.setMessage("Check your network connection."); //Message here
            alert.setIcon(R.drawable.ic_alert);


            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    dialog.cancel();
                    changePasswordLayout.setVisibility(View.GONE);
                    buttonLayout.setVisibility(View.VISIBLE);

                } // End of onClick(DialogInterface dialogqqqq, int whichButton)
            }); //End of alert.setPositiveButton

            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }


//        Log.d("error", "statusReport:");
//        new SecondCallAPI(inputEmail2.getText().toString().trim()).execute();


    }


    private void networkChecker() {

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                networkState = true;
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                networkState = true;
        } else
            networkState = false;
    }


    private boolean checkPasswordFields() {

        oldPassword = inputOldPass.getText().toString().trim();
        newPassword = inputNewPass.getText().toString().trim();
        confirmPassword = inputConfirmPass.getText().toString().trim();

        Log.d("editTextValues", "old: "+ oldPassword + " new: " + newPassword + " confirm: " + confirmPassword);


        if (oldPassword.length()<6 || newPassword.length()<6 || confirmPassword.length()<6|| !newPassword.equals(confirmPassword)) {

            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }


            /* Alert Dialog Code Start*/
            AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
            alert.setTitle("Password Error!"); //Set Alert dialog title here
            alert.setMessage("Password values are either invalid or they do not match"); //Message here
            alert.setIcon(R.drawable.ic_alert);


            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    dialog.cancel();

                    inputOldPass.setText("");
                    inputNewPass.setText("");
                    inputConfirmPass.setText("");

                    changePasswordLayout.setVisibility(View.VISIBLE);

                } // End of onClick(DialogInterface dialogqqqq, int whichButton)
            }); //End of alert.setPositiveButton

            AlertDialog alertDialog = alert.create();
            alertDialog.show();

            return false;

        } else {

            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            return true;
        }

    }


    private void getUserData() {

        //preferenceManager = PreferenceManager.getInstance(SettingsActivity.this);
        user_id = sharedPreferences.getString(Constant.user_id, "") ;
        user_token = sharedPreferences.getString(Constant.token, "") ;

        Log.d("user_id:", user_id);
        Log.d("user_token:", user_token);

    }

    private void initializeValues() {

        change_pass_button = (TextView) findViewById(R.id.changePass);

        cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();

        res = getResources();

        inputOldPass = (EditText) findViewById(R.id.inputOldPassword);
        inputNewPass = (EditText) findViewById(R.id.inputNewPassword);
        inputConfirmPass = (EditText) findViewById(R.id.inputConfirmPassword);

        buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
        changePasswordLayout = (RelativeLayout) findViewById(R.id.changePasswordLayout);

        submitButton = (Button) findViewById(R.id.btn_submit);
        cancelButton = (Button) findViewById(R.id.btn_cancel);

        innerLL = (LinearLayout) findViewById(R.id.innerLL);

    }


    private void onTouchListeners() {

        ((TextView) findViewById(R.id.changePass)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        TextView view = (TextView) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        // Your action here on button click
                        buttonLayout.setVisibility(View.GONE);
                        changePasswordLayout.setVisibility(View.VISIBLE);
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        TextView view = (TextView) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });


        ((Button) findViewById(R.id.btn_submit)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        // Your action here on button click

                        changePasswordLayout.setVisibility(View.GONE);
                        submitForgotForm();
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });


        ((Button) findViewById(R.id.btn_cancel)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        // Your action here on button click
                        inputOldPass.setText("");
                        inputNewPass.setText("");
                        inputConfirmPass.setText("");

                        changePasswordLayout.setVisibility(View.GONE);
                        buttonLayout.setVisibility(View.VISIBLE);
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
    }


    public class ChangePasswordAPI extends AsyncTask<String, String, String> {

        String oldPass, newPass, confirmPass;


        public ChangePasswordAPI(String oldPass, String newPass, String confirmPass) {
            //set context variables if required
            this.oldPass = oldPass;
            this.newPass = newPass;
            this.confirmPass = confirmPass;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("error", "statusReport:");

            changePasswordLayout.setVisibility(View.GONE);
            buttonLayout.setVisibility(View.GONE);
            innerLL.setVisibility(View.VISIBLE);

        }


        @Override
        protected String doInBackground(String... params) {

//            String urlString = params[0]; // URL to call

            String resultToDisplay = "";
//*****************************************************************
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/settings/" + user_id);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("token", user_token));
            nameValuePairs.add(new BasicNameValuePair("old_password", oldPass));
            nameValuePairs.add(new BasicNameValuePair("new_password", newPass));
            nameValuePairs.add(new BasicNameValuePair("confirm_new_password", confirmPass));


// Execute HTTP Post Request
            HttpResponse response = null;


            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }


        @Override
        protected void onPostExecute(String result) {
            //Update the UI
            super.onPostExecute(result);


            try {
                Log.d("jsonData", "+++++++++" + result);
                JSONObject jsonObj = new JSONObject(result);
                JSONObject jsonObj1 = new JSONObject(jsonObj.getString("result"));
                String status = jsonObj1.getString("status");
                Log.d("jsonData", "+++++++++" + status);
                if (status.equals("success")) {


                    AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
                    alert.setTitle("Success!"); //Set Alert dialogqqqq title here
                    alert.setMessage("Password successfully updated."); //Message here
                    alert.setIcon(R.drawable.ic_success);


                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            if (getCurrentFocus() != null) {
                                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            }

                            inputOldPass.setText("");
                            inputNewPass.setText("");
                            inputConfirmPass.setText("");
                            dialog.cancel();

                            innerLL.setVisibility(View.GONE);
                            changePasswordLayout.setVisibility(View.GONE);
                            buttonLayout.setVisibility(View.VISIBLE);

                            SharedPreferences.Editor editor = sharedPreferences.edit() ;
                            editor.putString(Constant.userlogin_flag, "0");
                            editor.putString(Constant.facebook_login, "0") ;
                            editor.putString(Constant.fb_req_login, "0") ;
                            editor.putString(Constant.token, "") ;
                            editor.putString(Constant.first_name, "") ;
                            editor.putString(Constant.last_name, "") ;
                            editor.putString(Constant.user_id, "") ;
                            editor.putString(Constant.user_name, "") ;
                            editor.putString(Constant.email, "");
                            editor.putString(Constant.user_pro_pic, "") ;
                            editor.putString(Constant.header_name, "");
                            editor.putString(Constant.birthday, "") ;
                            editor.putString(Constant.age_string, "") ;
                            editor.putString(Constant.district, "") ;
                            editor.putString(Constant.district_id, "");
                            editor.putString(Constant.upazilla, "") ;
                            editor.putString(Constant.upazilla_id, "");
                            editor.putString(Constant.profession, "");
                            editor.putString(Constant.phone, "");
                            editor.putString(Constant.main_problem, "");
                            editor.putString(Constant.gender, "") ;
                            editor.putString(Constant.dob, "");
                            editor.commit() ;

                            Intent intent = new Intent(SettingsActivity.this, LoginActivity.class) ;
                            startActivity(intent);

                        } // End of onClick(DialogInterface dialogqqqq, int whichButton)
                    }); //End of alert.setPositiveButton

                    AlertDialog alertDialog = alert.create();
                    alertDialog.show();


                } else {

                    inputOldPass.setText("");
                    inputNewPass.setText("");
                    inputConfirmPass.setText("");

                    if (getCurrentFocus() != null) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }

                    Toast.makeText(SettingsActivity.this, "Invalid credentials! Please try again.", Toast.LENGTH_LONG).show();

                    innerLL.setVisibility(View.GONE);
                    changePasswordLayout.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                Log.d("kothay", "JSONException e");

                inputOldPass.setText("");
                inputNewPass.setText("");
                inputConfirmPass.setText("");

                innerLL.setVisibility(View.GONE);
                changePasswordLayout.setVisibility(View.VISIBLE);

                Toast.makeText(SettingsActivity.this, e.toString(), Toast.LENGTH_LONG).show();

                e.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }


}

