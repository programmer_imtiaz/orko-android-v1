package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.arko.nextdot.Adapters.SearchBySpecialityAdapter;
import com.arko.nextdot.Interface.SearchBySpecialityView;
import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityProfile;
import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.SearchBySpecialityPresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 10/16/2017.
 */

public class SearchBySpecialityActivity extends ToolbarBaseActivity implements SearchBySpecialityView, SearchBySpecialityAdapter.ClickListener {

    @BindView(R.id.searchview_speciality)
    SearchView searchviewSpeciality;
    @BindView(R.id.search_by_speciality_recyler)
    RecyclerView searchBySpecialityRecyler;
    String end_point = "api/search/symptoms", token;
    SearchBySpecialityPresenter presenter ;
    List<SearchBySpecialityProfile> list ;
    SearchBySpecialityAdapter searchBySpecialityAdapter ;
    LinearLayoutManager layoutManager ;
    SharedPreferences sharedPreferences ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_speciality);
        ButterKnife.bind(this);
        presenter = new SearchBySpecialityPresenter(this) ;
        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "") ;
        Log.d("++token++", token) ;
        list = new ArrayList<>() ;
        initilization();



        searchviewSpeciality.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length() >= 1){
                    presenter.getSpecialitySearchResult(end_point, token, newText);
                }

                return false;
            }
        });
    }

    public void initilization(){
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        searchBySpecialityAdapter = new SearchBySpecialityAdapter(getAppContext(), list) ;
        searchBySpecialityRecyler.setLayoutManager(layoutManager);
        searchBySpecialityAdapter.setClickListener(this);
        searchBySpecialityRecyler.setAdapter(searchBySpecialityAdapter);
        searchBySpecialityAdapter.notifyDataSetChanged();
    }

    @Override
    public void showSearchResult(SearchBySpecialityRoot searchBySpecialityRoot) {
        list = searchBySpecialityRoot.getData() ;
        if(list != null){
            searchBySpecialityAdapter.setFilter(list);
            /*for(SearchBySpecialityProfile searchBySpecialityProfile : list){
                //Log.d("++Search Value++", searchBySpecialityProfile.getName()) ;
            }*/
        }
        else{
            searchBySpecialityAdapter.clearList();
        }
    }

    @Override
    public void startLoading() {

    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void ItemClicked(View view, int position) {

        SharedPreferences.Editor editor = sharedPreferences.edit() ;
        if(list.get(position).getName() != null){
            editor.putString(Constant.searchbyspeciality, list.get(position).getName()) ;
        }
        if(list.get(position).getName() == null){
            editor.putString(Constant.searchbyspeciality, list.get(position).getName_bn()) ;
        }
        editor.commit();
        Intent intent = new Intent(getAppContext(), SearchDoctorActivity.class) ;
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
