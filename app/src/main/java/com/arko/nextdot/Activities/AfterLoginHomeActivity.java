package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.arko.nextdot.R;


public class AfterLoginHomeActivity extends BaseActivity {

    CardView ambulance_card, med_report_card, doctor_card, appt_card, reminder_card , nearby_card;
    Toolbar toolbar ;
    TextView toolbar_title ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_after_login_home);
        setupNavDrawer();
        initilization();
        toolbar_title.setText("Home");

        ambulance_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AmbulanceActivity.class) ;
                startActivity(intent);
            }
        });
        med_report_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AfterLoginHomeActivity.this, MedicalHistoryPastActivity.class) ;
                intent.putExtra("caller", "AfterLoginHomeActivity");
                startActivity(intent);
            }
        });
        doctor_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DoctorActivity.class) ;
                startActivity(intent);
            }
        });
        appt_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChooseApptDateActivity.class) ;
                startActivity(intent);
            }
        });
        reminder_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReminderActivity.class) ;
                startActivity(intent);
            }
        });
        nearby_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NearbyDiagnosticsActivity.class) ;
                startActivity(intent);
            }
        });
    }

    private void initilization() {
        ambulance_card = (CardView) findViewById(R.id.card_ambulance);
        med_report_card = (CardView) findViewById(R.id.card_med_report);
        doctor_card = (CardView) findViewById(R.id.card_doctor) ;
        appt_card = (CardView) findViewById(R.id.card_appointment);
        reminder_card = (CardView) findViewById(R.id.card_reminder);
        nearby_card = (CardView) findViewById(R.id.card_nearby);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title) ;
    }
}
