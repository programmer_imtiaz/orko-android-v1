package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.arko.nextdot.Adapters.MyDoctorsAdapter;
import com.arko.nextdot.Model.RetrofitModel.MyDoctors.Data;
import com.arko.nextdot.Model.RetrofitModel.MyDoctors.MyDoctorsRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sakib on 9/17/2017.
 */

public class MyDoctorsActivity extends ToolbarBaseActivity implements MyDoctorsAdapter.ClickListener {

    RecyclerView recyclerView ;
    MyDoctorsAdapter myDoctorsAdapter ;
    LinearLayoutManager mydoctorLayoutManager ;
    RetrofitInterface retrofitInterface ;
    SharedPreferences sharedPreferences ;
    private String token, user_id ;
    ProgressDialog progressDialog ;
    List<Data> list ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_doctor);
        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        initilization();
        toolbarwithoutdrawer("My Doctors");
        list = new ArrayList<Data>() ;
        token = sharedPreferences.getString(Constant.token, "") ;
        user_id = sharedPreferences.getString(Constant.user_id, "") ;
        retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;

        String url = Constant.ROOT_URL+"api/my_doctors/"+user_id ;

        Call<MyDoctorsRoot> call = retrofitInterface.getMyDoctors(url, token) ;
        setProgressDialog();
        call.enqueue(new Callback<MyDoctorsRoot>() {
            @Override
            public void onResponse(Call<MyDoctorsRoot> call, Response<MyDoctorsRoot> response) {
                Log.d("+++CONNECTION++", "ashche") ;
                progressDialog.dismiss();
                Log.d("+++CONNECTION++", String.valueOf(response.body().getResult().getStatus())) ;

                if(response.body().getResult().getStatus().equals("success")){

                    if(response.body().getResult().getData() != null){
                        Log.d("+++check++", "not nul") ;
                        list.clear();
                        list = response.body().getResult().getData() ;
                        myDoctorsAdapter = new MyDoctorsAdapter(getApplicationContext(), list) ;
                        myDoctorsAdapter.setClicklistner(MyDoctorsActivity.this);
                        recyclerView.setAdapter(myDoctorsAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<MyDoctorsRoot> call, Throwable t) {
                Log.d("+++CONNECTION++", "ashe nai") ;
            }
        });
    }

    private void initilization(){
        recyclerView = (RecyclerView) findViewById(R.id.my_doctor_recyler);
        mydoctorLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mydoctorLayoutManager);
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(MyDoctorsActivity.this) ;
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void ItemClicked(View view, int position) {
        Intent intent = new Intent(getApplicationContext(), DoctorProfileActivity.class) ;
        String doctor_id = String.valueOf(list.get(position).getProfile().getUserId());
        Log.d("doctor_id_clicked", doctor_id);
        intent.putExtra("id", doctor_id) ;
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
