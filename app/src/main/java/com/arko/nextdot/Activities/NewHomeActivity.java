package com.arko.nextdot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.arko.nextdot.Fragment.MedicalReportFragment;
import com.arko.nextdot.Fragment.MedicineListFragment;
import com.arko.nextdot.Fragment.MessageFragment;
import com.arko.nextdot.Fragment.TherapyFragment;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class NewHomeActivity extends BaseActivity {

    TabLayout tabLayout ;
    ViewPager viewPager ;
    TextView toolbar_title ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
        setupNavDrawer();
        initilization();

        viewPager = (ViewPager) findViewById(R.id.home_viewpager) ;
        setUpViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.home_tablayout);
        tabLayout.setupWithViewPager(viewPager);
        setupicon();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position == 0){
                    toolbar_title.setText("Medicine");
                }
                else if(position == 1){
                    toolbar_title.setText("Therapy");
                }
                else if(position == 2){
                    toolbar_title.setText("Medical Report");
                }
                else if(position == 3){
                    toolbar_title.setText("Message");
                }
            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
    }

    private void initilization(){
        toolbar_title = (TextView) findViewById(R.id.toolbar_title) ;
    }

    private void setUpViewPager(ViewPager viewPager){

        ViewPagerAdapter_home adapter = new ViewPagerAdapter_home(getSupportFragmentManager()) ;
        adapter.addfragment(new MedicineListFragment(), "Medicine");
        adapter.addfragment(new TherapyFragment(), "Therapy");
        adapter.addfragment(new MedicalReportFragment(), "Medical Report");
        adapter.addfragment(new MessageFragment(), "Message");
        viewPager.setAdapter(adapter);
    }

    private void setupicon(){

        tabLayout.getTabAt(0).setIcon(R.drawable.report_state);
        tabLayout.getTabAt(1).setIcon(R.drawable.event_state);
        tabLayout.getTabAt(2).setIcon(R.drawable.newsfeed_state);
        tabLayout.getTabAt(3).setIcon(R.drawable.message_state);
    }


    class ViewPagerAdapter_home extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>() ;
        List<String> fragmenttitle = new ArrayList<>() ;

        public ViewPagerAdapter_home(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addfragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmenttitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_register, menu);
        return true ;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_power:{
                Intent intent = new Intent(getApplicationContext(), AfterLoginHomeActivity.class);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
