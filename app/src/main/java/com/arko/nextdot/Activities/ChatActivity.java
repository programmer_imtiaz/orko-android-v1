package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.arko.nextdot.Adapters.ChattingAdapter;
import com.arko.nextdot.Model.ChattingModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dipto on 7/3/2017.
 */

public class ChatActivity extends ToolbarBaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbarwithoutdrawer("Chat");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.chat_recyler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        ChattingAdapter chattingAdapter = new ChattingAdapter(getApplicationContext(), getData()) ;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(chattingAdapter);
    }

    private List<ChattingModel> getData(){
        List<ChattingModel> list = new ArrayList() ;

        String[] message = {"hi, are you there ?", "Yes Right Now I am here", "we have to think about our next meeting", "yes i have plan for this",
        "can you tell me somthing about it", "yes, we will try manage some soponsor for next venture", "That's a nice idea"} ;

        for(int i = 0 ; i < message.length ; i++){
            if(i % 2 == 0){
                ChattingModel chattingModel = new ChattingModel() ;
                chattingModel.setMessage(message[i]);
                chattingModel.setFlag(0);
                Log.d("TAG :::::::", String.valueOf(chattingModel.getFlag())) ;
                list.add(chattingModel);
            }
            else{
                ChattingModel chattingModel = new ChattingModel() ;
                chattingModel.setMessage(message[i]);
                chattingModel.setFlag(1);
                Log.d("TAG :::::::", String.valueOf(chattingModel.getFlag())) ;
                list.add(chattingModel);
            }

        }
        for (ChattingModel ch : list){
            Log.d("MYFLAG :::::::", String.valueOf(ch.getFlag())) ;
        }

        return list ;
    }
}
