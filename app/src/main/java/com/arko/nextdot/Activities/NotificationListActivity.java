package com.arko.nextdot.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.arko.nextdot.Adapters.NotificationAdapter;
import com.arko.nextdot.Model.RetrofitModel.Notification.NotificationList;
import com.arko.nextdot.Model.RetrofitModel.Notification.NotificationsItem;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitClient;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationListActivity extends ToolbarBaseActivity {

    List<NotificationsItem> notificationItemList;

    RecyclerView recyclerView;
    NotificationAdapter recyclerAdapter;


    SharedPreferences sharedPreferences ;

    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        toolbarwithoutdrawer("Notifications");

        notificationItemList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerAdapter = new NotificationAdapter(getApplicationContext(),notificationItemList);
//        recyclerView.setAdapter(recyclerAdapter);

        sharedPreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE) ;

        userId = sharedPreferences.getString(Constant.user_id, "");




//      Calling the Notification API to fetch the notification list

        String url = Constant.ROOT_URL+ Constant.notification_list+userId;

        Log.d("URL_NOTIFICATION", "NotificationListAPICall: "+url);

        NotificationListAPICall(url);



    }

    private void NotificationListAPICall(String url) {


        Log.d("NotCallAPI", "NotificationListAPICall: I AM HERE");

        RetrofitInterface apiInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);
        Call<NotificationList> call = apiInterface.getNotification(url);

        call.enqueue(new Callback<NotificationList>() {
            @Override
            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
                notificationItemList = response.body().getNotifications();

                Log.d("NotificationList","Response = "+notificationItemList.get(0));

                recyclerAdapter = new NotificationAdapter(getApplicationContext(),notificationItemList);
                recyclerView.setAdapter(recyclerAdapter);
                recyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<NotificationList> call, Throwable t) {
                Log.d("FailedMsg","Response = "+t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
