package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.arko.nextdot.Adapters.viewPagerCustomLayoutAdapter;
import com.arko.nextdot.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrescriptionsNReports extends AppCompatActivity {

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.mainviewpager)
    ViewPager viewPager;
    @BindView(R.id.maintablayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptions_nreports);
        ButterKnife.bind(this);
        mainToolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(mainToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.app_name));

        tabLayout = (TabLayout) findViewById(R.id.maintablayout);
        viewPager = (ViewPager) findViewById(R.id.mainviewpager);
        viewPager.setAdapter(new viewPagerCustomLayoutAdapter(getSupportFragmentManager(), this));

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
