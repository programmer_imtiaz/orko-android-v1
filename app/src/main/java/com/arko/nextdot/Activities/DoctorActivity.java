package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.arko.nextdot.Fragment.DoctorInfoFragment;
import com.arko.nextdot.Fragment.AppointmentFragment;
import com.arko.nextdot.Fragment.HistoryFragment;
import com.arko.nextdot.Fragment.AddDoctorFrament;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class DoctorActivity extends AppCompatActivity {

    Toolbar toolbar ;
    ViewPager viewPager ;
    TabLayout tabLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_tab);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        final TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        showCustomToolbar();

        viewPager = (ViewPager) findViewById(R.id.viewpager) ;
        setUpViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
        setupicon();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    toolbar_title.setText("Doctor Info");
                }
                else if(position == 1){
                    toolbar_title.setText("History");
                }
                else if(position == 2){
                    toolbar_title.setText("Appointment");
                }
                else if(position == 3){
                    toolbar_title.setText("Add Doctor");
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setupicon(){

        tabLayout.getTabAt(0).setIcon(R.drawable.tab_about_us);
        tabLayout.getTabAt(1).setIcon(R.drawable.tab_categories);
        tabLayout.getTabAt(2).setIcon(R.drawable.tab_ambu);
        tabLayout.getTabAt(3).setIcon(R.drawable.tab_profile);
    }

    private void setUpViewPager(ViewPager viewPager){

        ViewPagerAdapter_bottom adapter = new ViewPagerAdapter_bottom(getSupportFragmentManager()) ;
        adapter.addfragment(new DoctorInfoFragment(), "Doctor Info");
        adapter.addfragment(new HistoryFragment(), "History");
        adapter.addfragment(new AppointmentFragment(), "Appointment");
        adapter.addfragment(new AddDoctorFrament(), "Add Doctor");
        viewPager.setAdapter(adapter);
    }



    class ViewPagerAdapter_bottom extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>() ;
        List<String> fragmenttitle = new ArrayList<>() ;

        public ViewPagerAdapter_bottom(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addfragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmenttitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return fragmenttitle.get(position);
                case 1:
                    return fragmenttitle.get(position);
                case 2:
                    return fragmenttitle.get(position);
                case 3:
                    return fragmenttitle.get(position);
                case 4:
                    return fragmenttitle.get(position);
                case 5:
                    return fragmenttitle.get(position);
                default:
                    return "";
            }
        }
    }

    private void showCustomToolbar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
