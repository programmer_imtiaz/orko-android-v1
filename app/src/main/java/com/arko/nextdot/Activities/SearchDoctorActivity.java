package com.arko.nextdot.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sakib on 9/19/2017.
 */

public class SearchDoctorActivity extends ToolbarBaseActivity {

    MaterialSpinner district_spinner, thana_spinner, fees_spinner, speciality_spinner, rating_spinner ;
    List<String> district_list, thana_list, upazila_id, upazila_name, speciality_list, speciality_list_new ;
    ArrayList<String> attribute_list ;
    public static final int CONNECTION_TIMEOUT = 6000;
    public static final int READ_TIMEOUT = 6500;
    EditText doctor_editext, speciality_edittext ;
    String end_point = "api/doctor/specialities", speciality_hint ;
    RetrofitInterface retrofitInterface ;
    Button submit ;
    ProgressDialog progressDialog ;
    SharedPreferences sharedPreferences ;
    CrystalRangeSeekbar fees_seek_bar, exp_seek_bar ;
    TextView fees_min, fees_max, exp_min, exp_max ;
    String district_position = "", token = "",  upazila_position = "", doctor_name = "", fees_string = "", gender_string = "", rating_string = "", speciality_string = "", exp_string = "" ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_search);
        toolbarwithoutdrawer("Search Doctor");
        sharedPreferences  = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "") ;
        speciality_hint = sharedPreferences.getString(Constant.searchbyspeciality, "") ;
        if(speciality_hint.equals("Search By SpecialityClass, Issue")){
            speciality_string = "" ;
        }
        else{
            speciality_string = speciality_hint ;
        }

        Log.d("+++doctor_id++", speciality_hint+"") ;
        initilization();

        // RETRAVING DOCTOR SPECIALITIES

        /*retrofitInterface = RetrofitClient.getRetrofitClient().create(RetrofitInterface.class) ;
        String url = Constant.ROOT_URL+end_point ;
        Log.d("+++URL+++", url) ;
        Call<SearchBySpeciality> call = retrofitInterface.getDoctorsSpeciality(url, token) ;
        setProgressDialog();
        call.enqueue(new Callback<SearchBySpeciality>() {
            @Override
            public void onResponse(Call<SearchBySpeciality> call, Response<SearchBySpeciality> response) {
                progressDialog.dismiss();
                Log.d("+++CONNECTION++", "ashche") ;
                if(response.body().getResult().equals("Success")){
                    if(response.body().getData() != null){
                        speciality_list_new = new ArrayList<String>() ;
                        speciality_list_new = response.body().getData() ;
                        Log.d("+++speciality+++", speciality_list_new.get(0)) ;
                        addSpecialitytoSpinner();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchBySpeciality> call, Throwable t) {
                Log.d("+++CONNECTION++", "ashe nai") ;
            }
        });*/

        // RETRAVING DOCTOR SPECIALITIES

        district_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                if(position == 0){
                    Toast.makeText(SearchDoctorActivity.this, "ok got it !!", Toast.LENGTH_SHORT).show();
                    thana_list.clear();
                    district_position = "" ;
                    thana_list.add("Search by Upzila");
                    ArrayAdapter<String> thana_arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner_thana_view, thana_list) ;
                    thana_arrayAdapter.setDropDownViewResource(R.layout.item_spinner_thana);
                    thana_spinner.setAdapter(thana_arrayAdapter);
                }
                if(position > 0){
                    thana_list.clear();
                    thana_list.add("Search by Upzila");
                    district_position = Integer.toString(position) ;
                    Log.d("+++District+++", district_position) ;
                    String URL = Constant.ROOT_URL + "api/upazilla-list/" + district_position ;
                    Log.d("+++MAIN URL+++", URL) ;
                    ApiTaskUpzilaList apiTaskUpzilaList = new ApiTaskUpzilaList(SearchDoctorActivity.this, URL) ;
                    apiTaskUpzilaList.execute() ;

                }
            }
        });

        thana_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position == 0){
                    upazila_position = "" ;
                }
                else if(position > 0){

                    Log.d("++++THANA++++",upazila_id.get(position - 1)) ;
                    upazila_position = upazila_id.get(position - 1) ;
                }
            }
        });


        /*rating_spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position == 0){
                    rating_string = "" ;
                }
                if(position == 1){
                    rating_string = "1-2" ;
                }
                if(position == 2){
                    rating_string = "2-3" ;
                }
                if(position == 3){
                    rating_string = "3-4" ;
                }
                if(position == 4){
                    rating_string = "4-5" ;
                }
            }
        });*/

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctor_name = doctor_editext.getText().toString().trim() ;
                speciality_string = sharedPreferences.getString(Constant.searchbyspeciality, "") ;
                if(speciality_string.equals("Search By SpecialityClass, Issue")){
                    speciality_string = "" ;
                }

                if(district_position.equals("") && upazila_position.equals("") &&
                        speciality_string.equals("") && fees_string.equals("") &&
                        exp_string.equals("")){

                    Toast.makeText(SearchDoctorActivity.this, "Please Choose Atleast One Attributes", Toast.LENGTH_SHORT).show();
                }

                else{
                    attribute_list.clear();
                    attribute_list.add(exp_string);
                    attribute_list.add(speciality_string);
                    attribute_list.add(fees_string);
                    attribute_list.add(district_position);
                    attribute_list.add(upazila_position);

                    Intent intent = new Intent(SearchDoctorActivity.this, SearchDoctorResultActivity.class) ;
                    intent.putStringArrayListExtra("key", attribute_list);
                    startActivity(intent);
                    finish();
                    Log.d("++FINISH++", "ok done !!") ;
                }
            }
        });

        doctor_editext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SearchDoctorActivity.this, SearchByNameActivity.class) ;
                startActivity(intent);
            }
        });

        speciality_edittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchDoctorActivity.this, SearchBySpecialityActivity.class) ;
                startActivity(intent);
            }
        });


        expirenceSeekBarOnClick();
        feesSeekBarOnClick();
    }



    private void initilization(){

        district_spinner = (MaterialSpinner) findViewById(R.id.spinner_district);
        thana_spinner = (MaterialSpinner) findViewById(R.id.spinner_upazilla);
        //fees_spinner = (MaterialSpinner) findViewById(R.id.spinner_fees);
        //speciality_spinner = (MaterialSpinner) findViewById(R.id.spinner_speciality);
        //rating_spinner = (MaterialSpinner) findViewById(R.id.spinner_ratings);
        doctor_editext = (EditText) findViewById(R.id.search_doc_by_name) ;
        speciality_edittext = (EditText) findViewById(R.id.search_by_speciality) ;
        submit = (Button) findViewById(R.id.btn_submit_search) ;
        fees_seek_bar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbarfees);
        exp_seek_bar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbarexp);
        fees_max = (TextView) findViewById(R.id.fees_max_value) ;
        fees_min = (TextView) findViewById(R.id.fees_min_value) ;
        exp_max = (TextView) findViewById(R.id.exp_max_value) ;
        exp_min = (TextView) findViewById(R.id.exp_min_value) ;

        district_list = new ArrayList<String>() ;
        thana_list = new ArrayList<String>() ;
        upazila_id = new ArrayList<String>() ;
        upazila_name = new ArrayList<String>() ;
        attribute_list = new ArrayList<String>() ;

        for(int i = 0 ; i < Constant.district_array.length ; i++){

            district_list.add(Constant.district_array[i]);
        }

        speciality_edittext.setHint(speciality_hint);

        district_spinner.setItems(district_list);
        //speciality_spinner.setItems("Search By speciality");
        //fees_spinner.setItems("Search by fees", "100-500 BDT", "500-1000 BDT", "1000-1500 BDT", "1500-2000 BDT");
        //rating_spinner.setItems("Search By Rating", "1 - 2", "2 - 3", "3 - 4", "4 - 5");


        //TODO Do something here
    }

    private void expirenceSeekBarOnClick(){
        exp_seek_bar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                exp_min.setText(String.valueOf(minValue)+" Years");
                exp_max.setText(String.valueOf(maxValue)+" Years");
            }
        });

        exp_seek_bar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + "-" + String.valueOf(maxValue));
                exp_string = String.valueOf(minValue) + "-" + String.valueOf(maxValue) ;
            }
        });
    }


    private void feesSeekBarOnClick(){
        fees_seek_bar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                fees_min.setText(String.valueOf(minValue)+" BDT");
                fees_max.setText(String.valueOf(maxValue)+" BDT");
            }
        });

        fees_seek_bar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + "-" + String.valueOf(maxValue));
                fees_string = String.valueOf(minValue) + "-" + String.valueOf(maxValue) ;
            }
        });
    }

    private void setProgressDialog(){
        progressDialog = new ProgressDialog(SearchDoctorActivity.this) ;
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Loading Please Wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public class ApiTaskUpzilaList extends AsyncTask<String, Void, String> {

        Context context ;
        String main_URL ;
        ProgressDialog progressDialog ;


        public ApiTaskUpzilaList(Context context, String URL){
            this.context = context ;
            this.main_URL = URL ;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //login_report = new AlertDialog.Builder(context);
            progressDialog = new ProgressDialog(context) ;
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Loading Please Wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                URL url = new URL(main_URL) ;
                Log.d("+++URL+++", main_URL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection() ;
                httpURLConnection.setReadTimeout(READ_TIMEOUT);
                httpURLConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);


                OutputStream outputStream = httpURLConnection.getOutputStream() ;
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8")) ;

                /*String login_username = params[0] ;
                String login_password = params[1] ;

                String data = URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(login_username, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(login_password, "UTF-8") ;
*/
                //bufferedWriter.write();
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                int response_code = httpURLConnection.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = httpURLConnection.getInputStream() ;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream)) ;
                    StringBuilder stringBuilder = new StringBuilder() ;
                    String line = "" ;

                    while((line = bufferedReader.readLine()) != null){
                        Log.d("++++TRUE+++", "YES");
                        stringBuilder.append(line + "\n") ;
                        Log.d("++++value+++", line) ;
                    }

                    httpURLConnection.disconnect();
                    return stringBuilder.toString().trim() ;
                }
                else{
                    Log.d("++++NOT WORKING+++", "network problem") ;
                    progressDialog.dismiss();
                    Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
                }

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("++++MalformedURLExp++++", String.valueOf(e)) ;
            }
            catch (IOException e) {
                e.printStackTrace();
                Log.d("++++IOException+++++", String.valueOf(e)) ;
            }
            /*finally {
                progressDialog.dismiss();
                Toast.makeText(context, "Network Problem", Toast.LENGTH_SHORT).show() ;
            }*/
            return null ;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String json) {
            try {
                progressDialog.dismiss();
                JSONObject jsonObject = new JSONObject(json) ;
                JSONArray mainjson_array = jsonObject.getJSONArray("result") ;
                Log.d("++++JSONObject+++++", String.valueOf(mainjson_array)) ;

                upazila_id.clear();
                upazila_name.clear();
                //thana_list.clear();

                for(int i = 0 ; i < mainjson_array.length() ; i++){
                    JSONObject array_jsonobj = mainjson_array.getJSONObject(i) ;
                    String id = array_jsonobj.getString("id");
                    String name = array_jsonobj.getString("name");
                    Log.d("+++id+++", id) ;
                    Log.d("+++name+++", name) ;
                    upazila_id.add(id);
                    upazila_name.add(name);
                }

                for(int i = 0 ; i < upazila_name.size() ; i++){
                    thana_list.add(upazila_name.get(i));
                    Log.d("+++thana+++", thana_list.get(i)) ;
                }
                thana_spinner.setItems(thana_list);
                //JSONObject jsonObject = new JSONObject(json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)) ;

            }
            catch (JSONException e) {
                e.printStackTrace();
                Log.d("++++JSONException+++++", String.valueOf(e)) ;
            }

        }
    }

    /*public void selectGender(View view){

        boolean check_gender = ((RadioButton) view).isChecked() ;
        switch (view.getId()){

            case R.id.search_male_btn:
                if(check_gender){
                    gender_string = "Male" ;
                }
                break ;

            case R.id.search_female_btn:
                if(check_gender){
                    gender_string = "Female" ;
                }
                break ;
        }

    }*/

    private void addSpecialitytoSpinner(){
        speciality_list = new ArrayList<String>() ;
        speciality_list.add("Search by SpecialityClass");
        for(int i = 0 ; i < speciality_list_new.size() ; i++){

            speciality_list.add(speciality_list_new.get(i));

            Log.d("+++speciality loop++", speciality_list.get(i)) ;
        }
        speciality_spinner.setItems(speciality_list);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
