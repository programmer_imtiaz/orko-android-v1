package com.arko.nextdot.Activities;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arko.nextdot.Adapters.AmbulanceAdapter;
import com.arko.nextdot.Model.AmbulanceModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class AmbulanceActivity extends ToolbarBaseActivity {

    private RecyclerView recyclerView_ambu ;
    private AmbulanceAdapter ambulanceAdapter ;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_ambulance);
        toolbarwithoutdrawer("Ambulance");
        recyclerView_ambu = (RecyclerView) findViewById(R.id.recyler_ambulance) ;
         RecyclerView.LayoutManager gridlayoutmanager = new GridLayoutManager(getApplicationContext(), 2);
         recyclerView_ambu.setLayoutManager(gridlayoutmanager);
         //recyclerView_ambu.setItemAnimator(new DefaultItemAnimator());
         recyclerView_ambu.setAdapter(new AmbulanceAdapter(getApplicationContext(), getData()));
    }

    public static List<AmbulanceModel> getData(){

        List<AmbulanceModel> list = new ArrayList<>() ;
        String[] ambu_name = {"Ambulance 1", "Ambulance 2", "Ambulance 3", "Ambulance 4",
                "Ambulance 5", "Ambulance 6", "Ambulance 7", "Ambulance 8"} ;

        for(int i = 0 ; i < ambu_name.length ; i++){
            AmbulanceModel ambulanceModel = new AmbulanceModel() ;
            ambulanceModel.setAmbulance_name(ambu_name[i]);
            list.add(ambulanceModel);
        }
        return list ;
    }
}
