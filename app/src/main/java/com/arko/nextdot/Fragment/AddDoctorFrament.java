package com.arko.nextdot.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.arko.nextdot.Activities.DoctorActivity;
import com.arko.nextdot.Activities.ToolbarBaseActivity;
import com.arko.nextdot.R;



public class AddDoctorFrament extends Fragment {

    public AddDoctorFrament(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    Button add_request ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_doctor, container, false) ;
        //initilization();
        add_request = (Button) view.findViewById(R.id.add_doc_request);
        add_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Request Send to Doctor", Toast.LENGTH_SHORT).show();
            }
        });
        return view ;
    }

//    private void initilization(){
//        add_request = (Button) getView().findViewById(R.id.add_doc_request);
//    }
}
