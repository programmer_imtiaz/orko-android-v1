package com.arko.nextdot.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.PrescriptionsAdapter;
import com.arko.nextdot.Interface.PrescriptionView;
import com.arko.nextdot.Model.RetrofitModel.PrescriptionItem;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.PrescriptionsPresenter;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPrescriptions extends Fragment implements PrescriptionView {


    @BindView(R.id.prescriptionsnotice)
    TextView prescriptionsnotice;
    @BindView(R.id.prescriptionsRecyclerView)
    RecyclerView prescriptionsRecyclerView;
    @BindView(R.id.prescriptionsSwipe)
    SwipeRefreshLayout prescriptionsSwipe;


    PrescriptionsAdapter prescriptionsAdapter;
    List<PrescriptionItem> prescriptionsList;
    Unbinder unbinder;
    String token,patient_id;
    PreferenceManager preferenceManager;
    PrescriptionsPresenter presenter;
    SharedPreferences sharedPreferences;

    public FragmentPrescriptions() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_prescriptions, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),2);
        prescriptionsRecyclerView.setLayoutManager(linearLayoutManager);
        prescriptionsRecyclerView.setHasFixedSize(true);
        presenter=new PrescriptionsPresenter(this);
        sharedPreferences = getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        prescriptionsList=new ArrayList();
        Log.e("lifecycle",prescriptionsList.size()+"");
//        preferenceManager= PreferenceManager.getInstance(getActivity());
        token= sharedPreferences.getString(Constant.token,"");
        patient_id=sharedPreferences.getString(Constant.user_id,"");
        presenter.getPrescriptions(token,patient_id);
        prescriptionsSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //prescriptionsList.clear();
                presenter.getPrescriptions(token,patient_id);
            }
        });


        Log.e("Lifecycle","onView Created called");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void passData(List<PrescriptionItem> prescriptionItems) {
        prescriptionsAdapter=new PrescriptionsAdapter(getActivity(),prescriptionItems);
        prescriptionsRecyclerView.setAdapter(prescriptionsAdapter);
        prescriptionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startLoading() {

        prescriptionsSwipe.setRefreshing(true);
    }

    @Override
    public void stopLoading() {

        prescriptionsSwipe.setRefreshing(false);
    }

    @Override
    public Context getAppContext() {
        return getActivity();
    }
}
