package com.arko.nextdot.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Adapters.PastApptAdapter;
import com.arko.nextdot.Adapters.PresentApptAdapter;
import com.arko.nextdot.Model.PresentApptModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dipto on 5/7/2017.
 */

public class PresentHistoryFragment extends Fragment{

    public PresentHistoryFragment(){

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_present_history, container, false) ;
        RecyclerView present_appt_recyclerView = (RecyclerView) view.findViewById(R.id.fragment_present_recyler);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        PresentApptAdapter myAdapter = new PresentApptAdapter(getActivity(), getData()) ;
        present_appt_recyclerView.setAdapter(myAdapter);
        present_appt_recyclerView.setLayoutManager(layoutManager);
        return view ;
    }

    public static List<PresentApptModel> getData(){

        List<PresentApptModel> list = new ArrayList<>() ;
        String[] present_appt_name = {"Appt 1", "Appt 2", "Appt 3", "Appt 4"} ;

        for(int i = 0 ; i < present_appt_name.length ; i++){
            PresentApptModel presentApptModel = new PresentApptModel() ;
            presentApptModel.setPresent_appt_title(present_appt_name[i]);
            list.add(presentApptModel);
        }
        return list ;
    }
}
