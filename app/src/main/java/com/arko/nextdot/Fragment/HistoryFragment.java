package com.arko.nextdot.Fragment;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class HistoryFragment extends Fragment {

    public HistoryFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    TabLayout tabLayout ;
    ViewPager viewPager ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_history, container, false) ;
        viewPager = (ViewPager) view.findViewById(R.id.fragment_history_view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.fragment_history_tablayout);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);

        return view ;
    }

    private void setupViewPager(ViewPager viewPager){
        FragmentHistoryViewPagerAdapter viewPagerAdapter = new FragmentHistoryViewPagerAdapter(getChildFragmentManager()) ;
        viewPagerAdapter.addFragment(new PastHistoryFragment(), "Past");
        viewPagerAdapter.addFragment(new PresentHistoryFragment(), "Present");
        viewPager.setAdapter(viewPagerAdapter);
    }

    class FragmentHistoryViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>() ;
        private final List<String> fragmentTitle = new ArrayList<>() ;

        public FragmentHistoryViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return fragmentList.get(0) ;
                case 1:
                    return fragmentList.get(1) ;
                default:
                    return fragmentList.get(0) ;
            }
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }


        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment) ;
            fragmentTitle.add(title) ;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position) ;
        }
    }
}
