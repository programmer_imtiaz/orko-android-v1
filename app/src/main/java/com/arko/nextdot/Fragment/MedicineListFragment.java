package com.arko.nextdot.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Activities.MedicineTimeDetailsActivity;
import com.arko.nextdot.Adapters.MedicalListAdapter;
import com.arko.nextdot.Model.MedicalListModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class MedicineListFragment extends Fragment implements MedicalListAdapter.ClickListener{

    public MedicineListFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    LinearLayoutManager layoutManager;
    MedicalListAdapter medicalListAdapter ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_medicine_list, container, false) ;
        RecyclerView medicine_recyclerView = (RecyclerView) view.findViewById(R.id.medicine_list_recyler);
        medicalListAdapter = new MedicalListAdapter(getActivity(), getdata()) ;
        medicalListAdapter.setClickListener(this);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        medicine_recyclerView.setLayoutManager(layoutManager);
        medicine_recyclerView.setAdapter(medicalListAdapter);
        return view ;
    }

    private List<MedicalListModel> getdata(){
        List<MedicalListModel> list = new ArrayList() ;

        String[] medicine_name = {"Ibruprofen", "Robitussin","Cotrim DS", "Multivit E"} ;
        String[] medicine_cause = {"Fever", "Headache", "Fever", "Vision Blur"} ;

        for(int i = 0 ; i < medicine_name.length ; i++){

            MedicalListModel medicalListModel = new MedicalListModel() ;
            medicalListModel.setMedicine_name(medicine_name[i]);
            medicalListModel.setMedicine_cause(medicine_cause[i]);
            list.add(medicalListModel);
        }
        return list ;
    }

    @Override
    public void ItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), MedicineTimeDetailsActivity.class) ;
        startActivity(intent);
    }
}
