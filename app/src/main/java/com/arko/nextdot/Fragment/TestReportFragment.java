package com.arko.nextdot.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;

import com.arko.nextdot.Adapters.TestReportAdapter;
import com.arko.nextdot.Model.TestReportModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;


public class TestReportFragment extends Fragment {

    public TestReportFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    RecyclerView test_recyler;
    TestReportAdapter testReportAdapter;
    Button test_report_view_btn ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_testreport, container, false);

        test_recyler = (RecyclerView) view.findViewById(R.id.test_report_recyler) ;
        RecyclerView.LayoutManager gridlayoutmanager = new GridLayoutManager(getActivity(), 2);
        test_recyler.setLayoutManager(gridlayoutmanager);
        TestReportAdapter testReportAdapter = new TestReportAdapter(getActivity(), getData()) ;
        test_recyler.setAdapter(testReportAdapter);
        return view ;
    }


    public static List<TestReportModel> getData(){

        List<TestReportModel> list = new ArrayList<>() ;
        String[] test_name = {"Blood Test", "Urine Test", "Blood Test", "Urine Test",
                "Blood Test", "Blood Test", "Urine Test", "Blood Test"} ;

        for(int i = 0 ; i < test_name.length ; i++){
            TestReportModel testReportModel = new TestReportModel() ;
            testReportModel.setTestname(test_name[i]);
            list.add(testReportModel);
        }
        return list ;
    }

}