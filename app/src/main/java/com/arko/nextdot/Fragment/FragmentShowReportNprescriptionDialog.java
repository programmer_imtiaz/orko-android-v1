package com.arko.nextdot.Fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentShowReportNprescriptionDialog extends DialogFragment {


    @BindView(R.id.showPrescriptionNReports)
    ImageView showPrescriptionNReports;
    Unbinder unbinder;

    public FragmentShowReportNprescriptionDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_report_and_prescription_dialog, container);

        unbinder = ButterKnife.bind(this, view);

        String imageurl=getArguments().getString("imageurl");
        Glide.with(getActivity()).load(imageurl).fitCenter().dontAnimate().into(showPrescriptionNReports);

        return view;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        super.onResume();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.showPrescriptionNReports)
    public void onViewClicked() {

    }
}


