package com.arko.nextdot.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.MedicalReportsAdapter;
import com.arko.nextdot.Interface.ReportsView;
import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.ReportsPresenter;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMedicalReports extends Fragment implements ReportsView {


    @BindView(R.id.reportsNotice)
    TextView reportsNotice;
    @BindView(R.id.reportsRecyclerView)
    RecyclerView reportsRecyclerView;
    @BindView(R.id.reportsSwipe)
    SwipeRefreshLayout reportsSwipe;
    Unbinder unbinder;
    List<MedicalReportsItem> medicalReportsItems;
    MedicalReportsAdapter medicalReportsAdapter;
    PreferenceManager preferenceManager;
    String token,patient_id;
    ReportsPresenter presenter;
    SharedPreferences sharedPreferences;

    public FragmentMedicalReports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_medical_reports, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),2);
        reportsRecyclerView.setLayoutManager(linearLayoutManager);
        reportsRecyclerView.setHasFixedSize(true);
        presenter=new ReportsPresenter(this);
        sharedPreferences = getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        medicalReportsItems=new ArrayList();
        Log.e("lifecycle",medicalReportsItems.size()+"");
//        preferenceManager= PreferenceManager.getInstance(getActivity());

        token= sharedPreferences.getString(Constant.token,"");
        patient_id=sharedPreferences.getString(Constant.user_id,"");
        presenter.getReports(token,patient_id);
        reportsSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //prescriptionsList.clear(); presenter.getReports(token,patient_id);
            }
        });


        Log.e("Lifecycle","onView Created called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

/*
    public void getReports(String token,String patient_id){
        final ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        ApiInterface apiInterface= RetrofitClient.getApiInterface();
        Call<MedicalRepots> medicalRepotsCall=apiInterface.getMedicalReportsjson(token,patient_id);
        medicalRepotsCall.enqueue(new Callback<MedicalRepots>() {
            @Override
            public void onResponse(Call<MedicalRepots> call, Response<MedicalRepots> response) {
                progressDialog.dismiss();
                reportsSwipe.setRefreshing(false);
                if(response.isSuccessful()){
                    medicalReportsItems=response.body().getData();

                    Log.e("reprecy",medicalReportsItems.size()+" size");

                }
            }

            @Override
            public void onFailure(Call<MedicalRepots> call, Throwable t) {


                reportsSwipe.setRefreshing(false);
                progressDialog.dismiss();
            }
        });
    }
*/

    @Override
    public void passData(List<MedicalReportsItem> medicalReportsItems) {
        medicalReportsAdapter=new MedicalReportsAdapter(getActivity(),medicalReportsItems);
        reportsRecyclerView.setAdapter(medicalReportsAdapter);
        medicalReportsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String msg) {

        Toast.makeText(getAppContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startLoading() {

        reportsSwipe.setRefreshing(true);
    }

    @Override
    public void stopLoading() {

        reportsSwipe.setRefreshing(false);
    }

    @Override
    public Context getAppContext() {
        return getActivity();
    }
}
