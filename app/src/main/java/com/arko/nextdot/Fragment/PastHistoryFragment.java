package com.arko.nextdot.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Adapters.PastApptAdapter;
import com.arko.nextdot.Model.PastApptModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class PastHistoryFragment extends Fragment {

    public PastHistoryFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_past_history, container, false) ;
        RecyclerView past_appt_recyclerView = (RecyclerView) view.findViewById(R.id.fragment_past_recyler);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        PastApptAdapter myAdapter = new PastApptAdapter(getActivity(), getData()) ;
        past_appt_recyclerView.setAdapter(myAdapter);
        past_appt_recyclerView.setLayoutManager(layoutManager);
        return view ;
    }


    public static List<PastApptModel> getData(){

        List<PastApptModel> list = new ArrayList<>() ;
        String[] past_appt_name = {"Appt 1", "Appt 2", "Appt 3", "Appt 4"} ;

        for(int i = 0 ; i < past_appt_name.length ; i++){
            PastApptModel pastApptModel = new PastApptModel() ;
            pastApptModel.setPast_appt_title(past_appt_name[i]);
            list.add(pastApptModel);
        }
        return list ;
    }
}
