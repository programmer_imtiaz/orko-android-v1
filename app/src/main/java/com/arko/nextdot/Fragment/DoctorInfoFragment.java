package com.arko.nextdot.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View ;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.arko.nextdot.Activities.AfterLoginHomeActivity;
import com.arko.nextdot.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class DoctorInfoFragment extends Fragment implements OnMapReadyCallback {

    public DoctorInfoFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private GoogleMap mMap;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_info, container, false) ;
        Button take_appt_btn = (Button) view.findViewById(R.id.take_appointment);
        take_appt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Appointment Taken succesfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), AfterLoginHomeActivity.class);
                startActivity(intent);
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.doc_map);
        mapFragment.getMapAsync(this);

        return view ;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera

        LatLng apollo = new LatLng(23.810103, 90.431047);
        mMap.addMarker(new MarkerOptions().position(apollo).title("Apollo Hospital"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(apollo, 15f));


    }
}
