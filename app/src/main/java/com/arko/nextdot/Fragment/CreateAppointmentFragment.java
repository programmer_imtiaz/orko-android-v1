package com.arko.nextdot.Fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.PatientListAdapter;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Network.RetrofitInterface;
import com.arko.nextdot.R;
import com.arko.nextdot.Retrofit.ApiInterface;
import com.arko.nextdot.Retrofit.RetrofitClient;
import com.arko.nextdot.Retrofit.RetrofitModel.Appointments.DaywiseSchedule;
import com.arko.nextdot.Retrofit.RetrofitModel.Appointments.SchedulePair;
import com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list.DoctorListItem;
import com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list.DoctorList;
import com.arko.nextdot.Utils.PreferenceManager;
import com.arko.nextdot.Utils.RecyclerItemClickListener;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAppointmentFragment extends DialogFragment implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    private String selectedDayForSchedule;
    Spinner spinner;
    List<String> scheduleList;
    List<SchedulePair> scheduleListToSend;
    DaywiseSchedule daywiseSchedule;
    String[] appointmentTimeArray=new String[3];
    ArrayAdapter<String> appointmentTimeAdapter;
    List<DoctorListItem> doctorlists = null;
    RecyclerView patientItemRecyclerview;
    TextView appoinment_nothing_to_show;
    PatientListAdapter patientListAdapter = null;
    LinearLayout ll_timesetLayout;
    Button submitButton;
    ImageButton closeButton;
    String selectedTime=null,patientsId=null,doctorId=null;
    String selectedDate, selectedDay;
    int selectedItemPosition=0;

    boolean morningFalse, afternoonFalse, eveningFalse;
    String token = null;
    String userID = null;
    String startTime=null, endTime=null;
    String appointmentStartTimeForAPI=null;
    String appointmentEndTimeForAPI=null;
    PreferenceManager preferenceManager;


    private String morningStartHoursInString = "", morningStartMinutesInString = "", morningEndHoursInString = "", morningEndMinutesInString = "", afternoonStartHoursInString = "", afternoonStartMinutesInString = "", afternoonEndHoursInString = "", afternoonEndMinutesInString = "", eveningStartHoursInString = "", eveningStartMinutesInString = "", eveningEndHoursInString = "", eveningEndMinutesInString = "", morningStartTimeForAPI = "", morningEndTimeForAPI = "", afternoonStartTimeForAPI = "", afternoonEndTimeForAPI = "", eveningStartTimeForAPI = "", eveningEndTimeForAPI = "";

    private int counter = 0;

    private String sundayStartTime = "", mondayStartTime = "", tuesdayStartTime = "", wednesdayStartTime = "", thursdayStartTime = "", fridayStartTime = "", saturdayStartTime = "";
    private String sundayEndTime, mondayEndTime, tuesdayEndTime, wednesdayEndTime, thursdayEndTime, fridayEndTime, saturdayEndTime;
    private String[] sundayStart, mondayStart, tuesdayStart, wednesdayStart, thursdayStart, fridayStart, saturdayStart;
    private String[] sundayEnd, mondayEnd, tuesdayEnd, wednesdayEnd, thursdayEnd, fridayEnd, saturdayEnd;
    private String sundayDuration, mondayDuration, tuesdayDuration, wednesdayDuration, thursdayDuration, fridayDuration, saturdayDuration, selectedDayName;

    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());

    private String timeDuration = "15";

    private ProgressDialog progressDialog ;
    private LinearLayout loaderLayout;

    private int c = 0;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitButton:

                if(doctorId==null){
                    Toast.makeText(getActivity(), "Please select a doctor", Toast.LENGTH_SHORT).show();
                    return;
                }else {

                    Log.d("finalTesForS",selectedDayForSchedule+" "+appointmentStartTimeForAPI+" " +appointmentEndTimeForAPI);
                    new createAppointmentAPI(userID, token, doctorId, selectedDayForSchedule, appointmentStartTimeForAPI, appointmentEndTimeForAPI).execute();
                }

//                Intent returnIntent = new Intent();
//                returnIntent.putExtra("result",result);
//                getActivity().setResult(Activity.RESULT_OK,returnIntent);
//                getActivity().finish();


//                dismiss();
                break;
            case R.id.close:
                dismiss();
                break;

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

       /* if(spinner.getId() == R.id.appointmentTimeSpinner)
        {
            String txt=appointmentTimeArray.get(position).toString();
            Log.d("ttxt", txt+" "+"data nai "+appointmentTimeArray.size());
        }*/

        selectedTime = spinner.getSelectedItem().toString();

        selectedItemPosition = spinner.getSelectedItemPosition();

        if(selectedItemPosition == 0){

            appointmentStartTimeForAPI = scheduleListToSend.get(0).getStart();
            appointmentEndTimeForAPI = scheduleListToSend.get(0).getEnd();

        } else if(selectedItemPosition == 1){
            appointmentStartTimeForAPI = scheduleListToSend.get(1).getStart();
            appointmentEndTimeForAPI = scheduleListToSend.get(1).getEnd();

        } else if(selectedItemPosition == 2){

            appointmentStartTimeForAPI = scheduleListToSend.get(2).getStart();
            appointmentEndTimeForAPI = scheduleListToSend.get(2).getEnd();

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface UserNameListener {
        void onFinishUserDialog(String user);
    }

    // Empty constructor required for DialogFragment
    public CreateAppointmentFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_add_appointment, container, false);
        preferenceManager = PreferenceManager.getInstance(getActivity());
        appoinment_nothing_to_show = (TextView) view.findViewById(R.id.appoinment_nothing_to_show);
        ll_timesetLayout = (LinearLayout) view.findViewById(R.id.ll_timesetLayout);
        loaderLayout = (LinearLayout) view.findViewById(R.id.circularLoader);
        spinner= (Spinner) view.findViewById(R.id.appointmentTimeSpinner);
        spinner.setOnItemSelectedListener(this);
        submitButton= (Button) view.findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);
        closeButton= (ImageButton) view.findViewById(R.id.close);
        closeButton.setOnClickListener(this);
        doctorlists = new ArrayList<>();
        scheduleList=new ArrayList<>();
        scheduleListToSend=new ArrayList<>();



        userID = preferenceManager.getUserID();
        token = preferenceManager.getAccessToken();
        selectedDate = preferenceManager.getSelectedDate();
        selectedDay = preferenceManager.getSelectedDay();

        getPatients();

        patientItemRecyclerview = (RecyclerView) view.findViewById(R.id.patientListRecyclereView);
        patientItemRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));

        patientItemRecyclerview.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        changeAllItemPosition();
                        doctorlists.get(position).setSet(true);
                        doctorId = doctorlists.get(position).getProfile().getUserId()+"";
                        Log.d("doctorIDFromList", "Doctor ID: "+doctorId);
                        patientListAdapter.notifyDataSetChanged();
                        schedule_api(doctorId,token);
                        //new scheduleAPI(doctorId, token).execute();
                    }
                })
        );


        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        selectedDayForSchedule=getArguments().getString("date");
        Log.d("receivedDate",selectedDayForSchedule+" :) oncreate called ");
    }


    void getPatients() {

        ApiInterface apiInterface = RetrofitClient.getApiInterface();
        Call<DoctorList> call = apiInterface.patientsList(preferenceManager.getAccessToken());
        call.enqueue(new Callback<DoctorList>() {
            @Override
            public void onResponse(Call<DoctorList> call, Response<DoctorList> response) {

                if (response.isSuccessful()) {
                    Log.e("responsecheck", "true "+response.body().getResult());

                    doctorlists= response.body().getResult().getData();
                    changeAllItemPosition();
                    patientListAdapter = new PatientListAdapter(response.body(), getActivity());
                    patientItemRecyclerview.setAdapter(patientListAdapter);
                    patientListAdapter.notifyDataSetChanged();


                    if (doctorlists != null) {
                        if (doctorlists.size() == 0) {
                            appoinment_nothing_to_show.setText("No Doctor Available");
                            appoinment_nothing_to_show.setVisibility(View.VISIBLE);
                            patientItemRecyclerview.setVisibility(View.GONE);
                        } else if (doctorlists.size() > 0) {
                            patientItemRecyclerview.setVisibility(View.VISIBLE);
                            appoinment_nothing_to_show.setVisibility(View.GONE);
                        }
                    }
                    //Log.e("Patient", Doctorlists.get(0).getUsername() + " name");

                } else{
                    Toast.makeText(getActivity(), "Request timed out!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DoctorList> call, Throwable t) {

            }
        });
    }

    public void changeAllItemPosition(){
        if(doctorlists !=null){

            for(int i = 0; i< doctorlists.size(); i++){
                doctorlists.get(i).setSet(false);
            }
        }
    }


    public void schedule_api(String doctorId,String token){


        loaderLayout.setVisibility(View.VISIBLE);
        ll_timesetLayout.setVisibility(View.GONE);

        RetrofitInterface retrofitInterface=RetrofitClient.getRetrofitClient().create(RetrofitInterface.class);
        Call<DaywiseSchedule> call=retrofitInterface.getScheduleByDate(token,doctorId,selectedDate);

        call.enqueue(new Callback<DaywiseSchedule>() {
            @Override
            public void onResponse(Call<DaywiseSchedule> call, Response<DaywiseSchedule> response) {
                if(response.isSuccessful()){
                    loaderLayout.setVisibility(View.GONE);
                    ll_timesetLayout.setVisibility(View.VISIBLE);
                    Log.d("receivedSchedule",response.body().toString());
                    daywiseSchedule=response.body();

                    scheduleList.clear();
                    scheduleListToSend.clear();

                    if(daywiseSchedule.getTimeschedule().getMorning().getStatus().equals("1")){
                        scheduleListToSend.add(new SchedulePair(daywiseSchedule.getTimeschedule().getMorning().getStart(),daywiseSchedule.getTimeschedule().getMorning().getEnd()));
                        scheduleList.add(get12TimeFormat(daywiseSchedule.getTimeschedule().getMorning().getStart())+" - "+get12TimeFormat(daywiseSchedule.getTimeschedule().getMorning().getEnd()));
                    }/*else{
                        morningFalse = true;
                    }*/

                    if(daywiseSchedule.getTimeschedule().getAfternoon().getStatus().equals("1")){
                    /*    if(morningFalse)
                        appointmentTimeArray[0]="Afternoon :"+daywiseSchedule.getTimeschedule().getAfternoon().getStart()+" - "+daywiseSchedule.getTimeschedule().getAfternoon().getEnd();
                        else()*/
                        scheduleListToSend.add(new SchedulePair(daywiseSchedule.getTimeschedule().getAfternoon().getStart(),daywiseSchedule.getTimeschedule().getAfternoon().getEnd()));

                        scheduleList.add(get12TimeFormat(daywiseSchedule.getTimeschedule().getAfternoon().getStart())+" - "+get12TimeFormat(daywiseSchedule.getTimeschedule().getAfternoon().getEnd()));
                    }

                    if(daywiseSchedule.getTimeschedule().getEvening().getStatus().equals("1")){
                        scheduleListToSend.add(new SchedulePair(daywiseSchedule.getTimeschedule().getEvening().getStart(),daywiseSchedule.getTimeschedule().getEvening().getEnd()));

                        scheduleList.add(get12TimeFormat(daywiseSchedule.getTimeschedule().getEvening().getStart())+" - "+get12TimeFormat(daywiseSchedule.getTimeschedule().getEvening().getEnd()));

                    }
                    appointmentTimeAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_districtitem_spinner_view, scheduleList);
                    appointmentTimeAdapter.setDropDownViewResource(R.layout.item_districtitem_spinner_layout);
                    appointmentTimeAdapter.notifyDataSetChanged();
                    spinner.setAdapter(appointmentTimeAdapter);


                }
            }

            @Override
            public void onFailure(Call<DaywiseSchedule> call, Throwable t) {

                loaderLayout.setVisibility(View.GONE);
                Toast.makeText(getContext(), "No schedule available", Toast.LENGTH_SHORT).show();
            }
        });

    }
    public String get24TimeFormat(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
        String formatedTime = null;
        try {
            Date s_time = sdf.parse(time);
            formatedTime = new SimpleDateFormat("HH:mm").format(s_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatedTime;
    }

    public String get12TimeFormat(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.US);
        String formatedTime = null;
        try {
            Date s_time = sdf.parse(time);
            formatedTime = new SimpleDateFormat("hh:mm a",Locale.US).format(s_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatedTime;
    }




    //---------------------------------NETWORK CALL-----------------------------------

/*
    private class scheduleAPI extends AsyncTask<String, String, String> {
        String userId, userToken;


        private scheduleAPI(String userId, String userToken) {
            this.userId = userId;
            this.userToken = userToken;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            ll_timesetLayout.setVisibility(View.GONE);
            loaderLayout.setVisibility(View.VISIBLE);

            Log.d("error", "statusReport: onPreExecute e dhukse!!!");

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
/*/
/*****************************************************************


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.baseUrl + "api/doctor-schedule/" + userId);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("token", userToken));


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            /*/
/**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ArrayList listDecor = new ArrayList();

            try {
                Log.d("jsonDataSchedule", "+++++++++" + result);
                JSONObject jsonObj = new JSONObject(result);


                if (jsonObj.getString("status").equals("success")) {
                    JSONArray scheduleArray = jsonObj.getJSONArray("timeschedule");

                    Log.d("array", "length of scheduleArray: " + scheduleArray.length());


                    if (scheduleArray.length() != 0) {

                        for (int i = 0; i < scheduleArray.length(); i++) {

                            JSONObject scheduleJson = scheduleArray.getJSONObject(i);
                            Log.d("jsonData", "jsonData#" + i + "+++++++++" + scheduleJson);


                            AppointmentListModelClass schedule = new AppointmentListModelClass();
                            schedule.setDay(scheduleJson.getString("day"));
                            schedule.setStatus(scheduleJson.getString("status"));
                            schedule.setTimeDuration("15");

                            JSONObject morningJSON = new JSONObject(scheduleJson.getString("morning"));
                            schedule.setMorningStatus(morningJSON.getString("status"));
                            if (schedule.getMorningStatus().equals("1")) {
                                schedule.setMorningStartTime(morningJSON.getString("start"));
                                schedule.setMorningEndTime(morningJSON.getString("end"));


                                morningStartHoursInString = schedule.getMorningStartTime().substring(0, schedule.getMorningStartTime().indexOf(':'));
                                morningStartMinutesInString = schedule.getMorningStartTime().substring(schedule.getMorningStartTime().indexOf(':') + 1, schedule.getMorningStartTime().length());

                                if (Integer.parseInt(morningStartHoursInString) > 12) {
                                    morningStartHoursInString = String.valueOf(Integer.parseInt(morningStartHoursInString) - 12) + ":";
                                    morningStartMinutesInString = morningStartMinutesInString + " PM";
                                } else {
                                    morningStartHoursInString = morningStartHoursInString + ":";
                                    morningStartMinutesInString = morningStartMinutesInString + " AM";
                                }


                                morningEndHoursInString = schedule.getMorningEndTime().substring(0, schedule.getMorningEndTime().indexOf(':'));
                                morningEndMinutesInString = schedule.getMorningEndTime().substring(schedule.getMorningEndTime().indexOf(':') + 1, schedule.getMorningEndTime().length());


                                if (Integer.parseInt(morningEndHoursInString) > 12) {
                                    morningEndHoursInString = String.valueOf(Integer.parseInt(morningEndHoursInString) - 12) + ":";
                                    morningEndMinutesInString = morningEndMinutesInString + " PM";
                                } else {
                                    morningEndHoursInString = morningEndHoursInString + ":";
                                    morningEndMinutesInString = morningEndMinutesInString + " AM";
                                }

                                morningStartTimeForAPI = schedule.getMorningStartTime() + ":00";
                                morningEndTimeForAPI = schedule.getMorningEndTime() + ":00";

                                appointmentTimeArray[0] = "Morning (" + morningStartHoursInString + morningStartMinutesInString + " - " + morningEndHoursInString + morningEndMinutesInString + ")";
                                Log.d("spinneritems", "item 1: " + appointmentTimeArray[0]);

                            } else{
                                appointmentTimeArray[0]="N/A";
                                Log.d("spinneritems", "item 1: " + appointmentTimeArray[0]);
                            }

                            JSONObject afternoonJSON = new JSONObject(scheduleJson.getString("afternoon"));
                            schedule.setAfternoonStatus(afternoonJSON.getString("status"));
                            if (schedule.getAfternoonStatus().equals("1")) {
                                schedule.setAfternoonStartTime(afternoonJSON.getString("start"));
                                schedule.setAfternoonEndTime(afternoonJSON.getString("end"));

                                afternoonStartHoursInString = schedule.getAfternoonStartTime().substring(0, schedule.getAfternoonStartTime().indexOf(':'));
                                afternoonStartMinutesInString = schedule.getAfternoonStartTime().substring(schedule.getAfternoonStartTime().indexOf(':') + 1, schedule.getAfternoonStartTime().length());

                                if (Integer.parseInt(afternoonStartHoursInString) > 12) {
                                    afternoonStartHoursInString = String.valueOf(Integer.parseInt(afternoonStartHoursInString) - 12) + ":";
                                    afternoonStartMinutesInString = afternoonStartMinutesInString + " PM";
                                } else {
                                    afternoonStartHoursInString = afternoonStartHoursInString + ":";
                                    afternoonStartMinutesInString = afternoonStartMinutesInString + " AM";
                                }


                                afternoonEndHoursInString = schedule.getAfternoonEndTime().substring(0, schedule.getAfternoonEndTime().indexOf(':'));
                                afternoonEndMinutesInString = schedule.getAfternoonEndTime().substring(schedule.getAfternoonEndTime().indexOf(':') + 1, schedule.getAfternoonEndTime().length());


                                if (Integer.parseInt(afternoonEndHoursInString) > 12) {
                                    afternoonEndHoursInString = String.valueOf(Integer.parseInt(afternoonEndHoursInString) - 12) + ":";
                                    afternoonEndMinutesInString = afternoonEndMinutesInString + " PM";
                                } else {
                                    afternoonEndHoursInString = afternoonEndHoursInString + ":";
                                    afternoonEndMinutesInString = afternoonEndMinutesInString + " AM";
                                }


                                afternoonStartTimeForAPI = schedule.getAfternoonStartTime() + ":00";
                                afternoonEndTimeForAPI = schedule.getAfternoonEndTime() + ":00";

                                appointmentTimeArray[1]="Afternoon ("+afternoonStartHoursInString + afternoonStartMinutesInString+" - "+ afternoonEndHoursInString + afternoonEndMinutesInString+")";
                                Log.d("spinneritems", "item 2: " + appointmentTimeArray[1]);
                            } else{
                                appointmentTimeArray[1]="N/A";
                                Log.d("spinneritems", "item 2: " + appointmentTimeArray[1]);
                            }


                            JSONObject eveningJSON = new JSONObject(scheduleJson.getString("evening"));
                            schedule.setEveningStatus(eveningJSON.getString("status"));
                            if (schedule.getEveningStatus().equals("1")) {
                                schedule.setEveningStartTime(eveningJSON.getString("start"));
                                schedule.setEveningEndTime(eveningJSON.getString("end"));

                                eveningStartHoursInString = schedule.getEveningStartTime().substring(0, schedule.getEveningStartTime().indexOf(':'));
                                eveningStartMinutesInString = schedule.getEveningStartTime().substring(schedule.getEveningStartTime().indexOf(':') + 1, schedule.getEveningStartTime().length());

                                if (Integer.parseInt(eveningStartHoursInString) > 12) {
                                    eveningStartHoursInString = String.valueOf(Integer.parseInt(eveningStartHoursInString) - 12) + ":";
                                    eveningStartMinutesInString = eveningStartMinutesInString + " PM";
                                } else {
                                    eveningStartHoursInString = eveningStartHoursInString + ":";
                                    eveningStartMinutesInString = eveningStartMinutesInString + " AM";
                                }


                                eveningEndHoursInString = schedule.getEveningEndTime().substring(0, schedule.getEveningEndTime().indexOf(':'));
                                eveningEndMinutesInString = schedule.getEveningEndTime().substring(schedule.getEveningEndTime().indexOf(':') + 1, schedule.getEveningEndTime().length());


                                if (Integer.parseInt(eveningEndHoursInString) > 12) {
                                    eveningEndHoursInString = String.valueOf(Integer.parseInt(eveningEndHoursInString) - 12) + ":";
                                    eveningEndMinutesInString = eveningEndMinutesInString + " PM";
                                } else {
                                    eveningEndHoursInString = eveningEndHoursInString + ":";
                                    eveningEndMinutesInString = eveningEndMinutesInString + " AM";
                                }


                                eveningStartTimeForAPI = schedule.getEveningStartTime() + ":00";
                                eveningEndTimeForAPI = schedule.getEveningEndTime() + ":00";

                                appointmentTimeArray[2]="Evening ("+eveningStartHoursInString + eveningStartMinutesInString+" - "+ eveningEndHoursInString + eveningEndMinutesInString+")";
                                Log.d("spinneritems", "item 3: " + appointmentTimeArray[2]);
                            } else{
                                appointmentTimeArray[2]="N/A";
                                Log.d("spinneritems", "item 3: " + appointmentTimeArray[2]);
                            }


                            if (schedule.getStatus().equals("0")) {

                                currentCalender.setTime(new Date());

                                currentCalender.set(Calendar.WEEK_OF_YEAR, 1);

                                int remainingWeeks = 52 - currentCalender.get(Calendar.WEEK_OF_YEAR);
                                Log.d("dayOfWeek", "Remaining weeks in 2017: " + remainingWeeks);

                                for (int j = 0; j <= remainingWeeks; j++) {
                                    if (schedule.getDay().equals("Sun")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                                    } else if (schedule.getDay().equals("Mon")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                                    } else if (schedule.getDay().equals("Tue")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
                                    } else if (schedule.getDay().equals("Wed")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
                                    } else if (schedule.getDay().equals("Thu")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
                                    } else if (schedule.getDay().equals("Fri")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
                                    } else if (schedule.getDay().equals("Sat")) {
                                        currentCalender.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                                    }


                                    currentCalender.add(Calendar.WEEK_OF_YEAR, 1);

                                }

                            } else if (schedule.getDay().equals("Sun")) {

                                counter = 0;

                                sundayStart[0] = schedule.getMorningStartTime();
                                sundayStart[1] = schedule.getAfternoonStartTime();
                                sundayStart[2] = schedule.getEveningStartTime();

                                sundayEnd[0] = schedule.getMorningEndTime();
                                sundayEnd[1] = schedule.getAfternoonEndTime();
                                sundayEnd[2] = schedule.getEveningEndTime();


                                while (sundayStartTime.equals("") && counter < sundayStart.length) {

                                    if (sundayStart[counter] != null) {
                                        sundayStartTime = sundayStart[counter];
                                        sundayEndTime = sundayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "sundayStartTime: " + sundayStartTime);
                                Log.d("+++EndTime+++", "sundayEndTime: " + sundayEndTime);


                                sundayDuration = "15";
                                timeDuration = sundayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Mon")) {
                                counter = 0;

                                mondayStart[0] = schedule.getMorningStartTime();
                                mondayStart[1] = schedule.getAfternoonStartTime();
                                mondayStart[2] = schedule.getEveningStartTime();

                                mondayEnd[0] = schedule.getMorningEndTime();
                                mondayEnd[1] = schedule.getAfternoonEndTime();
                                mondayEnd[2] = schedule.getEveningEndTime();


                                while (mondayStartTime.equals("") && counter < mondayStart.length) {

                                    if (mondayStart[counter] != null) {
                                        mondayStartTime = mondayStart[counter];
                                        mondayEndTime = mondayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "mondayStartTime: " + mondayStartTime);
                                Log.d("+++EndTime+++", "mondayEndTime: " + mondayEndTime);

                                mondayDuration = "15";
                                timeDuration = mondayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Tue")) {

                                counter = 0;

                                tuesdayStart[0] = schedule.getMorningStartTime();
                                tuesdayStart[1] = schedule.getAfternoonStartTime();
                                tuesdayStart[2] = schedule.getEveningStartTime();

                                tuesdayEnd[0] = schedule.getMorningEndTime();
                                tuesdayEnd[1] = schedule.getAfternoonEndTime();
                                tuesdayEnd[2] = schedule.getEveningEndTime();


                                while (tuesdayStartTime.equals("") && counter < tuesdayStart.length) {

                                    if (tuesdayStart[counter] != null) {
                                        tuesdayStartTime = tuesdayStart[counter];
                                        tuesdayEndTime = tuesdayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "tuesdayStartTime: " + tuesdayStartTime);
                                Log.d("+++EndTime+++", "tuesdayEndTime: " + tuesdayEndTime);

                                tuesdayDuration = "15";
                                timeDuration = tuesdayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Wed")) {
                                counter = 0;

                                wednesdayStart[0] = schedule.getMorningStartTime();
                                wednesdayStart[1] = schedule.getAfternoonStartTime();
                                wednesdayStart[2] = schedule.getEveningStartTime();

                                wednesdayEnd[0] = schedule.getMorningEndTime();
                                wednesdayEnd[1] = schedule.getAfternoonEndTime();
                                wednesdayEnd[2] = schedule.getEveningEndTime();


                                while (wednesdayStartTime.equals("") && counter < wednesdayStart.length) {

                                    if (wednesdayStart[counter] != null) {
                                        wednesdayStartTime = wednesdayStart[counter];
                                        wednesdayEndTime = wednesdayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "wednesdayStartTime: " + wednesdayStartTime);
                                Log.d("+++EndTime+++", "wednesdayEndTime: " + wednesdayEndTime);

                                wednesdayDuration = "15";
                                timeDuration = wednesdayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Thu")) {
                                counter = 0;

                                thursdayStart[0] = schedule.getMorningStartTime();
                                thursdayStart[1] = schedule.getAfternoonStartTime();
                                thursdayStart[2] = schedule.getEveningStartTime();

                                thursdayEnd[0] = schedule.getMorningEndTime();
                                thursdayEnd[1] = schedule.getAfternoonEndTime();
                                thursdayEnd[2] = schedule.getEveningEndTime();


                                while (thursdayStartTime.equals("") && counter < thursdayStart.length) {

                                    if (thursdayStart[counter] != null) {
                                        thursdayStartTime = thursdayStart[counter];
                                        thursdayEndTime = thursdayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "thursdayStartTime: " + thursdayStartTime);
                                Log.d("+++EndTime+++", "thursdayEndTime: " + thursdayEndTime);

                                thursdayDuration = "15";
                                timeDuration = thursdayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Fri")) {
                                counter = 0;

                                fridayStart[0] = schedule.getMorningStartTime();
                                fridayStart[1] = schedule.getAfternoonStartTime();
                                fridayStart[2] = schedule.getEveningStartTime();

                                fridayEnd[0] = schedule.getMorningEndTime();
                                fridayEnd[1] = schedule.getAfternoonEndTime();
                                fridayEnd[2] = schedule.getEveningEndTime();


                                while (fridayStartTime.equals("") && counter < fridayStart.length) {

                                    if (fridayStart[counter] != null) {
                                        fridayStartTime = fridayStart[counter];
                                        fridayEndTime = fridayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "fridayStartTime: " + fridayStartTime);
                                Log.d("+++EndTime+++", "fridayEndTime: " + fridayEndTime);

                                fridayDuration = "15";
                                timeDuration = fridayDuration;
                                counter = 0;

                            } else if (schedule.getDay().equals("Sat")) {
                                counter = 0;

                                saturdayStart[0] = schedule.getMorningStartTime();
                                saturdayStart[1] = schedule.getAfternoonStartTime();
                                saturdayStart[2] = schedule.getEveningStartTime();

                                saturdayEnd[0] = schedule.getMorningEndTime();
                                saturdayEnd[1] = schedule.getAfternoonEndTime();
                                saturdayEnd[2] = schedule.getEveningEndTime();


                                while (saturdayStartTime.equals("") && counter < saturdayStart.length) {

                                    if (saturdayStart[counter] != null) {
                                        saturdayStartTime = saturdayStart[counter];
                                        saturdayEndTime = saturdayEnd[counter];
                                    }

                                    counter++;
                                }

                                Log.d("+++StartTime+++", "saturdayStartTime: " + saturdayStartTime);
                                Log.d("+++EndTime+++", "saturdayEndTime: " + saturdayEndTime);

                                saturdayDuration = "15";
                                timeDuration = saturdayDuration;
                                counter = 0;

                            }
                        }
                    }

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            appointmentTimeAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_districtitem_spinner_view, appointmentTimeArray);
            appointmentTimeAdapter.setDropDownViewResource(R.layout.item_districtitem_spinner_layout);
            appointmentTimeAdapter.notifyDataSetChanged();
            spinner.setAdapter(appointmentTimeAdapter);



            loaderLayout.setVisibility(View.GONE);
            ll_timesetLayout.setVisibility(View.VISIBLE);


        }
    }
*/



    private class createAppointmentAPI extends AsyncTask<String, String, String> {
        String doctorId, userToken, patientId, selectedDateInString, startTime, endTime;


        private createAppointmentAPI(String patientId, String userToken, String doctorId, String selectedDateInString, String startTime, String endTime) {
            this.patientId = patientId;
            this.userToken = userToken;
            this.doctorId = doctorId;
            this.selectedDateInString = selectedDateInString;
            this.startTime = startTime;
            this.endTime = endTime;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressDialog = new ProgressDialog(getActivity()) ;
            progressDialog.setTitle("Loading...");
            progressDialog.setMessage("Creating new appoinment..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();


            Log.d("error", "statusReport: onPreExecute e dhukse!!!");
            Log.d("patientDetails", "patientId: "+patientId+", userToken: "+userToken);
            ll_timesetLayout.setClickable(false);

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentPatientStore");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            nameValuePairs.add(new BasicNameValuePair("doctor_id", doctorId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("patient_id", patientId));
            nameValuePairs.add(new BasicNameValuePair("appointment_date", selectedDateInString));
            nameValuePairs.add(new BasicNameValuePair("appointment_start_time", startTime));
            nameValuePairs.add(new BasicNameValuePair("appointment_end_time", endTime));


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();

            Toast.makeText(getActivity(), "New appointment created", Toast.LENGTH_SHORT).show();

            Log.d("patientDetails", "patientId: "+patientId+", userToken: "+token);


            ll_timesetLayout.setClickable(true);
            dismiss();
        }
    }




}
