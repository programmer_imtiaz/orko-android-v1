package com.arko.nextdot.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Activities.ChatActivity;
import com.arko.nextdot.Adapters.MessageAdapter;
import com.arko.nextdot.Model.MessageModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class MessageFragment extends Fragment implements MessageAdapter.ClickListener{


    public MessageFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager ;
    RecyclerView recyclerView ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_message, container, false) ;
        recyclerView = (RecyclerView) view.findViewById(R.id.message_recyler);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        MessageAdapter messageAdapter = new MessageAdapter(getActivity(), getdata()) ;

        messageAdapter.setClickListener(this) ;//

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(messageAdapter);
        return view ;
    }

    private List<MessageModel> getdata(){

        String[] lastmessage = {"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo",} ;

        String[] sendername = {"Md. Imtiaz Uddin", "Md. Samsul hoque", "Md. Mahbubur Rahman", "Md. MirBahar",
                "Md. Imtiaz Uddin", "Md. Samsul hoque", "Md. Mahbubur Rahman", "Md. MirBahar"} ;

        String[] last_date = {"JAN 7", "FEB 7", "MAR 7", "APR 7", "MAY 7", "JUN 7", "JUL 7", "AUG 7"} ;

        List<MessageModel> list = new ArrayList() ;
        for(int i = 0 ; i < lastmessage.length ; i++){
            MessageModel messageModel = new MessageModel() ;
            messageModel.setMessage(lastmessage[i]);
            messageModel.setLast_date(last_date[i]);
            messageModel.setSender_name(sendername[i]);
            list.add(messageModel);
        }
        return list ;
    }

    @Override
    public void ItemClicked(View v, int position) {//
        Log.d("TAG !!!!!!!", "CLICKEDDDDDDDDDDD") ;
        Intent intent = new Intent(getActivity(), ChatActivity.class) ;
        startActivity(intent);
    }
}
