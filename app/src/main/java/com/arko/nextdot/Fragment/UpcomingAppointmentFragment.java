package com.arko.nextdot.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arko.nextdot.Adapters.UpcomingAppointmentAdapter;
import com.arko.nextdot.Interface.UpcomingAppointmentView;
import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsList;
import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.UpcomingAppointmentsPresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by sakib on 11/23/2017.
 */

public class UpcomingAppointmentFragment extends Fragment implements UpcomingAppointmentView {

    @BindView(R.id.upcoming_progreesbar)
    ProgressBar upcomingProgreesbar;
    @BindView(R.id.upcoming_recyler)
    RecyclerView upcomingRecyler;
    @BindView(R.id.upcoming_appointments_no_result)
    TextView upcomingAppointmentsNoResult;

    Unbinder unbinder;
    UpcomingAppointmentsPresenter presenter;
    List<UpcomingAppointmentsList> list;
    private LinearLayoutManager layoutManager;
    UpcomingAppointmentAdapter upcomingAppointmentAdapter;
    SharedPreferences sharedPreferences ;
    String token="", patient_id="" ;

    public UpcomingAppointmentFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upcoming_appointment, container, false);
        unbinder = ButterKnife.bind(this, view);
        intilization();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showUpcomingAppointmentResult(UpcomingAppointmentsRoot upcomingAppointmentsRoot) {

        Log.d("+++RESULT METHOD+++","yup there!!") ;
        list = upcomingAppointmentsRoot.getResult().getData();
        if (list.isEmpty()) {

        }
        else if (!list.isEmpty()) {
            Log.d("+++LIST CHECK+++","list is not empty") ;
            upcomingAppointmentAdapter = new UpcomingAppointmentAdapter(getAppContext(), list);
            upcomingRecyler.setLayoutManager(layoutManager);
            //upcomingAppointmentAdapter.setClicklistner(this);
            upcomingRecyler.setAdapter(upcomingAppointmentAdapter);
        }
    }

    @Override
    public void startLoading() {
        upcomingProgreesbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        upcomingProgreesbar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getActivity();
    }

    public void intilization() {
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getAppContext(), LinearLayoutManager.VERTICAL, false);
        presenter = new UpcomingAppointmentsPresenter(this);
        sharedPreferences = getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "");
        patient_id = sharedPreferences.getString(Constant.user_id, "") ;
        presenter.getUpcomingAppointments(token, patient_id);
    }
}
