package com.arko.nextdot.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arko.nextdot.Adapters.PredictiveLocationAdapter;
import com.arko.nextdot.Interface.PredictiveLocationFragmentView;
import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictionsItem;
import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;
import com.arko.nextdot.Presenter.PredictiveLocationPresenter;
import com.arko.nextdot.R;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by sakib on 11/19/2017.
 */

public class SearchDoctorFillterFragment extends DialogFragment implements PredictiveLocationFragmentView, PredictiveLocationAdapter.ClickListener {

    @BindView(R.id.search_by_fess_text)
    TextView searchByFessText;
    @BindView(R.id.rangeSeekbarfees)
    CrystalRangeSeekbar rangeSeekbarfees;
    @BindView(R.id.fees_min_value)
    TextView feesMinValue;
    @BindView(R.id.fees_max_value)
    TextView feesMaxValue;
    @BindView(R.id.fees_seekbar_text_layout)
    LinearLayout feesSeekbarTextLayout;
    Unbinder unbinder;
    @BindView(R.id.search_by_exp_text)
    TextView searchByExpText;
    @BindView(R.id.rangeSeekbarexp)
    CrystalRangeSeekbar rangeSeekbarexp;
    @BindView(R.id.exp_min_value)
    TextView expMinValue;
    @BindView(R.id.exp_max_value)
    TextView expMaxValue;
    @BindView(R.id.exp_seekbar_text_layout)
    LinearLayout expSeekbarTextLayout;
    @BindView(R.id.search_again)
    Button searchAgain;
    @BindView(R.id.close_flitter)
    Button closeFlitter;
    @BindView(R.id.fillter_by_location)
    EditText fillterByLocation;
    @BindView(R.id.predictive_locations)
    RecyclerView predictiveLocationsRecyler;
    @BindView(R.id.fillter_result_not_found)
    TextView fillterResultNotFound;
    @BindView(R.id.location_progressBar)
    ProgressBar locationProgressBar;
    @BindView(R.id.Seek_bar_layout)
    RelativeLayout SeekBarLayout;

    String exp_string = "", fees_string = "", speciality_string = "", location_string = "";
    List<String> list;
    List<PredictionsItem> locationList;
    DataPassToActivity dataPassToActivity;
    private LinearLayoutManager layoutManager;
    private PredictiveLocationAdapter predictivelocationAdapter;
    PredictiveLocationPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_doctor_fillter, container, false);
        unbinder = ButterKnife.bind(this, view);
        speciality_string = getArguments().getString("speciality", "");
        recyleAdapterInitilization();
        getDialog().setTitle("Search Fillter");
        expirenceSeekBarOnClick();
        feesSeekBarOnClick();
        locationList = new ArrayList<>();
        presenter = new PredictiveLocationPresenter(this);
        return view;
    }

    public static SearchDoctorFillterFragment newInstance(String speciality) {
        SearchDoctorFillterFragment searchDoctorFillterFragment = new SearchDoctorFillterFragment();
        Bundle args = new Bundle();
        args.putString("speciality", speciality);
        searchDoctorFillterFragment.setArguments(args);
        return searchDoctorFillterFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPassToActivity = (DataPassToActivity) context;
    }

    @OnClick(R.id.search_again)
    public void onSearchAgainClicked() {
        dataPassToActivity.dataPassing(exp_string, speciality_string, fees_string, location_string);
        getDialog().dismiss();
    }


    public interface DataPassToActivity {

        public void dataPassing(String exp_string, String speciality_string, String fees_string, String location);
    }


    @OnClick(R.id.close_flitter)
    public void onCloseFlitterClicked() {
        getDialog().dismiss();
    }


    private void expirenceSeekBarOnClick() {
        rangeSeekbarexp.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                expMinValue.setText(String.valueOf(minValue) + " Years");
                expMaxValue.setText(String.valueOf(maxValue) + " Years");
            }
        });

        rangeSeekbarexp.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("Doctor Experience=>", String.valueOf(minValue) + "-" + String.valueOf(maxValue));
                exp_string = String.valueOf(minValue) + "-" + String.valueOf(maxValue);
            }
        });
    }

    private void feesSeekBarOnClick() {
        rangeSeekbarfees.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                feesMinValue.setText(String.valueOf(minValue) + " BDT");
                feesMaxValue.setText(String.valueOf(maxValue) + " BDT");
            }
        });

        rangeSeekbarfees.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("Doctor Fees=>", String.valueOf(minValue) + "-" + String.valueOf(maxValue));
                fees_string = String.valueOf(minValue) + "-" + String.valueOf(maxValue);
            }
        });
    }

    @OnClick(R.id.fillter_by_location)
    public void onFillterbyLocationClicked() {

        fillterByLocation.requestFocus();
        fillterByLocation.setFocusableInTouchMode(true);

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(fillterByLocation, InputMethodManager.SHOW_FORCED);

        fillterByLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String query = s.toString();
                Log.d("++++QUERY++++", query);
                if (query.length() >= 1) {
                    SeekBarLayout.setVisibility(View.GONE);
                    predictiveLocationsRecyler.setVisibility(View.VISIBLE);
                    fillterResultNotFound.setVisibility(View.GONE);
                    presenter.getPredictiveLocation(query);
                }
                if (query.length() < 1) {
                    fillterResultNotFound.setVisibility(View.GONE);
                    predictiveLocationsRecyler.setVisibility(View.GONE);
                    SeekBarLayout.setVisibility(View.VISIBLE);
                    stopLoading();
                    predictivelocationAdapter.clearList();
                }
                //presenter.getPredictiveSearchData(query);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void showSearchResult(PredictiveLocationRoot predictiveLocationRoot) {
        if (predictiveLocationRoot.getStatus().equals("OK")) {
            locationList = predictiveLocationRoot.getPredictions();
            if (locationList.isEmpty()) {
                Log.d("+++LIST_EMPTY_CHECK+++", "YES");
                predictivelocationAdapter.clearList();
                fillterResultNotFound.setVisibility(View.VISIBLE);

            } else if (!locationList.isEmpty()) {
                Log.d("+++LIST_EMPTY_CHECK+++", "NO");
                fillterResultNotFound.setVisibility(View.GONE);
                predictivelocationAdapter.setFilter(locationList);
            }
        }
    }

    @Override
    public void startLoading() {
        locationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        locationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {

    }

    @Override
    public Context getAppContext() {
        return getActivity();
    }


    private void recyleAdapterInitilization() {

        locationList = new ArrayList<>();
        locationList.clear();
        layoutManager = new LinearLayoutManager(getAppContext(), LinearLayoutManager.VERTICAL, false);
        predictivelocationAdapter = new PredictiveLocationAdapter(getAppContext(), locationList);
        predictiveLocationsRecyler.setLayoutManager(layoutManager);
        predictivelocationAdapter.setClickListener(this);
        predictiveLocationsRecyler.setAdapter(predictivelocationAdapter);
        predictivelocationAdapter.notifyDataSetChanged();
    }

    @Override
    public void ItemClicked(View view, int position) {
        location_string = locationList.get(position).getDescription() ;
        fillterByLocation.setText(location_string);
        predictiveLocationsRecyler.setVisibility(View.GONE);
        SeekBarLayout.setVisibility(View.VISIBLE);
    }
}
