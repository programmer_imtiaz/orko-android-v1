package com.arko.nextdot.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.arko.nextdot.R;
import com.arko.nextdot.Retrofit.RetrofitModel.Appointments.PatientList;
import com.arko.nextdot.Utils.PreferenceManager;
import com.arko.nextdot.Adapters.PatientListAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentSetAppointment extends DialogFragment implements View.OnClickListener,AdapterView.OnItemSelectedListener {


    Spinner spinner;
    ArrayList<String> appointmentTimeArray=null;;
    List<PatientList> patientList = null;
    RecyclerView patientItemRecyclerview;
    TextView appoinment_nothing_to_show;
    PatientListAdapter patientListAdapter = null;
    LinearLayout ll_timesetLayout;
    Button submitButton;
    String selectedTime=null, appointmentID = null, patientsId=null;


    String token = null;
    PreferenceManager preferenceManager;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitButton:

                Intent i = new Intent();
                i.putExtra("selectedTime", selectedTime);
                i.putExtra("appointmentID", appointmentID);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);

                dismiss();
                break;

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View veiw, int position, long id) {

       /* if(spinner.getId() == R.id.appointmentTimeSpinner)
        {
            String txt=appointmentTimeArray.get(position).toString();
            Log.d("ttxt", txt+" "+"data nai "+appointmentTimeArray.size());
        }*/

        selectedTime=spinner.getSelectedItem().toString();
        Log.d("ttxt", selectedTime+" "+"data nai "+appointmentTimeArray.size());


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface UserNameListener {
        void onFinishUserDialog(String user);
    }

    // Empty constructor required for DialogFragment
    public FragmentSetAppointment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.set_appointment_time_dialog, container);
        preferenceManager = PreferenceManager.getInstance(getActivity());
        ll_timesetLayout = (LinearLayout) view.findViewById(R.id.setAppointmentLayout);
        spinner= (Spinner) view.findViewById(R.id.appointmentTimeSpinner);
        spinner.setOnItemSelectedListener(this);
        submitButton= (Button) view.findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);
        appointmentTimeArray=new ArrayList<>();

        appointmentTimeArray=preferenceManager.getAppointmentTimeRangeArray();
        appointmentID = preferenceManager.getAppointmentID();

        if(appointmentTimeArray!=null){

            if(appointmentTimeArray.size()>0)
                appointmentTimeArray.remove(appointmentTimeArray.size()-1);

            for (int l = 0; l < appointmentTimeArray.size(); l++) {
                Log.d("timeArray", appointmentTimeArray.get(l) + " ");
            }

            ArrayAdapter<String> appointmentTimeAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_districtitem_spinner_view, appointmentTimeArray);
            appointmentTimeAdapter.setDropDownViewResource(R.layout.item_districtitem_spinner_layout);
            spinner.setAdapter(appointmentTimeAdapter);
            appointmentTimeAdapter.notifyDataSetChanged();

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setTitle("Set Appointment Time");


    }
}
