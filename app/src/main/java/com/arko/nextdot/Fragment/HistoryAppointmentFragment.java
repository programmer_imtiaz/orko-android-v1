package com.arko.nextdot.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Adapters.HistoryAppointmentAdapter;
import com.arko.nextdot.Interface.HistoryAppointmentView;
import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentList;
import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.Presenter.HistoryAppointmentsPresenter;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by sakib on 11/23/2017.
 */

public class HistoryAppointmentFragment extends Fragment implements HistoryAppointmentView {

    @BindView(R.id.history_appointments_no_result)
    TextView historyAppointmentsNoResult;
    @BindView(R.id.history_progreesbar)
    ProgressBar historyProgreesbar;
    @BindView(R.id.history_recyler)
    RecyclerView historyRecyler;
    Unbinder unbinder;

    HistoryAppointmentsPresenter presenter;
    List<HistoryAppointmentList> list;
    private LinearLayoutManager layoutManager;
    HistoryAppointmentAdapter historyAppointmentAdapter;
    SharedPreferences sharedPreferences ;
    String token="", patient_id="" ;

    public HistoryAppointmentFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_appointment, container, false);
        unbinder = ButterKnife.bind(this, view);
        intilization();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void intilization() {
        list = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getAppContext(), LinearLayoutManager.VERTICAL, false);
        presenter = new HistoryAppointmentsPresenter(this);
        sharedPreferences = getActivity().getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constant.token, "");
        patient_id = sharedPreferences.getString(Constant.user_id, "") ;
        presenter.getHistoryAppointments(token, patient_id);
    }

    @Override
    public void showHistoryAppointmentResult(HistoryAppointmentsRoot historyAppointmentsRoot) {
        list = historyAppointmentsRoot.getResult().getData();
        if (list.isEmpty()) {

        }
        else if (!list.isEmpty()) {
            Log.d("+++LIST CHECK+++","list is not empty") ;
            historyAppointmentAdapter = new HistoryAppointmentAdapter(getAppContext(), list);
            historyRecyler.setLayoutManager(layoutManager);
            //upcomingAppointmentAdapter.setClicklistner(this);
            historyRecyler.setAdapter(historyAppointmentAdapter);
        }
    }

    @Override
    public void startLoading() {
        historyProgreesbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        historyProgreesbar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getAppContext() {
        return getActivity();
    }
}
