package com.arko.nextdot.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.arko.nextdot.Activities.TherapyDetailsActivity;
import com.arko.nextdot.Adapters.TherapyAdapter;
import com.arko.nextdot.Model.TherapyModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by Dipto on 5/5/2017.
 */

public class TherapyFragment extends Fragment implements TherapyAdapter.ClickListener {

    public TherapyFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_therapy, container, false);
        RecyclerView therapy_recyclerView = (RecyclerView) view.findViewById(R.id.therapy_recyler);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        TherapyAdapter myAdapter = new TherapyAdapter(getActivity(), getData()) ;
        myAdapter.setClickListener(this);

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(myAdapter);

        therapy_recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
        therapy_recyclerView.setLayoutManager(layoutManager);

        return view ;
    }

    public static List<TherapyModel> getData(){

        List<TherapyModel> list = new ArrayList<>() ;
        String[] therapy_name = {"Neak Massage", "Leg Massage", "Neak Massage", "Leg Massage", "Neak Massage", "Leg Massage",
                "Neak Massage", "Leg Massage", "Neak Massage", "Leg Massage" } ;

        for(int i = 0 ; i < therapy_name.length ; i++){
            TherapyModel therapyModel = new TherapyModel() ;
            therapyModel.setTherapy_title(therapy_name[i]);
            list.add(therapyModel);
        }
        return list ;
    }

    @Override
    public void ItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), TherapyDetailsActivity.class) ;
        startActivity(intent);
    }
}
