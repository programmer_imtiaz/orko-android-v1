package com.arko.nextdot.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Activities.MedicineTimeDetailsActivity;
import com.arko.nextdot.Adapters.MedicalReportAdapter;
import com.arko.nextdot.Model.MedicalReportModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class MedicalReportFragment extends Fragment implements MedicalReportAdapter.ClickListener {

    public MedicalReportFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager ;
    MedicalReportAdapter medicalReportAdapter ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_medical_report, container, false) ;
        RecyclerView medicine_report_recyclerView = (RecyclerView) view.findViewById(R.id.medical_report_recyler);
        medicalReportAdapter = new MedicalReportAdapter(getActivity(), getdata()) ;
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        medicine_report_recyclerView.setLayoutManager(layoutManager);
        medicine_report_recyclerView.setAdapter(medicalReportAdapter);
        return view ;
    }

    private List<MedicalReportModel> getdata(){
        List<MedicalReportModel> list = new ArrayList() ;

        String[] lab_name = {"Apollo Hospital", "popular dignostic", "Apollo Hospital", "popular dignostic"} ;
        String[] report_name = {"Urine Test", "Blood Test", "Angiogram", "Blood Pressure"} ;
        int[] report_icon = {R.drawable.urine_test, R.drawable.blood_test, R.drawable.cardiology_test, R.drawable.blood_pressure} ;

        for(int i = 0 ; i < lab_name.length ; i++){

            MedicalReportModel medicalReportModel = new MedicalReportModel() ;
            medicalReportModel.setLab_name(lab_name[i]);
            medicalReportModel.setReport_name(report_name[i]);
            medicalReportModel.setReport_icon(report_icon[i]);
            list.add(medicalReportModel);
        }
        return list ;
    }

    @Override
    public void ItemClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), MedicineTimeDetailsActivity.class) ;
        startActivity(intent);
    }
}
