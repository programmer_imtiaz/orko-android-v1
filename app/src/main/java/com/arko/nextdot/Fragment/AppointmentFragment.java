package com.arko.nextdot.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arko.nextdot.Adapters.AppointmentAdapter;
import com.arko.nextdot.Model.AppointmentModel;
import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;


public class AppointmentFragment extends Fragment {

    public AppointmentFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    LinearLayoutManager layoutManager ;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appoinment, container, false) ;
        RecyclerView appt_recyclerView = (RecyclerView) view.findViewById(R.id.appt_recylerview);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        AppointmentAdapter myAdapter = new AppointmentAdapter(getActivity(), getData()) ;
        appt_recyclerView.setAdapter(myAdapter);
        appt_recyclerView.setLayoutManager(layoutManager);
        return view ;
    }


    public static List<AppointmentModel> getData(){

        List<AppointmentModel> list = new ArrayList<>() ;
        String[] appt_name = {"Appt 1", "Appt 2", "Appt 3"} ;

        for(int i = 0 ; i < appt_name.length ; i++){
            AppointmentModel appointmentModel = new AppointmentModel() ;
            appointmentModel.setAppt_title(appt_name[i]);
            list.add(appointmentModel);
        }
        return list ;
    }
}
