package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.MedicineListModel;
import com.arko.nextdot.Model.PastApptModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;



public class MedicineListAdapter extends RecyclerView.Adapter<MedicineListAdapter.MedicineListViewHolder> {


    private LayoutInflater inflater;
    List<MedicineListModel> list = Collections.emptyList();
    Context context ;
    private ClickListener clickListener ;

    public MedicineListAdapter(Context context, List<MedicineListModel> list){

        inflater = LayoutInflater.from(context) ;
        this.list = list ;
        this.context = context ;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public MedicineListViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_medicine_list, viewGroup, false) ;
        MedicineListViewHolder medicineListViewHolder = new MedicineListViewHolder(view) ;
        return medicineListViewHolder;
    }

    @Override
    public void onBindViewHolder(MedicineListViewHolder medicineListViewHolder, int position) {
        MedicineListModel medicineListModel = list.get(position) ;
        medicineListViewHolder.medicine_name.setText(medicineListModel.getMedicine_name());
        medicineListViewHolder.medicine_price.setText(medicineListModel.getMedicine_price());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MedicineListViewHolder extends RecyclerView.ViewHolder{

        TextView medicine_name, medicine_price ;

        public MedicineListViewHolder(View itemView) {
            super(itemView);
            medicine_name = (TextView) itemView.findViewById(R.id.medicine_name);
            medicine_price = (TextView) itemView.findViewById(R.id.medicine_price) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }
}
