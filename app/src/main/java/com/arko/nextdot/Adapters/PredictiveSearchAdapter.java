package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchList;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 11/15/2017.
 */

public class PredictiveSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final LayoutInflater inflater;
    private Context context;
    private List<SearchList> list = Collections.emptyList();
    private ClickListener clickListener ;

    public PredictiveSearchAdapter(Context context, List<SearchList> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    public void setClicklistner(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        PredictiveSearchNameViewHolder predictiveSearchNameViewHolder = null;
        PredictiveSearchSpecialityViewHolder predictiveSearchSpecialityViewHolder = null;

        if (viewType == 1) {
            View view = inflater.inflate(R.layout.item_search_by_name, parent, false);
            predictiveSearchNameViewHolder = new PredictiveSearchNameViewHolder(view);
            return predictiveSearchNameViewHolder;
        }
        else if (viewType == 2) {
            View view = inflater.inflate(R.layout.item_search_by_speciality, parent, false);
            predictiveSearchSpecialityViewHolder = new PredictiveSearchSpecialityViewHolder(view);
            return predictiveSearchSpecialityViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case 1:
                PredictiveSearchNameViewHolder nameViewHolder = (PredictiveSearchNameViewHolder) holder ;
                SearchList searchList = list.get(position) ;
                String header_name = "" ;

                if (searchList.getFirstName() != null) {
                    header_name = searchList.getFirstName();
                    Log.d("++++First Name+++", header_name) ;
                }
                else if (searchList.getFirstName() == null) {
                    header_name = "";
                }
                if (searchList.getLastName() != null) {
                    if (searchList.getFirstName() != null) {
                        header_name = header_name + " " +searchList.getLastName();
                    }
                    if (searchList.getFirstName() == null) {
                        header_name = searchList.getLastName();
                    }
                }
                else if (searchList.getLastName() == null) {
                    header_name = header_name;
                }
                nameViewHolder.searchByDoctorName.setText(header_name);
                /*if(header_name.equals("")){
                    nameViewHolder.searchByDoctorName.setText("speciality not available");
                }*/

                if(searchList.getDocSpeciality() != null){
                    nameViewHolder.searchByDoctorNameSpeciality.setText(searchList.getDocSpeciality());
                }
                else if(searchList.getDocSpeciality() == null){
                    nameViewHolder.searchByDoctorNameSpeciality.setText("Speciality Not Found");
                }

                if (searchList.getAddress() != null) {
                    nameViewHolder.searchByDoctorAddress.setText(searchList.getAddress());
                }
                else if (searchList.getAddress() == null) {
                    nameViewHolder.searchByDoctorAddress.setText("Address Not Found");
                }
                break;

            case 2:
                PredictiveSearchSpecialityViewHolder searchSpecialityViewHolder = (PredictiveSearchSpecialityViewHolder) holder ;
                SearchList speciality_list = list.get(position) ;

                if (speciality_list.getName() != null) {
                    searchSpecialityViewHolder.searchBySpecialityText.setText(speciality_list.getName());
                }
                else if (speciality_list.getNameBn() != null) {
                    searchSpecialityViewHolder.searchBySpecialityText.setText(speciality_list.getNameBn());
                }
                else {
                    searchSpecialityViewHolder.searchBySpecialityText.setText("");
                }
                break;
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PredictiveSearchNameViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.search_by_doctor_name_speciality)
        TextView searchByDoctorNameSpeciality;

        @BindView(R.id.search_by_doctor_address)
        TextView searchByDoctorAddress;

        @BindView(R.id.search_by_doctor_name)
        TextView searchByDoctorName;

        public PredictiveSearchNameViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.itemClicked(view, getPosition());
                    }
                }
            });
        }
    }


    public class PredictiveSearchSpecialityViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.search_by_speciality_text)
        TextView searchBySpecialityText;

        public PredictiveSearchSpecialityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.itemClicked(view, getPosition());
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list != null) {
            SearchList searchList = list.get(position);
            if (searchList != null) {
                if (searchList.getSearchType().equals("name")) {
                    Log.d("+++VIEW_TYPE+++", "1");
                    return 1;
                } else if (searchList.getSearchType().equals("symptom_speciality")) {
                    Log.d("+++VIEW_TYPE+++", "2");
                    return 2;
                }
            }
        }
        return 0;
    }

    public void setFilter(List<SearchList> searchlist) {
        this.list.clear();
        this.list = searchlist;
        notifyDataSetChanged();
    }

    public void clearList() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public interface ClickListener{

        public void itemClicked(View view, int position);
    }
}
