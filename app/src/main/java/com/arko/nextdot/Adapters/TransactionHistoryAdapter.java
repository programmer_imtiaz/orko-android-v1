package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.TransactionHistoryModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;


public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.TransactionHistoryViewHolder> {

    private LayoutInflater inflater;
    List<TransactionHistoryModel> list = Collections.emptyList();
    Context context ;

    public TransactionHistoryAdapter(Context context, List<TransactionHistoryModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public TransactionHistoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_transaction_history, viewGroup, false) ;
        TransactionHistoryViewHolder transactionHistoryViewHolder = new TransactionHistoryViewHolder(view) ;
        return transactionHistoryViewHolder;
    }

    @Override
    public void onBindViewHolder(TransactionHistoryViewHolder transactionHistoryViewHolder, int position) {
        TransactionHistoryModel transactionHistoryModel = list.get(position) ;
        transactionHistoryViewHolder.trns_date_time.setText(transactionHistoryModel.getTrns_time_date());
        transactionHistoryViewHolder.trns_payment.setText(transactionHistoryModel.getTrns_payment());
        transactionHistoryViewHolder.trns_points.setText(transactionHistoryModel.getTrns_points());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TransactionHistoryViewHolder extends RecyclerView.ViewHolder{

        TextView trns_date_time, trns_payment, trns_points ;

        public TransactionHistoryViewHolder(View itemView) {
            super(itemView);

            trns_date_time = (TextView) itemView.findViewById(R.id.transaction_time_date);
            trns_payment = (TextView) itemView.findViewById(R.id.transaction_payment) ;
            trns_points = (TextView) itemView.findViewById(R.id.transaction_points) ;
        }
    }
}
