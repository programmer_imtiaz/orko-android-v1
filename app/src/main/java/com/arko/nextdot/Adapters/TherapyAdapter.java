package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arko.nextdot.Model.TherapyModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;


public class TherapyAdapter extends RecyclerView.Adapter<TherapyAdapter.MyTherapyHolder> {

    private LayoutInflater inflater;
    List<TherapyModel> list = Collections.emptyList();
    Context context ;

    private ClickListener clickListener ;

    public TherapyAdapter(Context context, List<TherapyModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public MyTherapyHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_therapy, viewGroup, false) ;
        MyTherapyHolder myTherapyHolder = new MyTherapyHolder(view) ;
        return myTherapyHolder;
    }

    @Override
    public void onBindViewHolder(MyTherapyHolder myTherapyHolder, int position) {
        TherapyModel therapyModel = list.get(position) ;
        myTherapyHolder.therapy_title.setText(therapyModel.getTherapy_title());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyTherapyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView therapy_title ;

        public MyTherapyHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            therapy_title = (TextView) itemView.findViewById(R.id.therapy_title);
        }

        @Override
        public void onClick(View v) {

            if(clickListener != null){
                clickListener.ItemClicked(v, getPosition());
            }
        }
    }
    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }

}
