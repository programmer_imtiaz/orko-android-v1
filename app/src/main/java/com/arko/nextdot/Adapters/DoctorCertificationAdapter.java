package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Certificate;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorCertificationAdapter extends RecyclerView.Adapter<DoctorCertificationAdapter.DoctorCertificationViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Certificate> list = Collections.emptyList() ;

    public DoctorCertificationAdapter(Context context, List<Certificate> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorCertificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_certification, parent, false) ;
        DoctorCertificationViewHolder doctorCertificationViewHolder = new DoctorCertificationViewHolder(view) ;
        return doctorCertificationViewHolder;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorCertificationViewHolder holder, int position) {

        Certificate certificate = list.get(position) ;
        holder.tvCertificationName.setText(certificate.getCertificateName());
        holder.tvCertificationAuthority.setText(certificate.getCertificateAuthority());
        holder.tvCertificationDescription.setText(certificate.getDescription());
        holder.tvCertificationLicense.setText(certificate.getLicenseNumber());
        holder.tvCertificationTimePeriod.setText(certificate.getFromDate() + " - " + certificate.getToDate());
    }

    public class DoctorCertificationViewHolder extends RecyclerView.ViewHolder{

        TextView tvCertificationName, tvCertificationAuthority, tvCertificationLicense, tvCertificationTimePeriod, tvCertificationDescription ;

        public DoctorCertificationViewHolder(View itemView) {
            super(itemView);
            tvCertificationName = (TextView) itemView.findViewById(R.id.tvCertificationName);
            tvCertificationAuthority = (TextView) itemView.findViewById(R.id.tvCertificationAuthority);
            tvCertificationLicense = (TextView) itemView.findViewById(R.id.tvCertificationLicense);
            tvCertificationTimePeriod = (TextView) itemView.findViewById(R.id.tvCertificationTimePeriod);
            tvCertificationDescription = (TextView) itemView.findViewById(R.id.tvCertificationDescription);
        }
    }
}
