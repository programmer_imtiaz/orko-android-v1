package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.arko.nextdot.Fragment.FragmentMedicalReports;
import com.arko.nextdot.Fragment.FragmentPrescriptions;


/**
 * Created by Sadikul on 2/23/2017.
 */


public class viewPagerCustomLayoutAdapter extends FragmentPagerAdapter {
    String[] item={"Prescriptions","Reports"};
    Context context;
    public viewPagerCustomLayoutAdapter(FragmentManager fragmentManager, Context context){
        super(fragmentManager);
        this.context=context;
    }

    @Override
    public int getCount() {
        return item.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:{
                return new FragmentPrescriptions();
            }
            case 1:{
                return new FragmentMedicalReports();
            }
            default:{
                return new FragmentPrescriptions();
            }
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return item[position];
    }
}
