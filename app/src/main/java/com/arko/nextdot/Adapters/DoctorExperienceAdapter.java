package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Education;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Experience;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorExperienceAdapter extends RecyclerView.Adapter<DoctorExperienceAdapter.DoctorExperienceViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Experience> list = Collections.emptyList() ;

    public DoctorExperienceAdapter(Context context, List<Experience> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_experience, parent, false) ;
        DoctorExperienceViewHolder doctorExperienceViewHolder = new DoctorExperienceViewHolder(view) ;
        return doctorExperienceViewHolder ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorExperienceViewHolder holder, int position) {

        Experience experience = list.get(position) ;
        if(experience.getTitle() != null){
            holder.tv_job_title.setText(experience.getTitle());
        }
        if(experience.getCompany() != null){
            holder.tv_compnay_name.setText(experience.getCompany());
        }
        if(experience.getAddress() != null){
            holder.tvCompany_location.setText(experience.getAddress());
        }
        if(experience.getFromDate() != null && experience.getToDate() != null){
            holder.tvJobDuration.setText(experience.getFromDate() + " - " + experience.getToDate());
        }

        //holder.tvCourseAchievement.setText(courses.);
    }

    public class DoctorExperienceViewHolder extends RecyclerView.ViewHolder{

        TextView tv_job_title, tv_compnay_name, tvCompany_location, tvJobDuration ;

        public DoctorExperienceViewHolder(View itemView) {

            super(itemView);
            tv_job_title = (TextView) itemView.findViewById(R.id.tv_job_title);
            tv_compnay_name = (TextView) itemView.findViewById(R.id.tv_compnay_name);
            tvCompany_location = (TextView) itemView.findViewById(R.id.tvCompany_location);
            tvJobDuration = (TextView) itemView.findViewById(R.id.tvJobDuration);
        }
    }

}
