package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arko.nextdot.Model.RetrofitModel.PrescriptionItem;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sakib on 5/17/2017.
 */

public class PrescriptionsShowAdapter extends PagerAdapter implements  CardAdapter{

    ShowReportAndPrescriptoin showReportAndPrescriptoin;
    private List<CardView> mViews;
    private List<PrescriptionItem> mData;
    private float mBaseElevation;
    Context context;

    public PrescriptionsShowAdapter(Context context, ShowReportAndPrescriptoin showReportAndPrescriptoin){
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        this.context=context;
        this.showReportAndPrescriptoin= showReportAndPrescriptoin;
    }

    public void addCardItem(PrescriptionItem item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.item_walkthrough, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        CardView cardView = (CardView) view.findViewById(R.id.walkthrough_card);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }
        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
        mViews.set(position, null) ;
    }

    private void bind(PrescriptionItem walkthroughModel, View view){

        ImageView walk_img ;
        walk_img = (ImageView) view.findViewById(R.id.walk_img) ;
        //walk_img.setImageResource(walkthroughModel.getImageUrl());
        //Picasso.with(context).load(walkthroughModel.getImageUrl()).into(walk_img);
        Glide.with(context).load(Constants.baseUrl+walkthroughModel.getImageUrl()).fitCenter().dontAnimate().into(walk_img);

        walk_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReportAndPrescriptoin.show(Constants.baseUrl+walkthroughModel.getImageUrl());
            }
        });
    }

    public interface ShowReportAndPrescriptoin{
        public void show(String imageurl);
    }
}
