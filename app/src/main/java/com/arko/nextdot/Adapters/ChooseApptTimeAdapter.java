package com.arko.nextdot.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class ChooseApptTimeAdapter extends ArrayAdapter{

    List<String> time_list = new ArrayList() ;
    List<Integer> size = new ArrayList() ;
    Activity activity ;
    LayoutInflater inflater ;

    public ChooseApptTimeAdapter(@NonNull Context context, @LayoutRes int resource, List<String> time_list){
        super(context, resource);
        this.time_list = time_list ;
    }

    static class LayoutHandeler{
        TextView time_row ;
    }

    @Override
    public int getCount() {
        return time_list.size();
    }

    @Override
    public Object getItem(int position) {
        return time_list.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView ;
        LayoutHandeler layoutHandeler ;

        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
            row =layoutInflater.inflate(R.layout.item_choose_time, parent, false) ;
            layoutHandeler = new LayoutHandeler() ;
            layoutHandeler.time_row  = (TextView) row.findViewById(R.id.appt_choose_time);
            row.setTag(layoutHandeler);
        }
        else {

            layoutHandeler = (LayoutHandeler) row.getTag();

        }

        layoutHandeler.time_row.setText(time_list.get(position));
        return row ;
    }
}
