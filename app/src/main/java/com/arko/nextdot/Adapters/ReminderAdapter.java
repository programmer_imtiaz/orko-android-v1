package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.ReminderModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;


public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder> {


    private LayoutInflater inflater;
    List<ReminderModel> list = Collections.emptyList();
    Context context ;

    public ReminderAdapter(Context context, List<ReminderModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_reminder, viewGroup, false) ;
        ReminderViewHolder reminderViewHolder = new ReminderViewHolder(view) ;
        return reminderViewHolder ;
    }

    @Override
    public void onBindViewHolder(ReminderViewHolder reminderViewHolder, int position) {
        ReminderModel reminderModel = list.get(position) ;
        reminderViewHolder.reminder_doc_name.setText(reminderModel.getConsult_doc());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ReminderViewHolder extends RecyclerView.ViewHolder{

        TextView reminder_doc_name ;
        public ReminderViewHolder(View itemView) {
            super(itemView);
            reminder_doc_name = (TextView) itemView.findViewById(R.id.doctor_name) ;
        }
    }
}
