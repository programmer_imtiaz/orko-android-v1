package com.arko.nextdot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arko.nextdot.Activities.TestReportActivity;
import com.arko.nextdot.Fragment.TestReportFragment;
import com.arko.nextdot.Model.AmbulanceModel;
import com.arko.nextdot.Model.TestReportModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;


public class TestReportAdapter extends RecyclerView.Adapter<TestReportAdapter.MyTestReportHolder> {

    private LayoutInflater inflater;
    List<TestReportModel> list = Collections.emptyList();
    Context context ;

    public TestReportAdapter(Context context, List<TestReportModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public MyTestReportHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = inflater.inflate(R.layout.item_test_report, viewGroup, false) ;
        MyTestReportHolder myTestReportHolder = new MyTestReportHolder(view) ;
        return myTestReportHolder;
    }

    @Override
    public void onBindViewHolder(MyTestReportHolder myTestReportHolder, int position) {
        TestReportModel testReportModel = list.get(position) ;
        myTestReportHolder.testname.setText(testReportModel.getTestname());
        myTestReportHolder.btn_test_report_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TestReportActivity.class) ;
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyTestReportHolder extends RecyclerView.ViewHolder{

        TextView testname ;
        Button btn_test_report_view ;

        public MyTestReportHolder(View itemView) {
            super(itemView);
            testname = (TextView) itemView.findViewById(R.id.test_name);
            btn_test_report_view = (Button) itemView.findViewById(R.id.btn_test_report_view) ;
        }
    }
}
