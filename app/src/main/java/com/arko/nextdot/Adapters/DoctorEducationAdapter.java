package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Courses;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Education;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorEducationAdapter extends RecyclerView.Adapter<DoctorEducationAdapter.DoctorEducationViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Education> list = Collections.emptyList() ;

    public DoctorEducationAdapter(Context context, List<Education> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorEducationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_education, parent, false) ;
        DoctorEducationViewHolder doctorEducationViewHolder = new DoctorEducationViewHolder(view) ;
        return doctorEducationViewHolder;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorEducationViewHolder holder, int position) {

        Education education = list.get(position) ;
        holder.tv_education_degree.setText(education.getDegree());
        holder.tv_education_fieldOfStudy.setText(education.getFieldOfStudy());
        holder.tv_education_grade.setText(education.getGrade());
        holder.tv_education_duration.setText(education.getFromDate() + " - " + education.getToDate());
        //holder.tvCourseAchievement.setText(courses.);
    }

    public class DoctorEducationViewHolder extends RecyclerView.ViewHolder{

        TextView tv_education_degree, tv_education_fieldOfStudy, tv_education_grade, tv_education_duration ;

        public DoctorEducationViewHolder(View itemView) {

            super(itemView);
            tv_education_degree = (TextView) itemView.findViewById(R.id.tv_education_degree);
            tv_education_fieldOfStudy = (TextView) itemView.findViewById(R.id.tv_education_fieldOfStudy);
            tv_education_grade = (TextView) itemView.findViewById(R.id.tv_education_grade);
            tv_education_duration = (TextView) itemView.findViewById(R.id.tv_education_duration);
        }
    }
}
