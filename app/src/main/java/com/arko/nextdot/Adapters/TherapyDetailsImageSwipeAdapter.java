package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.arko.nextdot.R;


public class TherapyDetailsImageSwipeAdapter extends PagerAdapter {

    private int[] image_list = {R.drawable.neak_massage, R.drawable.neak_massage, R.drawable.neak_massage} ;
    private Context context ;
    private LayoutInflater layoutInflater ;

    public TherapyDetailsImageSwipeAdapter(Context context){
        this.context = context ;

    }
    @Override
    public int getCount() {
        return image_list.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return (view == (LinearLayout)obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
        View img_item_view = layoutInflater.inflate(R.layout.therapy_details_image_swiper, container, false);
        ImageView imageView = (ImageView) img_item_view.findViewById(R.id.image_slider);
        imageView.setBackgroundResource(image_list[position]);
        container.addView(img_item_view);
        return img_item_view ;
        //return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
