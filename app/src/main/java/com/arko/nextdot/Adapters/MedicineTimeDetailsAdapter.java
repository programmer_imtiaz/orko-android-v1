package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.MedicineTimeDetailsModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 8/7/2017.
 */

public class MedicineTimeDetailsAdapter extends RecyclerView.Adapter<MedicineTimeDetailsAdapter.MedicineTimeDetailsViewHolder> {


    private final LayoutInflater inflater;
    private List<MedicineTimeDetailsModel> list = Collections.emptyList() ;
    private Context context ;

    public MedicineTimeDetailsAdapter(Context context, List<MedicineTimeDetailsModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public MedicineTimeDetailsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_medicine_time, viewGroup, false) ;
        MedicineTimeDetailsViewHolder medicineTimeDetailsViewHolder = new MedicineTimeDetailsViewHolder(view) ;
        return medicineTimeDetailsViewHolder ;
    }

    @Override
    public void onBindViewHolder(MedicineTimeDetailsViewHolder medicineTimeDetailsViewHolder, int position) {
        MedicineTimeDetailsModel medicineTimeDetailsModel = list.get(position) ;
        medicineTimeDetailsViewHolder.medicine_time.setText(medicineTimeDetailsModel.getTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MedicineTimeDetailsViewHolder extends RecyclerView.ViewHolder{

        TextView medicine_time ;

        public MedicineTimeDetailsViewHolder(View itemView) {
            super(itemView);

            medicine_time = (TextView) itemView.findViewById(R.id.medicine_time_text);
        }
    }
}
