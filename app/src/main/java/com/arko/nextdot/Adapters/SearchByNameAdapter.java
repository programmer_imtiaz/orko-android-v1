package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameProfile;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 10/16/2017.
 */

public class SearchByNameAdapter extends RecyclerView.Adapter<SearchByNameAdapter.SearchByNameViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<SearchByNameProfile> list = Collections.emptyList() ;
    private ClickListener clickListener ;

    public SearchByNameAdapter(Context context, List<SearchByNameProfile> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public SearchByNameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_search_by_name, parent, false) ;
        SearchByNameViewHolder searchByNameViewHolder = new SearchByNameViewHolder(view) ;
        return searchByNameViewHolder;
    }

    @Override
    public void onBindViewHolder(SearchByNameViewHolder holder, int position) {
        SearchByNameProfile searchByNameProfile = list.get(position) ;
        /*holder.doctor_name.setText(searchByNameProfile.getName());
        holder.doctor_speciality.setText(searchByNameProfile.getSpeciality());
        holder.doctor_rating.setRating(searchByNameProfile.getRating());*/
        /*String header_name = "" ;
        if(searchByNameProfile.getFirstName() != null){
            header_name = searchByNameProfile.getFirstName() ;
        }
        else{
            header_name = "" ;
        }
        if(searchByNameProfile.getLastName() != null){
            header_name = header_name+" "+searchByNameProfile.getLastName() ;
        }
        else{
            header_name = header_name+"" ;
        }
        holder.doctor_name.setText(header_name);

        if(searchByNameProfile.getSpeciality() != null){
            holder.doctor_speciality.setText(searchByNameProfile.getSpeciality()+" Specialist");
        }
        holder.doctor_rating.setRating(4.00f);*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SearchByNameViewHolder extends RecyclerView.ViewHolder{

        TextView doctor_name, doctor_speciality ;
        RatingBar doctor_rating ;

        public SearchByNameViewHolder(View itemView) {
            super(itemView);
            doctor_name = (TextView) itemView.findViewById(R.id.search_by_doctor_name);
            doctor_speciality = (TextView) itemView.findViewById(R.id.search_by_doctor_name_speciality) ;
            //doctor_rating = (RatingBar) itemView.findViewById(R.id.new_doc_rating_bar) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public void setFilter(List<SearchByNameProfile> searchlist){
        this.list.clear();
        this.list = searchlist ;
        notifyDataSetChanged();
    }

    public void clearList(){
        this.list.clear();
        notifyDataSetChanged();
    }

    public interface ClickListener{

        public void ItemClicked(View view, int position) ;
    }
}