package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.MedicalListModel;
import com.arko.nextdot.R;

import java.util.Collection;
import java.util.Collections;
import java.util.List;



public class MedicalListAdapter extends RecyclerView.Adapter<MedicalListAdapter.MedicalListViewHolder> {

    private final LayoutInflater inflater;
    List<MedicalListModel> list = Collections.emptyList() ;
    Context context ;
    private ClickListener clickListener ;

    public MedicalListAdapter(Context context, List<MedicalListModel>list){
        
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }
    
    @Override
    public MedicalListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_medicine_list_prescription, parent, false) ;
        MedicalListViewHolder medicalListViewHolder = new MedicalListViewHolder(view) ;
        return medicalListViewHolder;
    }

    @Override
    public void onBindViewHolder(MedicalListViewHolder holder, int position) {
        MedicalListModel medicalListModel = list.get(position) ;
        holder.medicine_name.setText(medicalListModel.getMedicine_name());
        holder.medicine_cause.setText(medicalListModel.getMedicine_cause());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MedicalListViewHolder extends RecyclerView.ViewHolder {

        TextView daily_medicine_time, medicine_cause, medicine_name ;

        public MedicalListViewHolder(View itemView) {
            super(itemView);
            medicine_name = (TextView) itemView.findViewById(R.id.patient_medicine_name);
            medicine_cause = (TextView) itemView.findViewById(R.id.medicine_cause) ;
            daily_medicine_time = (TextView) itemView.findViewById(R.id.daily_medicine_time) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }
}
