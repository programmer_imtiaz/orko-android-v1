package com.arko.nextdot.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.Model.SubTitleModel;
import com.arko.nextdot.Model.TitleModel;
import com.arko.nextdot.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

/**
 * Created by sakib on 8/2/2017.
 */

public class PatientMedicalProfileAdapter extends ExpandableRecyclerViewAdapter<PatientMedicalProfileAdapter.TitleViewHolder, PatientMedicalProfileAdapter.SubTitleViewHolder> {

    private Context context ;
    public PatientMedicalProfileAdapter(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.context = context ;
    }

    @Override
    public TitleViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_title_patient_medical_profile, parent,false) ;
        return new TitleViewHolder(view);
    }

    @Override
    public SubTitleViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_subtitle_patient_medical_profile, parent,false) ;
        return new SubTitleViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SubTitleViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

        final SubTitleModel subTitleModel = ((TitleModel) group).getItems().get(childIndex);
        holder.setSubTitletName(subTitleModel.getSubtitle(), subTitleModel.getImg());

    }

    @Override
    public void onBindGroupViewHolder(TitleViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreTitle(context, group);
    }

    public class TitleViewHolder extends GroupViewHolder{


        ImageView img ;
        TextView titlename, title_paragraph ;
        public TitleViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.title_icon);
            titlename = (TextView) itemView.findViewById(R.id.title_text) ;
            title_paragraph = (TextView) itemView.findViewById(R.id.title_paragraph) ;
        }

        public void setGenreTitle(Context context, ExpandableGroup group){

            if (group instanceof TitleModel) {
                titlename.setText(group.getTitle());
                img.setImageResource(((TitleModel) group).getImg());
                title_paragraph.setText(((TitleModel) group).getTitle_paragraph());
            }
        }


    }

    public class SubTitleViewHolder extends ChildViewHolder{

        ImageView img_icon ;
        TextView subtitle_text ;
        public SubTitleViewHolder(View itemView) {
            super(itemView);
            img_icon = (ImageView) itemView.findViewById(R.id.subtitle_icon);
            subtitle_text = (TextView) itemView.findViewById(R.id.subtitle_text);

        }
        public void setSubTitletName(String subtitle, int img){
             img_icon.setImageResource(img);
             subtitle_text.setText(subtitle);
        }
    }
}
