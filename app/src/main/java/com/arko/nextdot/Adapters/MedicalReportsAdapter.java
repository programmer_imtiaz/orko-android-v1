package com.arko.nextdot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arko.nextdot.Activities.ReportsShowActivity;
import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.Constants;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sadikul on 2/12/2017.
 */

public class MedicalReportsAdapter extends RecyclerView.Adapter<MedicalReportsAdapter.Viewholder>{
    List<MedicalReportsItem> medicalReportsItems;
    Context context;
    public MedicalReportsAdapter(Context ctx, List<MedicalReportsItem> list) {

        this.context=ctx;

        medicalReportsItems =list;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_medical_reports,parent,false);
        Viewholder viewholder=new Viewholder(view,context, medicalReportsItems);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, int position) {
        final MedicalReportsItem item = medicalReportsItems.get(position);
        //Glide.with(context).load(item.getLink()).into(holder.imageView);
        //Glide.with(context).load(item.getLink()).override(100, 100).into(holder.imageView);
        //Glide.with(context).load("http://imagizer.imageshack.us/a/img924/5750/m2tP80.jpg").into(holder.imageView);
        Glide.with(context).load(Constants.baseUrl+item.getImageUrl()).override(512, 512).dontAnimate().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return medicalReportsItems.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        Context ctx;
        List<MedicalReportsItem> holderList=new ArrayList<>();
        public Viewholder(View itemView, Context ctx, List<MedicalReportsItem> newList) {
            super(itemView);
            this.ctx=ctx;
            this.holderList=newList;
            imageView= (ImageView) itemView.findViewById(R.id.report_item_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos=getAdapterPosition();
            MedicalReportsItem item=holderList.get(pos);
            Intent intent=new Intent(ctx,ReportsShowActivity.class);
            intent.putParcelableArrayListExtra("datalist", (ArrayList<? extends Parcelable>) medicalReportsItems);
            ctx.startActivity(intent);
        }
    }


}