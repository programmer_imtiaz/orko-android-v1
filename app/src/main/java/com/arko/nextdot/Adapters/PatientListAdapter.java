package com.arko.nextdot.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arko.nextdot.R;
import com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list.DoctorList;
import com.arko.nextdot.Retrofit.RetrofitModel.Doctor_list.DoctorListItem;
import com.arko.nextdot.Utils.Constants;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by ASUS on 24-Aug-17.
 */

public class PatientListAdapter extends RecyclerView.Adapter<PatientListAdapter.ExperienceViewHolder> {

    private List<DoctorListItem> patientList;
    DoctorList patients;
    Context context;


    public PatientListAdapter(DoctorList patientList, Context context) {
        this.patientList = patientList.getResult().getData();
        this.patients = patientList;
        this.context = context;
    }

    @Override
    public PatientListAdapter.ExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_patient_profile_item, parent, false);

        return new PatientListAdapter.ExperienceViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final PatientListAdapter.ExperienceViewHolder holder, final int position) {
        DoctorListItem item = patientList.get(position);
        if (item.getProfile() != null) {
            holder.tv_appinment_patientName.setText(item.getProfile().getFirstName() + " " + item.getProfile().getLastName());
            holder.tv_appinment_mainProblem.setText(item.getProfile().getSpeciality());
            String url = Constants.baseUrl + item.getAvatar();
            //Log.e("patientsUrl",url);
            Glide.with(context).load(url).placeholder(R.drawable.patient_placeholder).dontAnimate().fitCenter().override(100, 100).into(holder.proPic);

        } else if (item.getProfile() == null) {
            holder.tv_appinment_patientName.setText("Null");
            holder.tv_appinment_mainProblem.setText("Null");
            Glide.with(context).load(Constants.baseUrl).placeholder(R.drawable.patient_placeholder).fitCenter().dontAnimate().override(100, 100).into(holder.proPic);
        }

        if (item.getSet()) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#dadada"));
        } else {
            // holder.cardview.setBackgroundColor(Color.parseColor(context.getResources().getResourceName(R.color.whiteColor)));
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }


    }

    public class ExperienceViewHolder extends RecyclerView.ViewHolder {
        TextView tv_appinment_patientName, tv_appinment_mainProblem;


        ImageView proPic;
        RelativeLayout relativeLayout = null;

        public ExperienceViewHolder(View view) {
            super(view);
            proPic = (ImageView) view.findViewById(R.id.patientProfileImageview);
            tv_appinment_patientName = (TextView) view.findViewById(R.id.tv_appinment_patientName);
            tv_appinment_mainProblem = (TextView) view.findViewById(R.id.tv_appinment_mainProblem);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.cv_paientProfileContainer);
        }

    }

    @Override
    public int getItemCount() {
        if (patientList != null) {
            return patientList.size();
        } else {
            return 0;
        }
    }
}
