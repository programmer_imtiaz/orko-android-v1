package com.arko.nextdot.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.R;

import java.util.ArrayList;
import java.util.List;



public class NavDrawerAdapter extends BaseAdapter {

    List<String> nav_item_titles = new ArrayList() ;
    List<Integer> nav_item_img = new ArrayList() ;
    Activity activity ;
    LayoutInflater inflater;

    public NavDrawerAdapter(Activity activity, List<String> nav_item_titles, List<Integer> nav_item_img){

        this.nav_item_img = nav_item_img ;
        this.nav_item_titles = nav_item_titles ;
        this.activity = activity ;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class Holder{
        ImageView icon;
        TextView name;

    }

    @Override
    public int getCount() {
        return nav_item_img.size() ;
    }

    @Override
    public Object getItem(int position) {
        return nav_item_img.get(position);
    }

    @Override
    public long getItemId(int position) {
        return nav_item_img.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView ;
        Holder holder = new Holder() ;
        view = inflater.inflate(R.layout.nav_drawer_items,null);
        holder.icon = (ImageView) view.findViewById(R.id.nav_item_icon);
        holder.name = (TextView) view.findViewById(R.id.nav_item_txt) ;
        holder.name.setText(nav_item_titles.get(position));
        holder.icon.setImageResource(nav_item_img.get(position));

        return view ;
    }
}
