package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentList;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 11/26/2017.
 */

public class HistoryAppointmentAdapter extends RecyclerView.Adapter<HistoryAppointmentAdapter.HistoryAppointmentViewHolder> {

    private final LayoutInflater inflater;
    private List<HistoryAppointmentList> list = Collections.emptyList();
    private Context context;

    public HistoryAppointmentAdapter(Context context, List<HistoryAppointmentList> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public HistoryAppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_history_appointment, parent, false);
        HistoryAppointmentViewHolder historyAppointmentViewHolder = new HistoryAppointmentViewHolder(view);
        return historyAppointmentViewHolder;
    }

    @Override
    public void onBindViewHolder(HistoryAppointmentViewHolder holder, int position) {

        HistoryAppointmentList historyAppointmentList = list.get(position) ;
        String header_name = "", pro_pic_image_URL ;
        if(historyAppointmentList.getDoctorFirstName() != null){
            header_name = historyAppointmentList.getDoctorFirstName() ;
        }
        if(historyAppointmentList.getDoctorLastName() != null){
            header_name = header_name+" "+historyAppointmentList.getDoctorLastName() ;
        }
        else if(historyAppointmentList.getDoctorLastName() == null){
            header_name = header_name+"" ;
        }

        holder.historyDocName.setText(header_name);
        pro_pic_image_URL = historyAppointmentList.getDoctorPic() ;

        if (Patterns.WEB_URL.matcher(pro_pic_image_URL).matches()) {

            Glide.with(context).load(pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.historyDocPic);
        }
        else {

            Glide.with(context).load(Constant.ROOT_URL + pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.historyDocPic);
        }
        holder.historyStatus.setText(historyAppointmentList.getAppointmentStatus());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HistoryAppointmentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.history_doc_pic)
        CircularImageView historyDocPic;
        @BindView(R.id.tv_name_title)
        TextView tvNameTitle;
        @BindView(R.id.history_doc_name)
        TextView historyDocName;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.history_appt_date)
        TextView historyApptDate;
        @BindView(R.id.tv_range)
        TextView tvRange;
        @BindView(R.id.history_start_time)
        TextView historyStartTime;
        @BindView(R.id.tv_status_title)
        TextView tvStatusTitle;
        @BindView(R.id.history_status)
        TextView historyStatus;
        @BindView(R.id.card_item_text)
        LinearLayout cardItemText;
        @BindView(R.id.details)
        TextView details;
        @BindView(R.id.mainLayout)
        RelativeLayout mainLayout;
        @BindView(R.id.cv_pending_work)
        CardView cvPendingWork;

        public HistoryAppointmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView) ;
        }
    }
}
