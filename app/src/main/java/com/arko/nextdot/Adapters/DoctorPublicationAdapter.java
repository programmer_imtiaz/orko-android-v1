package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Experience;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Publication;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorPublicationAdapter extends RecyclerView.Adapter<DoctorPublicationAdapter.DoctorPublicationViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Publication> list = Collections.emptyList() ;

    public DoctorPublicationAdapter(Context context, List<Publication> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorPublicationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_publication, parent, false) ;
        DoctorPublicationViewHolder doctorPublicationViewHolder = new DoctorPublicationViewHolder(view) ;
        return doctorPublicationViewHolder ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorPublicationViewHolder holder, int position) {

        Publication publication = list.get(position) ;
        holder.tvPublisherName.setText(publication.getTitle());
        holder.tvPublicationAuthor.setText(publication.getAuthor());
        holder.tvPublicationUrl.setText(publication.getPublicationUrl());
        holder.tvPublicationDescription.setText(publication.getDescription());
        //holder.tvCourseAchievement.setText(courses.);
    }

    public class DoctorPublicationViewHolder extends RecyclerView.ViewHolder{

        TextView tvPublisherName, tvPublicationAuthor, tvPublicationUrl, tvPublicationDescription ;

        public DoctorPublicationViewHolder(View itemView) {

            super(itemView);
            tvPublisherName = (TextView) itemView.findViewById(R.id.tvPublisherName);
            tvPublicationAuthor = (TextView) itemView.findViewById(R.id.tvPublicationAuthor);
            tvPublicationUrl = (TextView) itemView.findViewById(R.id.tvPublicationUrl);
            tvPublicationDescription = (TextView) itemView.findViewById(R.id.tvPublicationDescription);
        }
    }

}
