package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.AppointmentModel;
import com.arko.nextdot.Model.PresentApptModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dipto on 5/8/2017.
 */

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder> {

    private LayoutInflater inflater;
    List<AppointmentModel> list = Collections.emptyList();
    Context context ;

    public AppointmentAdapter(Context context, List<AppointmentModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_appointment, viewGroup, false) ;
        AppointmentViewHolder appointmentViewHolder = new AppointmentViewHolder(view) ;
        return appointmentViewHolder;
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder appointmentViewHolder, int position) {
        AppointmentModel appointmentModel = list.get(position) ;
        appointmentViewHolder.present_appt_title.setText(appointmentModel.getAppt_title());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class AppointmentViewHolder extends RecyclerView.ViewHolder{

        TextView present_appt_title ;
        public AppointmentViewHolder(View itemView) {
            super(itemView);
            present_appt_title = (TextView) itemView.findViewById(R.id.appt_serial);
        }
    }
}
