package com.arko.nextdot.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.MessageModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{

    private final LayoutInflater inflater;
    List<MessageModel> list = Collections.emptyList() ;
    private Context context ;
    private ClickListener clickListener ;
    
    public MessageAdapter(Context context, List<MessageModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }
    
    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.item_message, viewGroup, false) ;
        MessageViewHolder messageViewHolder = new MessageViewHolder(view) ;
        return messageViewHolder;
    }

    public void setClickListener(ClickListener clickListener){
        Log.d("setClickListener:::::::", "ascheee !!!!!!!!!!!!!") ;
        this.clickListener = clickListener ;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder messageViewHolder, int position) {

        MessageModel messageModel = list.get(position) ;
        messageViewHolder.sender_name.setText(messageModel.getSender_name());
        messageViewHolder.last_message.setText(messageModel.getMessage());
        messageViewHolder.last_message_date.setText(messageModel.getLast_date());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView sender_name, last_message, last_message_date ;
        public MessageViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            sender_name = (TextView) itemView.findViewById(R.id.sender_name);
            last_message = (TextView) itemView.findViewById(R.id.last_message) ;
            last_message_date = (TextView) itemView.findViewById(R.id.last_message_date) ;
        }

        @Override
        public void onClick(View v) {//

            Log.d("onClick:::::::::::::", "ase nai !!!!!!!!!!!!!") ;

            if(clickListener != null){
                clickListener.ItemClicked(v, getPosition());
            }
            else{
                clickListener.ItemClicked(v, getPosition());
                Log.d("ELSE+++++++++++", "ase nai !!!!!!!!!!!!!") ;
            }
        }
    }

    public interface ClickListener{//
        public void ItemClicked(View v, int position) ;
    }
}
