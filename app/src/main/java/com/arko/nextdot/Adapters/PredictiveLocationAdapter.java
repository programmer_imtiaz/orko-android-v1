package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictionsItem;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 11/21/2017.
 */

public class PredictiveLocationAdapter extends RecyclerView.Adapter<PredictiveLocationAdapter.PredictiveLocationViewHolder> {

    private final LayoutInflater inflater;
    private Context context;
    private List<PredictionsItem> list = Collections.emptyList();
    ClickListener clickListener ;


    public PredictiveLocationAdapter(Context context, List<PredictionsItem> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public PredictiveLocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_adress, parent, false);
        PredictiveLocationViewHolder predictiveLocationViewHolder = new PredictiveLocationViewHolder(view);
        return predictiveLocationViewHolder;
    }

    @Override
    public void onBindViewHolder(PredictiveLocationViewHolder holder, int position) {
        PredictionsItem predictionsItem = list.get(position) ;
        if(predictionsItem.getDescription() != null){

            holder.content.setText(predictionsItem.getDescription());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PredictiveLocationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.id)
        ImageView id;
        @BindView(R.id.content)
        TextView content;

        public PredictiveLocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.ItemClicked(view, getPosition());
                    }
                }
            });
        }
    }

    public void setFilter(List<PredictionsItem> searchlist) {
        this.list.clear();
        this.list = searchlist;
        notifyDataSetChanged();
    }

    public void clearList() {
        this.list.clear();
        notifyDataSetChanged();
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position);
    }
}
