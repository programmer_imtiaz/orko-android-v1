package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Courses;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorCoursesAdapter extends RecyclerView.Adapter<DoctorCoursesAdapter.DoctorCoursesViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Courses> list = Collections.emptyList() ;

    public DoctorCoursesAdapter(Context context, List<Courses> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorCoursesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_courses, parent, false) ;
        DoctorCoursesViewHolder doctorCoursesViewHolder = new DoctorCoursesViewHolder(view) ;
        return doctorCoursesViewHolder;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorCoursesViewHolder holder, int position) {

        Courses courses = list.get(position) ;
        holder.tvCourseName.setText(courses.getTitle());
        holder.tvCourseOrganization.setText(courses.getOrganizedBy());
        holder.tvCourseCompanyVanue.setText(courses.getVenue());
        holder.tvCourseDuration.setText(courses.getFromDate() + " - " + courses.getToDate());
        //holder.tvCourseAchievement.setText(courses.);
    }

    public class DoctorCoursesViewHolder extends RecyclerView.ViewHolder{

        TextView tvCourseName, tvCourseOrganization, tvCourseCompanyVanue, tvCourseDuration, tvCourseAchievement ;

        public DoctorCoursesViewHolder(View itemView) {
            super(itemView);
            tvCourseName = (TextView) itemView.findViewById(R.id.tvCourseName);
            tvCourseOrganization = (TextView) itemView.findViewById(R.id.tvCourseOrganization);
            tvCourseCompanyVanue = (TextView) itemView.findViewById(R.id.tvCourseCompanyVanue);
            tvCourseDuration = (TextView) itemView.findViewById(R.id.tvCourseDuration);
            tvCourseAchievement = (TextView) itemView.findViewById(R.id.tvCourseAchievement);
        }
    }
}
