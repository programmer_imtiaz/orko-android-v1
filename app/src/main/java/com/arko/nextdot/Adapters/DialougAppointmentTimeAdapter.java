package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.arko.nextdot.Model.DialougAppointmentTimeModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by dipto on 7/27/2017.
 */

public class DialougAppointmentTimeAdapter extends RecyclerView.Adapter<DialougAppointmentTimeAdapter.DialougAppointmentViewHolder> {

    private final LayoutInflater inflater;
    private List<DialougAppointmentTimeModel> list = Collections.emptyList() ;
    private Context context ;
    private int lastCheckedPosition = -1;

    public DialougAppointmentTimeAdapter(Context context, List<DialougAppointmentTimeModel> list){

        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DialougAppointmentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = inflater.inflate(R.layout.item_dialoug_appt_time, viewGroup, false) ;
        DialougAppointmentViewHolder dialougAppointmentViewHolder = new DialougAppointmentViewHolder(view) ;
        return dialougAppointmentViewHolder ;
    }

    @Override
    public void onBindViewHolder(DialougAppointmentViewHolder dialougAppointmentViewHolder, int position) {

        DialougAppointmentTimeModel dialougAppointmentTimeModel = list.get(position) ;
        dialougAppointmentViewHolder.appt_time_text.setText(dialougAppointmentTimeModel.getTime_dataset());
        dialougAppointmentViewHolder.appt_time.setChecked(position == lastCheckedPosition);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class DialougAppointmentViewHolder extends RecyclerView.ViewHolder{

        AppCompatRadioButton appt_time  ;
        TextView appt_time_text ;

        public DialougAppointmentViewHolder(View itemView) {
            super(itemView);
            appt_time = (AppCompatRadioButton) itemView.findViewById(R.id.appt_time_btn);
            appt_time_text = (TextView) itemView.findViewById(R.id.appt_time_text) ;

            appt_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastCheckedPosition = getAdapterPosition() ;
                    notifyItemRangeChanged(0, list.size());
                    DialougAppointmentTimeModel dialougAppointmentTimeModel = new DialougAppointmentTimeModel() ;
                    dialougAppointmentTimeModel.setSelected_time(lastCheckedPosition);
                }
            });
        }
    }
}
