package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.AmbulanceModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dipto on 5/5/2017.
 */

public class AmbulanceAdapter extends RecyclerView.Adapter<AmbulanceAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    List<AmbulanceModel> list = Collections.emptyList();
    Context context ;

    public AmbulanceAdapter(Context context, List<AmbulanceModel> list){

        inflater = LayoutInflater.from(context) ;
        this.list = list ;
        this.context = context ;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = inflater.inflate(R.layout.item_ambulance, viewGroup, false) ;
        MyViewHolder myviewholder = new MyViewHolder(view) ;
        return myviewholder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        AmbulanceModel ambulanceModel = list.get(position) ;
        myViewHolder.textView.setText(ambulanceModel.getAmbulance_name());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView ;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.ambulance_text);
        }
    }
}
