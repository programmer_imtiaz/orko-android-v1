package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arko.nextdot.Activities.ProfileActivity;
import com.arko.nextdot.Model.RetrofitModel.MyDoctors.Data;
import com.arko.nextdot.Model.RetrofitModel.MyDoctors.MyDoctorsRoot;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sakib on 9/17/2017.
 */

public class MyDoctorsAdapter extends RecyclerView.Adapter<MyDoctorsAdapter.MyDoctorsViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Data> list = Collections.emptyList() ;

    private ClickListener clickListener ;

    public MyDoctorsAdapter(Context context, List<Data> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClicklistner(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public MyDoctorsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_my_doctors, parent, false) ;
        MyDoctorsViewHolder myDoctorsViewHolder = new MyDoctorsViewHolder(view) ;
        return myDoctorsViewHolder;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(MyDoctorsViewHolder holder, int position) {

        Data data = list.get(position) ;
        String header_name = "" ;
        if(data.getProfile() != null){
            if(data.getProfile().getFirstName() != null ){
                //holder.my_docotor_name.setText(data.getProfile().getFirstName()+" ");
                header_name = data.getProfile().getFirstName() ;
            }
            else{
                header_name = "" ;
            }
            if(data.getProfile().getLastName() != null){
                //holder.my_docotor_name.setText(data.getProfile().getLastName());
                header_name = header_name+" "+data.getProfile().getLastName() ;
            }
            else{
                header_name = header_name+" "+"" ;
            }
            holder.my_docotor_name.setText(header_name);

            if(data.getProfile().getSpeciality() != null){
                holder.my_docotor_speciality.setText(data.getProfile().getSpeciality());
            }
            else{
                holder.my_docotor_speciality.setText("") ;
            }
            holder.mydoctors_rating.setRating(data.getProfile().getRating());
            String pro_pic_image_URL = data.getAvatar() ;
            if(Patterns.WEB_URL.matcher(pro_pic_image_URL).matches()){

                Glide.with(context).load(pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.my_doctor_image);
            }
            else{

                Glide.with(context).load(Constant.ROOT_URL+pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.my_doctor_image);
            }

        }
    }

    public class MyDoctorsViewHolder extends RecyclerView.ViewHolder{

        TextView my_docotor_name, my_docotor_speciality ;
        RatingBar mydoctors_rating ;
        CircleImageView my_doctor_image ;

        public MyDoctorsViewHolder(View itemView) {
            super(itemView);
            my_docotor_name = (TextView) itemView.findViewById(R.id.my_doctors_name) ;
            my_docotor_speciality = (TextView) itemView.findViewById(R.id.speciality_text) ;
            mydoctors_rating  = (RatingBar) itemView.findViewById(R.id.my_doctor_rating) ;
            my_doctor_image = (CircleImageView) itemView.findViewById(R.id.circle_my_doctor_pic) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }
}
