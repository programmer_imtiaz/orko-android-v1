package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultProfile;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sakib on 9/20/2017.
 */

public class DoctorSearchResultAdapter extends RecyclerView.Adapter<DoctorSearchResultAdapter.DoctorSearchResultViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<SearchDoctorResultProfile> list = Collections.emptyList() ;
    private ClickListener clickListener ;
    //private List<String> list = Collections.emptyList() ;

    public DoctorSearchResultAdapter(Context context, List<SearchDoctorResultProfile> list){

        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClicklistner(ClickListener clickListener){
        this.clickListener = clickListener ;
    }


    @Override
    public DoctorSearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_doctor_search_result, parent, false) ;
        DoctorSearchResultViewHolder doctorSearchResultViewHolder = new DoctorSearchResultViewHolder(view) ;
        return doctorSearchResultViewHolder;
    }


    @Override
    public void onBindViewHolder(DoctorSearchResultViewHolder holder, int position) {
        //holder.ratingBar.setRating(Float.parseFloat(list.get(position)));
        SearchDoctorResultProfile searchDoctorResultProfile = list.get(position) ;
        String header_name = "" ;


        if(searchDoctorResultProfile.getFirstName() != null){
            header_name = searchDoctorResultProfile.getFirstName()+" " ;
        }
        else {

            header_name = "" ;
        }

        if(searchDoctorResultProfile.getLastName() != null){

            header_name = header_name+searchDoctorResultProfile.getLastName() ;
        }
        else{
            header_name = header_name+"" ;
        }
        holder.doctor_search_doctor_name.setText(header_name);

        if(searchDoctorResultProfile.getSpeciality() != null){
            holder.doctor_search_doctor_speciality.setText(searchDoctorResultProfile.getSpeciality()+" Specialist");
        }
        else{
            holder.doctor_search_doctor_speciality.setText("Speciality Not Found");
        }

        if(searchDoctorResultProfile.getFees() != null){
            holder.doctor_search_doctor_fees.setText("Fees "+searchDoctorResultProfile.getFees()+ " Tk");
        }
        else{
            holder.doctor_search_doctor_fees.setText("Fees not Found");
        }

        if(searchDoctorResultProfile.getAvatar() != null){
            if(Patterns.WEB_URL.matcher(searchDoctorResultProfile.getAvatar()).matches()){
                String avatar_path = searchDoctorResultProfile.getAvatar() ;
                Glide.with(context).load(avatar_path).placeholder(R.drawable.avtaar).dontAnimate().into(holder.doctor_search_doctor_pic);
            }
            else{
                String avatar_path = searchDoctorResultProfile.getAvatar() ;
                Glide.with(context).load(Constant.ROOT_URL+avatar_path).placeholder(R.drawable.avtaar).dontAnimate().into(holder.doctor_search_doctor_pic);
            }
        }
        else{
            holder.doctor_search_doctor_pic.setImageResource(R.drawable.avtaar);
        }

        if(searchDoctorResultProfile.getRating() > 0){
            holder.doctor_search_ratingBar.setRating(searchDoctorResultProfile.getRating());
        }
        else{
            float rate = 3.5f;
            holder.doctor_search_ratingBar.setRating(rate);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DoctorSearchResultViewHolder extends RecyclerView.ViewHolder{

        RatingBar doctor_search_ratingBar ;
        TextView doctor_search_doctor_name, doctor_search_doctor_fees, doctor_search_doctor_speciality ;
        CircleImageView doctor_search_doctor_pic ;

        public DoctorSearchResultViewHolder(View itemView) {
            super(itemView);
            doctor_search_ratingBar = (RatingBar) itemView.findViewById(R.id.search_result_doctor_rating);
            doctor_search_doctor_name = (TextView) itemView.findViewById(R.id.search_result_doctor_name) ;
            doctor_search_doctor_fees = (TextView) itemView.findViewById(R.id.search_result_doctor_fees) ;
            doctor_search_doctor_speciality = (TextView) itemView.findViewById(R.id.search_result_doctor_speciality);
            doctor_search_doctor_pic = (CircleImageView) itemView.findViewById(R.id.search_result_doc_pic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }
}
