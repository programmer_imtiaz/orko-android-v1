package com.arko.nextdot.Adapters;

import android.support.v7.widget.CardView;

/**
 * Created by sakib on 5/17/2017.
 */

public interface CardAdapter {

    int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
