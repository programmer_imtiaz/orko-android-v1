package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsList;
import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nightonke.boommenu.BoomMenuButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sakib on 11/23/2017.
 */

public class UpcomingAppointmentAdapter extends RecyclerView.Adapter<UpcomingAppointmentAdapter.UpcomingAppointmentViewHolder> {

    private final LayoutInflater inflater;
    private Context context;
    private List<UpcomingAppointmentsList> list;

    public UpcomingAppointmentAdapter(Context context, List<UpcomingAppointmentsList> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public UpcomingAppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_upcoming_appointment, parent, false);
        UpcomingAppointmentViewHolder upcomingAppointmentViewHolder = new UpcomingAppointmentViewHolder(view);
        return upcomingAppointmentViewHolder;
    }

    @Override
    public void onBindViewHolder(UpcomingAppointmentViewHolder holder, int position) {
        UpcomingAppointmentsList upcomingAppointmentsList = list.get(position) ;
        String header_name = "", pro_pic_image_URL ;
        if(upcomingAppointmentsList.getDoctorFirstName() != null){
            header_name = upcomingAppointmentsList.getDoctorFirstName() ;
        }
        if(upcomingAppointmentsList.getDoctorLastName() != null){
            header_name = header_name+" "+upcomingAppointmentsList.getDoctorLastName() ;
        }
        else if(upcomingAppointmentsList.getDoctorLastName() == null){
            header_name = header_name+"" ;
        }

        holder.tvServiceName.setText(header_name);
        pro_pic_image_URL = upcomingAppointmentsList.getDoctorPic() ;

        if (Patterns.WEB_URL.matcher(pro_pic_image_URL).matches()) {

            Glide.with(context).load(pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.imageStuffProfile);
        }
        else {

            Glide.with(context).load(Constant.ROOT_URL + pro_pic_image_URL).placeholder(R.drawable.avtaar).dontAnimate().into(holder.imageStuffProfile);
        }
        holder.tvStatus.setText(upcomingAppointmentsList.getAppointmentStatus());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UpcomingAppointmentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_stuff_profile)
        CircularImageView imageStuffProfile;
        @BindView(R.id.pop_up_icon)
        ImageView popUpIcon;
        @BindView(R.id.bmb)
        BoomMenuButton bmb;
        @BindView(R.id.tv_name_title)
        TextView tvNameTitle;
        @BindView(R.id.tv_service_name)
        TextView tvServiceName;
        @BindView(R.id.tv_range)
        TextView tvRange;
        @BindView(R.id.tv_start_time)
        TextView tvStartTime;
        @BindView(R.id.tv_status_title)
        TextView tvStatusTitle;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.card_item_text)
        LinearLayout cardItemText;
        @BindView(R.id.details)
        TextView details;
        @BindView(R.id.mainLayout)
        RelativeLayout mainLayout;
        @BindView(R.id.cv_pending_work)
        CardView cvPendingWork;

        public UpcomingAppointmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
