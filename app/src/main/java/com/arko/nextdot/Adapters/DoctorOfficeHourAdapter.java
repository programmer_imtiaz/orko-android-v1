package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Experience;
import com.arko.nextdot.Model.RetrofitModel.DoctorProfile.Officehour;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 9/12/2017.
 */

public class DoctorOfficeHourAdapter extends RecyclerView.Adapter<DoctorOfficeHourAdapter.DoctorOfficeHourViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<Officehour> list = Collections.emptyList() ;

    public DoctorOfficeHourAdapter(Context context, List<Officehour> list){
        this.list = list ;
        this.context = context ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public DoctorOfficeHourViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_doctor_office_hour, parent, false) ;
        DoctorOfficeHourViewHolder doctorOfficeHourViewHolder = new DoctorOfficeHourViewHolder(view) ;
        return doctorOfficeHourViewHolder ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(DoctorOfficeHourViewHolder holder, int position) {

        Officehour officehour = list.get(position) ;
        holder.doctorOfficeDay.setText(officehour.getDayFrom() + " - " + officehour.getDayTo());
        holder.doctorOfficeTime.setText(officehour.getTimeFrom() + " - " + officehour.getTimeTo());
        //holder.tvCourseAchievement.setText(courses.);
    }

    public class DoctorOfficeHourViewHolder extends RecyclerView.ViewHolder{

        TextView doctorOfficeDay, doctorOfficeTime ;

        public DoctorOfficeHourViewHolder(View itemView) {

            super(itemView);
            doctorOfficeDay = (TextView) itemView.findViewById(R.id.doctorOfficeDay);
            doctorOfficeTime = (TextView) itemView.findViewById(R.id.doctorOfficeTime);
        }
    }
}
