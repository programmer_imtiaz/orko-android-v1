package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityProfile;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by sakib on 10/16/2017.
 */

public class SearchBySpecialityAdapter extends RecyclerView.Adapter<SearchBySpecialityAdapter.SeachBySpecialityViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<SearchBySpecialityProfile> list = Collections.emptyList() ;
    private ClickListener clickListener ;

    public SearchBySpecialityAdapter(Context context, List<SearchBySpecialityProfile> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public SeachBySpecialityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search_by_speciality, parent, false) ;
        SeachBySpecialityViewHolder seachBySpecialityViewHolder = new SeachBySpecialityViewHolder(view) ;
        return seachBySpecialityViewHolder;
    }

    @Override
    public void onBindViewHolder(SeachBySpecialityViewHolder holder, int position) {
        SearchBySpecialityProfile searchBySpecialityProfile = list.get(position) ;
        if(searchBySpecialityProfile.getName() != null){
            holder.speciality.setText(searchBySpecialityProfile.getName());
        }
        if(searchBySpecialityProfile.getName() == null){
            holder.speciality.setText(searchBySpecialityProfile.getName_bn());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SeachBySpecialityViewHolder extends RecyclerView.ViewHolder{

        TextView speciality ;
        public SeachBySpecialityViewHolder(View itemView) {
            super(itemView);
            speciality = (TextView) itemView.findViewById(R.id.search_by_speciality_text) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }


    public void setFilter(List<SearchBySpecialityProfile> searchlist){
        this.list.clear();
        this.list = searchlist ;
        notifyDataSetChanged();
    }

    public void clearList(){
        this.list.clear();
        notifyDataSetChanged();
    }

    public interface ClickListener{

        public void ItemClicked(View view, int position) ;
    }
}
