package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;
import com.arko.nextdot.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Sadikul on 2/12/2017.
 */

public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.Viewholder>{
    ArrayList<MedicalReportsItem> myList;
    Context context;
    public ReportsAdapter(ArrayList<MedicalReportsItem> list, Context ctx) {
        myList=list;
        context=ctx;
    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_medical_reports,parent,false);
        Viewholder viewholder=new Viewholder(view,context,myList);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, int position) {
        MedicalReportsItem item =myList.get(position);
        //Glide.with(context).load(item.getLink()).into(holder.imageView);
        //Glide.with(context).load(item.getLink()).override(100, 100).into(holder.imageView);
        //Glide.with(context).load("http://imagizer.imageshack.us/a/img924/5750/m2tP80.jpg").into(holder.imageView);
        Glide.with(context).load(item.getImageUrl()).override(512, 512).dontAnimate().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView imageView;
        Context ctx;
        ArrayList<MedicalReportsItem> holderList=new ArrayList<>();
        public Viewholder(View itemView, Context ctx, ArrayList<MedicalReportsItem> newList) {
            super(itemView);
            this.ctx=ctx;
            this.holderList=newList;
            imageView= (ImageView) itemView.findViewById(R.id.report_item_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos=getAdapterPosition();
          /*  MedicalReportsItem item=holderList.get(pos);
            Intent intent=new Intent(ctx,FulScreenView.class);
            //intent.putExtra("link",item.getLink());
            intent.putExtra("link",item.getLink());
            ctx.startActivity(intent);*/
        }
    }


}