package com.arko.nextdot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.arko.nextdot.Activities.DoctorActivity;
import com.arko.nextdot.Activities.MedicalHistoryPastActivity;
import com.arko.nextdot.Model.PastApptModel;
import com.arko.nextdot.Model.TherapyModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dipto on 5/7/2017.
 */

public class PastApptAdapter extends RecyclerView.Adapter<PastApptAdapter.PastViewHolder> {

    private LayoutInflater inflater;
    List<PastApptModel> list = Collections.emptyList();
    Context context ;

    public PastApptAdapter(Context context, List<PastApptModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public PastViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_past_appt, viewGroup, false) ;
        PastViewHolder pastViewHolder = new PastViewHolder(view) ;
        return pastViewHolder;
    }

    @Override
    public void onBindViewHolder(PastViewHolder pastViewHolder, int position) {
        PastApptModel pastApptModel = list.get(position) ;
        pastViewHolder.past_appt_title.setText(pastApptModel.getPast_appt_title());
        pastViewHolder.past_appt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MedicalHistoryPastActivity.class) ;
                intent.putExtra("caller", "DoctorActivity");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PastViewHolder extends RecyclerView.ViewHolder{

        TextView past_appt_title ;
        Button past_appt_btn ;

        public PastViewHolder(View itemView) {
            super(itemView);
            past_appt_title = (TextView) itemView.findViewById(R.id.past_appt_serial);
            past_appt_btn = (Button) itemView.findViewById(R.id.btn_past_view);
        }
    }

}
