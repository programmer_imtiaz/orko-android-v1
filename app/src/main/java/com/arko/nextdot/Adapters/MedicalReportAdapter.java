package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.Model.MedicalReportModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;



public class MedicalReportAdapter extends RecyclerView.Adapter<MedicalReportAdapter.MedicalReportViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    private List<MedicalReportModel> list = Collections.emptyList() ;

    private ClickListener clickListener ;

    public MedicalReportAdapter(Context context, List<MedicalReportModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    public void setClicklistner(ClickListener clickListener){
        this.clickListener = clickListener ;
    }

    @Override
    public MedicalReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_medical_report, parent, false) ;
        MedicalReportViewHolder medicalReportViewHolder = new MedicalReportViewHolder(view) ;
        return medicalReportViewHolder;
    }

    @Override
    public void onBindViewHolder(MedicalReportViewHolder holder, int position) {

        MedicalReportModel medicalReportModel = list.get(position) ;
        holder.report_name.setText(medicalReportModel.getReport_name());
        holder.lab_name.setText(medicalReportModel.getLab_name());
        holder.report_icon.setImageResource(medicalReportModel.getReport_icon());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MedicalReportViewHolder extends RecyclerView.ViewHolder{

        TextView lab_name, report_name, delivary_time ;
        ImageView report_icon ;

        public MedicalReportViewHolder(View itemView) {
            super(itemView);
            lab_name = (TextView) itemView.findViewById(R.id.lab_name) ;
            report_name = (TextView) itemView.findViewById(R.id.medical_report_name) ;
            report_icon = (ImageView) itemView.findViewById(R.id.circle_medical_report_icon) ;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(clickListener != null){
                        clickListener.ItemClicked(v, getPosition());
                    }
                }
            });
        }
    }

    public interface ClickListener{
        public void ItemClicked(View view, int position) ;
    }
}
