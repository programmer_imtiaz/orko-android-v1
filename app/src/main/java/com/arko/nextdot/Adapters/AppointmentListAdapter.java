package com.arko.nextdot.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arko.nextdot.Activities.AppointmentDetailsActivity;
import com.arko.nextdot.Activities.Appointments;
import com.arko.nextdot.Activities.DoctorProfileActivity;
import com.arko.nextdot.Fragment.FragmentSetAppointment;
import com.arko.nextdot.Model.AppointmentListModelClass;

import com.arko.nextdot.Network.Constant;
import com.arko.nextdot.R;
import com.arko.nextdot.Utils.Constants;
import com.arko.nextdot.Utils.PreferenceManager;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arabi on 08/09/2017.
 */

public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.ShowsViewHolder> {

    public Context context;
    private String startHoursInString = "", startMinutesInString = "", endHoursInString = "", endMinutesInString = "", startTime = "", endTime = "";
    private List<AppointmentListModelClass> pendingJobItems;
    private String changeMessage = "";
    private SimpleDateFormat dateFormatForDay = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
    private PreferenceManager preferenceManager = null;
    private Boolean isPast;
    private SharedPreferences mSharedPreferences;
    private AppointmentListModelClass stuffsPendingJobs, newListItem;
    private String user_token = "";
    private String doctor_id="";
    private String patient_id = "";
    private String doctor_name="";
    private String doctor_speciality="";
    private Double doctor_rating=0.0;
    private String doctor_gender="";
    private String doctor_email="";
    private String doctor_contact="";
    private String appointment_reason="";
    private String appointment_id="";
    private String appointment_date="";
    private String appointment_time="";
    private String appointment_status="";
    private String appointment_status_color="#000000";
    private String appointment_time_title="";
    private String appointment_creator="";
    private String doctorAvatar;
    private String appointmentLocation;
    private String appointmentTimeForAPI;

    private int adapterPosition;

    private FragmentSetAppointment setAppointmentDialog;
    private android.support.v4.app.Fragment fragment;


    private static int imageResourceIndex = 0;
    private static int colorResourceIndex = 0;
    private static int stringResourceIndex = 0;

    private static int imageResourceIndex1 = 0;
    private static int colorResourceIndex1 = 0;
    private static int stringResourceIndex1 = 0;

    //    private FragmentSetAppointment setAppointmentDialog;
//    private FragmentChangeAppointmentNew changeAppointmentDialog;
//    private FragmentRejectAppointment rejectAppointmentDialog;
    private android.support.v4.app.FragmentManager manager;

    static int getImageResource() {
        if (imageResourceIndex >= imageResources.length) imageResourceIndex = 0;
        return imageResources[imageResourceIndex++];
    }


    static int getColorResource() {
        if (colorResourceIndex >= colorResources.length) colorResourceIndex = 0;
        return colorResources[colorResourceIndex++];
    }


    static int getStringResource() {
        if (stringResourceIndex >= stringResources.length) stringResourceIndex = 0;
        return stringResources[stringResourceIndex++];
    }

    private static int[] imageResources = new int[]{
            R.drawable.ic_approve,
            R.drawable.ic_reject
    };

    private static int[] colorResources = new int[]{
            Color.parseColor("#006400"),
            Color.parseColor("#FC464A")
    };

    private static int[] stringResources = new int[]{
            R.string.accept,
            R.string.reject
    };


    static int getImageResource1() {
        if (imageResourceIndex1 >= imageResources1.length) imageResourceIndex1 = 0;
        return imageResources1[imageResourceIndex1++];
    }


    static int getColorResource1() {
        if (colorResourceIndex1 >= colorResources1.length) colorResourceIndex1 = 0;
        return colorResources1[colorResourceIndex1++];
    }


    static int getStringResource1() {
        if (stringResourceIndex1 >= stringResources1.length) stringResourceIndex1 = 0;
        return stringResources1[stringResourceIndex1++];
    }

    private static int[] imageResources1 = new int[]{
            R.drawable.ic_change,
            R.drawable.ic_reject
    };

    private static int[] colorResources1 = new int[]{
            Color.parseColor("#2BB8E8"),
            Color.parseColor("#FC464A")
    };

    private static int[] stringResources1 = new int[]{
            R.string.change,
            R.string.reject
    };

    public AppointmentListAdapter(Context context, List<AppointmentListModelClass> pendingJobItems) {
        this.context = context;
        this.pendingJobItems = pendingJobItems;
    }

    @Override
    public ShowsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.appointment_list_item, parent, false);
        return new ShowsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ShowsViewHolder holder, int position) {

        preferenceManager = PreferenceManager.getInstance(context);

        mSharedPreferences = context.getSharedPreferences(Constants.APPLICATION_ID, Context.MODE_PRIVATE);
        isPast = mSharedPreferences.getBoolean("isPast", false);

        Log.d("pastValue", isPast + "");

        stuffsPendingJobs = pendingJobItems.get(holder.getAdapterPosition());

        user_token = stuffsPendingJobs.getUserToken();
        patient_id = stuffsPendingJobs.getPatientID();
        doctor_id = stuffsPendingJobs.getDoctorID();
        doctor_name = stuffsPendingJobs.getDoctorFirstName() + " " + stuffsPendingJobs.getDoctorLastName();
        appointment_reason = stuffsPendingJobs.getReason();
        doctor_speciality = stuffsPendingJobs.getDoctorSpeciality();
        doctor_rating = stuffsPendingJobs.getDoctorRating();
        doctor_gender = stuffsPendingJobs.getDoctorGender();
        doctor_email = stuffsPendingJobs.getDoctorEmail();
        doctor_contact = stuffsPendingJobs.getDoctorContact();
        appointment_id = stuffsPendingJobs.getAppointmentID();
        appointment_date = stuffsPendingJobs.getDay();
        doctorAvatar = stuffsPendingJobs.getDoctorAvatar();
        appointmentLocation = stuffsPendingJobs.getAppointmentLocation();


        holder.tvServiceName.setText(doctor_name);
        holder.tvReasonName.setText(appointment_reason);

        if (stuffsPendingJobs.getStartTime() != null) {
            appointmentTimeForAPI = stuffsPendingJobs.getStartTime()+":00";
            Log.d("appointmentTimeForAPI", "appointmentTimeForAPI: " + appointmentTimeForAPI);
            startHoursInString = stuffsPendingJobs.getStartTime().substring(0, stuffsPendingJobs.getStartTime().indexOf(':'));
            startMinutesInString = stuffsPendingJobs.getStartTime().substring(stuffsPendingJobs.getStartTime().indexOf(':') + 1, stuffsPendingJobs.getStartTime().length());

            if (Integer.parseInt(startHoursInString) > 12) {
                startHoursInString = String.valueOf(Integer.parseInt(startHoursInString) - 12) + ":";
                startMinutesInString = startMinutesInString + " PM";
            } else {
                startHoursInString = startHoursInString + ":";
                startMinutesInString = startMinutesInString + " AM";
            }
        }

        if (stuffsPendingJobs.getEndTime() != null) {
            endHoursInString = stuffsPendingJobs.getEndTime().substring(0, stuffsPendingJobs.getEndTime().indexOf(':'));
            endMinutesInString = stuffsPendingJobs.getEndTime().substring(stuffsPendingJobs.getEndTime().indexOf(':') + 1, stuffsPendingJobs.getEndTime().length());


            if (Integer.parseInt(endHoursInString) > 12) {
                endHoursInString = String.valueOf(Integer.parseInt(endHoursInString) - 12) + ":";
                endMinutesInString = endMinutesInString + " PM";
            } else {
                endHoursInString = endHoursInString + ":";
                endMinutesInString = endMinutesInString + " AM";
            }
        }


        startTime = startHoursInString + startMinutesInString;
        endTime = endHoursInString + endMinutesInString;

        holder.tvStartTime.setText(startTime);
        holder.tvEndTime.setText(endTime);

        if (!isPast) {
            assert holder.bmb != null;
            holder.bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_2_1);
            holder.bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_2_1);
            holder.bmb.setInFragment(true);
            holder.bmb.setInList(true);
            holder.bmb.clearBuilders();
            for (int i = 0; i < holder.bmb.getPiecePlaceEnum().pieceNumber(); i++) {

                TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                        .normalImageRes(getImageResource())
                        .normalTextRes(getStringResource())
                        .normalColor(getColorResource())
                        .rippleEffect(true)
                        .imagePadding(new Rect(5, 5, 5, 5))
                        .textPadding(new Rect(5, 5, 5, 5))
                        .typeface(Typeface.DEFAULT_BOLD)
                        .textSize(10)
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                // When the boom-button corresponding this builder is clicked.

                                int start_time_position = holder.getAdapterPosition();
                                adapterPosition = start_time_position;
                                newListItem = pendingJobItems.get(start_time_position);

                                preferenceManager.setAppointmentID(newListItem.getAppointmentID());

                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                editor.putString("appointmentID", newListItem.getAppointmentID());
                                editor.apply();
                                editor.commit();

                                if (index == 0) {

                                    new setAppointmentAPI(newListItem.getPatientID(), newListItem.getUserToken(), newListItem.getStartTime()+":00", newListItem.getAppointmentID()).execute();

//                                    populateSpinner(newListItem.getStartTime(), newListItem.getEndTime(), "15");

//                                    setAppointmentDialog = new FragmentSetAppointment();
//                                    manager = fragment.getFragmentManager();
//                                    setAppointmentDialog.setTargetFragment(fragment, Constants.FragmentSetAppointmentKey);
//                                    setAppointmentDialog.show(manager, "fragment_edit_name");
//
//                                    Log.d("kothay", "Accept clicked, Start Time: " + newListItem.getStartTime() + " End Time: " + newListItem.getEndTime() + " appointment ID: " + newListItem.getAppointmentID() + " adapterPosition: " + holder.getAdapterPosition());

                                } else if (index == 1) {

                                    new rejectAppointmentAPI(newListItem.getPatientID(), newListItem.getUserToken(), newListItem.getStartTime()+":00", newListItem.getAppointmentID()).execute();

//                                    rejectAppointmentDialog = new FragmentRejectAppointment();
//                                    manager = fragment.getFragmentManager();
//                                    rejectAppointmentDialog.setTargetFragment(fragment, Constants.FragmentRejectAppointmentKey);
//                                    rejectAppointmentDialog.show(manager, "fragment_edit_name");

//                                    Log.d("konLoop", "Reject clicked");
                                }
                            }
                        });

                holder.bmb.addBuilder(builder);
            }


        } else {

            holder.bmb.setVisibility(View.GONE);

        }


//        holder.pop_up_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PopupMenu popup = new PopupMenu(context, holder.pop_up_icon);
//                //inflating menu from xml resource
//                popup.inflate(R.menu.pop_up_menu);
//                //adding click listener
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        switch (item.getItemId()) {
//                            case R.id.changeAppointment:
//                                //handle menuItem1 click
//                                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                                alertDialog.setTitle("Change Appointment Date");
//                                alertDialog.setIcon(R.mipmap.ic_launcher);
//                                alertDialog.setMessage("The patient will be asked to schedule another appointment");
//                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                                alertDialog.show();
//                                break;
//                            case R.id.cancelAppointment:
//                                //handle menuItem2 click
//                                AlertDialog alertDialog1 = new AlertDialog.Builder(context).create();
//                                alertDialog1.setTitle("Cancel Appointment");
//                                alertDialog1.setIcon(R.mipmap.ic_launcher);
//                                alertDialog1.setMessage("The patient will be notified about your cancellation of the appointment");
//                                alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
//                                        new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                                alertDialog1.show();
//                                break;
//                        }
//                        return false;
//                    }
//                });
//                //displaying the popup
//                popup.show();
//            }
//        });


        if (stuffsPendingJobs.getStatus().equals("approved")) {
            appointment_status = "APPROVED";
            appointment_status_color = "#006400";
            appointment_time_title = "Appointment Time: ";
            stuffsPendingJobs.setAppointmentTimeTitle(appointment_time_title);
            appointment_time = startTime;
            stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
            stuffsPendingJobs.setStatusColor(appointment_status_color);
            holder.tvStatus.setText(appointment_status);
            holder.tvStatus.setTextColor(Color.parseColor(appointment_status_color));
            holder.tvTimeRangeTitle.setText(appointment_time_title);
            holder.tvTimeSeperator.setVisibility(View.GONE);
            holder.tvEndTime.setVisibility(View.GONE);
            holder.bmb.setVisibility(View.GONE);

            Log.d("time", "appointment time: " + appointment_time);

        } else if (stuffsPendingJobs.getStatus().equals("pending")) {
            appointment_status = "PENDING";
            appointment_status_color = "#2BB8E8";
            stuffsPendingJobs.setStatusColor(appointment_status_color);
            appointment_time_title = "Requested Time: ";
            stuffsPendingJobs.setAppointmentTimeTitle(appointment_time_title);
            holder.tvStatus.setText(appointment_status);
            holder.tvStatus.setTextColor(Color.parseColor(appointment_status_color));
            holder.tvTimeRangeTitle.setText(appointment_time_title);

            if (stuffsPendingJobs.getCreatorID().equals(stuffsPendingJobs.getDoctorID())) {
                appointment_creator = "doctor";
                appointment_time = startTime;
                stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
                holder.bmb.setVisibility(View.VISIBLE);
                holder.tvTimeSeperator.setVisibility(View.GONE);
                holder.tvEndTime.setVisibility(View.GONE);
            } else {
                appointment_creator = "patient";
                appointment_time = startTime + " - " + endTime;
                stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
                holder.bmb.setVisibility(View.GONE);
                holder.tvTimeSeperator.setVisibility(View.VISIBLE);
                holder.tvEndTime.setVisibility(View.VISIBLE);
            }

            Log.d("time", "appointment time: " + appointment_time);

        } else if (stuffsPendingJobs.getStatus().equals("changed")) {
            appointment_status = "CHANGED";
            appointment_status_color = "#2BB8E8";
            stuffsPendingJobs.setStatusColor(appointment_status_color);
            appointment_time_title = "Re-scheduled Time: ";
            stuffsPendingJobs.setAppointmentTimeTitle(appointment_time_title);
            appointment_time = startTime;
            stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
            holder.tvStatus.setText(appointment_status);
            holder.tvStatus.setTextColor(Color.parseColor(appointment_status_color));
            holder.tvTimeRangeTitle.setText(appointment_time_title);
            holder.tvTimeSeperator.setVisibility(View.GONE);
            holder.tvEndTime.setVisibility(View.GONE);
            holder.bmb.setVisibility(View.VISIBLE);

            Log.d("time", "appointment time: " + appointment_time);

        } else if (stuffsPendingJobs.getStatus().equals("rejected")) {
            appointment_status = "REJECTED";
            appointment_status_color = "#FC464A";
            stuffsPendingJobs.setStatusColor(appointment_status_color);
            appointment_time_title = "Requested Time: ";
            stuffsPendingJobs.setAppointmentTimeTitle(appointment_time_title);
            holder.tvStatus.setText(appointment_status);
            holder.tvStatus.setTextColor(Color.parseColor(appointment_status_color));
            holder.tvTimeRangeTitle.setText(appointment_time_title);

            if (stuffsPendingJobs.getRejectorID().equals(stuffsPendingJobs.getPatientID())) {
                appointment_time = startTime;
                stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
                holder.bmb.setVisibility(View.GONE);
                holder.tvTimeSeperator.setVisibility(View.GONE);
                holder.tvEndTime.setVisibility(View.GONE);
            } else if(stuffsPendingJobs.getRejectorID().equals(stuffsPendingJobs.getDoctorID())){
                appointment_time = startTime + " - " + endTime;
                stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
                holder.bmb.setVisibility(View.GONE);
                holder.tvTimeSeperator.setVisibility(View.VISIBLE);
                holder.tvEndTime.setVisibility(View.VISIBLE);
            }

        } else if (stuffsPendingJobs.getStatus().equals("completed")) {
            appointment_status = "COMPLETED";
            appointment_status_color = "#006400";
            appointment_time_title = "Appointment Time: ";
            stuffsPendingJobs.setAppointmentTimeTitle(appointment_time_title);
            appointment_time = startTime;
            stuffsPendingJobs.setAppointmentTimeForView(appointment_time);
            stuffsPendingJobs.setStatusColor(appointment_status_color);
            holder.tvStatus.setText(appointment_status);
            holder.tvStatus.setTextColor(Color.parseColor(appointment_status_color));
            holder.tvTimeRangeTitle.setText(appointment_time_title);
            holder.tvTimeSeperator.setVisibility(View.GONE);
            holder.tvEndTime.setVisibility(View.GONE);
            holder.bmb.setVisibility(View.GONE);

            Log.d("time", "appointment time: " + appointment_time);

        }


        Glide.with(context).load(Constant.ROOT_URL + doctorAvatar).dontAnimate().into(holder.imageStuffProfile);

    }



    private class setAppointmentAPI extends AsyncTask<String, String, String> {
        String userId, userToken, startTime, appointmentID;


        private setAppointmentAPI(String userId, String userToken, String startTime, String appointmentID) {
            this.userId = userId;
            this.userToken = userToken;
            this.startTime = startTime;
            this.appointmentID = appointmentID;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("error", "statusReport: onPreExecute e dhukse!!!");

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************//


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentApproved");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            nameValuePairs.add(new BasicNameValuePair("approved_by", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("status", "approved"));
            nameValuePairs.add(new BasicNameValuePair("appointment_id", appointmentID));
            nameValuePairs.add(new BasicNameValuePair("appointment_start_time", startTime));


            Log.d("getAppointmentID", appointmentID + " " + startTime + " " + userId + " " + userToken);


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(context, "New appointment set", Toast.LENGTH_LONG).show();

            Intent testIntent2 = new Intent(context, Appointments.class);
            context.startActivity(testIntent2);
            ((Activity)context).finish();

        }
    }


    private class rejectAppointmentAPI extends AsyncTask<String, String, String> {
        String userId, userToken, startTime, appointmentID;


        private rejectAppointmentAPI(String userId, String userToken, String startTime, String appointmentID) {
            this.userId = userId;
            this.userToken = userToken;
            this.startTime = startTime;
            this.appointmentID = appointmentID;
        }


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            Log.d("error", "statusReport: onPreExecute e dhukse!!!");

        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("error", "statusReport: doInBackground e dhukse!!!");

            String resultToDisplay = "";
//*****************************************************************//


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.ROOT_URL + "api/appointmentReject");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
            nameValuePairs.add(new BasicNameValuePair("rejected_by", userId));
            nameValuePairs.add(new BasicNameValuePair("token", userToken));
            nameValuePairs.add(new BasicNameValuePair("status", "rejected"));
            nameValuePairs.add(new BasicNameValuePair("appointment_id", appointmentID));


            Log.d("getAppointmentID", appointmentID + " " + startTime + " " + userId + " " + userToken);


//// Execute HTTP Post Request
            HttpResponse response = null;
            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                response = httpclient.execute(httppost);
                resultToDisplay = EntityUtils.toString(response.getEntity());

                Log.v("Util response", resultToDisplay);
            } catch (IOException e) {
                e.printStackTrace();
            }


            //**************************************************************
            return resultToDisplay;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(context, "Appointment request rejected", Toast.LENGTH_LONG).show();

            Intent testIntent2 = new Intent(context, Appointments.class);
            context.startActivity(testIntent2);
            ((Activity)context).finish();

        }
    }


    ArrayList<String> populateSpinner(String startingTime, String endingTime, String duration) {

        if (startingTime == null || endingTime == null || duration == null)
            return null;

        int start_time, end_time, counter = 0;
        String sTime, eTime;
        String ampm;
        String appointmentId;
        ArrayList<String> arrayList = new ArrayList<>();
        String[] array = new String[1000];

        start_time = Integer.parseInt(startingTime.substring(0, 2) + startingTime.substring(3, 5));
        end_time = Integer.parseInt(endingTime.substring(0, 2) + endingTime.substring(3, 5));
        int d = Integer.parseInt(duration);
        Log.e("start_end", start_time + " " + end_time);

       /* while ((start_time + Integer.parseInt(duration)) < end_time) {*/
        while (start_time <= end_time - d) {


            if ((start_time % 100) >= 60) {
                start_time = start_time + 40;
            }


            if (start_time / 100 > 12) {

                ampm = " PM";

                if (start_time % 100 > 9) {
                    sTime = String.valueOf((start_time / 100) - 12) + ":" + String.valueOf(start_time % 100) + ampm;
                } else {
                    sTime = String.valueOf((start_time / 100) - 12) + ":" + "0" + String.valueOf(start_time % 100) + ampm;
                }

            } else if (start_time / 100 == 12) {

                ampm = " PM";

                if (start_time % 100 > 9) {
                    sTime = String.valueOf(start_time / 100) + ":" + String.valueOf(start_time % 100) + ampm;
                } else {
                    sTime = String.valueOf(start_time / 100) + ":" + "0" + String.valueOf(start_time % 100) + ampm;
                }

            } else {

                ampm = " AM";


                if (start_time % 100 > 9) {
                    sTime = String.valueOf(start_time / 100) + ":" + String.valueOf(start_time % 100) + ampm;
                } else {
                    sTime = String.valueOf(start_time / 100) + ":" + "0" + String.valueOf(start_time % 100) + ampm;
                }

            }

            arrayList.add(sTime);
            //array[counter] = sTime;

            Log.d("startTime", "startTime: " + start_time + ", endTime: " + end_time + ", array " + counter + ": " + sTime);

            start_time = start_time + d;
            counter++;
        }


        for (int l = 0; l < arrayList.size(); l++) {
            //if(array[l]!=null)
            Log.d("SetArrayT", arrayList.get(l) + "");
        }


        preferenceManager.setAppointmentTimeRangeArray(arrayList);

        return arrayList;
    }


    @Override
    public int getItemCount() {
        return pendingJobItems.size();
    }

    public class ShowsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.mainLayout)
        RelativeLayout mainLayout;
        @BindView(R.id.image_stuff_profile)
        CircularImageView imageStuffProfile;
        @BindView(R.id.tv_service_name)
        TextView tvServiceName;
        @BindView(R.id.tv_reason_name)
        TextView tvReasonName;
        @BindView(R.id.tv_range)
        TextView tvTimeRangeTitle;
        @BindView(R.id.tv_start_time)
        TextView tvStartTime;
        @BindView(R.id.to)
        TextView tvTimeSeperator;
        @BindView(R.id.tv_end_time)
        TextView tvEndTime;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.pop_up_icon)
        ImageView pop_up_icon;
        @BindView(R.id.bmb)
        BoomMenuButton bmb;
        @BindView(R.id.details)
        TextView details;

        public ShowsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            imageStuffProfile.setOnClickListener(this);
            tvServiceName.setOnClickListener(this);
            details.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            int pos = getAdapterPosition();

            String doctor_id = pendingJobItems.get(pos).getDoctorID();
            String user_token = pendingJobItems.get(pos).getUserToken();
            String patient_id = pendingJobItems.get(pos).getPatientID();
            String doctor_name = pendingJobItems.get(pos).getDoctorFirstName() + " " + pendingJobItems.get(pos).getDoctorLastName();
            String appointment_reason = pendingJobItems.get(pos).getReason();
            String doctor_speciality = pendingJobItems.get(pos).getDoctorSpeciality();
            Double doctor_rating = pendingJobItems.get(pos).getDoctorRating();
            String doctor_gender = pendingJobItems.get(pos).getDoctorGender();
            String doctor_email = pendingJobItems.get(pos).getDoctorEmail();
            String doctor_contact = pendingJobItems.get(pos).getDoctorContact();
            String appointment_id = pendingJobItems.get(pos).getAppointmentID();
            String appointment_date = pendingJobItems.get(pos).getDay();
            String doctorAvatar = pendingJobItems.get(pos).getDoctorAvatar();
            String appointmentLocation = pendingJobItems.get(pos).getAppointmentLocation();
            String appointmentTimeForView = pendingJobItems.get(pos).getAppointmentTimeForView();
            String appointmentTimeForAPI = pendingJobItems.get(pos).getStartTime()+":00";

            String appointment_status = "";
            String appointment_status_color = "";
            if(pendingJobItems.get(pos).getStatus().equals("approved")){
                appointment_status = "APPROVED";
                appointment_status_color = "#006400";
            } else if(pendingJobItems.get(pos).getStatus().equals("pending")){
                appointment_status = "PENDING";
                appointment_status_color = "#2BB8E8";
            } else if(pendingJobItems.get(pos).getStatus().equals("changed")){
                appointment_status = "CHANGED";
                appointment_status_color = "#2BB8E8";
            } else if(pendingJobItems.get(pos).getStatus().equals("rejected")){
                appointment_status = "REJECTED";
                appointment_status_color = "#FC464A";
            } else if(pendingJobItems.get(pos).getStatus().equals("completed")){
                appointment_status = "COMPLETED";
                appointment_status_color = "#006400";
            }

            String appointmentTimeTitle = pendingJobItems.get(pos).getAppointmentTimeTitle();
            String appointment_creator = pendingJobItems.get(pos).getCreatorID();

            switch (v.getId()) {
                case R.id.image_stuff_profile:
                    Intent testIntent = new Intent(context, DoctorProfileActivity.class);
                    testIntent.putExtra("id", doctor_id) ;
                    context.startActivity(testIntent);
                    break;
                case R.id.tv_service_name:
                    Intent testIntent1 = new Intent(context, DoctorProfileActivity.class);
                    testIntent1.putExtra("id", doctor_id) ;
                    context.startActivity(testIntent1);
                    break;
                case R.id.details:
                    Intent testIntent2 = new Intent(context, AppointmentDetailsActivity.class);
                    testIntent2.putExtra("user_token", user_token);
                    testIntent2.putExtra("patient_id", patient_id);
                    testIntent2.putExtra("appointment_id", appointment_id);
                    testIntent2.putExtra("appointment_reason", appointment_reason);
                    testIntent2.putExtra("appointment_date", appointment_date);
                    testIntent2.putExtra("appointment_time_for_API", appointmentTimeForAPI);
                    testIntent2.putExtra("doctor_id", doctor_id);
                    testIntent2.putExtra("doctor_name", doctor_name);
                    testIntent2.putExtra("doctor_speciality", doctor_speciality);
                    testIntent2.putExtra("doctor_avatar", doctorAvatar);
                    testIntent2.putExtra("doctor_rating", doctor_rating);
                    testIntent2.putExtra("doctor_gender", doctor_gender);
                    testIntent2.putExtra("doctor_email", doctor_email);
                    testIntent2.putExtra("doctor_contact", doctor_contact);
                    testIntent2.putExtra("appointment_status", appointment_status);
                    testIntent2.putExtra("appointment_status_color", appointment_status_color);
                    testIntent2.putExtra("appointment_time", appointmentTimeForView);
                    testIntent2.putExtra("appointment_time_title", appointmentTimeTitle);
                    testIntent2.putExtra("appointment_creator", appointment_creator);
                    testIntent2.putExtra("appointment_location", appointmentLocation);
                    testIntent2.putExtra("is_past", isPast);
                    context.startActivity(testIntent2);
                    ((Activity)context).finish();
                    break;
            }
        }
    }

}
