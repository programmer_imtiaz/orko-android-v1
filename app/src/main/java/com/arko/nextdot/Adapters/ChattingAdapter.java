package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arko.nextdot.Model.ChattingModel;
import com.arko.nextdot.R;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dipto on 7/4/2017.
 */

public class ChattingAdapter extends RecyclerView.Adapter<ChattingAdapter.ChattingViewHolder> {

    private final LayoutInflater inflater;
    private Context context ;
    List<ChattingModel> list = Collections.emptyList() ;

    public ChattingAdapter(Context context, List<ChattingModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public ChattingViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        View view ;
        ChattingViewHolder chattingViewHolder = null;
        //Log.d("FLAG :::::::", chattingModel.getFlag()) ;

        if(viewtype == 1){
            //Log.d("TAG :::::::", "sender") ;
            view = inflater.inflate(R.layout.item_sender_chatting, viewGroup, false) ;
            chattingViewHolder = new ChattingViewHolder(view) ;
            return chattingViewHolder ;
        }
        else if(viewtype == 0){
            //Log.d("TAG :::::::", "reciver") ;
            view = inflater.inflate(R.layout.item_reciver_chatting, viewGroup, false) ;
            chattingViewHolder = new ChattingViewHolder(view) ;
            return chattingViewHolder ;
        }

        return null ;
    }

    @Override
    public void onBindViewHolder(ChattingViewHolder chattingViewHolder, int i) {
        ChattingModel chattingModel = list.get(i) ;

//        if(chattingModel.getFlag() == "0"){
//            chattingViewHolder.message.setText(chattingModel.getMessage());
//        }
//        else if(chattingModel.getFlag() == "1"){
//
//            //chattingViewHolder.sender_pic.setImageResource(chattingModel.getPro_pic());
//        }

        chattingViewHolder.message.setText(chattingModel.getMessage());
    }

    @Override
    public int getItemViewType(int position) {
        if(list != null){
            ChattingModel ch = list.get(position) ;
            if(ch != null){
                return ch.getFlag() ;
            }
        }
        return 0 ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ChattingViewHolder extends  RecyclerView.ViewHolder{

        TextView message, reciver_message ;
        ImageView sender_pic ;

        public ChattingViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.message);
            sender_pic = (ImageView) itemView.findViewById(R.id.circle_sender_pic) ;
        }
    }
}
