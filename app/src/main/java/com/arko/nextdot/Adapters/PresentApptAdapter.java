package com.arko.nextdot.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arko.nextdot.Model.PresentApptModel;
import com.arko.nextdot.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dipto on 5/8/2017.
 */

public class PresentApptAdapter extends RecyclerView.Adapter<PresentApptAdapter.PresentViewHolder> {


    private LayoutInflater inflater;
    List<PresentApptModel> list = Collections.emptyList();
    Context context ;

    public PresentApptAdapter(Context context, List<PresentApptModel> list){
        this.context = context ;
        this.list = list ;
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public PresentViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = inflater.inflate(R.layout.item_present_appt, viewGroup, false) ;
        PresentViewHolder presentViewHolder = new PresentViewHolder(view) ;
        return presentViewHolder;
    }

    @Override
    public void onBindViewHolder(PresentViewHolder presentViewHolder, int position) {
        PresentApptModel presentApptModel = list.get(position) ;
        presentViewHolder.present_appt_title.setText(presentApptModel.getPresent_appt_title());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PresentViewHolder extends RecyclerView.ViewHolder{

        TextView present_appt_title ;
        public PresentViewHolder(View itemView) {
            super(itemView);
            present_appt_title = (TextView) itemView.findViewById(R.id.present_appt_serial);
        }
    }
}
