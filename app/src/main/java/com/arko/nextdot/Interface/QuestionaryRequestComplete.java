package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;

/**
 * Created by sakib on 10/29/2017.
 */

public interface QuestionaryRequestComplete {

    public void questionaryOnRequestComplete(QuestionaryRoot questionaryRoot) ;

    public void questionaryOnRequestError(String msg) ;
}
