package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.Questionary.QuestionaryRoot;

/**
 * Created by sakib on 10/29/2017.
 */

public interface QuestionaryView {

    public void showQuestionarySubmitResult(QuestionaryRoot questionaryRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
