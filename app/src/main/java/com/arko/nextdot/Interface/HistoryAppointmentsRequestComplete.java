package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;

/**
 * Created by sakib on 11/26/2017.
 */

public interface HistoryAppointmentsRequestComplete {

    public void onRequestComplete(HistoryAppointmentsRoot historyAppointmentsRoot) ;

    public void onRequestError(String msg) ;
}
