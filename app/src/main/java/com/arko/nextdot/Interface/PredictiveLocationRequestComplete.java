package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;

/**
 * Created by sakib on 11/22/2017.
 */

public interface PredictiveLocationRequestComplete {

    public void predictiveLocationRequestComplete(PredictiveLocationRoot predictiveLocationRoot) ;

    public void predictiveLocationRequestError(String msg) ;
}
