package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;

/**
 * Created by sakib on 11/20/2017.
 */

public interface SearchDoctorResultRequestComplete {

    public void searchDoctorResultRequestComplete(SearchDoctorResultRoot searchDoctorResultRoot) ;

    public void searchDoctorResultRequestError(String msg) ;
}
