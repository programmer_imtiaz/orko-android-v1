package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;

/**
 * Created by sakib on 10/17/2017.
 */

public interface SearchBySpecialityView {

    public void showSearchResult(SearchBySpecialityRoot searchBySpecialityRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
