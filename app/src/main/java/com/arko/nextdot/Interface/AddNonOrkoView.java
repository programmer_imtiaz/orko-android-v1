package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;

/**
 * Created by sakib on 10/30/2017.
 */

public interface AddNonOrkoView {

    public void showAddNonOrkoSuccessMsg(AddNonOrkoDoctorSuccessMessage addNonOrkoDoctorSuccessMessage) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
