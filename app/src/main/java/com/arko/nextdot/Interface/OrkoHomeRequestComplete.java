package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;

/**
 * Created by sakib on 11/15/2017.
 */

public interface OrkoHomeRequestComplete {

    public void onRequestComplete(PredictiveSearchRoot predictiveSearchRoot) ;

    public void onRequestError(String msg) ;

}
