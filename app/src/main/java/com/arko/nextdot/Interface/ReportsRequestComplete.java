package com.arko.nextdot.Interface;


import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;

import java.util.List;

/**
 * Created by ASUS on 01-Nov-17.
 */

public interface ReportsRequestComplete {
    void onSuccess(List<MedicalReportsItem> medicalReportsItems);
    void onError(String message);
}
