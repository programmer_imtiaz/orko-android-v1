package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.SearchBySpeciality.SearchBySpecialityRoot;

/**
 * Created by sakib on 10/17/2017.
 */

public interface SearchBySpecialityRequestComplete {

    public void searchBySpecialityRequestComplete(SearchBySpecialityRoot searchBySpecialityRoot) ;

    public void searchBySpecialityRequestError(String msg) ;
}
