package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.PredictiveLocation.PredictiveLocationRoot;

/**
 * Created by sakib on 11/22/2017.
 */

public interface PredictiveLocationFragmentView {

    public void showSearchResult(PredictiveLocationRoot predictiveLocationRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;

}
