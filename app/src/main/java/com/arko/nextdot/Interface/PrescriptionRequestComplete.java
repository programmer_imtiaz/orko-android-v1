package com.arko.nextdot.Interface;


import com.arko.nextdot.Model.RetrofitModel.PrescriptionItem;

import java.util.List;

/**
 * Created by ASUS on 01-Nov-17.
 */

public interface PrescriptionRequestComplete {
    void onSuccess(List<PrescriptionItem> prescriptionItems);
    void onError(String message);
}
