package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.MedicalReportsItem;

import java.util.List;

/**
 * Created by ASUS on 01-Nov-17.
 */

public interface ReportsView {
    public void passData(List<MedicalReportsItem> medicalReportsItems);
    public void onError(String msg);
    public void startLoading();
    public void stopLoading();
    public Context getAppContext();
}
