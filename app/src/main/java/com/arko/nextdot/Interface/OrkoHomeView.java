package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.PredictiveSearchRoot;

/**
 * Created by sakib on 11/15/2017.
 */

public interface OrkoHomeView {

    public void showSearchResult(PredictiveSearchRoot predictiveSearchRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
