package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameRoot;

public interface SearchByNameRequestComplete {

    public void onRequestComplete(SearchByNameRoot searchByNameRoot) ;

    public void onRequestError(String msg) ;
}
