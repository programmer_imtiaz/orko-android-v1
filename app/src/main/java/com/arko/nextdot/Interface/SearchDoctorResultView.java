package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.SearchDoctorResult.SearchDoctorResultRoot;

/**
 * Created by sakib on 11/20/2017.
 */

public interface SearchDoctorResultView {

    public void showSearchDoctorResult(SearchDoctorResultRoot searchDoctorResultRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
