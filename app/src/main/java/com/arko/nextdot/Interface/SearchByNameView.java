package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.PredictiveSearchResult.SearchByNameRoot;

/**
 * Created by sakib on 10/16/2017.
 */

public interface SearchByNameView {

    public void showSearchResult(SearchByNameRoot searchByNameRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}