package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;

/**
 * Created by sakib on 11/23/2017.
 */

public interface UpcomingAppointmentView {

    public void showUpcomingAppointmentResult(UpcomingAppointmentsRoot upcomingAppointmentsRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
