package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.AddNonOrkoDoctor.AddNonOrkoDoctorSuccessMessage;

/**
 * Created by sakib on 10/30/2017.
 */

public interface AddNonOrkoDoctorRequestComplete {

    public void onRequestComplete(AddNonOrkoDoctorSuccessMessage addNonOrkoDoctorSuccessMessage) ;

    public void onRequestError(String msg) ;
}
