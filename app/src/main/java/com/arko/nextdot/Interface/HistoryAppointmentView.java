package com.arko.nextdot.Interface;

import android.content.Context;

import com.arko.nextdot.Model.RetrofitModel.HistoryApppointments.HistoryAppointmentsRoot;

/**
 * Created by sakib on 11/26/2017.
 */

public interface HistoryAppointmentView {

    public void showHistoryAppointmentResult(HistoryAppointmentsRoot historyAppointmentsRoot) ;

    public void startLoading() ;

    public void stopLoading() ;

    public void showMessage(String msg) ;

    Context getAppContext() ;
}
