package com.arko.nextdot.Interface;

import com.arko.nextdot.Model.RetrofitModel.UpcomingAppointments.UpcomingAppointmentsRoot;

/**
 * Created by sakib on 11/23/2017.
 */

public interface UpcomingAppointmentsRequestComplete {

    public void UpcomingAppointmentsRequestComplete(UpcomingAppointmentsRoot upcomingAppointmentsRoot) ;

    public void UpcomingAppointmentsRequestError(String msg) ;
}
